FROM node:10-alpine AS builder
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .

FROM node:10-alpine
WORKDIR /app
COPY --from=builder /app ./
RUN apk add --no-cache openssl
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz
CMD ["npm", "run", "build"]
