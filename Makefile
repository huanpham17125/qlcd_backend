EC2_STAGING_SERVER=52.197.133.216
EC2_USER=ec2-user@$(EC2_STAGING_SERVER)
EC2_KEY=-i ~/.ssh/IoT-adi.pem
MAKE=make

build_staging:
	docker-compose -f docker/staging/staging.docker-compose.yml build api
	docker save omairi-staging-api:latest > omairi-staging-api.tar

upload_staging:
	ssh $(EC2_KEY) $(EC2_USER) mkdir -p docker/staging
	ssh $(EC2_KEY) $(EC2_USER) mkdir -p docker/sql
	scp $(EC2_KEY) docker/staging/staging.docker-compose.yml $(EC2_USER):./
	scp $(EC2_KEY) docker/sql/init.sql $(EC2_USER):docker/sql/
	scp $(EC2_KEY) docker/staging/staging.docker-compose.yml $(EC2_USER):docker/staging/
	scp $(EC2_KEY) omairi-staging-api.tar $(EC2_USER):./

deploy_staging:
	ssh $(EC2_KEY) $(EC2_USER) docker-compose -f docker/staging/staging.docker-compose.yml down --remove-orphans
	ssh $(EC2_KEY) $(EC2_USER) docker image prune -f
	ssh $(EC2_KEY) $(EC2_USER) docker load < omairi-staging-api.tar
	ssh $(EC2_KEY) $(EC2_USER) docker-compose -f docker/staging/staging.docker-compose.yml up -d

staging_full_deploy:
	@$(MAKE) build_staging
	@$(MAKE) upload_staging
	@$(MAKE) deploy_staging
