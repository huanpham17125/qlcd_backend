import { join } from 'path';

type AppSettingConfigType = {
  nodeEnv: string;
  port: number;
  dbConnection: {
    type: string;
    host: string;
    port: number;
    database: string;
    username: string;
    password: string;
  };
  jwt: {
    secret: string;
    expiration: number; //InMinutes
    expirationRefresh: number; //InMinutes
    Issuer: string;
    Audience: string;
    RequiredHttpsMetadata: boolean;
    TokenValidationParameter: {
      ValidateIssuer: boolean;
      ValidateAudience: boolean;
      ValidateLifetime: boolean;
      ValidateIssuerSigningKey: boolean;
    };
  };
};

const AppSettingConfig: AppSettingConfigType = {
  nodeEnv: process.env.NODE_ENV || 'localhost',
  port: Number(process.env.PORT) || 4000,
  dbConnection: {
    type: 'mysql',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  jwt: {
    secret: process.env.JWT_SECRETKEY,
    expiration: Number(process.env.JwT_EXPIRATION_ACCESS), //InMinutes
    expirationRefresh: Number(process.env.JWT_EXPIRATION_REFRESH), //InMinutes
    Issuer: process.env.JWT_ISSUER,
    Audience: process.env.JWT_AUDIENCE,
    RequiredHttpsMetadata: true,
    TokenValidationParameter: {
      ValidateIssuer: true,
      ValidateAudience: true,
      ValidateLifetime: true,
      ValidateIssuerSigningKey: true,
    },
  },
};

export default AppSettingConfig;
