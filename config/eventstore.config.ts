/* eslint-disable prettier/prettier */
import { registerAs } from '@nestjs/config';

export const eventstoreConfig = registerAs('Db_test', () => ({
  url: process.env.DATABASE_URL || 'mysql://localhost:3306/test',
}));