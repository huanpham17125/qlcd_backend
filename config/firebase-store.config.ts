const FireBaseStoreConfig = [
  {
    id: 'notification_1',
    serverKey:
      'AAAAMZynKlU:APA91bErXbH79pk1fPzx8WfNXl_hPTSg76zOnr1RdkTFtFbkqQrFsQThlHUybuHOZpRq3r9B2ZzjqUVQAZRjQMclT91YjMib2vjA2klVY7tXED2O08Yxx1kx9FfazcXj93W0SRhU6ifj',
    databaseURL: 'https://demo-65e29-default-rtdb.firebaseio.com/',
    serviceAccount: {
      type: 'service_account',
      project_id: 'demo-65e29',
      private_key_id: '<PRIVATE_KEY_ID>',
      private_key: `AIzaSyAWTNtJKjvKOG8MGQta5LIpDMPahIL2qX4`,
      client_email: 'foo@<PROJECT_ID>.iam.gserviceaccount.com',
      client_id: '<CLIENT_ID>',
      auth_uri: '<AUTH_URI>',
      token_uri: '<TOKEN_URI>',
      auth_provider_x509_cert_url: '<AUTH_PROVIDER_URL>',
      client_x509_cert_url: '<CLIENT_URL>',
    },
  },
];

export default FireBaseStoreConfig;
