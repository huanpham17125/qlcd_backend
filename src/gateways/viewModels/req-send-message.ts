import { TypeContentMessage } from 'src/domain/enum/domainEnum';

export class ReqSendMessage {
  public content: any;
  public scheduleId: number;
}
