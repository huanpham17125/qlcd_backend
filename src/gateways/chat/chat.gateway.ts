import { QueryBus } from '@nestjs/cqrs';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { Constants } from 'src/application/common/constants';
import { GetRoomByAccountIdQuery } from 'src/application/roomChats/queries/get-room-by-account-id-query';
import { AccountService } from 'src/infrastructure/services/account.service';
import { MessageService } from 'src/infrastructure/services/message.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { ReqLeaveRoom } from '../viewModels/req-leave-room';
import { ReqSendMessage } from '../viewModels/req-send-message';

@WebSocketGateway(8088)
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server;
  connectedUsers: string[] = [];

  constructor(
    private readonly jwtService: TokenService,
    private readonly messageService: MessageService,

    private readonly accountService: AccountService,
    private readonly queryBus: QueryBus,
  ) {}

  //@UseGuards(GatewayAuthorizeGuard)
  async handleConnection(socket) {
    const claims = await this.jwtService.verifyToken(
      socket.handshake.query.token,
    );

    if (!claims) {
      socket.emit('error', 'un-authorized');
      socket.disconnect(true);
    }

    await this.accountService.updateIsOnlineByAccountId(
      claims.id,
      Constants.IsOnlineAccount.Online,
    );

    const itemRoom = await this.queryBus.execute(
      new GetRoomByAccountIdQuery(claims.id),
    );

    if (!itemRoom) {
      socket.join(itemRoom.Id);
      socket.room = itemRoom.Id;
    }

    this.connectedUsers = [...this.connectedUsers, String(claims.id)];
    this.server.emit('connected', 'Welcome to the QLCD chat server.');

    this.server.emit('users', this.connectedUsers);
  }
  async handleDisconnect(socket) {
    const claims = await this.jwtService.verifyToken(
      socket.handshake.query.token,
    );
    const userPos = this.connectedUsers.indexOf(String(claims._id));

    if (userPos > -1) {
      this.connectedUsers = [
        ...this.connectedUsers.slice(0, userPos),
        ...this.connectedUsers.slice(userPos + 1),
      ];
    }

    await this.accountService.updateIsOnlineByAccountId(
      claims.id,
      Constants.IsOnlineAccount.Offline,
    );

    this.server.emit('users', this.connectedUsers);
  }

  @SubscribeMessage('message')
  async onMessage(
    @MessageBody() data: ReqSendMessage,
    @ConnectedSocket() client: any,
  ) {
    const event = 'message';
    const claims = await this.jwtService.verifyToken(
      client.handshake.query.token,
    );

    if (!claims) {
      client.emit('error', 'un-authorized');
      client.disconnect(true);
    }

    const resCreateMessage = await this.messageService.createMessage(
      claims.id,
      claims.templeId,
      data.content,
      data.scheduleId,
    );

    const itemRoom = await this.queryBus.execute(
      new GetRoomByAccountIdQuery(claims.id),
    );

    client.broadcast.to(itemRoom).emit(event, resCreateMessage);
    
    return new Observable((observer) => {
      observer.next({ event, data: resCreateMessage });
    });
  }

  @SubscribeMessage('join')
  async onRoomJoin(
    @MessageBody() data: ReqLeaveRoom,
    @ConnectedSocket() client: any,
  ): Promise<any> {
    client.join(data[0]);

    client.emit('message', 'joined');
  }

  @SubscribeMessage('leave')
  onRoomLeave(
    @MessageBody() data: ReqLeaveRoom,
    @ConnectedSocket() client: any,
  ): void {
    client.leave(data[0]);
  }
}
