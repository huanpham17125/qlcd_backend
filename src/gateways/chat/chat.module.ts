import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GatewayAuthorizeGuard } from 'src/api/guards/gateway-authorize.guard';
import { UpdateIsOnlineByIdHandler } from 'src/application/accounts/commands/update/update-is-online-by-id-handler';
import { Constants } from 'src/application/common/constants';
import { CreateMessageHandler } from 'src/application/messages/commands/create/create-message-handler';
import { UpdateContentMessageByIdHandler } from 'src/application/messages/commands/update/update-content-message-by-id-handler';
import { UpdateStatusMessageHandler } from 'src/application/messages/commands/update/update-status-message-handler';
import { GetRoomByAccountIdHandler } from 'src/application/roomChats/queries/get-room-by-account-id-handler';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import Account from 'src/domain/entities/account.entity';
import Message from 'src/domain/entities/message.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { MessageRepository } from 'src/infrastructure/repositories/message-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { AccountService } from 'src/infrastructure/services/account.service';
import { MessageService } from 'src/infrastructure/services/message.service';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { ChatGateway } from './chat.gateway';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Account,
      AccountSetting,
      AccountRoom,
      Message,
      RoomChat,
    ]),
    CqrsModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [
    {
      provide: Constants.AppProvide.ACCOUNT_PROVIDE,
      useClass: AccountAsyncRepository,
    },
    {
      provide: Constants.AppProvide.ROOM_CHAT_PROVIDE,
      useClass: RoomChatRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
      useClass: AccountRoomRepository,
    },
    {
      provide: Constants.AppProvide.MESSAGE_PROVIDE,
      useClass: MessageRepository,
    },
    ChatGateway,
    TokenService,
    MessageService,
    AccountService,
    RoomChatService,
    GatewayAuthorizeGuard,
    CreateMessageHandler,
    UpdateStatusMessageHandler,
    UpdateContentMessageByIdHandler,
    UpdateIsOnlineByIdHandler,
    GetRoomByAccountIdHandler,
  ],
})
export class ChatModule {}
