import { DynamicModule } from '@nestjs/common';
import { AuthModule } from './api/controllers/authController/auth.module';
import { BoostrapModule } from './boostrap.module';
import { APP_FILTER } from '@nestjs/core';
import { GlobalExceptionFilterAttribute } from './api/filters/global-exception-filter-attribute';
import { AccountModule } from './api/controllers/accountController/account.module';
import { AutomapperModule } from 'nest-automapper';
import AccountProfile from './application/models/mappings/account-mapping';
import { ConfigModule } from '@nestjs/config';
import { AccountRoleMapping } from './application/models/mappings/account-role-mapping';
import { ChatModule } from './gateways/chat/chat.module';
import { RoomChatModule } from './api/controllers/roomChatController/room-chat.module';
import { RoomChatMapping } from './application/models/mappings/room-chat-mapping';
import { MessageModule } from './api/controllers/messageController/message.module';
import { MessageMapping } from './application/models/mappings/message-mapping';
import { PublicModule } from './api/controllers/publicController/public.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { SendNotifyModule } from './api/controllers/sendNotifyController/send-notify.module';
import { SendNotifyLogMapping } from './application/models/mappings/send-notify-log-mapping';
import { CronNotifyScheduleModule } from './cron_jobs/cron-notify-schedule.module';
import { SendEmailLogMapping } from './application/models/mappings/send-email-log-mapping';

export class AppModule {
  static forRoot(): DynamicModule {
    return {
      module: this,
      imports: [
        BoostrapModule,
        ConfigModule.forRoot(),
        ServeStaticModule.forRoot({
          rootPath: join(__dirname, '..', 'data'),
        }),
        AutomapperModule.forRoot({
          profiles: [
            new AccountProfile(),
            new AccountRoleMapping(),
            new RoomChatMapping(),
            new MessageMapping(),
            new SendNotifyLogMapping(),
          ],
        }),
        AuthModule,
        AccountModule,
        //RoomChatModule,
        MessageModule,
        ChatModule,
        PublicModule,
        SendNotifyModule,
        //CronNotifyScheduleModule,
      ],
      providers: [
        {
          provide: APP_FILTER,
          useClass: GlobalExceptionFilterAttribute,
        },
      ],
    };
  }
}
