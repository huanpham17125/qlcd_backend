import { ICommand } from '@nestjs/cqrs';

export class CreateAccountRoleQuery implements ICommand {
  constructor(public accountId: string, public roleId: string) {}
}
