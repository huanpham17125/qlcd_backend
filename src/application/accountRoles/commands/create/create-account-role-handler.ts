import { Inject } from '@nestjs/common';
import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import AccountRole from 'src/domain/entities/account-role.entity';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
import { CreateAccountRoleQuery } from './create-account-role-query';

export class CreateAccountRoleHandler
  implements ICommandHandler<CreateAccountRoleQuery> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_ROLE)
    private readonly asyncRepository: AccountRoleRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: CreateAccountRoleQuery): Promise<AccountRole> {
    const entity = this._mapper.map(command, AccountRole);

    return this.asyncRepository.add(entity);
  }
}
