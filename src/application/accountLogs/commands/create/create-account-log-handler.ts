import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import AccountLog from 'src/domain/entities/account-log.entity';
import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { CreateAccountLogCommand } from './create-account-log-command';

@CommandHandler(CreateAccountLogCommand)
export class CreateAccountLogHandler
  implements ICommandHandler<CreateAccountLogCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_LOG_PROVIDE)
    private readonly _asyncRepository: IAsyncGenericRepository<AccountLog>,
  ) {}
  async execute(command: CreateAccountLogCommand): Promise<void> {
    const reqInsert = new AccountLog();
    reqInsert.AccountId = command.accountId;

    await this._asyncRepository.add(reqInsert);
  }
}
