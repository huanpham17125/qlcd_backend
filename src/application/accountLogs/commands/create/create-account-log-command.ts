import { ICommand } from '@nestjs/cqrs';

export class CreateAccountLogCommand implements ICommand {
  constructor(public accountId: string) {}
}
