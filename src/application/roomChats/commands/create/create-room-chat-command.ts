import { ICommand } from '@nestjs/cqrs';

export class CreateRoomChatCommand implements ICommand {
  constructor(
    public accountId: string,
    public name: string,
    public templeId: string,
  ) {}
}
