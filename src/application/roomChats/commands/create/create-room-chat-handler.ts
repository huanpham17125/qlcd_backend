import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { CreateRoomChatCommand } from './create-room-chat-command';

@CommandHandler(CreateRoomChatCommand)
export class CreateRoomChatHandler
  implements ICommandHandler<CreateRoomChatCommand> {
  constructor(
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly asyncRepository: RoomChatRepository,

    @Inject(Constants.AppProvide.ACCOUNT_ROOM_PROVIDE)
    private readonly accountRoomRepository: AccountRoomRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: CreateRoomChatCommand): Promise<RoomChat> {
    const req = this._mapper.map(command, RoomChat);

    const itemRoom = await this.asyncRepository.add(req);
    const reqCreateAccountRoom = new AccountRoom();
    reqCreateAccountRoom.AccountId = command.accountId;
    reqCreateAccountRoom.RoomId = itemRoom.Id;

    await this.accountRoomRepository.add(reqCreateAccountRoom);

    return itemRoom;
  }
}
