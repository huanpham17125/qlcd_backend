import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { GetRoomByAccountIdQuery } from './get-room-by-account-id-query';

@QueryHandler(GetRoomByAccountIdQuery)
export class GetRoomByAccountIdHandler
  implements IQueryHandler<GetRoomByAccountIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly asyncRepository: IroomChatRepository,
  ) {}
  async execute(query: GetRoomByAccountIdQuery): Promise<any> {
    return await this.asyncRepository.getByAccountId(query.accountId);
  }
}
