import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { GetRoomByCreatorIdAndCreatedIdQuery } from './get-room-by-creator-id-and-created-id-query';

@CommandHandler(GetRoomByCreatorIdAndCreatedIdQuery)
export class GetRoomByCreatorIdAndCreatedIdHandler
  implements ICommandHandler<GetRoomByCreatorIdAndCreatedIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly asyncRepository: RoomChatRepository,
  ) {}
  async execute(
    command: GetRoomByCreatorIdAndCreatedIdQuery,
  ): Promise<RoomChat | null> {
    const entity = await this.asyncRepository.getRoomByCreatorAndCreated(
      command.creatorId,
      command.createdId,
    );

    return entity;
  }
}
