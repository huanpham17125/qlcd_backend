import { ICommand } from "@nestjs/cqrs";

export class GetAllRoomByAccountIdQuery implements ICommand {
  constructor(
    public accountId: string,
    public pageSize?: number,
    public pageNumber?: number,
  ) {}
}
