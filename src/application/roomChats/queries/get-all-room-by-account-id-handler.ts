import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { GetAllRoomByAccountIdQuery } from './get-all-room-by-account-id-query';

@CommandHandler(GetAllRoomByAccountIdQuery)
export class GetAllRoomByAccountIdHandler
  implements ICommandHandler<GetAllRoomByAccountIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly asyncRepository: RoomChatRepository,
  ) {}
  async execute(command: GetAllRoomByAccountIdQuery): Promise<any> {
    return await this.asyncRepository.getAllByAccountId(
      command.accountId,
      command.pageSize || 20,
      command.pageNumber || 1,
    );
  }
}
