import { IQuery } from '@nestjs/cqrs';

export class GetRoomByAccountIdQuery implements IQuery {
  constructor(public accountId: string) {}
}
