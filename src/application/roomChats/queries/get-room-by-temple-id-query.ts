import { IQuery } from "@nestjs/cqrs";

export class GetRoomByTempleIdQuery implements IQuery {
    constructor(public templeId: string) {}
}
