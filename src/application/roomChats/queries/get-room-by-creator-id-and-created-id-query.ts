import { ICommand } from "@nestjs/cqrs";

export class GetRoomByCreatorIdAndCreatedIdQuery implements ICommand {
  constructor(public creatorId: string, public createdId: string){}
}
