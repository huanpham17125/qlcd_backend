import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { GetRoomByTempleIdQuery } from './get-room-by-temple-id-query';

@QueryHandler(GetRoomByTempleIdQuery)
export class GetRoomByTempleIdHandler
  implements IQueryHandler<GetRoomByTempleIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly asyncRepository: IroomChatRepository,
  ) {}
  async execute(query: GetRoomByTempleIdQuery): Promise<any> {
    return await this.asyncRepository.getByTempleId(query.templeId);
  }
}
