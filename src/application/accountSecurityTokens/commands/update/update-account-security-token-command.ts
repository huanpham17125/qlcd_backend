import { ICommand } from '@nestjs/cqrs';
import { TypeToken } from 'src/domain/enum/domainEnum';

export class UpdateAccountSecurityTokenCommand implements ICommand {
  constructor(
    public value: string,
    public expriryDate: Date,
    public id: string,
  ) {}
}
