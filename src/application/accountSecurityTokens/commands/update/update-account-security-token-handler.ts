import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { UpdateAccountSecurityTokenCommand } from './update-account-security-token-command';

@CommandHandler(UpdateAccountSecurityTokenCommand)
export class UpdateAccountSecurityTokenHandler
  implements ICommandHandler<UpdateAccountSecurityTokenCommand> {
  constructor(
    @Inject('ACCOUNSECURITYTOKENS')
    private readonly _asyncRepository: IAccountSecutiryTokenRepository,
  ) {}
  async execute(command: UpdateAccountSecurityTokenCommand): Promise<boolean> {
    const entity = await this._asyncRepository.getById(command.id);

    entity.ExpiryDate = command.expriryDate;
    entity.Value = command.value;
    entity.UpdatedDate = new Date();

    return await this._asyncRepository.updateById(entity, command.id);
  }
}
