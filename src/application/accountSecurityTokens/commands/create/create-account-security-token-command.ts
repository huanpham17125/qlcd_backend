import { ICommand } from '@nestjs/cqrs';
import { TypeToken } from 'src/domain/enum/domainEnum';

export class CreateAccountSecurityTokenCommand implements ICommand {
  constructor(
    public readonly AccountId: string,
    public readonly TokenType: TypeToken,
    public readonly Value: string,
    public readonly ExpiryDate: Date,
    public readonly RemoteIpAddress?: string,
  ) {}
}
