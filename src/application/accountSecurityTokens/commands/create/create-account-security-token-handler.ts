import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { CreateAccountSecurityTokenCommand } from './create-account-security-token-command';
import { v4 as uuid } from 'uuid';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';

@CommandHandler(CreateAccountSecurityTokenCommand)
export class CreateAccountSecurityTokenHandler
  implements ICommandHandler<CreateAccountSecurityTokenCommand> {
  constructor(
    @Inject('ACCOUNSECURITYTOKENS')
    private readonly _asyncRepository: IAccountSecutiryTokenRepository,
  ) {}
  async execute(
    command: CreateAccountSecurityTokenCommand,
  ): Promise<AccountSecurityToken> {
    const reqInsert = new AccountSecurityToken();
    reqInsert.Id = uuid();
    reqInsert.ExpiryDate = command.ExpiryDate;
    reqInsert.Value = command.Value;
    reqInsert.AccountId = command.AccountId;
    reqInsert.TokenType = command.TokenType;
    reqInsert.CreatedDate = new Date();
    reqInsert.UpdatedDate = new Date();
    reqInsert.IsActive = true;
    reqInsert.RemoteIpAddress = '';
    
    const item = await this._asyncRepository.add(reqInsert);

    return item;
  }
}
