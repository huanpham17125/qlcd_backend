import { ICommand } from '@nestjs/cqrs';

export class GetByAccountIdQuery implements ICommand {
  constructor(public readonly accountId: string) {}
}
