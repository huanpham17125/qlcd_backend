import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { GetByAccountIdQuery } from './get-by-account-id-query';

@CommandHandler(GetByAccountIdQuery)
export class GetByAccountIdHandler
  implements ICommandHandler<GetByAccountIdQuery> {
  constructor(
    @Inject('ACCOUNSECURITYTOKENS')
    private readonly _asyncRepository: IAccountSecutiryTokenRepository,
  ) {}
  async execute(
    command: GetByAccountIdQuery,
  ): Promise<AccountSecurityToken> | null {
    return this._asyncRepository.getByAccountId(command.accountId);
  }
}
