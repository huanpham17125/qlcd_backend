import { HttpException } from '@nestjs/common';
import { Constants } from '../constants';
export class UnAuthorizeException extends HttpException {
  constructor(message: string) {
    super(
      message ? message : Constants.AppResource.UNAUTHORIZE,
      Constants.AppStatus.StatusCode401,
    );
  }
}
