import { HttpException } from '@nestjs/common';
import { Constants } from '../constants';
export class UnKnowException extends HttpException {
  constructor(message: string) {
    super(
      message ? message : Constants.AppResource.ERROR_UNKNOWN,
      Constants.AppStatus.StatusCode500,
    );
  }
}
