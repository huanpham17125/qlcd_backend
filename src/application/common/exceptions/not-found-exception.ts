import { HttpException } from '@nestjs/common';
import { Constants } from '../constants';
export class NotFoundException extends HttpException {
  constructor(message: string) {
    super(
      message ? message : Constants.AppResource.NOT_FOUND,
      Constants.AppStatus.StatusCode404,
    );
  }
}
