import { HttpException } from '@nestjs/common';
import { Constants } from '../constants';
export class BadRequestException extends HttpException {
  constructor(message: string, code?: number ) {
    super(message, code ? code : Constants.AppStatus.StatusCode400);
  }
}
