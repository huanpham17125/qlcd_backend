import { HttpException, ValidationError } from '@nestjs/common';
import { Constants } from '../constants';
export class ValidationException extends HttpException {
  constructor(message: ValidationError[]) {
    super(
      message[0].constraints
        ? Object.values(message[0].constraints).toString()
        : getMessageError(message[0].children[0]),
      Constants.AppStatus.StatusCode422,
    );
  }
}

function getMessageError(childArray): string {
  if (childArray.constraints) {
    const msg = Object.values(childArray.constraints).toString();
    return msg;
  } else {
    return getMessageError(childArray.children[0]);
  }
}
