export interface IFireBaseStoreConfig {
  id: string;
  serverKey: string;
  databaseURL: string;
  serviceAccount: any;
}
