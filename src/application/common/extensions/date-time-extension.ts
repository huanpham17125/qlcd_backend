import * as moment from 'moment';
import { Constants } from '../constants';

export default class DateTimeExtension {
  constructor() {}
  static stringToDate(datetime: string, format: string, dtDefault: Date): Date {
    if (moment(datetime, format, true).isValid()) {
      return moment(datetime, format).toDate();
    } else {
      return dtDefault;
    }
  }

  static dateToString(datetime: Date, format: string): string {
    return moment(datetime).format(format).toString();
  }

  static getLifeExpectancy(birthday: Date, dateOfDeath: Date): number {
    const diff = dateOfDeath.getTime() - birthday.getTime();
    return Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
  }

  static getFirstAndLastDayByMonthAndYear(month: number, year: number) {
    const firstDay = new Date(Number(year), Number(month) - 1, 1);

    const lastDay = new Date(Number(year), Number(month), 0);

    return {
      firstDay: moment(firstDay).format(Constants.DEFAULT_FORMAT_DATE),
      lastDay: moment(lastDay).format(Constants.DEFAULT_FORMAT_DATE),
    };
  }
}
