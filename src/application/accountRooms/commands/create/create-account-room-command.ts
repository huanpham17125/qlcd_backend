import { ICommand } from "@nestjs/cqrs";

export class CreateAccountRoomCommand implements ICommand {
  constructor(public accountId: string, public roomId: string) {}
}
