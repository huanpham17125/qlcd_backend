import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { CreateAccountRoomCommand } from './create-account-room-command';

@CommandHandler(CreateAccountRoomCommand)
export class CreateAccountRoomHandler
  implements ICommandHandler<CreateAccountRoomCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_ROOM_PROVIDE)
    private readonly asyncRepository: IAsyncGenericRepository<AccountRoom>,
  ) {}
  async execute(command: CreateAccountRoomCommand): Promise<AccountRoom> {
    const reqCreate = new AccountRoom();
    reqCreate.AccountId = command.accountId;
    reqCreate.RoomId = command.roomId;

    const entity = await this.asyncRepository.add(reqCreate);

    return entity;
  }
}
