import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { GetNotifyMessageByAccountIdQuery } from './get-notify-message-by-account-id-query';

@QueryHandler(GetNotifyMessageByAccountIdQuery)
export class GetNotifyMessageByAccountIdHandler
  implements IQueryHandler<GetNotifyMessageByAccountIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)
    private readonly asyncRepository: ISendNotifyRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(query: GetNotifyMessageByAccountIdQuery): Promise<any> {
    const lst = await this.asyncRepository.getByAccountId(query.accountId);

    return lst;
  }
}
