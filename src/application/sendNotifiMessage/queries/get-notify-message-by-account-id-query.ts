import { IQuery } from '@nestjs/cqrs';

export class GetNotifyMessageByAccountIdQuery implements IQuery {
  constructor(public accountId: string) {}
}
