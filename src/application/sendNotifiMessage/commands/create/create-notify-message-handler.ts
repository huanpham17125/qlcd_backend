import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { CreateNotifyMessageCommand } from './create-notify-message-command';

@CommandHandler(CreateNotifyMessageCommand)
export class CreateNotifyMessageHandler
  implements ICommandHandler<CreateNotifyMessageCommand> {
  constructor(
    @Inject(Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)
    private readonly asyncRepository: ISendNotifyRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: CreateNotifyMessageCommand): Promise<any> {
    const req = this._mapper.map(command, SendNotifyLog);

    const entity = await this.asyncRepository.add(req);

    return entity;
  }
}
