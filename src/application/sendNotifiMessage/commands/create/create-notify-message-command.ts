import { ICommand } from '@nestjs/cqrs';
import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';

export class CreateNotifyMessageCommand implements ICommand {
  constructor(
    public fromAccountId: string,
    public toAccountId: string,
    public req: ReqCreateSendnotifyMessage,
  ) {}
}
