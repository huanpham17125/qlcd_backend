import { ICommand } from '@nestjs/cqrs';

export class CreateSendNotifyForScheduleCommand implements ICommand {
  constructor(
    public accountId: string,
    public title: string,
    public body: string,
    public msg: string,
    public typeNotify: number,
    public status,
    public detailScheduleId: number,
  ) {}
}
