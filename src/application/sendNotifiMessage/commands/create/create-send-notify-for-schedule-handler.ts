import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { StatusSendNotify, TypeSendNotify } from 'src/domain/enum/domainEnum';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { CreateSendNotifyForScheduleCommand } from './create-send-notify-for-schedule-command';

@CommandHandler(CreateSendNotifyForScheduleCommand)
export class CreateSendNotifyForScheduleHandler
  implements ICommandHandler<CreateSendNotifyForScheduleCommand> {
  constructor(
    @Inject(Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)
    private readonly asyncRepository: ISendNotifyRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(
    command: CreateSendNotifyForScheduleCommand,
  ): Promise<SendNotifyLog> {
    const reqInsert = new SendNotifyLog();
    reqInsert.AccountId = command.accountId;
    reqInsert.CreatedById = command.accountId;
    reqInsert.UpdatedById = command.accountId;
    reqInsert.Body = command.body;
    reqInsert.Title = command.title;
    reqInsert.Msg = command.msg;
    reqInsert.TypeNotify = command.typeNotify;
    reqInsert.Status = StatusSendNotify.Sent;

    return await this.asyncRepository.add(reqInsert);
  }
}
