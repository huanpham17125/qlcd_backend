import { ICommand } from '@nestjs/cqrs';
import { StatusSendNotify } from 'src/domain/enum/domainEnum';

export class UpdateNotifyMessageByIdCommand implements ICommand {
  constructor(
    public id: number,
    public accountId: string,
    public status: StatusSendNotify,
  ) {}
}
