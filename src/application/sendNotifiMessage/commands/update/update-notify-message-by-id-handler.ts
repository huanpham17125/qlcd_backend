import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { UpdateNotifyMessageByIdCommand } from './update-notify-message-by-id-command';

@CommandHandler(UpdateNotifyMessageByIdCommand)
export class UpdateNotifyMessageByIdHandler
  implements ICommandHandler<UpdateNotifyMessageByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)
    private readonly asyncRepository: ISendNotifyRepository,
  ) {}
  async execute(command: UpdateNotifyMessageByIdCommand): Promise<any> {
    const item = await this.asyncRepository.getById(command.id);

    if (!item) {
      throw new BadRequestException(
        Constants.AppResource.NOTIFY_MESSAGE_NOT_FOUND,
      );
    }

    item.UpdatedDate = new Date();
    item.UpdatedById = command.accountId;
    item.Status = command.status;

    await this.asyncRepository.updateById(item, item.Id);

    return item;
  }
}
