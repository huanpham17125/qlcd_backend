import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { DeleteNotifyMessageByIdCommand } from './delete-notify-message-by-id-command';

@CommandHandler(DeleteNotifyMessageByIdCommand)
export class DeleteNotifyMessageByIdHandler
  implements ICommandHandler<DeleteNotifyMessageByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)
    private readonly asyncRepository: ISendNotifyRepository,
  ) {}
  async execute(command: DeleteNotifyMessageByIdCommand): Promise<boolean> {
    const item = await this.asyncRepository.getById(command.id);

    if (!item) {
      throw new BadRequestException(
        Constants.AppResource.NOTIFY_MESSAGE_NOT_FOUND,
      );
    }

    item.UpdatedDate = new Date();
    item.UpdatedById = command.accountId;
    item.IsActive = false;

    await this.asyncRepository.updateById(item, item.Id);

    return true;
  }
}
