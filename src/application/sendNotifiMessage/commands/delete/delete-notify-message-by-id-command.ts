import { ICommand } from "@nestjs/cqrs";

export class DeleteNotifyMessageByIdCommand implements ICommand {
  constructor(public id: number, public accountId: string) {}
}
