import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import { ISendMailLog } from 'src/domain/interfaces/isend-mail-log.interface';
import { CreateSendMailLogCommand } from './create-send-mail-log-command';

@CommandHandler(CreateSendMailLogCommand)
export class CreateSendMailLogHandler
  implements ICommandHandler<CreateSendMailLogCommand> {
  constructor(
    @Inject(Constants.AppProvide.SEND_MAIL_LOG_PROVIDE)
    private readonly asyncRepository: ISendMailLog,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: CreateSendMailLogCommand): Promise<SendMailLog> {
    const reqCreate = this._mapper.map(
      command,
      SendMailLog,
    );

    return await this.asyncRepository.add(reqCreate);
  }
}
