import { ICommand } from '@nestjs/cqrs';

export class CreateSendMailLogCommand implements ICommand {
  constructor(
    public accountId: string,
    public mailSender: string,
    public mailTo: string,
    public mailCc: string,
    public mailBcc: string,
    public mailSubject: string,
    public mailBody: string,
    public mailType: number,
    public mailAttac: string,
  ) {}
}
