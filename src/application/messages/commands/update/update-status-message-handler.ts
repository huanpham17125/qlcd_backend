import { BadGatewayException, Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { UpdateStatusMessageCommand } from './update-status-message-command';

@CommandHandler(UpdateStatusMessageCommand)
export class UpdateStatusMessageHandler
  implements ICommandHandler<UpdateStatusMessageCommand> {
  constructor(
    @Inject(Constants.AppProvide.MESSAGE_PROVIDE)
    private readonly asyncRepository: ImessageRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: UpdateStatusMessageCommand): Promise<any> {
    const entity = await this.asyncRepository.getById(command.messageId);

    if (!entity) {
      throw new BadRequestException(Constants.AppResource.MESSAGE_NOT_FOUND);
    }

    entity.Status = command.status;
    entity.UpdatedDate = new Date();

    const isUpdate = await this.asyncRepository.updateById(entity, entity.Id);

    if (!isUpdate) {
      throw new BadRequestException(Constants.AppResource.UPDATE_FAIL);
    }

    return entity;
  }
}
