import { ICommand } from '@nestjs/cqrs';

export class UpdateContentMessageByIdCommand implements ICommand {
  constructor(
    public messageId: string,
    public accountId: string,
    public content: string,
  ) {}
}
