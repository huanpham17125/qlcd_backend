import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import Message from 'src/domain/entities/message.entity-gateway';
import { StatusMessage } from 'src/domain/enum/domainEnum';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { UpdateContentMessageByIdCommand } from './update-content-message-by-id-command';

@CommandHandler(UpdateContentMessageByIdCommand)
export class UpdateContentMessageByIdHandler
  implements ICommandHandler<UpdateContentMessageByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.MESSAGE_PROVIDE)
    private readonly asyncRepository: ImessageRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: UpdateContentMessageByIdCommand): Promise<Message> {
    const entity = await this.asyncRepository.getById(command.messageId);

    if (!entity) {
      throw new BadRequestException(Constants.AppResource.MESSAGE_NOT_FOUND);
    }

    entity.Status = StatusMessage.Updated;
    entity.UpdatedDate = new Date();
    entity.Content = command.content;

    const isUpdate = await this.asyncRepository.updateById(entity, entity.Id);

    if (!isUpdate) {
      throw new BadRequestException(Constants.AppResource.UPDATE_FAIL);
    }

    return entity;
  }
}
