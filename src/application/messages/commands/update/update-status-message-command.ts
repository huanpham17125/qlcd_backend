import { ICommand } from "@nestjs/cqrs";
import { StatusMessage } from "src/domain/enum/domainEnum";

export class UpdateStatusMessageCommand implements ICommand {
  constructor(public messageId: number, public status: StatusMessage) {}
}
