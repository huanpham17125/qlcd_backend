import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import Message from 'src/domain/entities/message.entity-gateway';
import { StatusMessage, TypeContentMessage } from 'src/domain/enum/domainEnum';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { ResGetMessageByAccountId } from '../../viewModels/res-get-message-by-account-id';
import { CreateMessageCommand } from './create-message-command';

@CommandHandler(CreateMessageCommand)
export class CreateMessageHandler
  implements ICommandHandler<CreateMessageCommand> {
  constructor(
    @Inject(Constants.AppProvide.MESSAGE_PROVIDE)
    private readonly asyncRepository: ImessageRepository,
    @Inject(Constants.AppProvide.ROOM_CHAT_PROVIDE)
    private readonly roomChatRepository: IroomChatRepository,
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly accountAsyncRepository: IaccountAsyncRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(
    command: CreateMessageCommand,
  ): Promise<ResGetMessageByAccountId> {
    const itemRoom = await this.roomChatRepository.getByTempleId(
      command.templeId,
    );
    const reqMessage = new Message();
    reqMessage.AccSenderId = command.senderId;
    reqMessage.Status = StatusMessage.Sent;
    reqMessage.ReplyId = 0;
    reqMessage.RoomId = itemRoom.Id;
    reqMessage.TypeContent = TypeContentMessage.Document;
    reqMessage.Content = command.content;

    const entity = await this.asyncRepository.add(reqMessage);

    entity.Account = await this.accountAsyncRepository.getById(
      command.senderId,
    );

    return this._mapper.map(entity, ResGetMessageByAccountId);
  }
}
