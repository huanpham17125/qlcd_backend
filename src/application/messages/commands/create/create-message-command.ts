import { ICommand } from '@nestjs/cqrs';

export class CreateMessageCommand implements ICommand {
  constructor(
    public senderId: string,
    public templeId: string,
    public content: string,
    public scheduleId: number,
  ) {}
}
