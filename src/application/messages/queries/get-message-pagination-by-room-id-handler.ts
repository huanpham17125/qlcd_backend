import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Constants } from 'src/application/common/constants';
import Message from 'src/domain/entities/message.entity-gateway';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { ResGetMessageByAccountId } from '../viewModels/res-get-message-by-account-id';
import { GetMessagePaginationByRoomIdQuery } from './get-message-pagination-by-room-id-query';

@CommandHandler(GetMessagePaginationByRoomIdQuery)
export class GetMessagePaginationByRoomIdHandler
  implements ICommandHandler<GetMessagePaginationByRoomIdQuery> {
  constructor(
    @Inject(Constants.AppProvide.MESSAGE_PROVIDE)
    private readonly _asyncRepository: ImessageRepository,
    @Inject(Constants.AppProvide.APP_PROVIDE)
    private readonly accountAsyncRepository: IaccountAsyncRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: GetMessagePaginationByRoomIdQuery): Promise<any> {
    const lstMessage = await this._asyncRepository.getMessagePaginationByAccountId(
      command.accountId,
      command.pageSize | 20,
      command.pageNumber | 1,
    );

    for (let i = 0; i <= lstMessage.items.length - 1; i++) {
      lstMessage.items[i].Account = await this.accountAsyncRepository.getById(
        lstMessage.items[i].AccSenderId,
      );
    }
    const resMessage = new Pagination(
      this._mapper.mapArray(lstMessage.items, ResGetMessageByAccountId),
      lstMessage.meta,
    );

    return resMessage;
  }
}
