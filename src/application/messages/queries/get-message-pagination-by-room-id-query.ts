import { ICommand } from "@nestjs/cqrs";

export class GetMessagePaginationByRoomIdQuery implements ICommand {
  constructor(
    public accountId: string,
    public pageSize?: number,
    public pageNumber?: number,
  ) {}
}
