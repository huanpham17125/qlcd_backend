import { TypeContentMessage } from 'src/domain/enum/domainEnum';

export class ResGetMessageByAccountId {
  public id: number;
  public typeMessage: TypeContentMessage;
  public content: any;
  public senderId: string;
  public senderName: string;
  public senderAvatar: string;
  public createdDate: Date;
  public status: number;
}
