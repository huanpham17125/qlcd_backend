export class ReqContentMessageSchedule {
  public typeSchedule: string;
  public dankaName: string;
  public houseHolderName: string;
  public departedPersonName: string;
  public eventDate: Date;
  public startTime: string;
  public endTime: string;
  public place: string;

  constructor(
    typeSchedule: string,
    dankaName: string,
    houseHolderName: string,
    departedPersonName: string,
    eventDate: Date,
    start: string,
    end: string,
    place: string,
  ) {
    (this.typeSchedule = typeSchedule),
      (this.dankaName = dankaName),
      (this.houseHolderName = houseHolderName),
      (this.departedPersonName = departedPersonName),
      (this.eventDate = eventDate),
      (this.startTime = start),
      (this.endTime = end),
      (this.place = place);

    return this;
  }
}
