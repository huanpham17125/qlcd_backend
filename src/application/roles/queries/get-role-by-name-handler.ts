import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import Role from 'src/domain/entities/role.entity';
import { RoleRepository } from 'src/infrastructure/repositories/role-repository';
import { GetRoleByNameQuery } from './get-role-by-name-query';

@QueryHandler(GetRoleByNameQuery)
export class GetRoleByNameHandler
  implements IQueryHandler<GetRoleByNameQuery> {
  constructor(
    @Inject(Constants.AppProvide.ROLE_PROVIDE)
    private readonly asyncRepository: RoleRepository,
  ) {}
  async execute(command: GetRoleByNameQuery): Promise<Role | null> {
    return await this.asyncRepository.getByName(command.roleName);
  }
}
