import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import { CreateNotifyMessageCommand } from 'src/application/sendNotifiMessage/commands/create/create-notify-message-command';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';

@Profile()
export class SendNotifyLogMapping extends MappingProfileBase {
  constructor() {
    super();
  }
  configure(): void {
    this.createMap(CreateNotifyMessageCommand, SendNotifyLog)
      .forMember('AccountId', (otp) => otp.mapFrom((o) => o.toAccountId))
      .forMember('Body', (otp) => otp.mapFrom((o) => o.req.body))
      .forMember('Title', (otp) => otp.mapFrom((o) => o.req.title))
      .forMember('Msg', (otp) => otp.mapFrom((o) => o.req.message))
      .forMember('CreatedById', (otp) => otp.mapFrom((o) => o.fromAccountId))
      .forMember('UpdatedById', (otp) => otp.mapFrom((o) => o.fromAccountId));
  }
}
