import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import { CreateMessageCommand } from 'src/application/messages/commands/create/create-message-command';
import { ResGetMessageByAccountId } from 'src/application/messages/viewModels/res-get-message-by-account-id';
import Message from 'src/domain/entities/message.entity-gateway';
import { TypeContentMessage } from 'src/domain/enum/domainEnum';

@Profile()
export class MessageMapping extends MappingProfileBase {
  constructor() {
    super();
  }
  configure(): void {
    this.createMap(Message, ResGetMessageByAccountId)
      .forMember('id', (src) => src.mapFrom((o) => o.Id))
      .forMember('senderId', (src) => src.mapFrom((o) => o.AccSenderId))
      .forMember('senderName', (src) => src.mapFrom((o) => o.Account.FullName))
      .forMember('senderAvatar', (src) =>
        src.mapFrom((o) => process.env.APP_DOMAIN + o.AccSenderId),
      )
      .forMember('typeMessage', (src) => src.mapFrom((o) => o.TypeContent))

      .forMember('content', (src) =>
        src.mapFrom((o) =>
          o.TypeContent == TypeContentMessage.Document
            ? o.Content
            : JSON.parse(o.Content),
        ),
      )
      .forMember('createdDate', (src) => src.mapFrom((o) => o.CreatedDate))
      .forMember('status', (src) => src.mapFrom((o) => o.Status));
  }
}
