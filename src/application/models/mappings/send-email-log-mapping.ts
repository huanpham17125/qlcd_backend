import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import { CreateSendMailLogCommand } from 'src/application/sendMailLogs/commands/create/create-send-mail-log-command';
import SendMailLog from 'src/domain/entities/send-mail-log.entity';

@Profile()
export class SendEmailLogMapping extends MappingProfileBase {
  constructor() {
    super();
  }
  configure(): void {
    this.createMap(CreateSendMailLogCommand, SendMailLog)
      .forMember('AccountId', (otp) => otp.mapFrom((o) => o.accountId))
      .forMember('MailSender', (otp) => otp.mapFrom((o) => o.mailSender))
      .forMember('MailTo', (otp) => otp.mapFrom((o) => o.mailTo))
      .forMember('MailCc', (otp) => otp.mapFrom((o) => o.mailCc))
      .forMember('MailBcc', (otp) => otp.mapFrom((o) => o.mailBcc))
      .forMember('MailSubject', (otp) => otp.mapFrom((o) => o.mailSubject))
      .forMember('MailBody', (otp) => otp.mapFrom((o) => o.mailBody))
      .forMember('MailType', (otp) => otp.mapFrom((o) => o.mailType))
      .forMember('MailAttach', (otp) => otp.mapFrom((o) => o.mailAttac));
  }
}
