import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import { CreateRoomChatCommand } from 'src/application/roomChats/commands/create/create-room-chat-command';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';

@Profile()
export class RoomChatMapping extends MappingProfileBase {
  constructor() {
    super();
  }
  configure(): void {
    this.createMap(CreateRoomChatCommand, RoomChat)
      .forMember('CreatedById', (otp) => otp.mapFrom((o) => o.accountId))
      .forMember('UpdatedById', (otp) => otp.mapFrom((o) => o.accountId))
      .forMember('Name', (otp) => otp.mapFrom((o) => o.name))
      .forMember('Tittle', (otp) => otp.mapFrom((o) => o.name));
  }
}
