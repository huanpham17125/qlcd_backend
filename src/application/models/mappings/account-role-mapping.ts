import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import { CreateAccountRoleQuery } from 'src/application/accountRoles/commands/create/create-account-role-query';
import AccountRole from 'src/domain/entities/account-role.entity';

@Profile()
export class AccountRoleMapping extends MappingProfileBase {
  constructor() {
    super();
  }

  configure(): void {
    this.createMap(CreateAccountRoleQuery, AccountRole)
      .forMember('AccountId', (otp) => otp.mapFrom((s) => s.accountId))
      .forMember('RoleId', (otp) => otp.mapFrom((s) => s.roleId));
  }
}
