import { MappingProfileBase } from 'automapper-nartc';
import { Profile } from 'nestjsx-automapper';
import ResGetAccount from 'src/application/accounts/viewModels/res-get-account';
import { ResUpdateAccount } from 'src/application/accounts/viewModels/res-update-account';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import Account from 'src/domain/entities/account.entity';
import * as moment from 'moment';

@Profile()
export default class AccountProfile extends MappingProfileBase {
  constructor() {
    super();
  }

  configure(): void {
    this.createMap(AccountSetting, ResUpdateAccount)
      .forMember('name', (opt) => opt.mapFrom((s) => s.Account.FullName))
      //.forMember('birthday', (opt) => opt.mapFrom((s) => s.Account.Birthday))
      .forMember('email', (opt) => opt.mapFrom((s) => s.Account.Email))
      //.forMember('phone', (opt) => opt.mapFrom((s) => s.Account.Phone))
      //.forMember('sex', (opt) => opt.mapFrom((s) => s.Account.Gender))
      //.forMember('zipCode', (opt) => opt.mapFrom((s) => s.Account.ZipCode))
      //.forMember('province', (opt) => opt.mapFrom((s) => s.Account.Province))
      //.forMember('street', (opt) => opt.mapFrom((s) => s.Account.Street))
      //.forMember('city', (opt) => opt.mapFrom((s) => s.Account.City))

    this.createMap(Account, ResGetAccount)
      .forMember('fullName', (otp) => otp.mapFrom((s) => s.FullName))
      .forMember('phone', (otp) => otp.mapFrom((s) => s.Phone))
      .forMember('address', (otp) =>
        otp.mapFrom(
          (s) =>
            s.Street +
            (s.Street != '' ? ' ' : '') +
            s.City +
            (s.City != '' ? ' ' : '') +
            s.Province,
        ),
      )
      .forMember('email', (otp) => otp.mapFrom((s) => s.Email))
      .forMember('birthday', (otp) =>
        otp.mapFrom((s) => moment(s.Birthday).format('YYYY-MM-DD').toString()),
      )
      .forMember('isActive', (otp) => otp.mapFrom((s) => s.IsActive))
      .forMember('createdDate', (otp) =>
        otp.mapFrom((s) =>
          moment(s.CreatedDate).format('YYYY-MM-DD').toString(),
        ),
      );
  }
}
