import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdatePasswordByIdCommand } from './update-password-by-id-command';

@CommandHandler(UpdatePasswordByIdCommand)
export class UpdatePasswordByIdHandler
  implements ICommandHandler<UpdatePasswordByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}

  async execute(command: UpdatePasswordByIdCommand): Promise<any> {
    await this._accountAsyncRepository.updatePasswordById(
      command.passwordHash,
      command.id,
    );

    return true;
  }
}
