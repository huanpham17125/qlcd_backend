import { ICommand } from '@nestjs/cqrs';

export class UpdateAccountSettingByAccountIdCommand implements ICommand {
  constructor(
    public accountId: string,
    public timePushNotifyId: number,
    public colorCodeId: number,
  ) {}
}
