import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { ResUpdateAccount } from '../../viewModels/res-update-account';
import { UpdateAccountByIdCommand } from './update-account-by-id-command';

@CommandHandler(UpdateAccountByIdCommand)
export class UpdateAccountByIdHandler
  implements ICommandHandler<UpdateAccountByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
    @Inject(Constants.AppProvide.ACCOUNT_SETTING_PROVIDE)
    private readonly _accountSettingAsyncRepository: IaccountSettingRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}

  async execute(command: UpdateAccountByIdCommand): Promise<any> {
    const account = await this._accountAsyncRepository.getById(command.Id);

    if (!account) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    }

    account.FullName = command.FullName;
    //account.Birthday = command.Birthday;
    //account.Phone = command.PhoneNumber;
    //account.City = command.City;
    //account.Province = command.Province;
    account.Email = command.Email;
    //account.Street = command.Street;
    //account.ZipCode = command.ZipCode;
    account.UpdatedDate = new Date();

    const entity = await this._accountAsyncRepository.updateById(
      account,
      command.Id,
    );

    if (!entity) {
      throw new BadRequestException(Constants.AppResource.UPDATE_FAIL);
    }
    const newAccountSetting = await this._accountSettingAsyncRepository.getByAccountId(
      command.Id,
    );
    
    return this._mapper.map(newAccountSetting, ResUpdateAccount);
  }
}
