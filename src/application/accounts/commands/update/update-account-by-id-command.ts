import { ICommand } from '@nestjs/cqrs';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
import { TypeGender } from 'src/domain/enum/domainEnum';

export class UpdateAccountByIdCommand implements ICommand {
  public Id: string;
  public ColorCodeId: number;
  public FullName: string;
  public Email: string;
  //public Birthday: Date;
  //public PhoneNumber: string;
  //public ZipCode: string;
  //public Province: string;
  //public City: string;
  //public Street: string;

  constructor(req: ReqUpdateAccount, id: string) {
    (this.Id = id), (this.FullName = req.fullName), (this.Email = req.email);
    //(this.Birthday = new Date(req.birthday)),
    //(this.PhoneNumber = req.phone);
    ///(this.ZipCode = req.zipCode),
    //(this.City = req.city),
    //(this.Street = req.street),
    //(this.Province = req.province);
  }
}
