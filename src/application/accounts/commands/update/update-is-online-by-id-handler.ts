import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdateIsOnlineByIdCommand } from './update-is-online-by-id-command';

@CommandHandler(UpdateIsOnlineByIdCommand)
export class UpdateIsOnlineByIdHandler
  implements ICommandHandler<UpdateIsOnlineByIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(command: UpdateIsOnlineByIdCommand): Promise<boolean> {
    const entity = await this._accountAsyncRepository.updateIsOnlineById(command.accountId, command.isOnline);

    return entity;
  }
}
