import { ICommand } from "@nestjs/cqrs";

export class UpdateIsOnlineByIdCommand implements ICommand {
  constructor(public accountId: string, public isOnline: boolean) {}
}
