import { ICommand } from '@nestjs/cqrs';

export class UpdateStatusAccountCommand implements ICommand {
  constructor(
    public accountId: string,
    public adminId: string,
    public status: number,
  ) {}
}
