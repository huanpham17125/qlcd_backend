import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdateStatusAccountCommand } from './update-status-account-command';

@CommandHandler(UpdateStatusAccountCommand)
export class UpdateStatusAccountHandler
  implements ICommandHandler<UpdateStatusAccountCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(command: UpdateStatusAccountCommand): Promise<boolean> {
    const itemAccount = await this._accountAsyncRepository.getById(
      command.accountId,
    );
    const isActiveUpdate = command.status == 0 ? false : true;

    if (!itemAccount) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    }

    if (isActiveUpdate == itemAccount.IsActive) {
      throw new BadRequestException(
        Constants.AppResource.ACCOUNT_STATUS_NOT_CHANGE,
      );
    }

    itemAccount.IsActive = isActiveUpdate;
    itemAccount.UpdatedById = command.adminId;
    itemAccount.UpdatedDate = new Date();

    return await this._accountAsyncRepository.updateById(
      itemAccount,
      command.accountId,
    );
  }
}
