import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { UpdateAccountSettingByAccountIdCommand } from './update-account-setting-by-account-id-command';

@CommandHandler(UpdateAccountSettingByAccountIdCommand)
export class UpdateAccountSettingByAccountIdHandler
  implements ICommandHandler<UpdateAccountSettingByAccountIdCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_SETTING_PROVIDE)
    private readonly _accountAsyncRepository: IaccountSettingRepository,
  ) {}
  async execute(
    command: UpdateAccountSettingByAccountIdCommand,
  ): Promise<boolean> {
    const item = await this._accountAsyncRepository.getByAccountId(
      command.accountId,
    );

    if (!item) {
      const reqInsert = new AccountSetting();
      reqInsert.AccountId = command.accountId;

      await this._accountAsyncRepository.add(reqInsert);
    } else {
      const reqUpdate = new AccountSetting();
      reqUpdate.UpdatedDate = new Date();

      await this._accountAsyncRepository.updateById(reqUpdate, item.Id);
    }

    return true;
  }
}
