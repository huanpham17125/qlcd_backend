import { ICommand } from '@nestjs/cqrs';
import { ReqChangePassword } from 'src/api/viewModels/req-change-password';

export class UpdatePasswordByIdCommand implements ICommand {
  constructor(public passwordHash: string, public id: string) {}
}
