import { ICommand } from '@nestjs/cqrs';

export class CreateAccountCommand implements ICommand {
  constructor(
    public roleId: string,
    public templeId: string,
    public creatorId: string,
    public readonly fullName: string,
    public readonly email: string,
    public readonly password: string,
    public readonly phone?: string,
    public readonly sex?: string,
  ) {}
}
