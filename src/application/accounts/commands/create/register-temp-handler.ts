import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { RegisterTempCommand } from './register-temp-command';

@CommandHandler(RegisterTempCommand)
export class RegisterTempHandler
  implements ICommandHandler<RegisterTempCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
    private readonly passwordService: PasswordService,
  ) {}
  async execute(command: RegisterTempCommand): Promise<any> {
    const isExist = await this._accountAsyncRepository.getByEmail(
      command.email,
    );

    if (isExist && isExist.IsActive != false) {
      throw new BadRequestException(
        Constants.AppResource.EMAIL_ALREADY_EXISTED,
      );
    } else if (isExist && isExist.IsActive == false) {
      return isExist;
    } else {
      const newAccount = new Account();
      newAccount.Birthday = new Date(Constants.DEFAULT_DATE);
      newAccount.Email = command.email;
      newAccount.FullName = '';
      newAccount.PasswordHash = command.password;
      newAccount.Gender = '';
      newAccount.Phone = '';
      newAccount.CreatedById = '';
      newAccount.UpdatedById = '';
      newAccount.IsActive = false;
      newAccount.IsOnline = false;
  
      const reqAccount = await this.passwordService.getPasswordHash(
        command.password,
        newAccount,
      );
  
      return await this._accountAsyncRepository.create(reqAccount);
    }
  }
}
