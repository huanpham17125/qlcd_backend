import { Constants } from './../../../common/constants';
import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from '../../../../domain/interfaces/iaccount-async-repository.interface';
import { BadRequestException } from '../../../common/exceptions/bad-request-exception';
import { CreateAccountCommand } from './create-account-command';
import Account from '../../../../domain/entities/account.entity';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
import AccountRole from 'src/domain/entities/account-role.entity';

@CommandHandler(CreateAccountCommand)
export class CreateAccountHandler
  implements ICommandHandler<CreateAccountCommand> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
    @Inject(Constants.AppProvide.ACCOUNT_ROLE)
    private readonly accountRoleRepository: AccountRoleRepository,
    private readonly passwordService: PasswordService,
  ) {}

  async execute(command: CreateAccountCommand): Promise<Account> {
    const accountByEmail = await this._accountAsyncRepository.findByEmail(
      command.email,
    );
    
    if (accountByEmail) {
      throw new BadRequestException(
        Constants.AppResource.EMAIL_ALREADY_EXISTED,
      );
    }

    const newAccount = new Account();
    newAccount.Birthday = new Date(Constants.DEFAULT_DATE);
    newAccount.Email = command.email;
    newAccount.FullName = command.fullName;
    newAccount.PasswordHash = command.password;
    newAccount.Gender = command.sex || '';
    newAccount.Phone = command.phone || '';
    newAccount.CreatedById = command.creatorId;
    newAccount.UpdatedById = command.creatorId;

    const reqAccount = await this.passwordService.getPasswordHash(
      command.password,
      newAccount,
    );
    const account = await this._accountAsyncRepository.create(reqAccount);

    const reqAccRole = new AccountRole();
    reqAccRole.AccountId = account.Id;
    reqAccRole.RoleId = command.roleId;

    await this.accountRoleRepository.add(reqAccRole);

    return account;
  }
}
