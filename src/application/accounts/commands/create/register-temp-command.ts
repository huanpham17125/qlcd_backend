import { ICommand } from "@nestjs/cqrs";

export class RegisterTempCommand implements ICommand {
  constructor(public email: string, public password: string) {}
}
