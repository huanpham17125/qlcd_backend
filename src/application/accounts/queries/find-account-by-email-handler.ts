import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { InjectMapper } from 'nest-automapper';
import { Constants } from 'src/application/common/constants';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import ResGetAccount from '../viewModels/res-get-account';
import { FindAccountByEmailQuery } from './find-account-by-email-query';

@CommandHandler(FindAccountByEmailQuery)
export class FindAccountByEmailHandler
  implements ICommandHandler<FindAccountByEmailQuery> {
  constructor(
    @Inject(Constants.AppProvide.APP_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
    @InjectMapper() private readonly _mapper: AutoMapper,
  ) {}
  async execute(command: FindAccountByEmailQuery): Promise<any> {
    const entity = await this._accountAsyncRepository.findByEmail(
      command.email,
    );
    
    return entity ? this._mapper.map(entity, ResGetAccount) : '';
  }
}
