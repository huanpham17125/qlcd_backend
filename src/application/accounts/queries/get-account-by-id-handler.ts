import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountByIdQuery } from './get-account-by-id-query';

@CommandHandler(GetAccountByIdQuery)
export class GetAccountByIdHandler
  implements ICommandHandler<GetAccountByIdQuery> {
  constructor(
    @Inject('ACCOUNTS')
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(command: GetAccountByIdQuery): Promise<Account> {
    const accountById = await this._accountAsyncRepository.getProfile(
      command.id,
    );

    if (!accountById) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    } else if (!accountById.IsActive) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_WAS_LOCKED);
    }

    return accountById;
  }
}
