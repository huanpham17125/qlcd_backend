import { IQuery } from "@nestjs/cqrs";

export class DetailAccountByEmailQuery implements IQuery {
  constructor(public email: string) {}
}
