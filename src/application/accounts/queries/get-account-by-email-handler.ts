import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { NotFoundException } from 'src/application/common/exceptions/not-found-exception';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { GetAccountByEmailQuery } from './get-account-by-email-query';

@CommandHandler(GetAccountByEmailQuery)
export class GetAccountByEmailHandler
  implements ICommandHandler<GetAccountByEmailQuery> {
  constructor(
    @Inject('ACCOUNTS')
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
    private readonly passwordService: PasswordService,
  ) {}
  async execute(command: GetAccountByEmailQuery): Promise<Account> {
    const accountByEmail = await this._accountAsyncRepository.findByEmail(
      command.email,
    );

    if (!accountByEmail) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    }

    const isComparePassword = await this.passwordService.comparePassword(
      command.password,
      accountByEmail,
    );

    if (!isComparePassword) {
      throw new BadRequestException(Constants.AppResource.PASSWORD_INCORECT);
    } else if (!accountByEmail.IsActive) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_WAS_LOCKED);
    }

    return accountByEmail;
  }
}
