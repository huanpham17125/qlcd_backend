import { ICommand } from '@nestjs/cqrs';

export class FindAccountByEmailQuery implements ICommand {
  constructor(public readonly email: string) {}
}
