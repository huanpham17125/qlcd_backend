import { BadRequestException, Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountProfileQuery } from './get-account-profile-query';

@CommandHandler(GetAccountProfileQuery)
export class GetAccountProfileHandler
  implements ICommandHandler<GetAccountProfileQuery> {
  constructor(
    @Inject(Constants.AppProvide.ACCOUNT_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(command: GetAccountProfileQuery): Promise<Account> {
    const account = await this._accountAsyncRepository.getProfile(command.id);

    if (!account) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    }

    return account;
  }
}
