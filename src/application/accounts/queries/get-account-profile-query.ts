import { ICommand } from '@nestjs/cqrs';

export class GetAccountProfileQuery implements ICommand {
  constructor(public readonly id: string) {}
}
