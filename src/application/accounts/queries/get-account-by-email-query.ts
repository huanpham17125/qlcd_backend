import { ICommand } from '@nestjs/cqrs';

export class GetAccountByEmailQuery implements ICommand {
  constructor(
    public readonly email: string,
    public readonly password: string,
  ) {}
}
