import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountDataByEmailQuery } from './get-account-data-by-email-query';

@CommandHandler(GetAccountDataByEmailQuery)
export class GetAccountDataByEmailHandler
  implements ICommandHandler<GetAccountDataByEmailQuery> {
  constructor(
    @Inject(Constants.AppProvide.APP_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(command: GetAccountDataByEmailQuery): Promise<Account | null> {
    const entity = await this._accountAsyncRepository.findByEmail(
      command.email,
    );

    return entity;
  }
}
