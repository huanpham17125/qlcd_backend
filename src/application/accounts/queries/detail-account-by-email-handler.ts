import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { DetailAccountByEmailQuery } from './detail-account-by-email-query';

@QueryHandler(DetailAccountByEmailQuery)
export class DetailAccountByEmailHandler
  implements IQueryHandler<DetailAccountByEmailQuery> {
  constructor(
    @Inject(Constants.AppProvide.APP_PROVIDE)
    private readonly _accountAsyncRepository: IaccountAsyncRepository,
  ) {}
  async execute(query: DetailAccountByEmailQuery): Promise<any> {
    const item = await this._accountAsyncRepository.getByEmail(query.email);

    if (!item) {
      throw new BadRequestException(Constants.AppResource.ACCOUNT_NOT_FOUND);
    }

    return item;
  }
}
