import { ICommand } from '@nestjs/cqrs';

export class GetAccountDataByEmailQuery implements ICommand {
  constructor(public readonly email: string) {}
}
