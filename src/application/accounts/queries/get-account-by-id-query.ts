import { ICommand } from '@nestjs/cqrs';

export class GetAccountByIdQuery implements ICommand {
  constructor(public readonly id: string) {}
}
