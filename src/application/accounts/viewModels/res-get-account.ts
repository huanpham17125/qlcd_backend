export default class ResGetAccount {
  public fullName: string;
  public phone: string;
  public address: string;
  public email: string;
  public birthday: string;
  public isActive: boolean;
  public createdDate: string;
  constructor() {}
}
