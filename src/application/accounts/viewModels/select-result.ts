export class ResSelectResult {
  public id: any;
  public value: string;
  constructor(id: any, value: string) {
    (this.id = id), (this.value = value);

    return this;
  }
}
