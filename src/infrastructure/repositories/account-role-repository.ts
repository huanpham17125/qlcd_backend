import { InjectRepository } from '@nestjs/typeorm';
import AccountRole from 'src/domain/entities/account-role.entity';
import { IAsyncRepository } from 'src/domain/interfaces/iasync-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(AccountRole)
export class AccountRoleRepository
  extends AsyncGenericRepository<AccountRole>
  implements IAsyncRepository<AccountRole> {
  constructor(
    @InjectRepository(AccountRole)
    private readonly _asyncRepository: Repository<AccountRole>,
  ) {
    super(_asyncRepository);
  }
  async getByAccountId(accountId: string): Promise<AccountRole> {
    return await this._asyncRepository.findOne({
      where: { AccountId: accountId },
    });
  }

  getListByAccountId(accountId: string): Promise<AccountRole[] | null> {
    throw new Error('Method not implemented.');
  }
  getByName(name: string): Promise<AccountRole | null> {
    throw new Error('Method not implemented.');
  }
}
