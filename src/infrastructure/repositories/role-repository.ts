import { InjectRepository } from '@nestjs/typeorm';
import Role from 'src/domain/entities/role.entity';
import { IAsyncRepository } from 'src/domain/interfaces/iasync-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(Role)
export class RoleRepository
  extends AsyncGenericRepository<Role>
  implements IAsyncRepository<Role> {
  constructor(
    @InjectRepository(Role)
    private readonly _asyncRepository: Repository<Role>,
  ) {
    super(_asyncRepository);
  }
  getByAccountId(accountId: string): Promise<Role> {
    throw new Error('Method not implemented.');
  }

  getListByAccountId(accountId: string): Promise<Role[] | null> {
    throw new Error('Method not implemented.');
  }
  async getByName(name: string): Promise<Role | null> {
    return await this._asyncRepository.findOne({
      where: { NormalizedName: name },
    });
  }
}
