import { InjectRepository } from '@nestjs/typeorm';
import { paginate, paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import Message from 'src/domain/entities/message.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(RoomChat)
export class RoomChatRepository
  extends AsyncGenericRepository<RoomChat>
  implements IroomChatRepository {
  constructor(
    @InjectRepository(RoomChat)
    private readonly _asyncRepository: Repository<RoomChat>,
  ) {
    super(_asyncRepository);
  }
  
  async getByTempleId(templeId: string): Promise<RoomChat> {
    return await this._asyncRepository.findOne({
      where: { TempleId: templeId, IsActive: true },
    });
  }

  async getRoomByCreatorAndCreated(
    creatorId: string,
    createdId: string,
  ): Promise<any> {
    const entity = await this._asyncRepository
      .createQueryBuilder('R')
      .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
      .select('R.*')
      .where(
        'AR.AccountId = :creatorId AND  AR.AccountId = :createdId ADN R.IsActive = true',
        {
          creatorId: creatorId,
          createdId: createdId,
        },
      )
      .getOne();

    return entity ? entity : null;
  }
  async getAllByAccountId(
    accountId: string,
    pageSize: number,
    pageNumber: number,
  ): Promise<Pagination<RoomChat>> {
    const entity = await paginateRaw(
      this._asyncRepository
        .createQueryBuilder('R')
        .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
        .leftJoin(
          (qb) =>
            qb
              .select('me.*')
              .from(Message, 'me')
              .orderBy({ 'me.CreatedDate': 'DESC' })
              .limit(1),
          'M',
          'R.Id = M.RoomId',
        )
        .select(['R.Id', 'R.Name', 'R.Tittle'])
        .addSelect('M.UpdatedDate', 'MessageDate')
        .addSelect('M.Status', 'StatusMessage')
        .addSelect('M.TypeContent', 'TypeMessage')
        .addSelect('M.Content', 'Content')
        .where('AR.AccountId = :accountId AND R.IsActive = :isActive', {
          accountId: accountId,
          isActive: true,
        })
        .orderBy('R.UpdatedDate', 'DESC')
        .groupBy('R.Name'),
      {
        page: pageNumber,
        limit: pageSize,
      },
    );

    return entity;
  }

  async getByAccountId(accountId: string): Promise<RoomChat> {
    const entity = await this._asyncRepository
      .createQueryBuilder('R')
      .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
      .select(['R.Id', 'R.Name', 'R.Tittle'])
      .where('AR.AccountId = :accountId AND R.IsActive = :isActive', {
        accountId: accountId,
        isActive: true,
      })
      .getOne();

    return entity;
  }
}
