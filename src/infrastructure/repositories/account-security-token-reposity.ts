import { InjectRepository } from '@nestjs/typeorm';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(AccountSecurityToken)
export class AccountSecurityTokenReposity
  extends AsyncGenericRepository<AccountSecurityToken>
  implements IAccountSecutiryTokenRepository {
  constructor(
    @InjectRepository(AccountSecurityToken)
    private readonly accountSecurityRepository: Repository<
      AccountSecurityToken
    >,
  ) {
    super(accountSecurityRepository);
  }

  async getByAccountId(
    accountId: string,
  ): Promise<AccountSecurityToken> | null {
    const entity = await this.accountSecurityRepository.findOne({
      where: { AccountId: accountId },
    });
    
    return entity ? entity : null;
  }
}
