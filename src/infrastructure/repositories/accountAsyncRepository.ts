import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import Account from '../../domain/entities/account.entity';
import { IaccountAsyncRepository } from '../../domain/interfaces/iaccount-async-repository.interface';

@Injectable()
export class AccountAsyncRepository implements IaccountAsyncRepository {
  constructor(
    @InjectRepository(Account)
    private readonly asyncRepository: Repository<Account>,
  ) {}

  async getByEmail(email: string): Promise<Account> {
    return await this.asyncRepository.findOne({ where: { Email: email } });
  }

  async updateIsOnlineById(id: string, isOnline: boolean): Promise<boolean> {
    const entity = await this.asyncRepository.save({
      IsOnline: isOnline,
      Id: id,
    });

    return entity ? true : false;
  }

  async getById(id: string): Promise<Account> | null {
    const entity = await this.asyncRepository.findOne({ Id: id });

    return entity ? entity : null;
  }

  async getProfile(id: string): Promise<Account> | null {
    const entity = await this.asyncRepository
      .createQueryBuilder('acc')
      .leftJoinAndSelect('acc.AccountRoles', 'accRole')
      .leftJoinAndSelect('accRole.Role', 'role')
      .leftJoin('acc.AccountSetting', 'AS', 'acc.Id = AS.AccountId')
      .select([
        'acc.Email',
        'acc.Id',
        'acc.FullName',
        'acc.Phone',
        'accTemple',
        'acc.PasswordHash',
        'acc.Province',
        'acc.Gender',
        'acc.Birthday',
        'acc.Street',
        'acc.City',
        'acc.IsActive',
        'accRole',
        'role'
      ])
      .where('acc.Id = :id', {
        id: id,
      })
      .getOne();

    return entity ? entity : null;
  }

  async updateById(account: Account, id: string): Promise<boolean> {
    await this.asyncRepository.update(id, account);

    return true;
  }

  async updatePasswordById(password: string, id: string) {
    await this.asyncRepository.update(id, { PasswordHash: password });

    return true;
  }

  async create(account: Account): Promise<Account> {
    await this.asyncRepository
      .createQueryBuilder()
      .insert()
      .into(Account)
      .values(account)
      .execute();

    return account;
  }

  public findByEmail = async (email: string): Promise<Account> => {
    const entity = await this.asyncRepository
      .createQueryBuilder('acc')
      .leftJoinAndSelect('acc.AccountRoles', 'accRole')
      .leftJoinAndSelect('accRole.Role', 'role')
      .select([
        'acc.Email',
        'acc.Id',
        'acc.FullName',
        'acc.Phone',
        'accRole',
        'role.Name',
        'role.NormalizedName',
        'role.Id',
        'acc.Province',
        'acc.City',
        'acc.Street',
        'acc.IsActive',
        'acc.PasswordHash',
      ])
      .where(
        'acc.Email = :email AND accRole.IsActive = :active AND role.IsActive = :active',
        {
          active: true,
          email: email,
        },
      )
      .getOne();

    return entity ? entity : null;
  };
}
