import { InjectRepository } from '@nestjs/typeorm';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(AccountRoom)
export class AccountRoomRepository extends AsyncGenericRepository<AccountRoom> {
  constructor(
    @InjectRepository(AccountRoom)
    private readonly _asyncRepository: Repository<AccountRoom>,
  ) {
    super(_asyncRepository);
  }
}
