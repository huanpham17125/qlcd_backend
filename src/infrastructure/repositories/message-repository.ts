import { InjectRepository } from '@nestjs/typeorm';
import { paginateRaw, Pagination } from 'nestjs-typeorm-paginate';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import Message from 'src/domain/entities/message.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { StatusMessage } from 'src/domain/enum/domainEnum';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(Message)
export class MessageRepository
  extends AsyncGenericRepository<Message>
  implements ImessageRepository {
  constructor(
    @InjectRepository(Message)
    private readonly _asyncRepository: Repository<Message>,
  ) {
    super(_asyncRepository);
  }
  updateStatusById(messageId: number, status: StatusMessage): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  async getMessagePaginationByAccountId(
    accountId: string,
    pageSize: number,
    pageNumber: any,
  ): Promise<Pagination<Message>> {
    const lstMessage = await paginateRaw(
      this._asyncRepository
        .createQueryBuilder('M')
        .innerJoin('M.RoomChat', 'R')
        .leftJoin('R.AccountRoom', 'AR')
        // .leftJoin(
        //   (qb) =>
        //     qb
        //       .select('me.*')
        //       .from(Message, 'me')
        //       .orderBy({ 'me.CreatedDate': 'DESC' })
        //       .limit(1),
        //   'M',
        //   'R.Id = M.RoomId',
        // )
        .select('M.*')
        .where('AR.AccountId = :accountId', {
          accountId: accountId,
        })
        .orderBy('M.CreatedDate', 'DESC')
        .groupBy('M.Id'),
      {
        page: pageNumber,
        limit: pageSize,
      },
    );

    return lstMessage;
  }
}
