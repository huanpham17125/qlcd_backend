import { InjectRepository } from '@nestjs/typeorm';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

export class AccountSettingRepository
  extends AsyncGenericRepository<AccountSetting>
  implements IaccountSettingRepository {
  constructor(
    @InjectRepository(AccountSetting)
    private readonly accountSettingRepository: Repository<AccountSetting>,
  ) {
    super(accountSettingRepository);
  }

  async getByAccountId(accountId: string): Promise<AccountSetting> | null {
    const entity = await this.accountSettingRepository
      .createQueryBuilder('setting')
      .leftJoinAndSelect('setting.ColorCode', 'color')
      .leftJoinAndSelect('setting.Account', 'acc')
      .select(['acc', 'setting', 'color.Name', 'color.Value', 'color.Id'])
      .where('setting.AccountId = :accountId', {
        accountId: accountId,
      })
      .getOne();
      
    return entity ? entity : null;
  }
}
