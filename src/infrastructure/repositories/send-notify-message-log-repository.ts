import { InjectRepository } from '@nestjs/typeorm';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(SendNotifyLog)
export class SendNotifyMessageLogRepository
  extends AsyncGenericRepository<SendNotifyLog>
  implements ISendNotifyRepository {
  constructor(
    @InjectRepository(SendNotifyLog)
    private readonly _asyncRepository: Repository<SendNotifyLog>,
  ) {
    super(_asyncRepository);
  }
  async getByAccountId(accountId: string): Promise<SendNotifyLog[]> {
    const lst = await this._asyncRepository.find({
      where: { AccountId: accountId, IsActive: true },
      order: { CreatedDate: 'DESC' },
    });

    return lst;
  }
}
