import { InjectRepository } from '@nestjs/typeorm';
import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import { ISendMailLog } from 'src/domain/interfaces/isend-mail-log.interface';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(SendMailLog)
export class SendEmailLogRepository
  extends AsyncGenericRepository<SendMailLog>
  implements ISendMailLog {
  constructor(
    @InjectRepository(SendMailLog)
    private readonly _asyncRepository: Repository<SendMailLog>,
  ) {
    super(_asyncRepository);
  }
  getByAccountId(accountId: string): Promise<SendMailLog[]> {
    throw new Error('Method not implemented.');
  }
}
