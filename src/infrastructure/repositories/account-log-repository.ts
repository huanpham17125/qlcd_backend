import { InjectRepository } from '@nestjs/typeorm';
import AccountLog from 'src/domain/entities/account-log.entity';
import { EntityRepository, Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';

@EntityRepository(AccountLog)
export class AccountLogRepository extends AsyncGenericRepository<AccountLog> {
  constructor(
    @InjectRepository(AccountLog)
    private readonly _asyncRepository: Repository<AccountLog>,
  ) {
    super(_asyncRepository);
  }
}
