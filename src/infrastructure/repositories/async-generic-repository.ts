import { Injectable } from '@nestjs/common';
import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { Repository } from 'typeorm';

@Injectable()
export class AsyncGenericRepository<TEntity>
  implements IAsyncGenericRepository<TEntity> {
  private readonly asyncRepository: Repository<TEntity>;
  constructor(repo: Repository<TEntity>) {
    this.asyncRepository = repo;
  }

  async add(entity: TEntity): Promise<TEntity> {
    await this.asyncRepository.save(entity);

    return entity;
  }

  async getById(id: string | number): Promise<TEntity> {
    const entity = await this.asyncRepository.findOne({ where: { Id: id } });

    return entity ? entity : null;
  }

  async getListActive(): Promise<TEntity[]> | null {
    const entity = await this.asyncRepository.find({
      where: { IsActive: true },
    });

    return entity.length ? entity : [];
  }

  async updateById(entity: TEntity, id: string): Promise<boolean> {
    await this.asyncRepository.update(id, entity);

    return true;
  }

  async deleteById(id: string): Promise<boolean> {
    await this.asyncRepository.delete(id);

    return true;
  }
}
