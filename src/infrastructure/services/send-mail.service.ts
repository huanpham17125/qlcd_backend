import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateSendMailLogCommand } from 'src/application/sendMailLogs/commands/create/create-send-mail-log-command';
import { TypeSendMail } from 'src/domain/enum/domainEnum';
import * as path from 'path';
import * as fs from 'fs';
import handlebars = require('handlebars');

@Injectable()
export class SendMailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly commandBus: CommandBus,
  ) {}

  public sendMailRegisterTemp(
    accountId: string,
    toEmail: string,
    subject: string,
  ) {
    this.mailerService
      .sendMail({
        to: toEmail,
        subject: subject,
        template: 'register-temp',
        context: {
          email: toEmail,
          webUrl: process.env.REGISTER_OFFICAL_WEB,
          androiDeepLink: process.env.REGISTER_OFFICAL_IOS,
          iosDeepLink: process.env.REGISTER_OFFICAL_ANDROI,
        },
      })
      .then((success) => {
        const filePath = path.join(
          process.cwd() + '/src/api/templates/register-temp.hbs',
        );
        const source = fs.readFileSync(filePath, 'utf-8').toString();

        this.createSendMailLog(
          accountId,
          success.envelope.from,
          toEmail,
          subject,
          source,
          TypeSendMail.Register_Office,
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }

  public async sendMailInvite(
    accountId: string,
    toEmail: string,
    subject: string,
    email: string,
  ) {
    await this.mailerService
      .sendMail({
        to: toEmail,
        subject: subject,
        template: 'invite',
        context: {
          email: email,
          webUrl: process.env.INVITE_USER_WEB,
          androiDeepLink: process.env.INVITE_USER_ANDROI,
          iosDeepLink: process.env.INVITE_USER_IOS,
        },
      })
      .then(async (success) => {
        const filePath = path.join(
          process.cwd() + '/src/api/templates/invite.hbs',
        );
        const source = fs.readFileSync(filePath, 'utf-8').toString();

        await this.createSendMailLog(
          accountId,
          success.envelope.from,
          toEmail,
          subject,
          source,
          TypeSendMail.Invite,
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }

  public async sendMailForgotPassword(
    accountId: string,
    toEmail: string,
    subject: string,
  ) {
    await this.mailerService
      .sendMail({
        to: toEmail,
        subject: subject,
        template: 'forgot-password',
        context: {
          email: toEmail,
          webUrl: process.env.RESET_PASSWORD_WEB,
          androiDeepLink: process.env.RESET_PASSWORD_ANDROI,
          iosDeepLink: process.env.RESET_PASSWORD_IOS,
        },
      })
      .then(async (success) => {
        const filePath = path.join(
          process.cwd() + '/src/api/templates/forgot-password.hbs',
        );
        const source = fs.readFileSync(filePath, 'utf-8').toString();

        await this.createSendMailLog(
          accountId,
          success.envelope.from,
          toEmail,
          subject,
          source,
          TypeSendMail.ForgotPassword,
        );
      })
      .catch((err) => {
        console.log(err);
      });
  }

  private async createSendMailLog(
    accountId: string,
    from: string,
    to: string,
    subject: string,
    body: string,
    type: number,
  ) {
    return await this.commandBus.execute(
      new CreateSendMailLogCommand(
        accountId,
        from,
        to,
        '',
        '',
        subject,
        body,
        type,
        '',
      ),
    );
  }
}
