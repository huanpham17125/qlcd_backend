import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import Account from 'src/domain/entities/account.entity';

@Injectable()
export class PasswordService {

  async getPasswordHash(password: string, account: Account): Promise<Account> {
    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(password, salt);
    account.PasswordHash = passwordHash;

    return account;
  }

  async comparePassword(password: string, account: Account): Promise<boolean> {
    return await bcrypt.compare(password, account.PasswordHash);
  }
}
