import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';
import { CreateNotifyMessageCommand } from 'src/application/sendNotifiMessage/commands/create/create-notify-message-command';
import { DeleteNotifyMessageByIdCommand } from 'src/application/sendNotifiMessage/commands/delete/delete-notify-message-by-id-command';
import { UpdateNotifyMessageByIdCommand } from 'src/application/sendNotifiMessage/commands/update/update-notify-message-by-id-command';
import { GetNotifyMessageByAccountIdQuery } from 'src/application/sendNotifiMessage/queries/get-notify-message-by-account-id-query';
import { StatusSendNotify } from 'src/domain/enum/domainEnum';
import { FireStoreService } from './fire-store.service';

@Injectable()
export class SendNotifyService {
  constructor(
    private readonly _firebase: FireStoreService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {
    this._firebase.configure();
  }

  async sendNotifyToFirebase(
    fromAccountId: string,
    body: ReqCreateSendnotifyMessage,
  ): Promise<any> {
    //await this._firebase.sendNotification(projectId, token, notification);
    return await this.commandBus.execute(
      new CreateNotifyMessageCommand(fromAccountId, body.accountId, body),
    );
  }

  async getActiveNotifyMessage(accountId: string) {
    const lstNotify = await this.queryBus.execute(
      new GetNotifyMessageByAccountIdQuery(accountId),
    );

    const resGetNotify = [];

    if (!lstNotify.length) {
      return resGetNotify;
    }

    return resGetNotify;
  }

  async deleteNotifyMessage(id: number, accountId: string) {
    return await this.commandBus.execute(
      new DeleteNotifyMessageByIdCommand(id, accountId),
    );
  }

  async updateNotifyMessageById(
    id: number,
    accountId: string,
    status: StatusSendNotify,
  ) {
    return await this.commandBus.execute(
      new UpdateNotifyMessageByIdCommand(id, accountId, status),
    );
  }
}
