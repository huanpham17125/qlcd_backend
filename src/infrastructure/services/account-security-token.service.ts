import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateAccountSecurityTokenCommand } from 'src/application/accountSecurityTokens/commands/create/create-account-security-token-command';
import { UpdateAccountSecurityTokenCommand } from 'src/application/accountSecurityTokens/commands/update/update-account-security-token-command';
import { GetByAccountIdQuery } from 'src/application/accountSecurityTokens/queries/get-by-account-id-query';
import { TypeToken } from 'src/domain/enum/domainEnum';

@Injectable()
export class AccountSecurityTokenService {
  constructor(private readonly commandBus: CommandBus) {}

  async getByAccountId(accountId: string) {
    return await this.commandBus.execute(new GetByAccountIdQuery(accountId));
  }

  async create(
    accountId: string,
    type: TypeToken,
    value: string,
    expiryDate: Date,
  ) {
    return await this.commandBus.execute(
      new CreateAccountSecurityTokenCommand(accountId, type, value, expiryDate),
    );
  }

  async update(value: string, expirseDate: Date, id: string) {
    return await this.commandBus.execute(
      new UpdateAccountSecurityTokenCommand(value, expirseDate, id),
    );
  }
}
