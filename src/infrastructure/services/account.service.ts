import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqRegister } from 'src/api/viewModels/req-register';
import { ReqRegisterTemp } from 'src/api/viewModels/req-register-temp';
import { ReqRegisterUser } from 'src/api/viewModels/req-register-user';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
import { CreateAccountCommand } from 'src/application/accounts/commands/create/create-account-command';
import { RegisterTempCommand } from 'src/application/accounts/commands/create/register-temp-command';
import { UpdateAccountByIdCommand } from 'src/application/accounts/commands/update/update-account-by-id-command';
import { UpdateAccountSettingByAccountIdCommand } from 'src/application/accounts/commands/update/update-account-setting-by-account-id-command';
import { UpdateIsOnlineByIdCommand } from 'src/application/accounts/commands/update/update-is-online-by-id-command';
import { UpdatePasswordByIdCommand } from 'src/application/accounts/commands/update/update-password-by-id-command';
import { UpdateStatusAccountCommand } from 'src/application/accounts/commands/update/update-status-account-command';
import { FindAccountByEmailQuery } from 'src/application/accounts/queries/find-account-by-email-query';
import { GetAccountByEmailQuery } from 'src/application/accounts/queries/get-account-by-email-query';
import { GetAccountByIdQuery } from 'src/application/accounts/queries/get-account-by-id-query';
import { GetAccountDataByEmailQuery } from 'src/application/accounts/queries/get-account-data-by-email-query';
import { GetAccountProfileQuery } from 'src/application/accounts/queries/get-account-profile-query';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import Account from 'src/domain/entities/account.entity';

@Injectable()
export class AccountService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  async createAccount(req: ReqRegister, creatorId: string) {
    const itemCreator = await this.commandBus.execute(
      new GetAccountByIdQuery(creatorId),
    );

    return await this.commandBus.execute(
      new CreateAccountCommand(
        itemCreator.AccountRole.RoleId,
        itemCreator.AccountTemple.TempleId,
        creatorId,
        req.fullName,
        req.email,
        req.password,
        req.phone,
        req.gender,
      ),
    );
  }

  async registerTemp(req: ReqRegisterTemp) {
    return await this.commandBus.execute(
      new RegisterTempCommand(req.email, req.password),
    );
  }

  async adminCreateAccount(req: ReqRegisterUser) {
    return null;
  }

  async getAccountByEmailAndPassword(email: string, password: string) {
    return await this.commandBus.execute(
      new GetAccountByEmailQuery(email, password),
    );
  }

  async getAccountByEmail(email: string) {
    return await this.commandBus.execute(new FindAccountByEmailQuery(email));
  }
  async getAccountById(id: string) {
    return await this.commandBus.execute(new GetAccountByIdQuery(id));
  }

  async getDetailProfile(id: string) {
    return await this.commandBus.execute(new GetAccountProfileQuery(id));
  }
  async updatelProfile(req: ReqUpdateAccount, id: string) {
    const isExist = await this.commandBus.execute(
      new GetAccountDataByEmailQuery(req.email),
    );

    if (isExist && isExist.Id != id) {
      throw new BadRequestException(
        Constants.AppResource.EMAIL_ALREADY_EXISTED,
      );
    }

    return await this.commandBus.execute(new UpdateAccountByIdCommand(req, id));
  }

  async updateStatusAccount(
    type: number,
    id: string,
    adminId: string,
  ): Promise<boolean> {
    return await this.commandBus.execute(
      new UpdateStatusAccountCommand(id, adminId, type),
    );
  }

  async changePassword(password: string, id: string) {
    return await this.commandBus.execute(
      new UpdatePasswordByIdCommand(password, id),
    );
  }

  async updateAccountSetting(
    accountId: string,
    timePushNotifyId: number,
    colorNum: number,
  ) {
    return await this.commandBus.execute(
      new UpdateAccountSettingByAccountIdCommand(
        accountId,
        timePushNotifyId,
        colorNum,
      ),
    );
  }
  async updateIsOnlineByAccountId(accountId: string, isOnline: boolean) {
    return await this.commandBus.execute(
      new UpdateIsOnlineByIdCommand(accountId, isOnline),
    );
  }
}
