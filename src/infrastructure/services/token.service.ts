import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ClaimsIdentityToken } from 'src/api/viewModels/claims-identity-token';
import Account from 'src/domain/entities/account.entity';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';

@Injectable()
export class TokenService {
  constructor(private readonly jwtService: JwtService) {}

  generateAccessToken = async (
    account: Account,
  ): Promise<{ accessToken: string; expiresIn: number; expiresDate: Date }> => {
    const expDate = this.getExpirseDate(
      Number(process.env.JWT_EXPIRATION_ACCESS),
    );
    const claims = new ClaimsIdentityToken(
      account.Id,
      account.Email,
      account.AccountRoles[0].Role.Id,
      account.AccountRoles[0].Role.NormalizedName,
      uuid(),
    );
    const claimTokens = {
      id: claims.Id,
      email: claims.Email,
      roleId: claims.RoleId,
      roleName: claims.RoleName,
      jti: claims.Jti,
    };

    const accessToken = await this.jwtService.sign(claimTokens, {
      secret: process.env.JWT_SECRETKEY,
      expiresIn: Number(process.env.JWT_EXPIRATION_ACCESS),
    });

    return {
      accessToken: accessToken,
      expiresIn: Number(process.env.JWT_EXPIRATION_ACCESS),
      expiresDate: expDate,
    };
  };

  generateRefreshToken = async (account: Account) => {
    return ''; //toDO
  };

  verifyToken = async (token: string): Promise<any> => {
    const claims = await this.jwtService.verifyAsync(token, {
      secret: process.env.JWT_SECRETKEY,
      ignoreExpiration: false,
    });

    return claims || null;
  };

  private getExpirseDate(seconds: number) {
    const now = new Date();

    return moment(now).add(seconds, 'seconds').toDate();
  }
}
