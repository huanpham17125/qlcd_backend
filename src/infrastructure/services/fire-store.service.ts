import { Injectable } from '@nestjs/common';
import * as fireStoreConfig from '../../../config/firebase-store.config';
import { IFireBaseStoreData } from 'src/application/common/interfaces/ifire-base-store-data.interface';
import { IFireBaseStoreConfig } from 'src/application/common/interfaces/ifire-base-store-config.interface';
import { credential, initializeApp, messaging } from 'firebase-admin';

@Injectable()
export class FireStoreService {
  private _projectItems = new Map<string, IFireBaseStoreData>();

  private projects(): Array<IFireBaseStoreConfig> {
    return fireStoreConfig.default as IFireBaseStoreConfig[];
  }

  configure(): void {
  }

  getProjectData(projectId: string): IFireBaseStoreData {
    const data = this._projectItems.get(projectId);
    if (!data) {
      throw new Error('project not found');
    }
    return data;
  }

  async sendNotification(
    projectId: string,
    token: string,
    notification: any,
  ): Promise<any> {
    try {
      const project = this.getProjectData(projectId);
      const data = { ...notification, token };
      return await messaging(project.ref).send(data);
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
