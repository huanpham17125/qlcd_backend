import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqGetAccountByEmail } from 'src/api/viewModels/req-get-account-by-email';
import { CreateAccountRoomCommand } from 'src/application/accountRooms/commands/create/create-account-room-command';
import { GetAccountDataByEmailQuery } from 'src/application/accounts/queries/get-account-data-by-email-query';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { CreateRoomChatCommand } from 'src/application/roomChats/commands/create/create-room-chat-command';
import { GetAllRoomByAccountIdQuery } from 'src/application/roomChats/queries/get-all-room-by-account-id-query';
import { GetRoomByAccountIdQuery } from 'src/application/roomChats/queries/get-room-by-account-id-query';

@Injectable()
export class RoomChatService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  async fillAllByAccountId(
    accountId: string,
    pageSize?: number,
    pageNumber?: number,
  ) {
    return await this.commandBus.execute(
      new GetAllRoomByAccountIdQuery(accountId, pageSize, pageNumber),
    );
  }

  async createRoom(
    accountId: string,
    email: string,
    nameRoom: string,
    templeId: string,
  ): Promise<void> {
    const itemRoom = await this.queryBus.execute(
      new GetRoomByAccountIdQuery(accountId),
    );

    if (!itemRoom) {
      await this.commandBus.execute(
        new CreateRoomChatCommand(accountId, nameRoom, templeId),
      );
    }
  }

  async addAccountToRoom(
    creator: string,
    roomId: string,
    lstEmail: ReqGetAccountByEmail[],
  ): Promise<boolean> {
    lstEmail.forEach(async (itemEmail) => {
      const itemAccount = await this.commandBus.execute(
        new GetAccountDataByEmailQuery(itemEmail.email),
      );

      if (!itemAccount) {
        throw new BadRequestException(
          Constants.AppResource.EMAIL_NOT_ALREADY_EXISTED + ' ' + itemEmail,
        );
      }

      await this.commandBus.execute(
        new CreateAccountRoomCommand(itemAccount.Id, roomId),
      );
    });

    return true;
  }
}
