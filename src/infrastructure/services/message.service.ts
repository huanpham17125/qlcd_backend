import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { Pagination } from 'nestjs-typeorm-paginate';
import { CreateMessageCommand } from 'src/application/messages/commands/create/create-message-command';
import { UpdateContentMessageByIdCommand } from 'src/application/messages/commands/update/update-content-message-by-id-command';
import { UpdateStatusMessageCommand } from 'src/application/messages/commands/update/update-status-message-command';
import { GetMessagePaginationByRoomIdQuery } from 'src/application/messages/queries/get-message-pagination-by-room-id-query';
import Message from 'src/domain/entities/message.entity-gateway';
import { StatusMessage, TypeContentMessage } from 'src/domain/enum/domainEnum';

@Injectable()
export class MessageService {
  constructor(private readonly commandBus: CommandBus) {}

  async getMessageByRoomId(
    accountId: string,
    pageSize?: number,
    pageNumber?: number,
  ): Promise<Pagination<Message>> {
    return await this.commandBus.execute(
      new GetMessagePaginationByRoomIdQuery(accountId, pageSize, pageNumber),
    );
  }

  async createMessage(
    senderId: string,
    templeId: string,
    content: any,
    scheduleId: number,
  ) {
    return await this.commandBus.execute(
      new CreateMessageCommand(senderId, templeId, content, scheduleId),
    );
  }

  async updateMessageById(
    messageId: string,
    accountId: string,
    content: string,
  ) {
    return await this.commandBus.execute(
      new UpdateContentMessageByIdCommand(messageId, accountId, content),
    );
  }

  async updateStatus(messageId: number, status: StatusMessage) {
    return await this.commandBus.execute(
      new UpdateStatusMessageCommand(messageId, status),
    );
  }
}
