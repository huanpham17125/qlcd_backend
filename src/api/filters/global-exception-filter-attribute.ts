import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  NotFoundException,
} from '@nestjs/common';
import { Constants } from 'src/application/common/constants';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { UnAuthorizeException } from 'src/application/common/exceptions/un-authorize-exception';
import { ValidationException } from 'src/application/common/exceptions/validation-exception';
@Catch()
export class GlobalExceptionFilterAttribute implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    //super.catch(exception, host);
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    
    if (exception instanceof BadRequestException) {
      this.handlerValidationException(exception, response);
    } else if (exception instanceof ValidationException) {
      this.handlerValidationException(exception, response);
    } else if (exception instanceof UnAuthorizeException) {
      this.handlerValidationException(exception, response);
    } else if (exception instanceof NotFoundException) {
      this.handlerValidationException(exception, response);
    } else {
      this.handerUnKnowException(exception, response);
    }
  }

  private handlerValidationException(
    exception:
      | BadRequestException
      | ValidationException
      | UnAuthorizeException
      | NotFoundException,
    res: {
      status: (
        arg0: any,
      ) => {
        (): any;
        new (): any;
        json: {
          (arg0: { code: any; data: any; message: any }): void;
          new (): any;
        };
      };
    },
  ) {
    return res.status(exception.getStatus()).json({
      code: exception.getStatus(),
      data: null,
      message: exception.getResponse(),
    });
  }

  private handerUnKnowException(exception, res) {
    return res.status(Constants.AppStatus.StatusCode500).json({
      code: Constants.AppStatus.StatusCode500,
      data: null,
      message: exception.message,
    });
  }
}
