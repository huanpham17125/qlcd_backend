import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class ReqGetAccountByEmail {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @ApiProperty({ example: 'vmouser@gmail.com' })
  email: string;
}
