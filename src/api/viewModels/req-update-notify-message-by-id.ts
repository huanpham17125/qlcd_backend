import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsNotEmpty } from 'class-validator';
import { StatusSendNotify } from 'src/domain/enum/domainEnum';

export class ReqUpdateNotifyMessageById {
  @IsNumber()
  @IsNotEmpty({ message: 'Please enter type notify' })
  @ApiProperty({ example: 3 })
  public id: number;

  @IsNumber()
  @IsNotEmpty({ message: 'Please enter type notify' })
  @ApiProperty({ example: 3 })
  public status: StatusSendNotify;
}
