import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  IsEmail,
  MinLength,
  MaxLength,
  Matches,
} from 'class-validator';

export class ReqRegisterTemp {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @ApiProperty({ example: '123456@gmail.com' })
  email: string;

  @ApiProperty({ example: 'Aa12345678' })
  @IsString()
  @IsNotEmpty({ message: 'Please enter a password than 8 characters.' })
  @MinLength(8, { message: 'Please enter a password than 8 characters.' })
  @MaxLength(50, { message: 'Please enter a password less 50 characters.' })
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak! Please fix to continue.',
  })
  password: string;
}
