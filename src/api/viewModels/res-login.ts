export class ResLogin {
  public accessToken: string;
  public expiresInSeconds: number;
  public expiresDate: Date;
  public refreshToken: string;

  constructor(
    accessToken: string,
    expirse: number,
    expirseDate: Date,
    refreshToken: string,
  ) {
    (this.accessToken = accessToken),
      (this.expiresDate = expirseDate),
      (this.expiresInSeconds = expirse),
      (this.refreshToken = refreshToken);

    return this;
  }
}
