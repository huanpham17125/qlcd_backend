import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsString,
  IsNotEmpty,
  MinLength,
  MaxLength,
} from 'class-validator';

export class ReqRegisterOffice {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @ApiProperty({ example: 'vmoadmintemple@gmail.com' })
  email: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter a name.' })
  @ApiProperty({ example: 'AdminUser' })
  name: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter a temple name.' })
  @ApiProperty({ example: 'Hiroshima Temple' })
  templeName: string;

  @IsString()
  @ApiProperty({ example: 'Buddhism' })
  domination: string;

  @IsString()
  @ApiProperty({ example: 'sub Buddhism' })
  subDomination: string;

  @IsString()
  @ApiProperty({ example: '8455555555' })
  @MinLength(9, {
    message: 'Phone is not format(min 9 characters). Please check again.',
  })
  @MaxLength(15, {
    message: 'Phone is not format(max 15 characters). Please check again.',
  })
  phone: string;
}
