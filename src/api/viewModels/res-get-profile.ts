
export class ResGetProfile {
  public id: string;
  public email: string;
  public name: string;
  public roleName: string;
  public isActive: boolean;
  public isAdmin: boolean;
  public canEdit: boolean;

  constructor(
    id: string,
    email: string,
    fullName: string,
    normalizedRole: string,
    isActive: boolean,
    isAdmin: boolean,
    canEdit: boolean,
  ) {
    (this.id = id),
      (this.email = email),
      (this.name = fullName),
      (this.roleName = normalizedRole),
      (this.isActive = isActive),
      (this.isAdmin = isAdmin),
      (this.canEdit = canEdit);

    return this;
  }
}
