import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';

export class ReqAccountLockOrUnlock {
  @IsNumber()
  @IsNotEmpty({ message: 'Please choose your type lock.' })
  @Min(0, { message: 'Type lock is not correct format.' })
  @Max(1, { message: 'Type lock is not correct format.' })
  @ApiProperty({ example: 1, description: '0: lock, 1: unlock' })
  type: number;

  @IsString()
  @IsNotEmpty({ message: 'Please choose account.' })
  @ApiProperty({ example: '0A6F6B55-2909-4469-B086-71E2600EEAC6' })
  accountId: string;
}
