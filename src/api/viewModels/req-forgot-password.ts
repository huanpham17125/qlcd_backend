import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";

export class ReqForgotPassword {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @IsNotEmpty({ message: 'Please enter a email.'})
  @ApiProperty({ example: 'vmouser@gmail.com' })
  public email: string;
}
