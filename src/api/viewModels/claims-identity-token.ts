export class ClaimsIdentityToken {
  public Id: string;
  public FullName: string;
  public Email: string;
  public RoleId: string;
  public RoleName: string;
  public Jti: string;
  constructor(
    id: string,
    email: string,
    roleId: string,
    roleName: string,
    jti: string,
  ) {
    this.Id = id;
    this.Email = email;
    this.RoleId = roleId;
    this.RoleName = roleName;
    this.Jti = jti;

    return this;
  }
}
