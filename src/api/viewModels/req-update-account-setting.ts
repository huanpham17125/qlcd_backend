import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class ReqUpdateAccountSetting {
  @IsNumber()
  @IsNotEmpty({ message: 'Please enter time push notify message id' })
  @ApiProperty({ example: 3 })
  timPushNotifyId: number;

  @IsNumber()
  @IsNotEmpty({ message: 'Please enter color number' })
  @ApiProperty({ example: 1 })
  colorNum: number;
}
