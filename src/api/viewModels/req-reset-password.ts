import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, Matches, MaxLength, MinLength } from "class-validator";

export class ReqResetPassword {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @IsNotEmpty({ message: 'Please enter a email.'})
  @ApiProperty({ example: 'vmouser@gmail.com' })
  public email: string;

  @ApiProperty({ example: 'Aa12345678' })
  @IsString()
  @IsNotEmpty({ message: 'Please enter a password new than 8 characters.' })
  @MinLength(8, { message: 'Please enter a password new than 8 characters.' })
  @MaxLength(50, { message: 'Please enter a password new less 50 characters.' })
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).*$/,
    {
      message: 'Password new too weak! Please fix to continue.',
    },
  )
  passwordNew: string;

  @ApiProperty({ example: 'Aa12345678' })
  @IsString()
  @IsNotEmpty({ message: 'Please enter a password confirm than 8 characters.' })
  @MinLength(8, {
    message: 'Please enter a password confirm than 8 characters.',
  })
  @MaxLength(50, {
    message: 'Please enter a password confirm less 50 characters.',
  })
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).*$/,
    {
      message: 'Password confirm too weak! Please fix to continue.',
    },
  )
  passwordConfirm: string;
}
