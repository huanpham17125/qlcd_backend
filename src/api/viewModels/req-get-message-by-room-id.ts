import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class ReqGetMessageByRoomId {
  @ApiProperty({ example: 20 })
  pageSize?: number;

  @ApiProperty({ example: 1 })
  pageNumber?: number;
}
