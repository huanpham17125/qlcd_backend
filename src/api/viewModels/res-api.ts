import { Constants } from '../../application/common/constants';

export class ResApi {
  public code: number;
  public data: any;
  public message: string;

  constructor() {
    this.code = Constants.AppStatus.StatusCode200;
    this.data = null;
    this.message = '';
  }

  public Success(code: number, data: any, message: string): ResApi {
    this.code = code;
    this.data = data;
    this.message = message ? message : Constants.AppResource.ACCTION_SUCCESS;

    return this;
  }

  public Error(code: number, data: any, message: string) {
    this.code = code;
    this.data = data;
    this.message = message ? message : Constants.AppResource.EXCEPTION_UNKNOWN;

    return this;
  }

  public ToJson(data: ResApi) {
    return JSON.stringify(data);
  }
}
