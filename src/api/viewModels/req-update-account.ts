import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class ReqUpdateAccount {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @ApiProperty({ example: '123456@gmail.com' })
  email: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter a fullName.' })
  @ApiProperty({ example: 'vmo user' })
  fullName: string;

  // @IsString({ message: 'Please enter your birthday.' })
  // @ApiProperty({
  //   example: '1990-01-01',
  //   description: 'format string : yyyy-dd-mm',
  // })
  // // birthday: string;

  // @IsNotEmpty({ message: 'Please enter your phone.' })
  // @ApiProperty({ example: '' })
  // phone: string;

  // @IsString({ message: 'Please enter zip code.' })
  // @ApiProperty({ example: '' })
  // zipCode: string;

  // @IsString({ message: 'Please enter your province.' })
  // @ApiProperty({ example: '' })
  // province: string;

  // @IsString({ message: 'Please enter your city.' })
  // @ApiProperty({ example: '' })
  // city: string;

  // @IsString({ message: 'Please enter your street.' })
  // @ApiProperty({ example: '' })
  // street: string;
}
