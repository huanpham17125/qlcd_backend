import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { TypeSendNotify } from 'src/domain/enum/domainEnum';

export class ReqCreateSendnotifyMessage {
  @IsString()
  @IsNotEmpty({ message: 'Please enter accountId' })
  @ApiProperty({ example: '196483EE-73F7-EA11-9A2D-887873E284BF' })
  public accountId: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter title message' })
  @ApiProperty({ example: 'notification' })
  public title: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter body message' })
  @ApiProperty({ example: 'Please complete your profile' })
  public body: string;

  @IsString()
  @IsNotEmpty({ message: 'Please enter message' })
  @ApiProperty({ example: 'Please complete your profile' })
  public message: string;

  @IsNumber({}, { message: 'TypeMessage is number format' })
  @IsNotEmpty({ message: 'Please enter type notify' })
  @ApiProperty({ example: 3 })
  public type: TypeSendNotify;
}
