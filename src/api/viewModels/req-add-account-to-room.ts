import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsDefined,
  IsNotEmpty,
  IsString,
  ValidateNested,
} from 'class-validator';
import { ReqGetAccountByEmail } from './req-get-account-by-email';

export class ReqAddAccountToRoom {
  @IsArray()
  @Type(() => ReqGetAccountByEmail)
  @ValidateNested({ each: true })
  @IsDefined()
  @ApiProperty({ type: ReqGetAccountByEmail, isArray: true })
  public lstEmail: ReqGetAccountByEmail[];

  @IsString({ message: 'Please enter the correct id room format.' })
  @IsNotEmpty({ message: 'Please enter this room Id' })
  @ApiProperty({ example: 'c69ab67f-f515-473b-bcdb-025b6f087ecf' })
  roomId: string;
}
