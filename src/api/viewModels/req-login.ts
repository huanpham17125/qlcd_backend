import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { DeviceName } from 'src/domain/enum/domainEnum';

export class ReqLogin {
  @ApiProperty({ example: 'vmomonktemple1@gmail.com' })
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  email: string;

  @ApiProperty({ example: 'Aa12345678' })
  @IsString()
  @IsNotEmpty({ message: 'Please enter a password than 8 characters.' })
  @MinLength(8, { message: 'Please enter a password than 8 characters.' })
  @MaxLength(50, { message: 'Please enter a password less 50 characters.' })
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak! Please fix to continue.',
  })
  password: string;

  @ApiProperty({ example: 'IOS', enum: DeviceName })
  @IsString()
  @IsNotEmpty({ message: 'Please enter the correct device name format.' })
  @IsEnum(DeviceName, { message: 'Please enter the correct device name.' })
  deviceName: DeviceName;

  @ApiProperty({
    example:
      'cXmaHHCKK2I:APA91bFlvyxajWzp0g1pdiwidjcO4CaBYuuNAqkLlAxr6xjKGwxESYigru5d4yY1kXCPfLrOoi4Ju9U-FRBe6BWBuDikwo6z6FhFx-5Ds3Ba-_6UfzXv0fpRvuJ8s-MEFRVNRJMMe6Zx',
  })
  @IsString()
  @IsNotEmpty({ message: 'Please enter the correct device name format.' })
  deviceId: string;
}
