import { StatusSendNotify, TypeSendNotify } from 'src/domain/enum/domainEnum';

export class ResGetNotify {
  public id: number;
  public createdDate: Date;
  public updatedDate: Date;
  public title: string;
  public body: string;
  public message: string;
  public type: string;
  public status: string;
  public dankaName?: string;
  public houseHolderName?: string;
  public passedPersonName?: string;
  public eventName: string;

  constructor(
    itemNotify: any,
    enventName?: string,
    dankaName?: string,
    houseHolderName?: string,
    passedPersonName?: string,
  ) {
    this.id = itemNotify.Id;
    this.createdDate = itemNotify.CreatedDate;
    this.updatedDate = itemNotify.UpdatedDate;
    this.title = itemNotify.Title;
    this.body = itemNotify.Body;
    this.message = itemNotify.Msg;
    this.type =
      itemNotify.Type == TypeSendNotify.AdditionalSchedule
        ? 'AdditionalSchedule'
        : 'Other';
    this.status =
      itemNotify.Status == StatusSendNotify.Sent
        ? 'Sent'
        : itemNotify.Status == StatusSendNotify.Watched
        ? 'Watched'
        : 'Deleted';
    dankaName ? (this.dankaName = dankaName) : null;
    houseHolderName ? (this.houseHolderName = houseHolderName) : null;
    passedPersonName ? (this.passedPersonName = passedPersonName) : null;
    enventName ? (this.eventName = enventName) : null;

    return this;
  }
}
