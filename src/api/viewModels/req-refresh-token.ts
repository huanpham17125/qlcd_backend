import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { Constants } from 'src/application/common/constants';

export class ReqRefreshToken {
  @IsString()
  @IsNotEmpty({ message: Constants.AppResource.ACCESSTOKEN_EMPTY })
  @ApiProperty({ example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9....' })
  accessToken: string;
}
