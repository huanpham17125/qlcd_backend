import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsArray, IsEmail, IsString } from 'class-validator';

export class ReqCreateRoomChat {
  @IsEmail({}, { message: 'Please enter the correct email format.' })
  @ApiProperty({ example: 'monthTempleName@gmail.com' })
  public email: string;
}
