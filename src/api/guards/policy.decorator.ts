import { SetMetadata } from '@nestjs/common';

export const Policies = (...roleNames: string[]) => SetMetadata('policies', roleNames);
