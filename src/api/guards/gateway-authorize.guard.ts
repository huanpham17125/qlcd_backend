import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { TokenService } from 'src/infrastructure/services/token.service';

@Injectable()
export class GatewayAuthorizeGuard implements CanActivate {
  constructor(private tokenService: TokenService) {}
  async canActivate(context: any): Promise<boolean> {
    const bearerToken = context.args[0].handshake.headers.authorization.split(
      ' ',
    )[1];
    try {
      const claimToken = await this.tokenService.verifyToken(bearerToken);

      if (!claimToken) {
        return false;
      }

      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }
}
