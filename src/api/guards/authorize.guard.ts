import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/application/common/constants';
import { UnAuthorizeException } from 'src/application/common/exceptions/un-authorize-exception';
import { TokenService } from 'src/infrastructure/services/token.service';

@Injectable()
export class AuthorizeGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private tokenService: TokenService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const tokenHeader = req.headers.authorization;

    if (!tokenHeader || tokenHeader == '') {
      throw new UnAuthorizeException(Constants.AppResource.NO_AUTHORIZE);
    }

    const bearerToken = tokenHeader.replace('Bearer ', '');
    const claimToken = await this.tokenService.verifyToken(bearerToken);
    
    if (!claimToken) {
      throw new UnAuthorizeException(Constants.AppResource.TOKEN_EXPIRED);
    }

    const roleNames = this.reflector.get<string[]>(
      'policies',
      context.getHandler(),
    );

    if (
      (!roleNames || roleNames.length > 0) &&
      !roleNames.includes(claimToken.roleName)
    ) {
      throw new UnAuthorizeException(Constants.AppResource.UNAUTHORIZE);
    }

    req.claims = claimToken;

    return true;
  }
}
