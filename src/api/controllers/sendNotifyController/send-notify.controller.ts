import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Policies } from 'src/api/guards/policy.decorator';
import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';
import { ReqUpdateNotifyMessageById } from 'src/api/viewModels/req-update-notify-message-by-id';
import { ResApi } from 'src/api/viewModels/res-api';
import { Constants } from 'src/application/common/constants';
import { SendNotifyService } from 'src/infrastructure/services/send-notify.service';
import { BaseController } from '../base.controller';

@ApiTags('Notify')
@Controller('Notify')
@ApiBearerAuth('access-token')
export class SendNotifyController extends BaseController {
  constructor(private readonly sendNotifyService: SendNotifyService) {
    super();
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 422, description: 'Error validation' })
  @ApiOperation({ summary: 'Create notify' })
  @Post('CreateNewNotify')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async PushNotify(@Req() req, @Body() body: ReqCreateSendnotifyMessage) {
    const item = await this.sendNotifyService.sendNotifyToFirebase(
      req.claims.id,
      body,
    );

    return this.Res(
      new ResApi().Success(201, item, Constants.AppResource.ACCTION_SUCCESS),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 422, description: 'Error validation' })
  @ApiOperation({ summary: 'Get Notify By AccountId' })
  @Get('GetNotify')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async GetNotify(@Req() req) {
    const lst = await this.sendNotifyService.getActiveNotifyMessage(
      req.claims.id,
    );
    
    return this.Res(
      new ResApi().Success(200, lst, Constants.AppResource.ACCTION_SUCCESS),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 422, description: 'Error validation' })
  @ApiOperation({ summary: 'Update notify by id' })
  @Put('UpdateNotifyById')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async UpdateNotifyById(@Req() req, @Body() body: ReqUpdateNotifyMessageById) {
    const resUpdate = await this.sendNotifyService.updateNotifyMessageById(
      body.id,
      req.claims.id,
      body.status,
    );

    return this.Res(
      new ResApi().Success(
        200,
        resUpdate,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }
}
