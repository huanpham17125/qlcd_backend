import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Constants } from 'src/application/common/constants';
import { CreateNotifyMessageHandler } from 'src/application/sendNotifiMessage/commands/create/create-notify-message-handler';
import { DeleteNotifyMessageByIdHandler } from 'src/application/sendNotifiMessage/commands/delete/delete-notify-message-by-id-handler';
import { UpdateNotifyMessageByIdHandler } from 'src/application/sendNotifiMessage/commands/update/update-notify-message-by-id-handler';
import { GetNotifyMessageByAccountIdHandler } from 'src/application/sendNotifiMessage/queries/get-notify-message-by-account-id-handler';
import AccountRole from 'src/domain/entities/account-role.entity';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import Account from 'src/domain/entities/account.entity';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { AccountSettingRepository } from 'src/infrastructure/repositories/account-setting-repository';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { SendNotifyMessageLogRepository } from 'src/infrastructure/repositories/send-notify-message-log-repository';
import { AccountService } from 'src/infrastructure/services/account.service';
import { FireStoreService } from 'src/infrastructure/services/fire-store.service';
import { SendNotifyService } from 'src/infrastructure/services/send-notify.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { SendNotifyController } from './send-notify.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Account,
      AccountSetting,
      AccountRole,
      SendNotifyLog,
    ]),
    CqrsModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [SendNotifyController],
  providers: [
    {
      provide: Constants.AppProvide.ACCOUNT_PROVIDE,
      useClass: AccountAsyncRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_SETTING_PROVIDE,
      useClass: AccountSettingRepository,
    },
    {
      provide: Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE,
      useClass: SendNotifyMessageLogRepository,
    },
    AuthorizeGuard,
    AccountService,
    TokenService,
    FireStoreService,
    SendNotifyService,
    CreateNotifyMessageHandler,
    GetNotifyMessageByAccountIdHandler,
    DeleteNotifyMessageByIdHandler,
    UpdateNotifyMessageByIdHandler,
  ]
})
export class SendNotifyModule {}
