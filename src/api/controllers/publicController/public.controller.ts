import { Get, Header, HttpCode, Param, Req, Res } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { createReadStream } from 'fs';
import { Constants } from 'src/application/common/constants';
import { PublicService } from 'src/infrastructure/services/public.service';
import * as fs from 'fs';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { QueryBus } from '@nestjs/cqrs';

@Controller('Public')
@ApiTags('Public')
export class PublicController {
  constructor(
    private readonly publicService: PublicService,
    private readonly queryBus: QueryBus,
  ) {}

  @ApiOperation({ summary: 'get avatar' })
  @HttpCode(Constants.AppStatus.StatusCode200)
  @Header('Content-Type', 'image/jpg')
  @Get('Avt/:id')
  async uploadAvatar(@Param('id') param: string, @Res() res) {
    let urlImg = `${process.env.RES_AVATAR + param + '.jpg'}`;

    if (!fs.existsSync(urlImg)) {
      urlImg = `${process.env.RES_AVATAR + process.env.DEFAULT_AVATAR}`;
    }
    fs.readFile(urlImg, (err, data) => {
      if (err) {
        throw new BadRequestException(Constants.AppResource.EXCEPTION_UNKNOWN);
      }

      res.end(data);
    });
  }

  @ApiOperation({ summary: 'get logo' })
  @HttpCode(Constants.AppStatus.StatusCode200)
  @Header('Content-Type', 'image/jpg')
  @Get('logo')
  async getLogo(@Res() res) {
    const urlImg = `${process.env.RES_LOGO + 'logo.png'}`;
   
    fs.readFile(urlImg, (err, data) => {
      res.end(data);
    });
  }

  @ApiOperation({ summary: 'get config' })
  @HttpCode(Constants.AppStatus.StatusCode200)
  @Get('GetConfig')
  async getConfig(@Res() res) {

    return res.send({
      resource: process.env.APP_DOMAIN,
      webUrl: '',
      androidUrl: '',
      iosUrl: '',
      appVersion: {
        android: '1.1.10',
        ios: '1.1.10',
        news: ['Please download the latest version of APP!'],
      },
    });
  }
}
