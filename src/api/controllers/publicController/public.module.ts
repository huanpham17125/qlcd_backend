import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PublicService } from 'src/infrastructure/services/public.service';
import { PublicController } from './public.controller';

@Module({
  imports: [TypeOrmModule.forFeature(), CqrsModule],
  controllers: [PublicController],
  providers: [PublicService],
})
export class PublicModule {}
