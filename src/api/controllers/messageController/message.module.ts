import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Constants } from 'src/application/common/constants';
import { GetMessagePaginationByRoomIdHandler } from 'src/application/messages/queries/get-message-pagination-by-room-id-handler';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import Account from 'src/domain/entities/account.entity';
import Message from 'src/domain/entities/message.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { MessageRepository } from 'src/infrastructure/repositories/message-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { AccountService } from 'src/infrastructure/services/account.service';
import { MessageService } from 'src/infrastructure/services/message.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { MessageController } from './message.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Account,
      AccountSetting,
      RoomChat,
      AccountRoom,
      Message,
    ]),
    CqrsModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [MessageController],
  providers: [
    {
      provide: Constants.AppProvide.ACCOUNT_PROVIDE,
      useClass: AccountAsyncRepository,
    },
    {
      provide: Constants.AppProvide.ROOM_CHAT_PROVIDE,
      useClass: RoomChatRepository,
    },
    {
      provide: Constants.AppProvide.MESSAGE_PROVIDE,
      useClass: MessageRepository,
    },
    AuthorizeGuard,
    AccountService,
    TokenService,
    MessageService,
    GetMessagePaginationByRoomIdHandler,
  ],
})
export class MessageModule {}
