import { Controller, Get, Param, Req, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Policies } from 'src/api/guards/policy.decorator';
import { ReqGetMessageByRoomId } from 'src/api/viewModels/req-get-message-by-room-id';
import { ResApi } from 'src/api/viewModels/res-api';
import { Constants } from 'src/application/common/constants';
import { MessageService } from 'src/infrastructure/services/message.service';
import { BaseController } from '../base.controller';

@ApiTags('Message')
@Controller('Message')
@ApiBearerAuth('access-token')
export class MessageController extends BaseController {
  constructor(private readonly messageService: MessageService) {
    super();
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiOperation({ summary: 'get list message by roomId pagination' })
  @Get('/:pageSize/:pageNumber')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async GetMessage(@Req() req, @Param() parram: ReqGetMessageByRoomId ) {
    const lst = await this.messageService.getMessageByRoomId(
      req.claims.id,
      parram.pageSize ? parram.pageSize : 1,
      parram.pageNumber ? parram.pageNumber : 10,
    );

    return this.Res(
      new ResApi().Success(200, lst, Constants.AppResource.ACCTION_SUCCESS),
    );
  }
}
