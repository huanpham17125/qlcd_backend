import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Policies } from 'src/api/guards/policy.decorator';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
import { ResGetProfile } from 'src/api/viewModels/res-get-profile';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { AccountService } from 'src/infrastructure/services/account.service';
import { Constants } from '../../../application/common/constants';
import { ResApi } from '../../viewModels/res-api';
import { BaseController } from '../base.controller';
import { ApiUploadFile } from '../decorators/api-upload-file.decorator';
import { diskStorage } from 'multer';
import { ReqAccountLockOrUnlock } from 'src/api/viewModels/req-account-lock-or-unlock';
import { SendMailService } from 'src/infrastructure/services/send-mail.service';

@ApiTags('Account')
@Controller('Account')
@ApiBearerAuth('access-token')
export class AccountController extends BaseController {
  constructor(
    private readonly accountService: AccountService,
    private readonly queryBus: QueryBus,
    private readonly sendMailService: SendMailService,
  ) {
    super();
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiOperation({ summary: 'getAccount' })
  @Get('Profile')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async Get(@Req() req) {
    const itemAccount = await this.accountService.getDetailProfile(
      req.claims.id,
    );

    const accountSetting = null;
    const roleNormalizedName = itemAccount.AccountRoles[0].Role.NormalizedName;

    const resAccount = new ResGetProfile(
      itemAccount.Id,
      itemAccount.Email,
      itemAccount.FullName,
      roleNormalizedName,
      itemAccount.IsActive,
      roleNormalizedName == Constants.AppPolicy.ADMIN ? false : true,
      true,
    );

    return this.Res(
      new ResApi().Success(
        200,
        resAccount,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiOperation({ summary: 'getAccount' })
  @Get('GetById/:id')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async GetAccountById(@Req() req, @Param('id') param: string) {
    const itemAccount = await this.accountService.getDetailProfile(param);

    const accountSetting = null;
    const roleNormalizedName = itemAccount.AccountRoles[0].Role.NormalizedName;

    const resAccount = new ResGetProfile(
      itemAccount.Id,
      itemAccount.Email,
      itemAccount.FullName,
      roleNormalizedName,
      itemAccount.IsActive,
      roleNormalizedName == Constants.AppPolicy.USER ? false : true,
      true,
    );

    return this.Res(
      new ResApi().Success(
        200,
        resAccount,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiResponse({ status: 422, description: 'Request validation required' })
  @ApiOperation({ summary: 'updateAccount' })
  @Put('UpdateProfile')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async UpdateAccount(@Req() req, @Body() body: ReqUpdateAccount) {
    const resAccount = await this.accountService.updatelProfile(
      body,
      req.claims.id,
    );

    return this.Res(
      new ResApi().Success(
        200,
        resAccount,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiResponse({ status: 422, description: 'Request validation required' })
  @ApiOperation({ summary: 'update status Account' })
  @Put('UpdateStatus')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async LockOrUnlockAccount(@Req() req, @Body() body: ReqAccountLockOrUnlock) {
    const resUpdate = await this.accountService.updateStatusAccount(
      body.type,
      body.accountId,
      req.claims.id,
    );

    return this.Res(
      new ResApi().Success(
        200,
        resUpdate,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiResponse({ status: 422, description: 'Request validation required' })
  @ApiOperation({ summary: 'uploadAvatar' })
  @ApiConsumes('multipart/form-data')
  @ApiUploadFile('file')
  @Post('UploadAvatar')
  @UseGuards(AuthorizeGuard)
  @Policies()
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: (req, file, cb) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
          file.originalname = req.claims.id;
          file.filename = `${file.originalname}`;
          cb(null, true);
        } else {
          cb(
            new BadRequestException(Constants.AppResource.FILE_NOT_TYPE_IMAGE),
            false,
          );
        }
      },
      storage: diskStorage({
        destination: process.env.RES_AVATAR,
        filename: (req, file, cb) => {
          return cb(null, `${file.originalname + '.jpg'}`);
        },
      }),
    }),
  )
  async UploadAvatar(@UploadedFile() dataFile) {
    return this.Res(
      new ResApi().Success(
        200,
        dataFile,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }
}
