import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { CreateAccountRoleHandler } from 'src/application/accountRoles/commands/create/create-account-role-handler';
import { UpdateAccountByIdHandler } from 'src/application/accounts/commands/update/update-account-by-id-handler';
import { UpdateAccountSettingByAccountIdHandler } from 'src/application/accounts/commands/update/update-account-setting-by-account-id-handler';
import { GetAccountDataByEmailHandler } from 'src/application/accounts/queries/get-account-data-by-email-handler';
import { GetAccountProfileHandler } from 'src/application/accounts/queries/get-account-profile-handler';
import { Constants } from 'src/application/common/constants';
import { GetRoleByNameHandler } from 'src/application/roles/queries/get-role-by-name-handler';
import { CreateSendMailLogHandler } from 'src/application/sendMailLogs/commands/create/create-send-mail-log-handler';
import AccountRole from 'src/domain/entities/account-role.entity';
import AccountSetting from 'src/domain/entities/account-setting.entity'
import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import Role from 'src/domain/entities/role.entity';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
import { AccountSettingRepository } from 'src/infrastructure/repositories/account-setting-repository';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { SendEmailLogRepository } from 'src/infrastructure/repositories/send-email-log-repository';
import { RoleRepository } from 'src/infrastructure/repositories/role-repository';
import { AccountService } from 'src/infrastructure/services/account.service';
import { SendMailService } from 'src/infrastructure/services/send-mail.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import Account from '../../../domain/entities/account.entity';
import { AccountController } from './account.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Account,
      AccountSetting,
      SendMailLog,
      Role,
      AccountRole,
      SendNotifyLog,
    ]),
    CqrsModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        dest: config.get<string>('RES_AVATAR'),
      }),
      inject: [ConfigService],
    })
  ],
  controllers: [AccountController],
  providers: [
    {
      provide: Constants.AppProvide.ACCOUNT_PROVIDE,
      useClass: AccountAsyncRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_SETTING_PROVIDE,
      useClass: AccountSettingRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_ROLE,
      useClass: AccountRoleRepository,
    },
    {
      provide: Constants.AppProvide.ROLE_PROVIDE,
      useClass: RoleRepository,
    },
    {
      provide: Constants.AppProvide.SEND_MAIL_LOG_PROVIDE,
      useClass: SendEmailLogRepository,
    },
    AuthorizeGuard,
    GetAccountProfileHandler,
    UpdateAccountByIdHandler,
    CreateAccountRoleHandler,
    GetRoleByNameHandler,
    UpdateAccountSettingByAccountIdHandler,
    GetAccountDataByEmailHandler,
    CreateSendMailLogHandler,
    AccountService,
    TokenService,
    SendMailService,
  ],
})
export class AccountModule {}
