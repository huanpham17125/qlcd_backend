import { Controller } from '@nestjs/common';
import { ResApi } from '../viewModels/res-api';

@Controller()
export class BaseController {
  protected Res(res: ResApi) {
    return res;
  }
}
