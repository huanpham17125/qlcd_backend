import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { GetAccountDataByEmailHandler } from 'src/application/accounts/queries/get-account-data-by-email-handler';
import { Constants } from 'src/application/common/constants';
import { CreateRoomChatHandler } from 'src/application/roomChats/commands/create/create-room-chat-handler';
import { GetAllRoomByAccountIdHandler } from 'src/application/roomChats/queries/get-all-room-by-account-id-handler';
import { GetRoomByCreatorIdAndCreatedIdHandler } from 'src/application/roomChats/queries/get-room-by-creator-id-and-created-id-handler';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import Account from 'src/domain/entities/account.entity';
import Message from 'src/domain/entities/message.entity-gateway';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { MessageRepository } from 'src/infrastructure/repositories/message-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { AccountService } from 'src/infrastructure/services/account.service';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { RoomChatController } from './room-chat.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Account,
      AccountSetting,
      RoomChat,
      AccountRoom,
      Message,
    ]),
    CqrsModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [RoomChatController],
  providers: [
    {
      provide: Constants.AppProvide.ACCOUNT_PROVIDE,
      useClass: AccountAsyncRepository,
    },
    {
      provide: Constants.AppProvide.ROOM_CHAT_PROVIDE,
      useClass: RoomChatRepository,
    },
    {
      provide: Constants.AppProvide.MESSAGE_PROVIDE,
      useClass: MessageRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
      useClass: AccountRoomRepository,
    },
    AuthorizeGuard,
    AccountService,
    TokenService,
    RoomChatService,
    GetAllRoomByAccountIdHandler,
    GetAccountDataByEmailHandler,
    CreateRoomChatHandler,
    GetRoomByCreatorIdAndCreatedIdHandler,
  ],
})
export class RoomChatModule {}
