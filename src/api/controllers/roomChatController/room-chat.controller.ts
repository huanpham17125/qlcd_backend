import { Body, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Policies } from 'src/api/guards/policy.decorator';
import { ReqAddAccountToRoom } from 'src/api/viewModels/req-add-account-to-room';
import { ReqCreateRoomChat } from 'src/api/viewModels/req-create-room-chat';
import { ResApi } from 'src/api/viewModels/res-api';
import { Constants } from 'src/application/common/constants';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';
import { BaseController } from '../base.controller';

@ApiTags('RoomChat')
@Controller('Room')
@ApiBearerAuth('access-token')
export class RoomChatController extends BaseController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly roomService: RoomChatService,
  ) {
    super();
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiOperation({ summary: 'get list room chat pagination' })
  @Get()
  @UseGuards(AuthorizeGuard)
  @Policies()
  async GetRoomChat(@Req() req) {
    const lst = await this.roomService.fillAllByAccountId(req.claims.id);

    return this.Res(
      new ResApi().Success(200, lst, Constants.AppResource.ACCTION_SUCCESS),
    );
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 400, description: 'Account not found' })
  @ApiOperation({ summary: 'create room chat' })
  @Post('CreateRoomChat')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async CreateRoomChat(@Req() req, @Body() body: ReqCreateRoomChat) {
    const item = await this.roomService.createRoom(
      req.claims.id,
      body.email,
      '',
      req.claims.templeId,
    );

    return this.Res(
      new ResApi().Success(201, item, Constants.AppResource.ACCTION_SUCCESS),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 400, description: 'Error request.' })
  @ApiResponse({ status: 401, description: 'UnAuthorize' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiOperation({ summary: 'add more account to room chat' })
  @Post('AddAccountToRoom')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async AddAccountToRoomChat(@Req() req, @Body() body: ReqAddAccountToRoom) {
    const itemRoomChat = await this.roomService.addAccountToRoom(
      req.claims.id,
      body.roomId,
      body.lstEmail,
    );

    return this.Res(
      new ResApi().Success(
        201,
        itemRoomChat,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }
}
