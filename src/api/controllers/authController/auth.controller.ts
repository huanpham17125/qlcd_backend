import { Body, Controller, Post, Put, Req, UseGuards } from '@nestjs/common';
import { BaseController } from '../base.controller';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ResApi } from '../../viewModels/res-api';
import { Constants } from '../../../application/common/constants';
import { ReqRegister } from '../../viewModels/req-register';
import { AccountService } from 'src/infrastructure/services/account.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { ResLogin } from 'src/api/viewModels/res-login';
import { ReqLogin } from 'src/api/viewModels/req-login';
import { TypeToken } from 'src/domain/enum/domainEnum';
import { AccountSecurityTokenService } from 'src/infrastructure/services/account-security-token.service';
import { ReqChangePassword } from 'src/api/viewModels/req-change-password';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { Policies } from 'src/api/guards/policy.decorator';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { BadRequestException } from 'src/application/common/exceptions/bad-request-exception';
import { CommandBus } from '@nestjs/cqrs';
import { CreateAccountLogCommand } from 'src/application/accountLogs/commands/create/create-account-log-command';
import { ReqRegisterUser } from 'src/api/viewModels/req-register-user';
import { SendMailService } from 'src/infrastructure/services/send-mail.service';
import { ReqForgotPassword } from 'src/api/viewModels/req-forgot-password';
import { ReqResetPassword } from 'src/api/viewModels/req-reset-password';
import { MessageService } from 'src/infrastructure/services/message.service';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';

@ApiTags('Auth')
@Controller('Auth')
export class AuthController extends BaseController {
  constructor(
    private readonly accountService: AccountService,
    private readonly tokenService: TokenService,

    private readonly commandBus: CommandBus,
    private readonly passwordService: PasswordService,
    private readonly accountSecurityTokenService: AccountSecurityTokenService,
    private readonly sendMailService: SendMailService,
    private readonly messageService: MessageService,
    private readonly roomChatService: RoomChatService,
  ) {
    super();
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'Data already exist' })
  @ApiOperation({ summary: 'Register' })
  @Post('Register')
  @UseGuards(AuthorizeGuard)
  @Policies()
  async Register(@Req() req, @Body() reqRegister: ReqRegister) {
    const newAccount = await this.accountService.createAccount(
      reqRegister,
      req.claims.id,
    );

    const accessToken = await this.tokenService.generateAccessToken(newAccount);

    const resLogin = new ResLogin(
      accessToken.accessToken,
      accessToken.expiresIn,
      accessToken.expiresDate,
      '',
    );

    return this.Res(
      new ResApi().Success(
        Constants.AppStatus.StatusCode201,
        resLogin,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'data not found' })
  @ApiOperation({ summary: 'Login' })
  @Post('Login')
  async Login(@Body() reqlogin: ReqLogin) {
    const account = await this.accountService.getAccountByEmailAndPassword(
      reqlogin.email,
      reqlogin.password,
    );

    await this.commandBus.execute(new CreateAccountLogCommand(account.Id));

    const accessToken = await this.tokenService.generateAccessToken(account);

    const accountToken = await this.accountSecurityTokenService.getByAccountId(
      account.Id,
    );

    if (!accountToken) {
      await this.accountSecurityTokenService.create(
        account.Id,
        TypeToken.Access_Token,
        accessToken.accessToken,
        accessToken.expiresDate,
      );
    } else {
      await this.accountSecurityTokenService.update(
        accessToken.accessToken,
        accessToken.expiresDate,
        accountToken.Id,
      );
    }

    const resLogin = new ResLogin(
      accessToken.accessToken,
      accessToken.expiresIn,
      accessToken.expiresDate,
      '',
    );

    return this.Res(
      new ResApi().Success(
        Constants.AppStatus.StatusCode200,
        resLogin,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 201, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'Data already exist' })
  @ApiOperation({ summary: 'register user' })
  @Post('Register/User')
  async RegisterUser(@Body() reqBody: ReqRegisterUser) {
    const resAccount = await this.accountService.adminCreateAccount(reqBody);

    const account = await this.accountService.getAccountByEmail(
      resAccount.Email,
    );

    const accessToken = await this.tokenService.generateAccessToken(account);

    const resToken = new ResLogin(
      accessToken.accessToken,
      accessToken.expiresIn,
      accessToken.expiresDate,
      '',
    );

    return this.Res(
      new ResApi().Success(
        Constants.AppStatus.StatusCode201,
        resToken,
        Constants.AppResource.ACCTION_SUCCESS,
      ),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'User not found' })
  @ApiOperation({ summary: 'ChangePassword' })
  @ApiBearerAuth('access-token')
  @UseGuards(AuthorizeGuard)
  @Policies()
  @Put('ChangePassword')
  async ChangePassword(@Req() req, @Body() body: ReqChangePassword) {
    if (body.passwordConfirm != body.passwordNew) {
      throw new BadRequestException(Constants.AppResource.PASSWORD_DISPARITY);
    }

    const account = await this.accountService.getAccountById(req.claims.id);
    const verifyPassword = await this.passwordService.comparePassword(
      body.password,
      account,
    );

    if (!verifyPassword) {
      throw new BadRequestException(Constants.AppResource.PASSWORD_INCORECT);
    }

    const newPasswordHash = await this.passwordService.getPasswordHash(
      body.passwordNew,
      account,
    );

    await this.accountService.changePassword(
      newPasswordHash.PasswordHash,
      account.Id,
    );

    return this.Res(
      new ResApi().Success(200, null, Constants.AppResource.ACCTION_SUCCESS),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'User not found' })
  @ApiOperation({ summary: 'Forgot password' })
  @Post('ForgotPassword')
  async ForgotPassword(@Body() body: ReqForgotPassword) {
    const account = await this.accountService.getAccountByEmail(body.email);

    this.sendMailService.sendMailForgotPassword(
      account.Id,
      account.Email,
      Constants.AppSubjectSendEmail.RESET_PASSWORD,
    );

    return this.Res(
      new ResApi().Success(
        200,
        Constants.AppResource.LOG_IN_EMAIL_TO_RESET_PASSWORD,
        Constants.AppResource.LOG_IN_EMAIL_TO_RESET_PASSWORD,
      ),
    );
  }

  @ApiResponse({ status: 200, description: 'Success.' })
  @ApiResponse({ status: 422, description: 'Validation' })
  @ApiResponse({ status: 400, description: 'User not found' })
  @ApiOperation({ summary: 'Reset password' })
  @Put('ResetPassword')
  async ResetPassword(@Body() body: ReqResetPassword) {
    const account = await this.accountService.getAccountByEmail(body.email);

    if (body.passwordConfirm != body.passwordNew) {
      throw new BadRequestException(Constants.AppResource.PASSWORD_DISPARITY);
    }

    const newPasswordHash = await this.passwordService.getPasswordHash(
      body.passwordNew,
      account,
    );

    await this.accountService.changePassword(
      newPasswordHash.PasswordHash,
      account.Id,
    );

    return this.Res(
      new ResApi().Success(
        200,
        Constants.AppResource.RESET_PASSWORD_SUCCESS,
        Constants.AppResource.RESET_PASSWORD_SUCCESS,
      ),
    );
  }
}
