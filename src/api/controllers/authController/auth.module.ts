import { CreateAccountHandler } from './../../../application/accounts/commands/create/create-account-handler';
import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AccountAsyncRepository } from 'src/infrastructure/repositories/accountAsyncRepository';
import { CqrsModule } from '@nestjs/cqrs';
import { AccountService } from 'src/infrastructure/services/account.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { JwtModule } from '@nestjs/jwt';
import { GetAccountByEmailHandler } from 'src/application/accounts/queries/get-account-by-email-handler';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import Account from 'src/domain/entities/account.entity';
import Role from 'src/domain/entities/role.entity';
import AccountRole from 'src/domain/entities/account-role.entity';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { CreateAccountSecurityTokenHandler } from 'src/application/accountSecurityTokens/commands/create/create-account-security-token-handler';
import { AccountSecurityTokenReposity } from 'src/infrastructure/repositories/account-security-token-reposity';
import { AccountSecurityTokenService } from 'src/infrastructure/services/account-security-token.service';
import { GetByAccountIdHandler } from 'src/application/accountSecurityTokens/queries/get-by-account-id-handler';
import { UpdateAccountSecurityTokenHandler } from 'src/application/accountSecurityTokens/commands/update/update-account-security-token-handler';
import AccountSetting from 'src/domain/entities/account-setting.entity';
import { UpdatePasswordByIdHandler } from 'src/application/accounts/commands/update/update-password-by-id-handler';
import { GetAccountByIdHandler } from 'src/application/accounts/queries/get-account-by-id-handler';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Constants } from 'src/application/common/constants';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
import { RoleRepository } from 'src/infrastructure/repositories/role-repository';
import { CreateAccountRoleHandler } from 'src/application/accountRoles/commands/create/create-account-role-handler';
import { GetRoleByNameHandler } from 'src/application/roles/queries/get-role-by-name-handler';
import { AccountLogRepository } from 'src/infrastructure/repositories/account-log-repository';
import { CreateAccountLogHandler } from 'src/application/accountLogs/commands/create/create-account-log-handler';
import { UpdateStatusAccountHandler } from 'src/application/accounts/commands/update/update-status-account-handler';
import AccountLog from 'src/domain/entities/account-log.entity';
import { FindAccountByEmailHandler } from 'src/application/accounts/queries/find-account-by-email-handler';
import { AuthorizeGuard } from 'src/api/guards/authorize.guard';
import { SendMailService } from 'src/infrastructure/services/send-mail.service';
import { DetailAccountByEmailHandler } from 'src/application/accounts/queries/detail-account-by-email-handler';
import { RegisterTempHandler } from 'src/application/accounts/commands/create/register-temp-handler';
import { SendEmailLogRepository } from 'src/infrastructure/repositories/send-email-log-repository';
import { CreateSendMailLogHandler } from 'src/application/sendMailLogs/commands/create/create-send-mail-log-handler';
import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import { MessageService } from 'src/infrastructure/services/message.service';
import { GetRoomByAccountIdHandler } from 'src/application/roomChats/queries/get-room-by-account-id-handler';
import { CreateRoomChatHandler } from 'src/application/roomChats/commands/create/create-room-chat-handler';
import { CreateAccountRoomHandler } from 'src/application/accountRooms/commands/create/create-account-room-handler';
import { GetRoomByTempleIdHandler } from 'src/application/roomChats/queries/get-room-by-temple-id-handler';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([
      Account,
      AccountLog,
      Role,
      AccountRole,
      AccountSecurityToken,
      AccountSetting,
      SendMailLog,
      RoomChat,
      AccountRoom,
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        signOptions: {
          expiresIn: config.get<number>('JWT_EXPIRATION_ACCESS'),
        },
        secret: config.get<string>('JWT_SECRETKEY'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [
    {
      provide: 'ACCOUNTS',
      useClass: AccountAsyncRepository,
    },
    {
      provide: 'ACCOUNSECURITYTOKENS',
      useClass: AccountSecurityTokenReposity,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_ROLE,
      useClass: AccountRoleRepository,
    },
    {
      provide: Constants.AppProvide.ROLE_PROVIDE,
      useClass: RoleRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_LOG_PROVIDE,
      useClass: AccountLogRepository,
    },
    {
      provide: Constants.AppProvide.SEND_MAIL_LOG_PROVIDE,
      useClass: SendEmailLogRepository,
    },
    {
      provide: Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
      useClass: AccountRoomRepository,
    },
    {
      provide: Constants.AppProvide.ROOM_CHAT_PROVIDE,
      useClass: RoomChatRepository,
    },
    AuthorizeGuard,
    AccountService,
    PasswordService,
    AccountSecurityTokenService,
    TokenService,
    SendMailService,
    MessageService,
    RoomChatService,
    CreateAccountHandler,
    GetAccountByEmailHandler,
    CreateAccountSecurityTokenHandler,
    GetByAccountIdHandler,
    UpdateAccountSecurityTokenHandler,
    UpdatePasswordByIdHandler,
    GetAccountByIdHandler,
    CreateAccountRoleHandler,
    GetRoleByNameHandler,
    CreateAccountLogHandler,
    UpdateStatusAccountHandler,
    FindAccountByEmailHandler,
    GetRoleByNameHandler,
    DetailAccountByEmailHandler, 
    RegisterTempHandler,
    CreateSendMailLogHandler,
    GetRoomByAccountIdHandler,
    CreateRoomChatHandler,
    CreateAccountRoomHandler,
    GetRoomByTempleIdHandler,
  ],
})
export class AuthModule {}
