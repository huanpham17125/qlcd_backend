import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { type } from 'os';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UnAuthorizeException } from 'src/application/common/exceptions/un-authorize-exception';
import { AccountService } from 'src/infrastructure/services/account.service';

type ClaimsToken = {
  id: string;
  email: string;
  fullName: string;
  roleId: string;
  groupId: string;
}
@Injectable()
export class AuthenticationStrategyExtension extends PassportStrategy(
  Strategy,
) {
  constructor(private readonly accountService: AccountService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'PDv7DrqznYL6nv7DrqzjnQYO9JxIsWdcjnQYL6nu0f',
      passReqToCallback: true,
    });
  }

  async validate(claims: ClaimsToken): Promise<ClaimsToken | null> {
    const account = await this.accountService.getAccountById(claims.id);
    if (!account) {
      throw new UnAuthorizeException('Token invalid or expired');
    }

    return claims;
  }
}
