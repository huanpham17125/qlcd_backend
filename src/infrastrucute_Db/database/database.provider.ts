import { ConfigService } from '@nestjs/config';
import { createConnection } from 'typeorm';
import { DB_CONNECTION } from './index';

export const DatabaseProvider = [
  {
    provide: DB_CONNECTION,
    useFactory: async (config: ConfigService) =>
      await createConnection({
        type: 'mysql',
        host: process.env.DB_HOST || 'localhost',
        port: Number(process.env.DB_PORT),
        database: process.env.DB_NAME,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD || '',
      }),
    inject: [ConfigService],
  },
];
