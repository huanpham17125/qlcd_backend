import { v4 as uuid } from 'uuid';

export const RoleSeeds = [
  {
    Id: '8e2b206e-449b-4321-956e-3d01619d6da6',
    Name: 'Admin Temple',
    NormalizedName: 'ADMIN_TEMPLE',
    Des: '',
    IsActive: true,
    CreatedDate: new Date(),
    UpdatedDate: new Date(),
    CreatedById: '196483EE-73F7-EA11-9A2D-887873E284BF',
    UpdatedById: '196483EE-73F7-EA11-9A2D-887873E284BF',
  },
  {
    Id: '198ff1e2-6720-479c-abc6-11ad93c5e3fd',
    Name: 'Monk Temple',
    NormalizedName: 'MONK_TEMPLE',
    Des: '',
    IsActive: true,
    CreatedDate: new Date(),
    UpdatedDate: new Date(),
    CreatedById: '196483EE-73F7-EA11-9A2D-887873E284BF',
    UpdatedById: '196483EE-73F7-EA11-9A2D-887873E284BF',
  },
];
