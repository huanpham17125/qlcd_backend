import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationError, ValidationPipe } from '@nestjs/common';
import { ValidationException } from './application/common/exceptions/validation-exception';
import { GlobalExceptionFilterAttribute } from './api/filters/global-exception-filter-attribute';

async function bootstrap() {
  const app = await NestFactory.create(AppModule.forRoot());
  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (validationErrors: ValidationError[]) => {
        return new ValidationException(validationErrors);
      },
    }),
  );
  app.useGlobalFilters(new GlobalExceptionFilterAttribute());

  const logger = new Logger('QLCD API');
  const swaggerOption = new DocumentBuilder()
    .setTitle('QLCD System API')
    .setVersion('1.0')
    .setDescription('APIs are built for QLCD system')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        in: 'header',
        description:
          "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter your token in the text input below.\r\n\r\nExample: 'yJhbG...'",
      },
      'access-token',
    )
    .build();

  const registerSwagger = SwaggerModule.createDocument(app, swaggerOption);
  SwaggerModule.setup('/swagger', app, registerSwagger);

  const port = process.env.PORT || 3000;

  await app
    .listen(port, () => {
      logger.log('Application is listening at http://localhost:3000/swagger.');
    })
    .catch((err) => {
      logger.log(err);
    });
}
bootstrap();
