import { Module } from '@nestjs/common';
import { AuthModule } from './api/controllers/authController/auth.module';
import { RouterModule, Routes } from 'nest-router';
import { AccountModule } from './api/controllers/accountController/account.module';

const app_Route: Routes = [
  {
    path: '/api',
    children: [
      {
        path: '',
        module: AuthModule,
      },
      {
        path: '',
        module: AccountModule,
      },
    ],
  },
];
@Module({
  imports: [RouterModule.forRoutes(app_Route)]
})
export class AppRoutingModule {}
