import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
import Role from './role.entity';

@Entity({ name: 'AccountRoles' })
export default class AccountRole extends BaseEntity {
  @Column({ name: 'AccountId'})
  public AccountId: string;

  @ManyToOne(() => Account, (account) => account.AccountRoles)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;

  @Column({ name: 'RoleId'})
  public RoleId: string;
  @ManyToOne(() => Role, (role) => role.AccountRoles)
  @JoinColumn([{ name: 'RoleId', referencedColumnName: 'Id' }])
  public Role: Role;
}
