import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import Account from './account.entity';
import BaseEntity from './bases/baseEntity';

@Entity({ name: 'AccountSecurityTokens' })
export default class AccountSecurityToken extends BaseEntity {
  @Column({ name: 'AccountId' })
  public AccountId: string;

  @ManyToOne(() => Account, (account) => account.AccountSecurityTokens)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;

  @Column({ type: 'int' })
  public TokenType: number;

  @Column({ length: 3072 })
  public Value: string;

  @Column({ type: 'datetime' })
  public ExpiryDate: Date;

  @Column({ length: 255 })
  public RemoteIpAddress: string;
}
