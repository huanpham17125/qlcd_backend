import { Column, Entity, OneToMany } from 'typeorm';
import AccountRole from './account-role.entity';
import AbstractBaseEntity from './bases/abstractBaseEntity';

@Entity({ name: 'Roles' })
export default class Role extends AbstractBaseEntity {
  @Column({ length: 255 })
  public Name: string;

  @Column({ length: 255 })
  public NormalizedName: string;

  @Column({ length: 512 })
  public Des: string;

  @OneToMany(() => AccountRole, (AccountRole) => AccountRole.Role)
  public AccountRoles: AccountRole[];
}
