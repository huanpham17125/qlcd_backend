import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { StatusSendEmail } from '../enum/domainEnum';
import Account from './account.entity';
import BaseEntityInt from './bases/baseEntityInt';

@Entity({ name: 'SendMailLogs' })
export default class SendMailLog extends BaseEntityInt {
  @Column({
    type: 'varchar',
    charset: 'utf8',
    length: 128,
    default: 'no-reply@test.jp',
  })
  public MailSender: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 128 })
  public MailTo: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 256, default: '' })
  public MailCc: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 256, default: '' })
  public MailBcc: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 512, default: '' })
  public MailSubject: string;

  @Column({ type: 'text' })
  public MailBody: string;

  @Column({ type: 'int',  })
  public MailType: number;

  @Column({ type: 'varchar', charset: 'utf8', length: 3000, default: '' })
  public MailAttach: string;

  @Column({ type: 'int', default: StatusSendEmail.Sent })
  public SendStatus: StatusSendEmail;

  @Column({ name: 'AccountId' })
  public AccountId: string;

  @ManyToOne(() => Account, (account) => account.SendMailLog)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;
}
