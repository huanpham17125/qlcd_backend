import { ReqContentMessageSchedule } from 'src/application/messages/viewModels/req-content-message-schedule';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { StatusMessage, TypeContentMessage } from '../enum/domainEnum';
import Account from './account.entity';
import { BaseEntityBigInt } from './bases/baseEntityInt';
import RoomChat from './room-chat.entity-gateway';

@Entity({ name: 'Messages', database: 'gateways' })
export default class Message extends BaseEntityBigInt {
  @Column({ name: 'RoomId' })
  public RoomId: string;

  @ManyToOne(() => RoomChat, (room) => room.Message)
  @JoinColumn({ name: 'RoomId' })
  public RoomChat: RoomChat;

  @Column({ name: 'AccSenderId', type: 'varchar', length: 36 })
  public AccSenderId: string;
  @JoinColumn({ name: 'AccSenderId'})
  public Account?: Account;
  
  @Column({ name: 'ReplyId', type: 'int' })
  public ReplyId: number;

  @Column({ type: 'int', default: 1})
  public TypeContent: TypeContentMessage;

  @Column({ type: 'varchar', length: 3000, charset: 'utf8' })
  public Content: string;

  @Column({ type: 'int', default: 1 })
  public Status: StatusMessage;
}
