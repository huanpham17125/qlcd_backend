import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
import RoomChat from './room-chat.entity-gateway';

@Entity({ name: 'AccountRooms', database: 'gateways' })
export default class AccountRoom extends BaseEntity {
  @Column({ name: 'RoomId', default: 0 })
  public RoomId: string;
  @ManyToOne(() => RoomChat, (roomChat) => roomChat.AccountRoom)
  @JoinColumn({ name: 'RoomId' })
  public RoomChat: RoomChat;

  @Column({ name: 'AccountId', type: 'varchar', length: 36 })
  public AccountId: string;
  @JoinColumn({ name: 'AccountId' })
  public Account?: Account;

  @Column({ default: false })
  public IsPushNotify: boolean;
}
