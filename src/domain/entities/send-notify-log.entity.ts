import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { StatusSendNotify, TypeSendNotify } from '../enum/domainEnum';
import Account from './account.entity';
import { AbstractBaseEntityBigInt } from './bases/abstractBaseEntityInt';

@Entity({ name: 'SendNotifyLogs' })
export default class SendNotifyLog extends AbstractBaseEntityBigInt {
  @Column({ name: 'AccountId' })
  public AccountId: string;
  @ManyToOne(() => Account, (account) => account.SendNotifyLog)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;

  @Column({ type: 'varchar', charset: 'utf8', length: 150 })
  public Title: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 512 })
  public Body: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 1050 })
  public Msg: string;

  @Column({ type: 'int', default: TypeSendNotify.Schedule })
  public TypeNotify: TypeSendNotify;

  @Column({ type: 'int', default: StatusSendNotify.Sent })
  public Status: StatusSendNotify;
}
