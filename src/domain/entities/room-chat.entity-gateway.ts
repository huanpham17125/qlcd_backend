import { Column, Entity, JoinColumn, OneToMany } from 'typeorm';
import AccountRoom from './account-room.entity-gateway';
import AbstractBaseEntity from './bases/abstractBaseEntity';
import Message from './message.entity-gateway';

@Entity({ name: 'RoomChats', database: 'gateways' })
export default class RoomChat extends AbstractBaseEntity {
  @OneToMany(() => Message, (message) => message.RoomChat)
  public Message: Message[];

  @OneToMany(() => AccountRoom, (accountRoom) => accountRoom.RoomChat)
  public AccountRoom: AccountRoom[];

  @Column({ type: 'varchar', charset: 'utf8', length: 250 })
  public Name: string;

  @Column({ type: 'varchar', charset: 'utf8', length: 500 })
  public Tittle: string;
}
