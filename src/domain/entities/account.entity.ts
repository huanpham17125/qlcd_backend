import { Column, Entity, OneToMany, OneToOne } from 'typeorm';
import AccountLog from './account-log.entity';
import AccountRole from './account-role.entity';
import AccountSecurityToken from './account-security-token.entity';
import AccountSetting from './account-setting.entity';
import AbstractBaseEntity from './bases/abstractBaseEntity';
import SendMailLog from './send-mail-log.entity';
import SendNotifyLog from './send-notify-log.entity';
@Entity({ name: 'Accounts' })
export default class Account extends AbstractBaseEntity {
  @Column({ length: 255 })
  public Email: string;

  @Column({ length: 512 })
  public PasswordHash: string;

  @Column({ type: 'varchar', length: 20, default: '' })
  public Phone: string;

  @Column({ length: 255, charset: 'utf8', default: '' })
  public FullName: string;

  @Column({ type: 'datetime', default: '1900-01-01' })
  public Birthday: Date;

  @Column({ type: 'varchar', length: 10 })
  public Gender: string;

  @Column({ type: 'varchar', length: 50, default: '' })
  public ZipCode: string;

  @Column({ length: 255, charset: 'utf8', default: '' })
  public Province: string;

  @Column({ length: 255, charset: 'utf8', default: '' })
  public City: string;

  @Column({ length: 255, charset: 'utf8', default: '' })
  public Street: string;

  @Column({ type: 'boolean', default: true })
  public IsOnline: boolean;
  @OneToOne(() => AccountSetting, (accountSetting) => accountSetting.Account)
  public AccountSetting: AccountSetting;

  @OneToMany(() => AccountRole, (AccountRole) => AccountRole.Account)
  public AccountRoles: AccountRole[];
  @OneToMany(() => SendMailLog, (sendmail) => sendmail.Account)
  public SendMailLog: SendMailLog[];

  @OneToMany(
    () => AccountSecurityToken,
    (accountSecurityToken) => accountSecurityToken.Account,
  )
  public AccountSecurityTokens: AccountSecurityToken[];
  @OneToMany(() => AccountLog, (accountLog) => accountLog.Account)
  public AccountLog: AccountLog[];

  @OneToMany(() => SendNotifyLog, (SendNotifyLog) => SendNotifyLog.Account)
  public SendNotifyLog: SendNotifyLog[];
}
