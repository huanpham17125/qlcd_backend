import { Column } from 'typeorm';
import BaseEntityInt, { BaseEntityBigInt } from './baseEntityInt';

export default class AbstractBaseEntityInt extends BaseEntityInt {
  @Column({ type: 'char', length: 36 })
  public CreatedById: string;

  @Column({ type: 'char', length: 36 })
  public UpdatedById: string;
}

export class AbstractBaseEntityBigInt extends BaseEntityBigInt {
  @Column({ type: 'char', length: 36 })
  public CreatedById: string;

  @Column({ type: 'char', length: 36 })
  public UpdatedById: string;
}
