import { type } from 'os';
import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

export default class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public Id: string;

  @Column({ type: 'bool', default: true })
  public IsActive: boolean;

  @VersionColumn()
  Version: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public CreatedDate: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public UpdatedDate: Date;
}
