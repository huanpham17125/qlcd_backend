import { Column } from 'typeorm';
import BaseEntity from './baseEntity';

export default class AbstractBaseEntity extends BaseEntity {
  @Column({ type: 'char', length: 36 })
  public CreatedById: string;

  @Column({ type: 'char', length: 36 })
  public UpdatedById: string;
}
