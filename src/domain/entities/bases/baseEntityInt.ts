import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

export default class BaseEntityInt {
  @PrimaryGeneratedColumn({ type: 'int' })
  public Id: number;

  @Column({ type: 'bool', default: true })
  public IsActive: boolean;

  @VersionColumn()
  Version: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public CreatedDate: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public UpdatedDate: Date;
}

export class BaseEntityBigInt {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  public Id: number;

  @Column({ type: 'bool', default: true })
  public IsActive: boolean;

  @VersionColumn()
  Version: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public CreatedDate: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public UpdatedDate: Date;
}

