import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import Account from './account.entity';
import { BaseEntityBigInt } from './bases/baseEntityInt';

@Entity({ name: 'AccountLogs' })
export default class AccountLog extends BaseEntityBigInt {
  @Column({ name: 'AccountId' })
  public AccountId: string;
  @ManyToOne(() => Account, (account) => account.AccountLog)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;
}
