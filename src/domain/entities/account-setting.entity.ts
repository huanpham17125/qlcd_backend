import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import Account from './account.entity';
import BaseEntity from './bases/baseEntity';

@Entity({ name: 'AccountSettings' })
export default class AccountSetting extends BaseEntity {
  @Column({ name: 'AccountId' })
  public AccountId: string;

  @OneToOne(() => Account, (account) => account.AccountSetting)
  @JoinColumn({ name: 'AccountId' })
  public Account: Account;
}
