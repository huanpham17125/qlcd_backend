
export interface IAsyncGenericRepository<TEntity> {
  add(entity: TEntity): Promise<TEntity> | null;
  getById(id: string | number): Promise<TEntity> | null;
  getListActive(): Promise<TEntity[]> | [];
  updateById(entity: TEntity, id: any): Promise<boolean>;
  deleteById(id: string): Promise<boolean>;
}
