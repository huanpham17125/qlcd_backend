import { Pagination } from 'nestjs-typeorm-paginate';
import RoomChat from '../entities/room-chat.entity-gateway';

export interface IroomChatRepository {
  getAllByAccountId(
    accountId: string,
    pageSize: number,
    pageNumber: number,
  ): Promise<Pagination<RoomChat>>;

  getRoomByCreatorAndCreated(
    creatorId: string,
    createdId: string,
  ): Promise<any>;
  getByAccountId(accountId: string): Promise<RoomChat>;
  getByTempleId(templeId: string): Promise<RoomChat>;
}
