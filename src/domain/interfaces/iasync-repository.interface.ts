import { IAsyncGenericRepository } from './iasync-generic-repository.interface';

export interface IAsyncRepository<TEntity>
  extends IAsyncGenericRepository<TEntity> {
  getByAccountId(accountId: string): Promise<TEntity | null>;

  getListByAccountId(accountId: string): Promise<TEntity[] | []>;

  getByName(name: string): Promise<TEntity | null>;
}
