export enum TypeGender {
  MALE = 'male',
  FEMALE = 'female',
  NOPE = '',
}

export enum TypeToken {
  Access_Token = 1,
  Refresh_Token = 2,
}

export enum StatusMessage {
  Deleted = -1,
  Sent = 0,
  Received = 1,
  Updated = 2,
  Watched = 3,
}

export enum TypeContentMessage {
  Document = 1,
  //Images = 2,
  //File = 3,
  //Icon = 4,
  Schedule = 2,
}

export enum StatusSendNotify {
  Sent = 0,
  Delete = -1,
  Watched = 1,
}

export enum TypeSendNotify {
  Schedule = 1,
  AdditionalSchedule = 0,
  Document = 2,
  Message = 3,
}

export enum TypeSendMail {
  Register_Office = 1,
  Schedule = 0,
  Invite = 2,
  Message = 3,
  ForgotPassword = 4,
}

export enum StatusSendEmail {
  Watched = 1,
  Sent = 0,
  Deleted = -1,
}

export enum StatusInviteEmail {
  Sent = 0,
  Refuse = -1,
  Accepted = 1,
}

export enum DeviceName {
  WEB = 'WEB',
  IOS = 'IOS',
  ANDROID = 'ANDROID',
}