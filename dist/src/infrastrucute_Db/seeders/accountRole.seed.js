"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountRoleSeeds = void 0;
const uuid_1 = require("uuid");
exports.AccountRoleSeeds = [
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: '196483EE-73F7-EA11-9A2D-887873E284BF',
        RoleId: '8e2b206e-449b-4321-956e-3d01619d6da6',
    },
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: 'cc5e74ae-4050-4251-a96b-40dafc208e69',
        RoleId: '8e2b206e-449b-4321-956e-3d01619d6da6',
    },
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: '8E39E1EA-365A-4A27-A4A5-C6D6428FFDB9',
        RoleId: '8e2b206e-449b-4321-956e-3d01619d6da6',
    },
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: '0A6F6B55-2909-4469-B086-71E2600EEAC6',
        RoleId: '198ff1e2-6720-479c-abc6-11ad93c5e3fd',
    },
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: '8E39E1EA-365A-4A27-A4A5-C6D6428FFAE1',
        RoleId: '198ff1e2-6720-479c-abc6-11ad93c5e3fd',
    },
    {
        Id: uuid_1.v4(),
        IsActive: true,
        CreatedDate: new Date(),
        UpdatedDate: new Date(),
        AccountId: '8E39E1EA-365A-4A27-A4A5-C6D6428FFAE1',
        RoleId: '198ff1e2-6720-479c-abc6-11ad93c5e3fd',
    },
];
//# sourceMappingURL=accountRole.seed.js.map