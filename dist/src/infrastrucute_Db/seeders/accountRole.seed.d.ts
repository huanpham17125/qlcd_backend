export declare const AccountRoleSeeds: {
    Id: string;
    IsActive: boolean;
    CreatedDate: Date;
    UpdatedDate: Date;
    AccountId: string;
    RoleId: string;
}[];
