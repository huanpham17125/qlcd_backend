export declare const AccountSeeds: {
    Id: string;
    FullName: string;
    Email: string;
    PasswordHash: string;
    Phone: string;
    Gender: string;
    Birthday: Date;
    ZipCode: string;
    Province: string;
    City: string;
    Street: string;
    IsActive: boolean;
    CreatedDate: Date;
    UpdatedDate: Date;
    CreatedById: string;
    UpdatedById: string;
}[];
