export declare const RoleSeeds: {
    Id: string;
    Name: string;
    NormalizedName: string;
    Des: string;
    IsActive: boolean;
    CreatedDate: Date;
    UpdatedDate: Date;
    CreatedById: string;
    UpdatedById: string;
}[];
