"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseProvider = void 0;
const config_1 = require("@nestjs/config");
const typeorm_1 = require("typeorm");
const index_1 = require("./index");
exports.DatabaseProvider = [
    {
        provide: index_1.DB_CONNECTION,
        useFactory: async (config) => await typeorm_1.createConnection({
            type: 'mysql',
            host: process.env.DB_HOST || 'localhost',
            port: Number(process.env.DB_PORT),
            database: process.env.DB_NAME,
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD || '',
        }),
        inject: [config_1.ConfigService],
    },
];
//# sourceMappingURL=database.provider.js.map