"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTableGateway1617101833353 = void 0;
class createTableGateway1617101833353 {
    constructor() {
        this.name = 'createTableGateway1617101833353';
    }
    async up(queryRunner) {
        await queryRunner.query("CREATE TABLE `gateways`.`Messages` (`Id` bigint NOT NULL AUTO_INCREMENT, `IsActive` tinyint NOT NULL DEFAULT 1, `Version` int NOT NULL, `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `RoomId` varchar(255) NOT NULL, `AccSenderId` varchar(36) NOT NULL, `ReplyId` int NOT NULL, `TypeContent` int NOT NULL DEFAULT '1', `Content` varchar(3000) CHARACTER SET \"utf8\" NOT NULL, `Status` int NOT NULL DEFAULT '1', `DetailScheduleId` bigint NOT NULL DEFAULT '0', PRIMARY KEY (`Id`)) ENGINE=InnoDB");
        await queryRunner.query('CREATE TABLE `gateways`.`RoomChats` (`Id` varchar(36) NOT NULL, `IsActive` tinyint NOT NULL DEFAULT 1, `Version` int NOT NULL, `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `CreatedById` char(36) NOT NULL, `UpdatedById` char(36) NOT NULL, `TempleId` varchar(36) NOT NULL, `Name` varchar(250) CHARACTER SET "utf8" NOT NULL, `Tittle` varchar(500) CHARACTER SET "utf8" NOT NULL, PRIMARY KEY (`Id`)) ENGINE=InnoDB');
        await queryRunner.query("CREATE TABLE `gateways`.`AccountRooms` (`Id` varchar(36) NOT NULL, `IsActive` tinyint NOT NULL DEFAULT 1, `Version` int NOT NULL, `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `RoomId` varchar(255) NOT NULL DEFAULT '0', `AccountId` varchar(36) NOT NULL, `IsPushNotify` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`Id`)) ENGINE=InnoDB");
        await queryRunner.query('ALTER TABLE `gateways`.`Messages` ADD CONSTRAINT `FK_9eccb3067b6100294f0a6af34e5` FOREIGN KEY (`RoomId`) REFERENCES `gateways`.`RoomChats`(`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION');
        await queryRunner.query('ALTER TABLE `gateways`.`AccountRooms` ADD CONSTRAINT `FK_76f0bf4266035f37672e3e1418c` FOREIGN KEY (`RoomId`) REFERENCES `gateways`.`RoomChats`(`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION');
    }
    async down(queryRunner) {
        await queryRunner.query('ALTER TABLE `gateways`.`AccountRooms` DROP FOREIGN KEY `FK_76f0bf4266035f37672e3e1418c`');
        await queryRunner.query('ALTER TABLE `gateways`.`Messages` DROP FOREIGN KEY `FK_9eccb3067b6100294f0a6af34e5`');
        await queryRunner.query('DROP TABLE `gateways`.`AccountRooms`');
        await queryRunner.query('DROP TABLE `gateways`.`RoomChats`');
        await queryRunner.query('DROP TABLE `gateways`.`Messages`');
    }
}
exports.createTableGateway1617101833353 = createTableGateway1617101833353;
//# sourceMappingURL=1617101833353-create-table-gateway.js.map