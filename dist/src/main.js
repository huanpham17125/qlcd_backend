"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const validation_exception_1 = require("./application/common/exceptions/validation-exception");
const global_exception_filter_attribute_1 = require("./api/filters/global-exception-filter-attribute");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule.forRoot());
    app.enableCors();
    app.useGlobalPipes(new common_1.ValidationPipe({
        exceptionFactory: (validationErrors) => {
            return new validation_exception_1.ValidationException(validationErrors);
        },
    }));
    app.useGlobalFilters(new global_exception_filter_attribute_1.GlobalExceptionFilterAttribute());
    const logger = new common_1.Logger('QLCD API');
    const swaggerOption = new swagger_1.DocumentBuilder()
        .setTitle('QLCD System API')
        .setVersion('1.0')
        .setDescription('APIs are built for QLCD system')
        .addBearerAuth({
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        in: 'header',
        description: "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter your token in the text input below.\r\n\r\nExample: 'yJhbG...'",
    }, 'access-token')
        .build();
    const registerSwagger = swagger_1.SwaggerModule.createDocument(app, swaggerOption);
    swagger_1.SwaggerModule.setup('/swagger', app, registerSwagger);
    const port = process.env.PORT || 3000;
    await app
        .listen(port, () => {
        logger.log('Application is listening at http://localhost:3000/swagger.');
    })
        .catch((err) => {
        logger.log(err);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map