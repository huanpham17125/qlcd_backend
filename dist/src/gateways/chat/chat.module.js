"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const cqrs_1 = require("@nestjs/cqrs");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const gateway_authorize_guard_1 = require("../../api/guards/gateway-authorize.guard");
const update_is_online_by_id_handler_1 = require("../../application/accounts/commands/update/update-is-online-by-id-handler");
const constants_1 = require("../../application/common/constants");
const create_message_handler_1 = require("../../application/messages/commands/create/create-message-handler");
const update_content_message_by_id_handler_1 = require("../../application/messages/commands/update/update-content-message-by-id-handler");
const update_status_message_handler_1 = require("../../application/messages/commands/update/update-status-message-handler");
const get_room_by_account_id_handler_1 = require("../../application/roomChats/queries/get-room-by-account-id-handler");
const account_room_entity_gateway_1 = require("../../domain/entities/account-room.entity-gateway");
const account_setting_entity_1 = require("../../domain/entities/account-setting.entity");
const account_entity_1 = require("../../domain/entities/account.entity");
const message_entity_gateway_1 = require("../../domain/entities/message.entity-gateway");
const room_chat_entity_gateway_1 = require("../../domain/entities/room-chat.entity-gateway");
const account_room_repository_1 = require("../../infrastructure/repositories/account-room-repository");
const accountAsyncRepository_1 = require("../../infrastructure/repositories/accountAsyncRepository");
const message_repository_1 = require("../../infrastructure/repositories/message-repository");
const room_chat_repository_1 = require("../../infrastructure/repositories/room-chat-repository");
const account_service_1 = require("../../infrastructure/services/account.service");
const message_service_1 = require("../../infrastructure/services/message.service");
const room_chat_service_1 = require("../../infrastructure/services/room-chat.service");
const token_service_1 = require("../../infrastructure/services/token.service");
const chat_gateway_1 = require("./chat.gateway");
let ChatModule = class ChatModule {
};
ChatModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                account_entity_1.default,
                account_setting_entity_1.default,
                account_room_entity_gateway_1.default,
                message_entity_gateway_1.default,
                room_chat_entity_gateway_1.default,
            ]),
            cqrs_1.CqrsModule,
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    signOptions: {
                        expiresIn: config.get('JWT_EXPIRATION_ACCESS'),
                    },
                    secret: config.get('JWT_SECRETKEY'),
                }),
                inject: [config_1.ConfigService],
            }),
        ],
        providers: [
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_PROVIDE,
                useClass: accountAsyncRepository_1.AccountAsyncRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE,
                useClass: room_chat_repository_1.RoomChatRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
                useClass: account_room_repository_1.AccountRoomRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.MESSAGE_PROVIDE,
                useClass: message_repository_1.MessageRepository,
            },
            chat_gateway_1.ChatGateway,
            token_service_1.TokenService,
            message_service_1.MessageService,
            account_service_1.AccountService,
            room_chat_service_1.RoomChatService,
            gateway_authorize_guard_1.GatewayAuthorizeGuard,
            create_message_handler_1.CreateMessageHandler,
            update_status_message_handler_1.UpdateStatusMessageHandler,
            update_content_message_by_id_handler_1.UpdateContentMessageByIdHandler,
            update_is_online_by_id_handler_1.UpdateIsOnlineByIdHandler,
            get_room_by_account_id_handler_1.GetRoomByAccountIdHandler,
        ],
    })
], ChatModule);
exports.ChatModule = ChatModule;
//# sourceMappingURL=chat.module.js.map