"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatGateway = void 0;
const cqrs_1 = require("@nestjs/cqrs");
const websockets_1 = require("@nestjs/websockets");
const rxjs_1 = require("rxjs");
const constants_1 = require("../../application/common/constants");
const get_room_by_account_id_query_1 = require("../../application/roomChats/queries/get-room-by-account-id-query");
const account_service_1 = require("../../infrastructure/services/account.service");
const message_service_1 = require("../../infrastructure/services/message.service");
const token_service_1 = require("../../infrastructure/services/token.service");
const req_leave_room_1 = require("../viewModels/req-leave-room");
const req_send_message_1 = require("../viewModels/req-send-message");
let ChatGateway = class ChatGateway {
    constructor(jwtService, messageService, accountService, queryBus) {
        this.jwtService = jwtService;
        this.messageService = messageService;
        this.accountService = accountService;
        this.queryBus = queryBus;
        this.connectedUsers = [];
    }
    async handleConnection(socket) {
        const claims = await this.jwtService.verifyToken(socket.handshake.query.token);
        if (!claims) {
            socket.emit('error', 'un-authorized');
            socket.disconnect(true);
        }
        await this.accountService.updateIsOnlineByAccountId(claims.id, constants_1.Constants.IsOnlineAccount.Online);
        const itemRoom = await this.queryBus.execute(new get_room_by_account_id_query_1.GetRoomByAccountIdQuery(claims.id));
        if (!itemRoom) {
            socket.join(itemRoom.Id);
            socket.room = itemRoom.Id;
        }
        this.connectedUsers = [...this.connectedUsers, String(claims.id)];
        this.server.emit('connected', 'Welcome to the QLCD chat server.');
        this.server.emit('users', this.connectedUsers);
    }
    async handleDisconnect(socket) {
        const claims = await this.jwtService.verifyToken(socket.handshake.query.token);
        const userPos = this.connectedUsers.indexOf(String(claims._id));
        if (userPos > -1) {
            this.connectedUsers = [
                ...this.connectedUsers.slice(0, userPos),
                ...this.connectedUsers.slice(userPos + 1),
            ];
        }
        await this.accountService.updateIsOnlineByAccountId(claims.id, constants_1.Constants.IsOnlineAccount.Offline);
        this.server.emit('users', this.connectedUsers);
    }
    async onMessage(data, client) {
        const event = 'message';
        const claims = await this.jwtService.verifyToken(client.handshake.query.token);
        if (!claims) {
            client.emit('error', 'un-authorized');
            client.disconnect(true);
        }
        const resCreateMessage = await this.messageService.createMessage(claims.id, claims.templeId, data.content, data.scheduleId);
        const itemRoom = await this.queryBus.execute(new get_room_by_account_id_query_1.GetRoomByAccountIdQuery(claims.id));
        client.broadcast.to(itemRoom).emit(event, resCreateMessage);
        return new rxjs_1.Observable((observer) => {
            observer.next({ event, data: resCreateMessage });
        });
    }
    async onRoomJoin(data, client) {
        client.join(data[0]);
        client.emit('message', 'joined');
    }
    onRoomLeave(data, client) {
        client.leave(data[0]);
    }
};
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", Object)
], ChatGateway.prototype, "server", void 0);
__decorate([
    websockets_1.SubscribeMessage('message'),
    __param(0, websockets_1.MessageBody()),
    __param(1, websockets_1.ConnectedSocket()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_send_message_1.ReqSendMessage, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "onMessage", null);
__decorate([
    websockets_1.SubscribeMessage('join'),
    __param(0, websockets_1.MessageBody()),
    __param(1, websockets_1.ConnectedSocket()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_leave_room_1.ReqLeaveRoom, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "onRoomJoin", null);
__decorate([
    websockets_1.SubscribeMessage('leave'),
    __param(0, websockets_1.MessageBody()),
    __param(1, websockets_1.ConnectedSocket()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_leave_room_1.ReqLeaveRoom, Object]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "onRoomLeave", null);
ChatGateway = __decorate([
    websockets_1.WebSocketGateway(8088),
    __metadata("design:paramtypes", [token_service_1.TokenService,
        message_service_1.MessageService,
        account_service_1.AccountService,
        cqrs_1.QueryBus])
], ChatGateway);
exports.ChatGateway = ChatGateway;
//# sourceMappingURL=chat.gateway.js.map