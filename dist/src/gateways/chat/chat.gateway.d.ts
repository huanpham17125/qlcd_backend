import { QueryBus } from '@nestjs/cqrs';
import { OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { AccountService } from 'src/infrastructure/services/account.service';
import { MessageService } from 'src/infrastructure/services/message.service';
import { TokenService } from 'src/infrastructure/services/token.service';
import { ReqLeaveRoom } from '../viewModels/req-leave-room';
import { ReqSendMessage } from '../viewModels/req-send-message';
export declare class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
    private readonly jwtService;
    private readonly messageService;
    private readonly accountService;
    private readonly queryBus;
    server: any;
    connectedUsers: string[];
    constructor(jwtService: TokenService, messageService: MessageService, accountService: AccountService, queryBus: QueryBus);
    handleConnection(socket: any): Promise<void>;
    handleDisconnect(socket: any): Promise<void>;
    onMessage(data: ReqSendMessage, client: any): Promise<Observable<unknown>>;
    onRoomJoin(data: ReqLeaveRoom, client: any): Promise<any>;
    onRoomLeave(data: ReqLeaveRoom, client: any): void;
}
