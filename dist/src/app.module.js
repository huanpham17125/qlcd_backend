"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const auth_module_1 = require("./api/controllers/authController/auth.module");
const boostrap_module_1 = require("./boostrap.module");
const core_1 = require("@nestjs/core");
const global_exception_filter_attribute_1 = require("./api/filters/global-exception-filter-attribute");
const account_module_1 = require("./api/controllers/accountController/account.module");
const nest_automapper_1 = require("nest-automapper");
const account_mapping_1 = require("./application/models/mappings/account-mapping");
const config_1 = require("@nestjs/config");
const account_role_mapping_1 = require("./application/models/mappings/account-role-mapping");
const chat_module_1 = require("./gateways/chat/chat.module");
const room_chat_mapping_1 = require("./application/models/mappings/room-chat-mapping");
const message_module_1 = require("./api/controllers/messageController/message.module");
const message_mapping_1 = require("./application/models/mappings/message-mapping");
const public_module_1 = require("./api/controllers/publicController/public.module");
const serve_static_1 = require("@nestjs/serve-static");
const path_1 = require("path");
const send_notify_module_1 = require("./api/controllers/sendNotifyController/send-notify.module");
const send_notify_log_mapping_1 = require("./application/models/mappings/send-notify-log-mapping");
class AppModule {
    static forRoot() {
        return {
            module: this,
            imports: [
                boostrap_module_1.BoostrapModule,
                config_1.ConfigModule.forRoot(),
                serve_static_1.ServeStaticModule.forRoot({
                    rootPath: path_1.join(__dirname, '..', 'data'),
                }),
                nest_automapper_1.AutomapperModule.forRoot({
                    profiles: [
                        new account_mapping_1.default(),
                        new account_role_mapping_1.AccountRoleMapping(),
                        new room_chat_mapping_1.RoomChatMapping(),
                        new message_mapping_1.MessageMapping(),
                        new send_notify_log_mapping_1.SendNotifyLogMapping(),
                    ],
                }),
                auth_module_1.AuthModule,
                account_module_1.AccountModule,
                message_module_1.MessageModule,
                chat_module_1.ChatModule,
                public_module_1.PublicModule,
                send_notify_module_1.SendNotifyModule,
            ],
            providers: [
                {
                    provide: core_1.APP_FILTER,
                    useClass: global_exception_filter_attribute_1.GlobalExceptionFilterAttribute,
                },
            ],
        };
    }
}
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map