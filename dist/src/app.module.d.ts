import { DynamicModule } from '@nestjs/common';
export declare class AppModule {
    static forRoot(): DynamicModule;
}
