import Account from 'src/domain/entities/account.entity';
export declare class PasswordService {
    getPasswordHash(password: string, account: Account): Promise<Account>;
    comparePassword(password: string, account: Account): Promise<boolean>;
}
