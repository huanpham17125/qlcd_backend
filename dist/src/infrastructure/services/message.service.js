"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageService = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const create_message_command_1 = require("../../application/messages/commands/create/create-message-command");
const update_content_message_by_id_command_1 = require("../../application/messages/commands/update/update-content-message-by-id-command");
const update_status_message_command_1 = require("../../application/messages/commands/update/update-status-message-command");
const get_message_pagination_by_room_id_query_1 = require("../../application/messages/queries/get-message-pagination-by-room-id-query");
const message_entity_gateway_1 = require("../../domain/entities/message.entity-gateway");
const domainEnum_1 = require("../../domain/enum/domainEnum");
let MessageService = class MessageService {
    constructor(commandBus) {
        this.commandBus = commandBus;
    }
    async getMessageByRoomId(accountId, pageSize, pageNumber) {
        return await this.commandBus.execute(new get_message_pagination_by_room_id_query_1.GetMessagePaginationByRoomIdQuery(accountId, pageSize, pageNumber));
    }
    async createMessage(senderId, templeId, content, scheduleId) {
        return await this.commandBus.execute(new create_message_command_1.CreateMessageCommand(senderId, templeId, content, scheduleId));
    }
    async updateMessageById(messageId, accountId, content) {
        return await this.commandBus.execute(new update_content_message_by_id_command_1.UpdateContentMessageByIdCommand(messageId, accountId, content));
    }
    async updateStatus(messageId, status) {
        return await this.commandBus.execute(new update_status_message_command_1.UpdateStatusMessageCommand(messageId, status));
    }
};
MessageService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [cqrs_1.CommandBus])
], MessageService);
exports.MessageService = MessageService;
//# sourceMappingURL=message.service.js.map