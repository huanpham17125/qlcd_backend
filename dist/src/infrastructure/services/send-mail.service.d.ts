import { MailerService } from '@nestjs-modules/mailer';
import { CommandBus } from '@nestjs/cqrs';
export declare class SendMailService {
    private readonly mailerService;
    private readonly commandBus;
    constructor(mailerService: MailerService, commandBus: CommandBus);
    sendMailRegisterTemp(accountId: string, toEmail: string, subject: string): void;
    sendMailInvite(accountId: string, toEmail: string, subject: string, email: string): Promise<void>;
    sendMailForgotPassword(accountId: string, toEmail: string, subject: string): Promise<void>;
    private createSendMailLog;
}
