import { IFireBaseStoreData } from 'src/application/common/interfaces/ifire-base-store-data.interface';
export declare class FireStoreService {
    private _projectItems;
    private projects;
    configure(): void;
    getProjectData(projectId: string): IFireBaseStoreData;
    sendNotification(projectId: string, token: string, notification: any): Promise<any>;
}
