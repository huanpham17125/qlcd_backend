import { JwtService } from '@nestjs/jwt';
import Account from 'src/domain/entities/account.entity';
export declare class TokenService {
    private readonly jwtService;
    constructor(jwtService: JwtService);
    generateAccessToken: (account: Account) => Promise<{
        accessToken: string;
        expiresIn: number;
        expiresDate: Date;
    }>;
    generateRefreshToken: (account: Account) => Promise<string>;
    verifyToken: (token: string) => Promise<any>;
    private getExpirseDate;
}
