"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendMailService = void 0;
const mailer_1 = require("@nestjs-modules/mailer");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const create_send_mail_log_command_1 = require("../../application/sendMailLogs/commands/create/create-send-mail-log-command");
const domainEnum_1 = require("../../domain/enum/domainEnum");
const path = require("path");
const fs = require("fs");
let SendMailService = class SendMailService {
    constructor(mailerService, commandBus) {
        this.mailerService = mailerService;
        this.commandBus = commandBus;
    }
    sendMailRegisterTemp(accountId, toEmail, subject) {
        this.mailerService
            .sendMail({
            to: toEmail,
            subject: subject,
            template: 'register-temp',
            context: {
                email: toEmail,
                webUrl: process.env.REGISTER_OFFICAL_WEB,
                androiDeepLink: process.env.REGISTER_OFFICAL_IOS,
                iosDeepLink: process.env.REGISTER_OFFICAL_ANDROI,
            },
        })
            .then((success) => {
            const filePath = path.join(process.cwd() + '/src/api/templates/register-temp.hbs');
            const source = fs.readFileSync(filePath, 'utf-8').toString();
            this.createSendMailLog(accountId, success.envelope.from, toEmail, subject, source, domainEnum_1.TypeSendMail.Register_Office);
        })
            .catch((err) => {
            console.log(err);
        });
    }
    async sendMailInvite(accountId, toEmail, subject, email) {
        await this.mailerService
            .sendMail({
            to: toEmail,
            subject: subject,
            template: 'invite',
            context: {
                email: email,
                webUrl: process.env.INVITE_USER_WEB,
                androiDeepLink: process.env.INVITE_USER_ANDROI,
                iosDeepLink: process.env.INVITE_USER_IOS,
            },
        })
            .then(async (success) => {
            const filePath = path.join(process.cwd() + '/src/api/templates/invite.hbs');
            const source = fs.readFileSync(filePath, 'utf-8').toString();
            await this.createSendMailLog(accountId, success.envelope.from, toEmail, subject, source, domainEnum_1.TypeSendMail.Invite);
        })
            .catch((err) => {
            console.log(err);
        });
    }
    async sendMailForgotPassword(accountId, toEmail, subject) {
        await this.mailerService
            .sendMail({
            to: toEmail,
            subject: subject,
            template: 'forgot-password',
            context: {
                email: toEmail,
                webUrl: process.env.RESET_PASSWORD_WEB,
                androiDeepLink: process.env.RESET_PASSWORD_ANDROI,
                iosDeepLink: process.env.RESET_PASSWORD_IOS,
            },
        })
            .then(async (success) => {
            const filePath = path.join(process.cwd() + '/src/api/templates/forgot-password.hbs');
            const source = fs.readFileSync(filePath, 'utf-8').toString();
            await this.createSendMailLog(accountId, success.envelope.from, toEmail, subject, source, domainEnum_1.TypeSendMail.ForgotPassword);
        })
            .catch((err) => {
            console.log(err);
        });
    }
    async createSendMailLog(accountId, from, to, subject, body, type) {
        return await this.commandBus.execute(new create_send_mail_log_command_1.CreateSendMailLogCommand(accountId, from, to, '', '', subject, body, type, ''));
    }
};
SendMailService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [mailer_1.MailerService,
        cqrs_1.CommandBus])
], SendMailService);
exports.SendMailService = SendMailService;
//# sourceMappingURL=send-mail.service.js.map