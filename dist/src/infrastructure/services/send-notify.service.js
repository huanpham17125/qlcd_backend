"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotifyService = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const req_create_sendnotify_message_1 = require("../../api/viewModels/req-create-sendnotify-message");
const create_notify_message_command_1 = require("../../application/sendNotifiMessage/commands/create/create-notify-message-command");
const delete_notify_message_by_id_command_1 = require("../../application/sendNotifiMessage/commands/delete/delete-notify-message-by-id-command");
const update_notify_message_by_id_command_1 = require("../../application/sendNotifiMessage/commands/update/update-notify-message-by-id-command");
const get_notify_message_by_account_id_query_1 = require("../../application/sendNotifiMessage/queries/get-notify-message-by-account-id-query");
const domainEnum_1 = require("../../domain/enum/domainEnum");
const fire_store_service_1 = require("./fire-store.service");
let SendNotifyService = class SendNotifyService {
    constructor(_firebase, commandBus, queryBus) {
        this._firebase = _firebase;
        this.commandBus = commandBus;
        this.queryBus = queryBus;
        this._firebase.configure();
    }
    async sendNotifyToFirebase(fromAccountId, body) {
        return await this.commandBus.execute(new create_notify_message_command_1.CreateNotifyMessageCommand(fromAccountId, body.accountId, body));
    }
    async getActiveNotifyMessage(accountId) {
        const lstNotify = await this.queryBus.execute(new get_notify_message_by_account_id_query_1.GetNotifyMessageByAccountIdQuery(accountId));
        const resGetNotify = [];
        if (!lstNotify.length) {
            return resGetNotify;
        }
        return resGetNotify;
    }
    async deleteNotifyMessage(id, accountId) {
        return await this.commandBus.execute(new delete_notify_message_by_id_command_1.DeleteNotifyMessageByIdCommand(id, accountId));
    }
    async updateNotifyMessageById(id, accountId, status) {
        return await this.commandBus.execute(new update_notify_message_by_id_command_1.UpdateNotifyMessageByIdCommand(id, accountId, status));
    }
};
SendNotifyService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [fire_store_service_1.FireStoreService,
        cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], SendNotifyService);
exports.SendNotifyService = SendNotifyService;
//# sourceMappingURL=send-notify.service.js.map