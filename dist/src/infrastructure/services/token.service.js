"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const claims_identity_token_1 = require("../../api/viewModels/claims-identity-token");
const account_entity_1 = require("../../domain/entities/account.entity");
const uuid_1 = require("uuid");
const moment = require("moment");
let TokenService = class TokenService {
    constructor(jwtService) {
        this.jwtService = jwtService;
        this.generateAccessToken = async (account) => {
            const expDate = this.getExpirseDate(Number(process.env.JWT_EXPIRATION_ACCESS));
            const claims = new claims_identity_token_1.ClaimsIdentityToken(account.Id, account.Email, account.AccountRoles[0].Role.Id, account.AccountRoles[0].Role.NormalizedName, uuid_1.v4());
            const claimTokens = {
                id: claims.Id,
                email: claims.Email,
                roleId: claims.RoleId,
                roleName: claims.RoleName,
                jti: claims.Jti,
            };
            const accessToken = await this.jwtService.sign(claimTokens, {
                secret: process.env.JWT_SECRETKEY,
                expiresIn: Number(process.env.JWT_EXPIRATION_ACCESS),
            });
            return {
                accessToken: accessToken,
                expiresIn: Number(process.env.JWT_EXPIRATION_ACCESS),
                expiresDate: expDate,
            };
        };
        this.generateRefreshToken = async (account) => {
            return '';
        };
        this.verifyToken = async (token) => {
            const claims = await this.jwtService.verifyAsync(token, {
                secret: process.env.JWT_SECRETKEY,
                ignoreExpiration: false,
            });
            return claims || null;
        };
    }
    getExpirseDate(seconds) {
        const now = new Date();
        return moment(now).add(seconds, 'seconds').toDate();
    }
};
TokenService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [jwt_1.JwtService])
], TokenService);
exports.TokenService = TokenService;
//# sourceMappingURL=token.service.js.map