"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSecurityTokenService = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const create_account_security_token_command_1 = require("../../application/accountSecurityTokens/commands/create/create-account-security-token-command");
const update_account_security_token_command_1 = require("../../application/accountSecurityTokens/commands/update/update-account-security-token-command");
const get_by_account_id_query_1 = require("../../application/accountSecurityTokens/queries/get-by-account-id-query");
const domainEnum_1 = require("../../domain/enum/domainEnum");
let AccountSecurityTokenService = class AccountSecurityTokenService {
    constructor(commandBus) {
        this.commandBus = commandBus;
    }
    async getByAccountId(accountId) {
        return await this.commandBus.execute(new get_by_account_id_query_1.GetByAccountIdQuery(accountId));
    }
    async create(accountId, type, value, expiryDate) {
        return await this.commandBus.execute(new create_account_security_token_command_1.CreateAccountSecurityTokenCommand(accountId, type, value, expiryDate));
    }
    async update(value, expirseDate, id) {
        return await this.commandBus.execute(new update_account_security_token_command_1.UpdateAccountSecurityTokenCommand(value, expirseDate, id));
    }
};
AccountSecurityTokenService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [cqrs_1.CommandBus])
], AccountSecurityTokenService);
exports.AccountSecurityTokenService = AccountSecurityTokenService;
//# sourceMappingURL=account-security-token.service.js.map