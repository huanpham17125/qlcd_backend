import { CommandBus } from '@nestjs/cqrs';
import { Pagination } from 'nestjs-typeorm-paginate';
import Message from 'src/domain/entities/message.entity-gateway';
import { StatusMessage } from 'src/domain/enum/domainEnum';
export declare class MessageService {
    private readonly commandBus;
    constructor(commandBus: CommandBus);
    getMessageByRoomId(accountId: string, pageSize?: number, pageNumber?: number): Promise<Pagination<Message>>;
    createMessage(senderId: string, templeId: string, content: any, scheduleId: number): Promise<any>;
    updateMessageById(messageId: string, accountId: string, content: string): Promise<any>;
    updateStatus(messageId: number, status: StatusMessage): Promise<any>;
}
