import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqRegister } from 'src/api/viewModels/req-register';
import { ReqRegisterTemp } from 'src/api/viewModels/req-register-temp';
import { ReqRegisterUser } from 'src/api/viewModels/req-register-user';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
export declare class AccountService {
    private readonly commandBus;
    private readonly queryBus;
    constructor(commandBus: CommandBus, queryBus: QueryBus);
    createAccount(req: ReqRegister, creatorId: string): Promise<any>;
    registerTemp(req: ReqRegisterTemp): Promise<any>;
    adminCreateAccount(req: ReqRegisterUser): Promise<any>;
    getAccountByEmailAndPassword(email: string, password: string): Promise<any>;
    getAccountByEmail(email: string): Promise<any>;
    getAccountById(id: string): Promise<any>;
    getDetailProfile(id: string): Promise<any>;
    updatelProfile(req: ReqUpdateAccount, id: string): Promise<any>;
    updateStatusAccount(type: number, id: string, adminId: string): Promise<boolean>;
    changePassword(password: string, id: string): Promise<any>;
    updateAccountSetting(accountId: string, timePushNotifyId: number, colorNum: number): Promise<any>;
    updateIsOnlineByAccountId(accountId: string, isOnline: boolean): Promise<any>;
}
