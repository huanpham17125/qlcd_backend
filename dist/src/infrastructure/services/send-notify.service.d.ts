import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';
import { StatusSendNotify } from 'src/domain/enum/domainEnum';
import { FireStoreService } from './fire-store.service';
export declare class SendNotifyService {
    private readonly _firebase;
    private readonly commandBus;
    private readonly queryBus;
    constructor(_firebase: FireStoreService, commandBus: CommandBus, queryBus: QueryBus);
    sendNotifyToFirebase(fromAccountId: string, body: ReqCreateSendnotifyMessage): Promise<any>;
    getActiveNotifyMessage(accountId: string): Promise<any[]>;
    deleteNotifyMessage(id: number, accountId: string): Promise<any>;
    updateNotifyMessageById(id: number, accountId: string, status: StatusSendNotify): Promise<any>;
}
