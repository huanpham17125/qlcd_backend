import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ReqGetAccountByEmail } from 'src/api/viewModels/req-get-account-by-email';
export declare class RoomChatService {
    private readonly commandBus;
    private readonly queryBus;
    constructor(commandBus: CommandBus, queryBus: QueryBus);
    fillAllByAccountId(accountId: string, pageSize?: number, pageNumber?: number): Promise<any>;
    createRoom(accountId: string, email: string, nameRoom: string, templeId: string): Promise<void>;
    addAccountToRoom(creator: string, roomId: string, lstEmail: ReqGetAccountByEmail[]): Promise<boolean>;
}
