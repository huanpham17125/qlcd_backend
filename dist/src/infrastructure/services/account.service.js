"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountService = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const req_register_1 = require("../../api/viewModels/req-register");
const req_register_temp_1 = require("../../api/viewModels/req-register-temp");
const req_register_user_1 = require("../../api/viewModels/req-register-user");
const req_update_account_1 = require("../../api/viewModels/req-update-account");
const create_account_command_1 = require("../../application/accounts/commands/create/create-account-command");
const register_temp_command_1 = require("../../application/accounts/commands/create/register-temp-command");
const update_account_by_id_command_1 = require("../../application/accounts/commands/update/update-account-by-id-command");
const update_account_setting_by_account_id_command_1 = require("../../application/accounts/commands/update/update-account-setting-by-account-id-command");
const update_is_online_by_id_command_1 = require("../../application/accounts/commands/update/update-is-online-by-id-command");
const update_password_by_id_command_1 = require("../../application/accounts/commands/update/update-password-by-id-command");
const update_status_account_command_1 = require("../../application/accounts/commands/update/update-status-account-command");
const find_account_by_email_query_1 = require("../../application/accounts/queries/find-account-by-email-query");
const get_account_by_email_query_1 = require("../../application/accounts/queries/get-account-by-email-query");
const get_account_by_id_query_1 = require("../../application/accounts/queries/get-account-by-id-query");
const get_account_data_by_email_query_1 = require("../../application/accounts/queries/get-account-data-by-email-query");
const get_account_profile_query_1 = require("../../application/accounts/queries/get-account-profile-query");
const constants_1 = require("../../application/common/constants");
const bad_request_exception_1 = require("../../application/common/exceptions/bad-request-exception");
const account_entity_1 = require("../../domain/entities/account.entity");
let AccountService = class AccountService {
    constructor(commandBus, queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }
    async createAccount(req, creatorId) {
        const itemCreator = await this.commandBus.execute(new get_account_by_id_query_1.GetAccountByIdQuery(creatorId));
        return await this.commandBus.execute(new create_account_command_1.CreateAccountCommand(itemCreator.AccountRole.RoleId, itemCreator.AccountTemple.TempleId, creatorId, req.fullName, req.email, req.password, req.phone, req.gender));
    }
    async registerTemp(req) {
        return await this.commandBus.execute(new register_temp_command_1.RegisterTempCommand(req.email, req.password));
    }
    async adminCreateAccount(req) {
        return null;
    }
    async getAccountByEmailAndPassword(email, password) {
        return await this.commandBus.execute(new get_account_by_email_query_1.GetAccountByEmailQuery(email, password));
    }
    async getAccountByEmail(email) {
        return await this.commandBus.execute(new find_account_by_email_query_1.FindAccountByEmailQuery(email));
    }
    async getAccountById(id) {
        return await this.commandBus.execute(new get_account_by_id_query_1.GetAccountByIdQuery(id));
    }
    async getDetailProfile(id) {
        return await this.commandBus.execute(new get_account_profile_query_1.GetAccountProfileQuery(id));
    }
    async updatelProfile(req, id) {
        const isExist = await this.commandBus.execute(new get_account_data_by_email_query_1.GetAccountDataByEmailQuery(req.email));
        if (isExist && isExist.Id != id) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.EMAIL_ALREADY_EXISTED);
        }
        return await this.commandBus.execute(new update_account_by_id_command_1.UpdateAccountByIdCommand(req, id));
    }
    async updateStatusAccount(type, id, adminId) {
        return await this.commandBus.execute(new update_status_account_command_1.UpdateStatusAccountCommand(id, adminId, type));
    }
    async changePassword(password, id) {
        return await this.commandBus.execute(new update_password_by_id_command_1.UpdatePasswordByIdCommand(password, id));
    }
    async updateAccountSetting(accountId, timePushNotifyId, colorNum) {
        return await this.commandBus.execute(new update_account_setting_by_account_id_command_1.UpdateAccountSettingByAccountIdCommand(accountId, timePushNotifyId, colorNum));
    }
    async updateIsOnlineByAccountId(accountId, isOnline) {
        return await this.commandBus.execute(new update_is_online_by_id_command_1.UpdateIsOnlineByIdCommand(accountId, isOnline));
    }
};
AccountService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], AccountService);
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map