import { CommandBus } from '@nestjs/cqrs';
import { TypeToken } from 'src/domain/enum/domainEnum';
export declare class AccountSecurityTokenService {
    private readonly commandBus;
    constructor(commandBus: CommandBus);
    getByAccountId(accountId: string): Promise<any>;
    create(accountId: string, type: TypeToken, value: string, expiryDate: Date): Promise<any>;
    update(value: string, expirseDate: Date, id: string): Promise<any>;
}
