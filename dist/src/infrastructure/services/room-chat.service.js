"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomChatService = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const req_get_account_by_email_1 = require("../../api/viewModels/req-get-account-by-email");
const create_account_room_command_1 = require("../../application/accountRooms/commands/create/create-account-room-command");
const get_account_data_by_email_query_1 = require("../../application/accounts/queries/get-account-data-by-email-query");
const constants_1 = require("../../application/common/constants");
const bad_request_exception_1 = require("../../application/common/exceptions/bad-request-exception");
const create_room_chat_command_1 = require("../../application/roomChats/commands/create/create-room-chat-command");
const get_all_room_by_account_id_query_1 = require("../../application/roomChats/queries/get-all-room-by-account-id-query");
const get_room_by_account_id_query_1 = require("../../application/roomChats/queries/get-room-by-account-id-query");
let RoomChatService = class RoomChatService {
    constructor(commandBus, queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }
    async fillAllByAccountId(accountId, pageSize, pageNumber) {
        return await this.commandBus.execute(new get_all_room_by_account_id_query_1.GetAllRoomByAccountIdQuery(accountId, pageSize, pageNumber));
    }
    async createRoom(accountId, email, nameRoom, templeId) {
        const itemRoom = await this.queryBus.execute(new get_room_by_account_id_query_1.GetRoomByAccountIdQuery(accountId));
        if (!itemRoom) {
            await this.commandBus.execute(new create_room_chat_command_1.CreateRoomChatCommand(accountId, nameRoom, templeId));
        }
    }
    async addAccountToRoom(creator, roomId, lstEmail) {
        lstEmail.forEach(async (itemEmail) => {
            const itemAccount = await this.commandBus.execute(new get_account_data_by_email_query_1.GetAccountDataByEmailQuery(itemEmail.email));
            if (!itemAccount) {
                throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.EMAIL_NOT_ALREADY_EXISTED + ' ' + itemEmail);
            }
            await this.commandBus.execute(new create_account_room_command_1.CreateAccountRoomCommand(itemAccount.Id, roomId));
        });
        return true;
    }
};
RoomChatService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [cqrs_1.CommandBus,
        cqrs_1.QueryBus])
], RoomChatService);
exports.RoomChatService = RoomChatService;
//# sourceMappingURL=room-chat.service.js.map