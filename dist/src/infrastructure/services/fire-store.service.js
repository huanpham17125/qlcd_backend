"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireStoreService = void 0;
const common_1 = require("@nestjs/common");
const fireStoreConfig = require("../../../config/firebase-store.config");
const ifire_base_store_data_interface_1 = require("../../application/common/interfaces/ifire-base-store-data.interface");
const ifire_base_store_config_interface_1 = require("../../application/common/interfaces/ifire-base-store-config.interface");
const firebase_admin_1 = require("firebase-admin");
let FireStoreService = class FireStoreService {
    constructor() {
        this._projectItems = new Map();
    }
    projects() {
        return fireStoreConfig.default;
    }
    configure() {
    }
    getProjectData(projectId) {
        const data = this._projectItems.get(projectId);
        if (!data) {
            throw new Error('project not found');
        }
        return data;
    }
    async sendNotification(projectId, token, notification) {
        try {
            const project = this.getProjectData(projectId);
            const data = Object.assign(Object.assign({}, notification), { token });
            return await firebase_admin_1.messaging(project.ref).send(data);
        }
        catch (error) {
            return Promise.reject(error);
        }
    }
};
FireStoreService = __decorate([
    common_1.Injectable()
], FireStoreService);
exports.FireStoreService = FireStoreService;
//# sourceMappingURL=fire-store.service.js.map