import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class SendNotifyMessageLogRepository extends AsyncGenericRepository<SendNotifyLog> implements ISendNotifyRepository {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<SendNotifyLog>);
    getByAccountId(accountId: string): Promise<SendNotifyLog[]>;
}
