import { Pagination } from 'nestjs-typeorm-paginate';
import Message from 'src/domain/entities/message.entity-gateway';
import { StatusMessage } from 'src/domain/enum/domainEnum';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class MessageRepository extends AsyncGenericRepository<Message> implements ImessageRepository {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<Message>);
    updateStatusById(messageId: number, status: StatusMessage): Promise<boolean>;
    getMessagePaginationByAccountId(accountId: string, pageSize: number, pageNumber: any): Promise<Pagination<Message>>;
}
