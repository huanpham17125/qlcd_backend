import { Pagination } from 'nestjs-typeorm-paginate';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class RoomChatRepository extends AsyncGenericRepository<RoomChat> implements IroomChatRepository {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<RoomChat>);
    getByTempleId(templeId: string): Promise<RoomChat>;
    getRoomByCreatorAndCreated(creatorId: string, createdId: string): Promise<any>;
    getAllByAccountId(accountId: string, pageSize: number, pageNumber: number): Promise<Pagination<RoomChat>>;
    getByAccountId(accountId: string): Promise<RoomChat>;
}
