"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AsyncGenericRepository = void 0;
const common_1 = require("@nestjs/common");
const iasync_generic_repository_interface_1 = require("../../domain/interfaces/iasync-generic-repository.interface");
const typeorm_1 = require("typeorm");
let AsyncGenericRepository = class AsyncGenericRepository {
    constructor(repo) {
        this.asyncRepository = repo;
    }
    async add(entity) {
        await this.asyncRepository.save(entity);
        return entity;
    }
    async getById(id) {
        const entity = await this.asyncRepository.findOne({ where: { Id: id } });
        return entity ? entity : null;
    }
    async getListActive() {
        const entity = await this.asyncRepository.find({
            where: { IsActive: true },
        });
        return entity.length ? entity : [];
    }
    async updateById(entity, id) {
        await this.asyncRepository.update(id, entity);
        return true;
    }
    async deleteById(id) {
        await this.asyncRepository.delete(id);
        return true;
    }
};
AsyncGenericRepository = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], AsyncGenericRepository);
exports.AsyncGenericRepository = AsyncGenericRepository;
//# sourceMappingURL=async-generic-repository.js.map