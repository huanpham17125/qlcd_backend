import AccountRole from 'src/domain/entities/account-role.entity';
import { IAsyncRepository } from 'src/domain/interfaces/iasync-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class AccountRoleRepository extends AsyncGenericRepository<AccountRole> implements IAsyncRepository<AccountRole> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<AccountRole>);
    getByAccountId(accountId: string): Promise<AccountRole>;
    getListByAccountId(accountId: string): Promise<AccountRole[] | null>;
    getByName(name: string): Promise<AccountRole | null>;
}
