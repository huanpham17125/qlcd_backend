import { Repository } from 'typeorm';
import Account from '../../domain/entities/account.entity';
import { IaccountAsyncRepository } from '../../domain/interfaces/iaccount-async-repository.interface';
export declare class AccountAsyncRepository implements IaccountAsyncRepository {
    private readonly asyncRepository;
    constructor(asyncRepository: Repository<Account>);
    getByEmail(email: string): Promise<Account>;
    updateIsOnlineById(id: string, isOnline: boolean): Promise<boolean>;
    getById(id: string): Promise<Account> | null;
    getProfile(id: string): Promise<Account> | null;
    updateById(account: Account, id: string): Promise<boolean>;
    updatePasswordById(password: string, id: string): Promise<boolean>;
    create(account: Account): Promise<Account>;
    findByEmail: (email: string) => Promise<Account>;
}
