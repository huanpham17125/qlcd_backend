import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import { ISendMailLog } from 'src/domain/interfaces/isend-mail-log.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class SendEmailLogRepository extends AsyncGenericRepository<SendMailLog> implements ISendMailLog {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<SendMailLog>);
    getByAccountId(accountId: string): Promise<SendMailLog[]>;
}
