"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountAsyncRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const account_entity_1 = require("../../domain/entities/account.entity");
let AccountAsyncRepository = class AccountAsyncRepository {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
        this.findByEmail = async (email) => {
            const entity = await this.asyncRepository
                .createQueryBuilder('acc')
                .leftJoinAndSelect('acc.AccountRoles', 'accRole')
                .leftJoinAndSelect('accRole.Role', 'role')
                .select([
                'acc.Email',
                'acc.Id',
                'acc.FullName',
                'acc.Phone',
                'accRole',
                'role.Name',
                'role.NormalizedName',
                'role.Id',
                'acc.Province',
                'acc.City',
                'acc.Street',
                'acc.IsActive',
                'acc.PasswordHash',
            ])
                .where('acc.Email = :email AND accRole.IsActive = :active AND role.IsActive = :active', {
                active: true,
                email: email,
            })
                .getOne();
            return entity ? entity : null;
        };
    }
    async getByEmail(email) {
        return await this.asyncRepository.findOne({ where: { Email: email } });
    }
    async updateIsOnlineById(id, isOnline) {
        const entity = await this.asyncRepository.save({
            IsOnline: isOnline,
            Id: id,
        });
        return entity ? true : false;
    }
    async getById(id) {
        const entity = await this.asyncRepository.findOne({ Id: id });
        return entity ? entity : null;
    }
    async getProfile(id) {
        const entity = await this.asyncRepository
            .createQueryBuilder('acc')
            .leftJoinAndSelect('acc.AccountRoles', 'accRole')
            .leftJoinAndSelect('accRole.Role', 'role')
            .leftJoin('acc.AccountSetting', 'AS', 'acc.Id = AS.AccountId')
            .select([
            'acc.Email',
            'acc.Id',
            'acc.FullName',
            'acc.Phone',
            'accTemple',
            'acc.PasswordHash',
            'acc.Province',
            'acc.Gender',
            'acc.Birthday',
            'acc.Street',
            'acc.City',
            'acc.IsActive',
            'accRole',
            'role'
        ])
            .where('acc.Id = :id', {
            id: id,
        })
            .getOne();
        return entity ? entity : null;
    }
    async updateById(account, id) {
        await this.asyncRepository.update(id, account);
        return true;
    }
    async updatePasswordById(password, id) {
        await this.asyncRepository.update(id, { PasswordHash: password });
        return true;
    }
    async create(account) {
        await this.asyncRepository
            .createQueryBuilder()
            .insert()
            .into(account_entity_1.default)
            .values(account)
            .execute();
        return account;
    }
};
AccountAsyncRepository = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(account_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], AccountAsyncRepository);
exports.AccountAsyncRepository = AccountAsyncRepository;
//# sourceMappingURL=accountAsyncRepository.js.map