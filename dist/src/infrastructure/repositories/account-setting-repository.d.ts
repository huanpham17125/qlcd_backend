import AccountSetting from 'src/domain/entities/account-setting.entity';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class AccountSettingRepository extends AsyncGenericRepository<AccountSetting> implements IaccountSettingRepository {
    private readonly accountSettingRepository;
    constructor(accountSettingRepository: Repository<AccountSetting>);
    getByAccountId(accountId: string): Promise<AccountSetting> | null;
}
