import Role from 'src/domain/entities/role.entity';
import { IAsyncRepository } from 'src/domain/interfaces/iasync-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class RoleRepository extends AsyncGenericRepository<Role> implements IAsyncRepository<Role> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<Role>);
    getByAccountId(accountId: string): Promise<Role>;
    getListByAccountId(accountId: string): Promise<Role[] | null>;
    getByName(name: string): Promise<Role | null>;
}
