"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountLogRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const account_log_entity_1 = require("../../domain/entities/account-log.entity");
const typeorm_2 = require("typeorm");
const async_generic_repository_1 = require("./async-generic-repository");
let AccountLogRepository = class AccountLogRepository extends async_generic_repository_1.AsyncGenericRepository {
    constructor(_asyncRepository) {
        super(_asyncRepository);
        this._asyncRepository = _asyncRepository;
    }
};
AccountLogRepository = __decorate([
    typeorm_2.EntityRepository(account_log_entity_1.default),
    __param(0, typeorm_1.InjectRepository(account_log_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], AccountLogRepository);
exports.AccountLogRepository = AccountLogRepository;
//# sourceMappingURL=account-log-repository.js.map