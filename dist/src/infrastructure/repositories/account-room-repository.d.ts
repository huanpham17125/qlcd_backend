import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class AccountRoomRepository extends AsyncGenericRepository<AccountRoom> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<AccountRoom>);
}
