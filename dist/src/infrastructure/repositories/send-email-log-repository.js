"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendEmailLogRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const send_mail_log_entity_1 = require("../../domain/entities/send-mail-log.entity");
const isend_mail_log_interface_1 = require("../../domain/interfaces/isend-mail-log.interface");
const typeorm_2 = require("typeorm");
const async_generic_repository_1 = require("./async-generic-repository");
let SendEmailLogRepository = class SendEmailLogRepository extends async_generic_repository_1.AsyncGenericRepository {
    constructor(_asyncRepository) {
        super(_asyncRepository);
        this._asyncRepository = _asyncRepository;
    }
    getByAccountId(accountId) {
        throw new Error('Method not implemented.');
    }
};
SendEmailLogRepository = __decorate([
    typeorm_2.EntityRepository(send_mail_log_entity_1.default),
    __param(0, typeorm_1.InjectRepository(send_mail_log_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SendEmailLogRepository);
exports.SendEmailLogRepository = SendEmailLogRepository;
//# sourceMappingURL=send-email-log-repository.js.map