"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const account_room_entity_gateway_1 = require("../../domain/entities/account-room.entity-gateway");
const message_entity_gateway_1 = require("../../domain/entities/message.entity-gateway");
const room_chat_entity_gateway_1 = require("../../domain/entities/room-chat.entity-gateway");
const domainEnum_1 = require("../../domain/enum/domainEnum");
const imessage_repository_interface_1 = require("../../domain/interfaces/imessage-repository.interface");
const typeorm_2 = require("typeorm");
const async_generic_repository_1 = require("./async-generic-repository");
let MessageRepository = class MessageRepository extends async_generic_repository_1.AsyncGenericRepository {
    constructor(_asyncRepository) {
        super(_asyncRepository);
        this._asyncRepository = _asyncRepository;
    }
    updateStatusById(messageId, status) {
        throw new Error('Method not implemented.');
    }
    async getMessagePaginationByAccountId(accountId, pageSize, pageNumber) {
        const lstMessage = await nestjs_typeorm_paginate_1.paginateRaw(this._asyncRepository
            .createQueryBuilder('M')
            .innerJoin('M.RoomChat', 'R')
            .leftJoin('R.AccountRoom', 'AR')
            .select('M.*')
            .where('AR.AccountId = :accountId', {
            accountId: accountId,
        })
            .orderBy('M.CreatedDate', 'DESC')
            .groupBy('M.Id'), {
            page: pageNumber,
            limit: pageSize,
        });
        return lstMessage;
    }
};
MessageRepository = __decorate([
    typeorm_2.EntityRepository(message_entity_gateway_1.default),
    __param(0, typeorm_1.InjectRepository(message_entity_gateway_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], MessageRepository);
exports.MessageRepository = MessageRepository;
//# sourceMappingURL=message-repository.js.map