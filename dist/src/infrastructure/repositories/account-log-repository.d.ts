import AccountLog from 'src/domain/entities/account-log.entity';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class AccountLogRepository extends AsyncGenericRepository<AccountLog> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: Repository<AccountLog>);
}
