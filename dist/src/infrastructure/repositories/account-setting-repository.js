"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSettingRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const account_setting_entity_1 = require("../../domain/entities/account-setting.entity");
const iaccount_setting_repository_interface_1 = require("../../domain/interfaces/iaccount-setting-repository.interface");
const typeorm_2 = require("typeorm");
const async_generic_repository_1 = require("./async-generic-repository");
let AccountSettingRepository = class AccountSettingRepository extends async_generic_repository_1.AsyncGenericRepository {
    constructor(accountSettingRepository) {
        super(accountSettingRepository);
        this.accountSettingRepository = accountSettingRepository;
    }
    async getByAccountId(accountId) {
        const entity = await this.accountSettingRepository
            .createQueryBuilder('setting')
            .leftJoinAndSelect('setting.ColorCode', 'color')
            .leftJoinAndSelect('setting.Account', 'acc')
            .select(['acc', 'setting', 'color.Name', 'color.Value', 'color.Id'])
            .where('setting.AccountId = :accountId', {
            accountId: accountId,
        })
            .getOne();
        return entity ? entity : null;
    }
};
AccountSettingRepository = __decorate([
    __param(0, typeorm_1.InjectRepository(account_setting_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], AccountSettingRepository);
exports.AccountSettingRepository = AccountSettingRepository;
//# sourceMappingURL=account-setting-repository.js.map