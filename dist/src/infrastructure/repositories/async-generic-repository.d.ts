import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { Repository } from 'typeorm';
export declare class AsyncGenericRepository<TEntity> implements IAsyncGenericRepository<TEntity> {
    private readonly asyncRepository;
    constructor(repo: Repository<TEntity>);
    add(entity: TEntity): Promise<TEntity>;
    getById(id: string | number): Promise<TEntity>;
    getListActive(): Promise<TEntity[]> | null;
    updateById(entity: TEntity, id: string): Promise<boolean>;
    deleteById(id: string): Promise<boolean>;
}
