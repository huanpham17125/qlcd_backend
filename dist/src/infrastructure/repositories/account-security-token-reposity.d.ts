import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { Repository } from 'typeorm';
import { AsyncGenericRepository } from './async-generic-repository';
export declare class AccountSecurityTokenReposity extends AsyncGenericRepository<AccountSecurityToken> implements IAccountSecutiryTokenRepository {
    private readonly accountSecurityRepository;
    constructor(accountSecurityRepository: Repository<AccountSecurityToken>);
    getByAccountId(accountId: string): Promise<AccountSecurityToken> | null;
}
