"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomChatRepository = void 0;
const typeorm_1 = require("@nestjs/typeorm");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const message_entity_gateway_1 = require("../../domain/entities/message.entity-gateway");
const room_chat_entity_gateway_1 = require("../../domain/entities/room-chat.entity-gateway");
const iroom_chat_repository_interface_1 = require("../../domain/interfaces/iroom-chat-repository.interface");
const typeorm_2 = require("typeorm");
const async_generic_repository_1 = require("./async-generic-repository");
let RoomChatRepository = class RoomChatRepository extends async_generic_repository_1.AsyncGenericRepository {
    constructor(_asyncRepository) {
        super(_asyncRepository);
        this._asyncRepository = _asyncRepository;
    }
    async getByTempleId(templeId) {
        return await this._asyncRepository.findOne({
            where: { TempleId: templeId, IsActive: true },
        });
    }
    async getRoomByCreatorAndCreated(creatorId, createdId) {
        const entity = await this._asyncRepository
            .createQueryBuilder('R')
            .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
            .select('R.*')
            .where('AR.AccountId = :creatorId AND  AR.AccountId = :createdId ADN R.IsActive = true', {
            creatorId: creatorId,
            createdId: createdId,
        })
            .getOne();
        return entity ? entity : null;
    }
    async getAllByAccountId(accountId, pageSize, pageNumber) {
        const entity = await nestjs_typeorm_paginate_1.paginateRaw(this._asyncRepository
            .createQueryBuilder('R')
            .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
            .leftJoin((qb) => qb
            .select('me.*')
            .from(message_entity_gateway_1.default, 'me')
            .orderBy({ 'me.CreatedDate': 'DESC' })
            .limit(1), 'M', 'R.Id = M.RoomId')
            .select(['R.Id', 'R.Name', 'R.Tittle'])
            .addSelect('M.UpdatedDate', 'MessageDate')
            .addSelect('M.Status', 'StatusMessage')
            .addSelect('M.TypeContent', 'TypeMessage')
            .addSelect('M.Content', 'Content')
            .where('AR.AccountId = :accountId AND R.IsActive = :isActive', {
            accountId: accountId,
            isActive: true,
        })
            .orderBy('R.UpdatedDate', 'DESC')
            .groupBy('R.Name'), {
            page: pageNumber,
            limit: pageSize,
        });
        return entity;
    }
    async getByAccountId(accountId) {
        const entity = await this._asyncRepository
            .createQueryBuilder('R')
            .innerJoin('R.AccountRoom', 'AR', 'R.Id = AR.RoomId')
            .select(['R.Id', 'R.Name', 'R.Tittle'])
            .where('AR.AccountId = :accountId AND R.IsActive = :isActive', {
            accountId: accountId,
            isActive: true,
        })
            .getOne();
        return entity;
    }
};
RoomChatRepository = __decorate([
    typeorm_2.EntityRepository(room_chat_entity_gateway_1.default),
    __param(0, typeorm_1.InjectRepository(room_chat_entity_gateway_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], RoomChatRepository);
exports.RoomChatRepository = RoomChatRepository;
//# sourceMappingURL=room-chat-repository.js.map