"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BoostrapModule = void 0;
const mailer_1 = require("@nestjs-modules/mailer");
const handlebars_adapter_1 = require("@nestjs-modules/mailer/dist/adapters/handlebars.adapter");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const path_1 = require("path");
let BoostrapModule = class BoostrapModule {
};
BoostrapModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot({
                envFilePath: [
                    `.env.${process.env.NODE_ENV}`,
                    `.env.${process.env.NODE_ENV}.local`,
                    '.env.local',
                    '.env',
                ],
                isGlobal: true,
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    type: 'mysql',
                    host: config.get('DB_HOST') || 'localhost',
                    port: config.get('DB_PORT'),
                    database: config.get('DB_NAME'),
                    username: config.get('DB_USER'),
                    password: config.get('DB_PASSWORD'),
                    entities: [path_1.join(__dirname, '../**/**/*entity{.ts,.js}')],
                    autoLoadEntities: true,
                    migrationsRun: true,
                    migrations: [path_1.join(__dirname, '../migration/**/*{.ts,.js}')],
                    migrationsTableName: 'migrations_typeorm',
                    cli: {
                        "entitiesDir": "src/domain/entities",
                        "migrationsDir": "src/infrastrucute_Db/migrations/db_qlcd"
                    },
                    logging: ["error", "migration"]
                }),
                inject: [config_1.ConfigService],
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                imports: [config_1.ConfigModule],
                name: 'gatewayConfig',
                useFactory: async (config) => ({
                    type: 'mysql',
                    host: config.get('DB_HOST') || 'localhost',
                    port: config.get('DB_PORT'),
                    database: config.get('DB_NAME_GATEWAY'),
                    username: config.get('DB_USER'),
                    password: config.get('DB_PASSWORD'),
                    entities: [path_1.join(__dirname, '../**/**/*entity-gateway{.ts,.js}')],
                    autoLoadEntities: true,
                    migrationsRun: true,
                    migrations: [path_1.join(__dirname, '../migration/**/*{.ts,.js}')],
                    migrationsTableName: 'migrations_typeorm',
                    cli: {
                        "entitiesDir": "src/domain/entities",
                        "migrationsDir": "src/infrastrucute_Db/migrations/db_gateway"
                    },
                    logging: ["error", "migration"]
                }),
                inject: [config_1.ConfigService],
            }),
            mailer_1.MailerModule.forRootAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    transport: {
                        host: config.get('EMAIL_HOST'),
                        port: config.get('EMAIL_PORT'),
                        secure: false,
                        auth: {
                            user: config.get('EMAIL_ID'),
                            pass: config.get('EMAIL_PASS'),
                        },
                    },
                    defaults: {
                        from: '"No-reply" <omairi@ai-ot.net>',
                    },
                    template: {
                        dir: process.cwd() + '/src/api/templates',
                        adapter: new handlebars_adapter_1.HandlebarsAdapter(),
                        options: {
                            strict: true,
                        },
                    },
                }),
                inject: [config_1.ConfigService],
            }),
        ],
    })
], BoostrapModule);
exports.BoostrapModule = BoostrapModule;
//# sourceMappingURL=boostrap.module.js.map