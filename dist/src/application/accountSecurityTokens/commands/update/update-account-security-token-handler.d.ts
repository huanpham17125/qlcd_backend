import { ICommandHandler } from '@nestjs/cqrs';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { UpdateAccountSecurityTokenCommand } from './update-account-security-token-command';
export declare class UpdateAccountSecurityTokenHandler implements ICommandHandler<UpdateAccountSecurityTokenCommand> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: IAccountSecutiryTokenRepository);
    execute(command: UpdateAccountSecurityTokenCommand): Promise<boolean>;
}
