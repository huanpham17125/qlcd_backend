import { ICommand } from '@nestjs/cqrs';
export declare class UpdateAccountSecurityTokenCommand implements ICommand {
    value: string;
    expriryDate: Date;
    id: string;
    constructor(value: string, expriryDate: Date, id: string);
}
