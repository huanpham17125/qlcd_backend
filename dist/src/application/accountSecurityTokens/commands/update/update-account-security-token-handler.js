"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountSecurityTokenHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const iaccount_secutiry_token_repository_interface_1 = require("../../../../domain/interfaces/iaccount-secutiry-token-repository.interface");
const update_account_security_token_command_1 = require("./update-account-security-token-command");
let UpdateAccountSecurityTokenHandler = class UpdateAccountSecurityTokenHandler {
    constructor(_asyncRepository) {
        this._asyncRepository = _asyncRepository;
    }
    async execute(command) {
        const entity = await this._asyncRepository.getById(command.id);
        entity.ExpiryDate = command.expriryDate;
        entity.Value = command.value;
        entity.UpdatedDate = new Date();
        return await this._asyncRepository.updateById(entity, command.id);
    }
};
UpdateAccountSecurityTokenHandler = __decorate([
    cqrs_1.CommandHandler(update_account_security_token_command_1.UpdateAccountSecurityTokenCommand),
    __param(0, common_1.Inject('ACCOUNSECURITYTOKENS')),
    __metadata("design:paramtypes", [Object])
], UpdateAccountSecurityTokenHandler);
exports.UpdateAccountSecurityTokenHandler = UpdateAccountSecurityTokenHandler;
//# sourceMappingURL=update-account-security-token-handler.js.map