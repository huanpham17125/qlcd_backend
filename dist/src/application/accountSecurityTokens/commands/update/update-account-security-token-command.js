"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountSecurityTokenCommand = void 0;
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
class UpdateAccountSecurityTokenCommand {
    constructor(value, expriryDate, id) {
        this.value = value;
        this.expriryDate = expriryDate;
        this.id = id;
    }
}
exports.UpdateAccountSecurityTokenCommand = UpdateAccountSecurityTokenCommand;
//# sourceMappingURL=update-account-security-token-command.js.map