import { ICommand } from '@nestjs/cqrs';
import { TypeToken } from 'src/domain/enum/domainEnum';
export declare class CreateAccountSecurityTokenCommand implements ICommand {
    readonly AccountId: string;
    readonly TokenType: TypeToken;
    readonly Value: string;
    readonly ExpiryDate: Date;
    readonly RemoteIpAddress?: string;
    constructor(AccountId: string, TokenType: TypeToken, Value: string, ExpiryDate: Date, RemoteIpAddress?: string);
}
