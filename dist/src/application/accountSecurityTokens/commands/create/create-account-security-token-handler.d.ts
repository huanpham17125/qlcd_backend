import { ICommandHandler } from '@nestjs/cqrs';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { CreateAccountSecurityTokenCommand } from './create-account-security-token-command';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
export declare class CreateAccountSecurityTokenHandler implements ICommandHandler<CreateAccountSecurityTokenCommand> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: IAccountSecutiryTokenRepository);
    execute(command: CreateAccountSecurityTokenCommand): Promise<AccountSecurityToken>;
}
