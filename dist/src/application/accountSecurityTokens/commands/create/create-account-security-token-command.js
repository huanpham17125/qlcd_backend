"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountSecurityTokenCommand = void 0;
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
class CreateAccountSecurityTokenCommand {
    constructor(AccountId, TokenType, Value, ExpiryDate, RemoteIpAddress) {
        this.AccountId = AccountId;
        this.TokenType = TokenType;
        this.Value = Value;
        this.ExpiryDate = ExpiryDate;
        this.RemoteIpAddress = RemoteIpAddress;
    }
}
exports.CreateAccountSecurityTokenCommand = CreateAccountSecurityTokenCommand;
//# sourceMappingURL=create-account-security-token-command.js.map