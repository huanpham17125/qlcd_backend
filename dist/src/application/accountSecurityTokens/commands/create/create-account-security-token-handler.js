"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountSecurityTokenHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const account_security_token_entity_1 = require("../../../../domain/entities/account-security-token.entity");
const create_account_security_token_command_1 = require("./create-account-security-token-command");
const uuid_1 = require("uuid");
const iaccount_secutiry_token_repository_interface_1 = require("../../../../domain/interfaces/iaccount-secutiry-token-repository.interface");
let CreateAccountSecurityTokenHandler = class CreateAccountSecurityTokenHandler {
    constructor(_asyncRepository) {
        this._asyncRepository = _asyncRepository;
    }
    async execute(command) {
        const reqInsert = new account_security_token_entity_1.default();
        reqInsert.Id = uuid_1.v4();
        reqInsert.ExpiryDate = command.ExpiryDate;
        reqInsert.Value = command.Value;
        reqInsert.AccountId = command.AccountId;
        reqInsert.TokenType = command.TokenType;
        reqInsert.CreatedDate = new Date();
        reqInsert.UpdatedDate = new Date();
        reqInsert.IsActive = true;
        reqInsert.RemoteIpAddress = '';
        const item = await this._asyncRepository.add(reqInsert);
        return item;
    }
};
CreateAccountSecurityTokenHandler = __decorate([
    cqrs_1.CommandHandler(create_account_security_token_command_1.CreateAccountSecurityTokenCommand),
    __param(0, common_1.Inject('ACCOUNSECURITYTOKENS')),
    __metadata("design:paramtypes", [Object])
], CreateAccountSecurityTokenHandler);
exports.CreateAccountSecurityTokenHandler = CreateAccountSecurityTokenHandler;
//# sourceMappingURL=create-account-security-token-handler.js.map