import { ICommandHandler } from '@nestjs/cqrs';
import AccountSecurityToken from 'src/domain/entities/account-security-token.entity';
import { IAccountSecutiryTokenRepository } from 'src/domain/interfaces/iaccount-secutiry-token-repository.interface';
import { GetByAccountIdQuery } from './get-by-account-id-query';
export declare class GetByAccountIdHandler implements ICommandHandler<GetByAccountIdQuery> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: IAccountSecutiryTokenRepository);
    execute(command: GetByAccountIdQuery): Promise<AccountSecurityToken> | null;
}
