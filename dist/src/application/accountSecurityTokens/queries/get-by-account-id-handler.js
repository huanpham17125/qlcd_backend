"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetByAccountIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const account_security_token_entity_1 = require("../../../domain/entities/account-security-token.entity");
const iaccount_secutiry_token_repository_interface_1 = require("../../../domain/interfaces/iaccount-secutiry-token-repository.interface");
const get_by_account_id_query_1 = require("./get-by-account-id-query");
let GetByAccountIdHandler = class GetByAccountIdHandler {
    constructor(_asyncRepository) {
        this._asyncRepository = _asyncRepository;
    }
    async execute(command) {
        return this._asyncRepository.getByAccountId(command.accountId);
    }
};
GetByAccountIdHandler = __decorate([
    cqrs_1.CommandHandler(get_by_account_id_query_1.GetByAccountIdQuery),
    __param(0, common_1.Inject('ACCOUNSECURITYTOKENS')),
    __metadata("design:paramtypes", [Object])
], GetByAccountIdHandler);
exports.GetByAccountIdHandler = GetByAccountIdHandler;
//# sourceMappingURL=get-by-account-id-handler.js.map