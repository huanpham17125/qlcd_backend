import { ICommand } from '@nestjs/cqrs';
export declare class GetByAccountIdQuery implements ICommand {
    readonly accountId: string;
    constructor(accountId: string);
}
