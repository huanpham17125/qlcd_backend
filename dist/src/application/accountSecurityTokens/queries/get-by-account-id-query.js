"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetByAccountIdQuery = void 0;
class GetByAccountIdQuery {
    constructor(accountId) {
        this.accountId = accountId;
    }
}
exports.GetByAccountIdQuery = GetByAccountIdQuery;
//# sourceMappingURL=get-by-account-id-query.js.map