"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountRoomHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const account_room_entity_gateway_1 = require("../../../../domain/entities/account-room.entity-gateway");
const iasync_generic_repository_interface_1 = require("../../../../domain/interfaces/iasync-generic-repository.interface");
const account_room_repository_1 = require("../../../../infrastructure/repositories/account-room-repository");
const create_account_room_command_1 = require("./create-account-room-command");
let CreateAccountRoomHandler = class CreateAccountRoomHandler {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
    }
    async execute(command) {
        const reqCreate = new account_room_entity_gateway_1.default();
        reqCreate.AccountId = command.accountId;
        reqCreate.RoomId = command.roomId;
        const entity = await this.asyncRepository.add(reqCreate);
        return entity;
    }
};
CreateAccountRoomHandler = __decorate([
    cqrs_1.CommandHandler(create_account_room_command_1.CreateAccountRoomCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_ROOM_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], CreateAccountRoomHandler);
exports.CreateAccountRoomHandler = CreateAccountRoomHandler;
//# sourceMappingURL=create-account-room-handler.js.map