import { ICommandHandler } from '@nestjs/cqrs';
import AccountRoom from 'src/domain/entities/account-room.entity-gateway';
import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { CreateAccountRoomCommand } from './create-account-room-command';
export declare class CreateAccountRoomHandler implements ICommandHandler<CreateAccountRoomCommand> {
    private readonly asyncRepository;
    constructor(asyncRepository: IAsyncGenericRepository<AccountRoom>);
    execute(command: CreateAccountRoomCommand): Promise<AccountRoom>;
}
