"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountRoomCommand = void 0;
class CreateAccountRoomCommand {
    constructor(accountId, roomId) {
        this.accountId = accountId;
        this.roomId = roomId;
    }
}
exports.CreateAccountRoomCommand = CreateAccountRoomCommand;
//# sourceMappingURL=create-account-room-command.js.map