import { ICommand } from "@nestjs/cqrs";
export declare class CreateAccountRoomCommand implements ICommand {
    accountId: string;
    roomId: string;
    constructor(accountId: string, roomId: string);
}
