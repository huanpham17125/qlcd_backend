import { IQueryHandler } from '@nestjs/cqrs';
import Role from 'src/domain/entities/role.entity';
import { RoleRepository } from 'src/infrastructure/repositories/role-repository';
import { GetRoleByNameQuery } from './get-role-by-name-query';
export declare class GetRoleByNameHandler implements IQueryHandler<GetRoleByNameQuery> {
    private readonly asyncRepository;
    constructor(asyncRepository: RoleRepository);
    execute(command: GetRoleByNameQuery): Promise<Role | null>;
}
