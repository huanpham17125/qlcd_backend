"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoleByNameQuery = void 0;
class GetRoleByNameQuery {
    constructor(roleName) {
        this.roleName = roleName;
    }
}
exports.GetRoleByNameQuery = GetRoleByNameQuery;
//# sourceMappingURL=get-role-by-name-query.js.map