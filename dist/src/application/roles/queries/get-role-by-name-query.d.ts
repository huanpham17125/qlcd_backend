import { IQuery } from '@nestjs/cqrs';
export declare class GetRoleByNameQuery implements IQuery {
    roleName: string;
    constructor(roleName: string);
}
