"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoleByNameHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const role_entity_1 = require("../../../domain/entities/role.entity");
const role_repository_1 = require("../../../infrastructure/repositories/role-repository");
const get_role_by_name_query_1 = require("./get-role-by-name-query");
let GetRoleByNameHandler = class GetRoleByNameHandler {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
    }
    async execute(command) {
        return await this.asyncRepository.getByName(command.roleName);
    }
};
GetRoleByNameHandler = __decorate([
    cqrs_1.QueryHandler(get_role_by_name_query_1.GetRoleByNameQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ROLE_PROVIDE)),
    __metadata("design:paramtypes", [role_repository_1.RoleRepository])
], GetRoleByNameHandler);
exports.GetRoleByNameHandler = GetRoleByNameHandler;
//# sourceMappingURL=get-role-by-name-handler.js.map