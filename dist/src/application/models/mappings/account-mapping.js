"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const res_get_account_1 = require("../../accounts/viewModels/res-get-account");
const res_update_account_1 = require("../../accounts/viewModels/res-update-account");
const account_setting_entity_1 = require("../../../domain/entities/account-setting.entity");
const account_entity_1 = require("../../../domain/entities/account.entity");
const moment = require("moment");
let AccountProfile = class AccountProfile extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(account_setting_entity_1.default, res_update_account_1.ResUpdateAccount)
            .forMember('name', (opt) => opt.mapFrom((s) => s.Account.FullName))
            .forMember('email', (opt) => opt.mapFrom((s) => s.Account.Email));
        this.createMap(account_entity_1.default, res_get_account_1.default)
            .forMember('fullName', (otp) => otp.mapFrom((s) => s.FullName))
            .forMember('phone', (otp) => otp.mapFrom((s) => s.Phone))
            .forMember('address', (otp) => otp.mapFrom((s) => s.Street +
            (s.Street != '' ? ' ' : '') +
            s.City +
            (s.City != '' ? ' ' : '') +
            s.Province))
            .forMember('email', (otp) => otp.mapFrom((s) => s.Email))
            .forMember('birthday', (otp) => otp.mapFrom((s) => moment(s.Birthday).format('YYYY-MM-DD').toString()))
            .forMember('isActive', (otp) => otp.mapFrom((s) => s.IsActive))
            .forMember('createdDate', (otp) => otp.mapFrom((s) => moment(s.CreatedDate).format('YYYY-MM-DD').toString()));
    }
};
AccountProfile = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], AccountProfile);
exports.default = AccountProfile;
//# sourceMappingURL=account-mapping.js.map