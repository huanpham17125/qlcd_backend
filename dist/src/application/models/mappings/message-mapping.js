"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageMapping = void 0;
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const create_message_command_1 = require("../../messages/commands/create/create-message-command");
const res_get_message_by_account_id_1 = require("../../messages/viewModels/res-get-message-by-account-id");
const message_entity_gateway_1 = require("../../../domain/entities/message.entity-gateway");
const domainEnum_1 = require("../../../domain/enum/domainEnum");
let MessageMapping = class MessageMapping extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(message_entity_gateway_1.default, res_get_message_by_account_id_1.ResGetMessageByAccountId)
            .forMember('id', (src) => src.mapFrom((o) => o.Id))
            .forMember('senderId', (src) => src.mapFrom((o) => o.AccSenderId))
            .forMember('senderName', (src) => src.mapFrom((o) => o.Account.FullName))
            .forMember('senderAvatar', (src) => src.mapFrom((o) => process.env.APP_DOMAIN + o.AccSenderId))
            .forMember('typeMessage', (src) => src.mapFrom((o) => o.TypeContent))
            .forMember('content', (src) => src.mapFrom((o) => o.TypeContent == domainEnum_1.TypeContentMessage.Document
            ? o.Content
            : JSON.parse(o.Content)))
            .forMember('createdDate', (src) => src.mapFrom((o) => o.CreatedDate))
            .forMember('status', (src) => src.mapFrom((o) => o.Status));
    }
};
MessageMapping = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], MessageMapping);
exports.MessageMapping = MessageMapping;
//# sourceMappingURL=message-mapping.js.map