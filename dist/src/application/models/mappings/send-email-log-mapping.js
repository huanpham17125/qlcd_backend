"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendEmailLogMapping = void 0;
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const create_send_mail_log_command_1 = require("../../sendMailLogs/commands/create/create-send-mail-log-command");
const send_mail_log_entity_1 = require("../../../domain/entities/send-mail-log.entity");
let SendEmailLogMapping = class SendEmailLogMapping extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(create_send_mail_log_command_1.CreateSendMailLogCommand, send_mail_log_entity_1.default)
            .forMember('AccountId', (otp) => otp.mapFrom((o) => o.accountId))
            .forMember('MailSender', (otp) => otp.mapFrom((o) => o.mailSender))
            .forMember('MailTo', (otp) => otp.mapFrom((o) => o.mailTo))
            .forMember('MailCc', (otp) => otp.mapFrom((o) => o.mailCc))
            .forMember('MailBcc', (otp) => otp.mapFrom((o) => o.mailBcc))
            .forMember('MailSubject', (otp) => otp.mapFrom((o) => o.mailSubject))
            .forMember('MailBody', (otp) => otp.mapFrom((o) => o.mailBody))
            .forMember('MailType', (otp) => otp.mapFrom((o) => o.mailType))
            .forMember('MailAttach', (otp) => otp.mapFrom((o) => o.mailAttac));
    }
};
SendEmailLogMapping = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], SendEmailLogMapping);
exports.SendEmailLogMapping = SendEmailLogMapping;
//# sourceMappingURL=send-email-log-mapping.js.map