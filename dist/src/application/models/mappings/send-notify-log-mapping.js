"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotifyLogMapping = void 0;
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const create_notify_message_command_1 = require("../../sendNotifiMessage/commands/create/create-notify-message-command");
const send_notify_log_entity_1 = require("../../../domain/entities/send-notify-log.entity");
let SendNotifyLogMapping = class SendNotifyLogMapping extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(create_notify_message_command_1.CreateNotifyMessageCommand, send_notify_log_entity_1.default)
            .forMember('AccountId', (otp) => otp.mapFrom((o) => o.toAccountId))
            .forMember('Body', (otp) => otp.mapFrom((o) => o.req.body))
            .forMember('Title', (otp) => otp.mapFrom((o) => o.req.title))
            .forMember('Msg', (otp) => otp.mapFrom((o) => o.req.message))
            .forMember('CreatedById', (otp) => otp.mapFrom((o) => o.fromAccountId))
            .forMember('UpdatedById', (otp) => otp.mapFrom((o) => o.fromAccountId));
    }
};
SendNotifyLogMapping = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], SendNotifyLogMapping);
exports.SendNotifyLogMapping = SendNotifyLogMapping;
//# sourceMappingURL=send-notify-log-mapping.js.map