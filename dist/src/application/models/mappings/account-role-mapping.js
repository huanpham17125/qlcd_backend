"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountRoleMapping = void 0;
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const create_account_role_query_1 = require("../../accountRoles/commands/create/create-account-role-query");
const account_role_entity_1 = require("../../../domain/entities/account-role.entity");
let AccountRoleMapping = class AccountRoleMapping extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(create_account_role_query_1.CreateAccountRoleQuery, account_role_entity_1.default)
            .forMember('AccountId', (otp) => otp.mapFrom((s) => s.accountId))
            .forMember('RoleId', (otp) => otp.mapFrom((s) => s.roleId));
    }
};
AccountRoleMapping = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], AccountRoleMapping);
exports.AccountRoleMapping = AccountRoleMapping;
//# sourceMappingURL=account-role-mapping.js.map