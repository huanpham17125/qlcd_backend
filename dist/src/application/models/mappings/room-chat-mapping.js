"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomChatMapping = void 0;
const automapper_nartc_1 = require("automapper-nartc");
const nestjsx_automapper_1 = require("nestjsx-automapper");
const create_room_chat_command_1 = require("../../roomChats/commands/create/create-room-chat-command");
const room_chat_entity_gateway_1 = require("../../../domain/entities/room-chat.entity-gateway");
let RoomChatMapping = class RoomChatMapping extends automapper_nartc_1.MappingProfileBase {
    constructor() {
        super();
    }
    configure() {
        this.createMap(create_room_chat_command_1.CreateRoomChatCommand, room_chat_entity_gateway_1.default)
            .forMember('CreatedById', (otp) => otp.mapFrom((o) => o.accountId))
            .forMember('UpdatedById', (otp) => otp.mapFrom((o) => o.accountId))
            .forMember('Name', (otp) => otp.mapFrom((o) => o.name))
            .forMember('Tittle', (otp) => otp.mapFrom((o) => o.name));
    }
};
RoomChatMapping = __decorate([
    nestjsx_automapper_1.Profile(),
    __metadata("design:paramtypes", [])
], RoomChatMapping);
exports.RoomChatMapping = RoomChatMapping;
//# sourceMappingURL=room-chat-mapping.js.map