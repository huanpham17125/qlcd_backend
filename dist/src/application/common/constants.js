"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Constants = void 0;
class Constants {
}
exports.Constants = Constants;
Constants.DB_CONNECTION = 'test';
Constants.DEFAULT_DATE = '1900-01-01';
Constants.DEFAULT_FORMAT_DATE = 'YYYY-MM-DD';
Constants.AppProvide = {
    ACCOUNT_PROVIDE: 'ACCOUNTS',
    ACCOUNT_LOG_PROVIDE: 'ACCOUNTLOGS',
    ACCOUNT_ROLE: 'ACCOUNTROLES',
    ACCOUNT_SETTING_PROVIDE: 'ACCOUNTSETTINGS',
    APP_PROVIDE: 'ACCOUNTS',
    ROLE_PROVIDE: 'ROLES',
    ROOM_CHAT_PROVIDE: 'ROOMCHATS',
    ACCOUNT_ROOM_PROVIDE: 'ACCOUNTROOMS',
    MESSAGE_PROVIDE: 'MESSAGES',
    SEND_NOTIFYMESSAGE_LOG_PROVIDE: 'SENDNOTIFYMESSAGELOGS',
    SEND_MAIL_LOG_PROVIDE: 'SENDMAILLOGPROVIDE',
};
Constants.AppStatus = {
    StatusCode200: 200,
    StatusCode201: 201,
    StatusCode400: 400,
    StatusCode401: 401,
    StatusCode404: 404,
    StatusCode422: 422,
    StatusCode500: 422,
};
Constants.IsOnlineAccount = {
    Online: true,
    Offline: false,
};
Constants.AppPolicy = {
    SUPER_ADMIN: 'SUPER_ADMIN',
    ADMIN: 'ADMIN',
    USER: 'USER',
};
Constants.AppSubjectSendEmail = {
    UPDATE_REGISTER_OFFCIAL: 'Please complete the registration process',
    INVITE_AN_ACCOUNT: 'You have an invitation to join APP',
    RESET_PASSWORD: 'Please complete the password reset process',
};
Constants.AppResource = {
    ACCTION_SUCCESS: 'Success!',
    EXCEPTION_UNKNOWN: 'Unknown exception!',
    ADD_SUCCESS: 'Add success!',
    UPDATE_SUCCESS: 'Update success!',
    UPDATE_FAIL: 'Update fail. Please try again!',
    DELETE_SUCCESS: 'Delete success!',
    DELETE_FAIL: 'Delete fail! Please try again',
    ERROR_UNKNOWN: 'Sorry. Error server!',
    NOT_FOUND: 'Not Found in System!',
    UNAUTHORIZE: 'Unthorize! No right.',
    NO_AUTHORIZE: 'Authorize empty.',
    TOKEN_EXPIRED: 'UNAUTHORIZE! Token expired.',
    ACCESSTOKEN_EMPTY: 'Access token is not empty',
    FILE_NOT_TYPE_IMAGE: 'Please chose file type image',
    WARNING_UPDATING: 'Warning! This function is updating.',
    LOGIN_SUCCESS: 'Login success!',
    REGISTER_SUCCESS: 'Register success!',
    ACCOUNT_NOT_FOUND: 'Account not found!',
    ACCOUNT_WAS_LOCKED: 'Account was locked!',
    ACCOUNT_STATUS_NOT_CHANGE: 'Account status not change!',
    ACCOUNT_ALREADY_EXISTED: 'Account already existed!',
    EMAIL_ALREADY_EXISTED: 'Email already existed!',
    EMAIL_NOT_ALREADY_EXISTED: 'Email does not exist in the system.',
    EMAIL_NOT_FOUND: 'Email not found!',
    PASSWORD_INCORECT: 'Password incorect!',
    PASSWORD_DISPARITY: 'Password confirm disparity with password new.',
    MESSAGE_NOT_FOUND: 'Message is not found!',
    NOTIFY_MESSAGE_NOT_FOUND: 'Message is not found!',
    LOG_IN_EMAIL_TO_RESET_PASSWORD: 'Please login to your email to continue the password reset process',
    RESET_PASSWORD_SUCCESS: 'Successful password retrieval',
};
//# sourceMappingURL=constants.js.map