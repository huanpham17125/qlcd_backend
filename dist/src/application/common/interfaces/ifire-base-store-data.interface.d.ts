export interface IFireBaseStoreData {
    id: string;
    ref: any;
    serverKey: string;
}
