"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnKnowException = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants");
class UnKnowException extends common_1.HttpException {
    constructor(message) {
        super(message ? message : constants_1.Constants.AppResource.ERROR_UNKNOWN, constants_1.Constants.AppStatus.StatusCode500);
    }
}
exports.UnKnowException = UnKnowException;
//# sourceMappingURL=un-know-exception.js.map