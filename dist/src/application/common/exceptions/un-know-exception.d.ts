import { HttpException } from '@nestjs/common';
export declare class UnKnowException extends HttpException {
    constructor(message: string);
}
