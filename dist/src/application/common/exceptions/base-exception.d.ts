export declare class BaseException extends Error {
    private readonly response;
    private readonly status;
    constructor(status: number, response: string | Record<string, any>);
    initMessage(): void;
    getResponse(): string | object;
    getStatus(): number;
    static createBody(objectOrError: object | string, message?: string, statusCode?: number): object;
}
