"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadRequestException = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants");
class BadRequestException extends common_1.HttpException {
    constructor(message, code) {
        super(message, code ? code : constants_1.Constants.AppStatus.StatusCode400);
    }
}
exports.BadRequestException = BadRequestException;
//# sourceMappingURL=bad-request-exception.js.map