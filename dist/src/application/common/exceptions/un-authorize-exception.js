"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnAuthorizeException = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants");
class UnAuthorizeException extends common_1.HttpException {
    constructor(message) {
        super(message ? message : constants_1.Constants.AppResource.UNAUTHORIZE, constants_1.Constants.AppStatus.StatusCode401);
    }
}
exports.UnAuthorizeException = UnAuthorizeException;
//# sourceMappingURL=un-authorize-exception.js.map