"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotFoundException = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants");
class NotFoundException extends common_1.HttpException {
    constructor(message) {
        super(message ? message : constants_1.Constants.AppResource.NOT_FOUND, constants_1.Constants.AppStatus.StatusCode404);
    }
}
exports.NotFoundException = NotFoundException;
//# sourceMappingURL=not-found-exception.js.map