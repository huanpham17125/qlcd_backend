"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationException = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../constants");
class ValidationException extends common_1.HttpException {
    constructor(message) {
        super(message[0].constraints
            ? Object.values(message[0].constraints).toString()
            : getMessageError(message[0].children[0]), constants_1.Constants.AppStatus.StatusCode422);
    }
}
exports.ValidationException = ValidationException;
function getMessageError(childArray) {
    if (childArray.constraints) {
        const msg = Object.values(childArray.constraints).toString();
        return msg;
    }
    else {
        return getMessageError(childArray.children[0]);
    }
}
//# sourceMappingURL=validation-exception.js.map