import { HttpException } from '@nestjs/common';
export declare class UnAuthorizeException extends HttpException {
    constructor(message: string);
}
