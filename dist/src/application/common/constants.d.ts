export declare class Constants {
    static DB_CONNECTION: string;
    static DEFAULT_DATE: string;
    static DEFAULT_FORMAT_DATE: string;
    static AppProvide: {
        ACCOUNT_PROVIDE: string;
        ACCOUNT_LOG_PROVIDE: string;
        ACCOUNT_ROLE: string;
        ACCOUNT_SETTING_PROVIDE: string;
        APP_PROVIDE: string;
        ROLE_PROVIDE: string;
        ROOM_CHAT_PROVIDE: string;
        ACCOUNT_ROOM_PROVIDE: string;
        MESSAGE_PROVIDE: string;
        SEND_NOTIFYMESSAGE_LOG_PROVIDE: string;
        SEND_MAIL_LOG_PROVIDE: string;
    };
    static AppStatus: {
        StatusCode200: number;
        StatusCode201: number;
        StatusCode400: number;
        StatusCode401: number;
        StatusCode404: number;
        StatusCode422: number;
        StatusCode500: number;
    };
    static IsOnlineAccount: {
        Online: boolean;
        Offline: boolean;
    };
    static AppPolicy: {
        SUPER_ADMIN: string;
        ADMIN: string;
        USER: string;
    };
    static AppSubjectSendEmail: {
        UPDATE_REGISTER_OFFCIAL: string;
        INVITE_AN_ACCOUNT: string;
        RESET_PASSWORD: string;
    };
    static AppResource: {
        ACCTION_SUCCESS: string;
        EXCEPTION_UNKNOWN: string;
        ADD_SUCCESS: string;
        UPDATE_SUCCESS: string;
        UPDATE_FAIL: string;
        DELETE_SUCCESS: string;
        DELETE_FAIL: string;
        ERROR_UNKNOWN: string;
        NOT_FOUND: string;
        UNAUTHORIZE: string;
        NO_AUTHORIZE: string;
        TOKEN_EXPIRED: string;
        ACCESSTOKEN_EMPTY: string;
        FILE_NOT_TYPE_IMAGE: string;
        WARNING_UPDATING: string;
        LOGIN_SUCCESS: string;
        REGISTER_SUCCESS: string;
        ACCOUNT_NOT_FOUND: string;
        ACCOUNT_WAS_LOCKED: string;
        ACCOUNT_STATUS_NOT_CHANGE: string;
        ACCOUNT_ALREADY_EXISTED: string;
        EMAIL_ALREADY_EXISTED: string;
        EMAIL_NOT_ALREADY_EXISTED: string;
        EMAIL_NOT_FOUND: string;
        PASSWORD_INCORECT: string;
        PASSWORD_DISPARITY: string;
        MESSAGE_NOT_FOUND: string;
        NOTIFY_MESSAGE_NOT_FOUND: string;
        LOG_IN_EMAIL_TO_RESET_PASSWORD: string;
        RESET_PASSWORD_SUCCESS: string;
    };
}
