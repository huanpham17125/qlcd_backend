"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const constants_1 = require("../constants");
class DateTimeExtension {
    constructor() { }
    static stringToDate(datetime, format, dtDefault) {
        if (moment(datetime, format, true).isValid()) {
            return moment(datetime, format).toDate();
        }
        else {
            return dtDefault;
        }
    }
    static dateToString(datetime, format) {
        return moment(datetime).format(format).toString();
    }
    static getLifeExpectancy(birthday, dateOfDeath) {
        const diff = dateOfDeath.getTime() - birthday.getTime();
        return Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
    }
    static getFirstAndLastDayByMonthAndYear(month, year) {
        const firstDay = new Date(Number(year), Number(month) - 1, 1);
        const lastDay = new Date(Number(year), Number(month), 0);
        return {
            firstDay: moment(firstDay).format(constants_1.Constants.DEFAULT_FORMAT_DATE),
            lastDay: moment(lastDay).format(constants_1.Constants.DEFAULT_FORMAT_DATE),
        };
    }
}
exports.default = DateTimeExtension;
//# sourceMappingURL=date-time-extension.js.map