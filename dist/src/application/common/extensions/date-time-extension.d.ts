export default class DateTimeExtension {
    constructor();
    static stringToDate(datetime: string, format: string, dtDefault: Date): Date;
    static dateToString(datetime: Date, format: string): string;
    static getLifeExpectancy(birthday: Date, dateOfDeath: Date): number;
    static getFirstAndLastDayByMonthAndYear(month: number, year: number): {
        firstDay: string;
        lastDay: string;
    };
}
