import { ICommand } from '@nestjs/cqrs';
export declare class CreateAccountRoleQuery implements ICommand {
    accountId: string;
    roleId: string;
    constructor(accountId: string, roleId: string);
}
