"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountRoleQuery = void 0;
class CreateAccountRoleQuery {
    constructor(accountId, roleId) {
        this.accountId = accountId;
        this.roleId = roleId;
    }
}
exports.CreateAccountRoleQuery = CreateAccountRoleQuery;
//# sourceMappingURL=create-account-role-query.js.map