"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountRoleHandler = void 0;
const common_1 = require("@nestjs/common");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const account_role_entity_1 = require("../../../../domain/entities/account-role.entity");
const account_role_repository_1 = require("../../../../infrastructure/repositories/account-role-repository");
let CreateAccountRoleHandler = class CreateAccountRoleHandler {
    constructor(asyncRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const entity = this._mapper.map(command, account_role_entity_1.default);
        return this.asyncRepository.add(entity);
    }
};
CreateAccountRoleHandler = __decorate([
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_ROLE)),
    __param(1, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [account_role_repository_1.AccountRoleRepository,
        automapper_nartc_1.AutoMapper])
], CreateAccountRoleHandler);
exports.CreateAccountRoleHandler = CreateAccountRoleHandler;
//# sourceMappingURL=create-account-role-handler.js.map