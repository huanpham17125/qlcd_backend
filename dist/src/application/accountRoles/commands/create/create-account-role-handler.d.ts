import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import AccountRole from 'src/domain/entities/account-role.entity';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
import { CreateAccountRoleQuery } from './create-account-role-query';
export declare class CreateAccountRoleHandler implements ICommandHandler<CreateAccountRoleQuery> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: AccountRoleRepository, _mapper: AutoMapper);
    execute(command: CreateAccountRoleQuery): Promise<AccountRole>;
}
