"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetMessagePaginationByRoomIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const constants_1 = require("../../common/constants");
const message_entity_gateway_1 = require("../../../domain/entities/message.entity-gateway");
const iaccount_async_repository_interface_1 = require("../../../domain/interfaces/iaccount-async-repository.interface");
const imessage_repository_interface_1 = require("../../../domain/interfaces/imessage-repository.interface");
const res_get_message_by_account_id_1 = require("../viewModels/res-get-message-by-account-id");
const get_message_pagination_by_room_id_query_1 = require("./get-message-pagination-by-room-id-query");
let GetMessagePaginationByRoomIdHandler = class GetMessagePaginationByRoomIdHandler {
    constructor(_asyncRepository, accountAsyncRepository, _mapper) {
        this._asyncRepository = _asyncRepository;
        this.accountAsyncRepository = accountAsyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const lstMessage = await this._asyncRepository.getMessagePaginationByAccountId(command.accountId, command.pageSize | 20, command.pageNumber | 1);
        for (let i = 0; i <= lstMessage.items.length - 1; i++) {
            lstMessage.items[i].Account = await this.accountAsyncRepository.getById(lstMessage.items[i].AccSenderId);
        }
        const resMessage = new nestjs_typeorm_paginate_1.Pagination(this._mapper.mapArray(lstMessage.items, res_get_message_by_account_id_1.ResGetMessageByAccountId), lstMessage.meta);
        return resMessage;
    }
};
GetMessagePaginationByRoomIdHandler = __decorate([
    cqrs_1.CommandHandler(get_message_pagination_by_room_id_query_1.GetMessagePaginationByRoomIdQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.MESSAGE_PROVIDE)),
    __param(1, common_1.Inject(constants_1.Constants.AppProvide.APP_PROVIDE)),
    __param(2, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, Object, automapper_nartc_1.AutoMapper])
], GetMessagePaginationByRoomIdHandler);
exports.GetMessagePaginationByRoomIdHandler = GetMessagePaginationByRoomIdHandler;
//# sourceMappingURL=get-message-pagination-by-room-id-handler.js.map