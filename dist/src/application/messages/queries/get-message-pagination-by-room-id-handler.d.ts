import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { GetMessagePaginationByRoomIdQuery } from './get-message-pagination-by-room-id-query';
export declare class GetMessagePaginationByRoomIdHandler implements ICommandHandler<GetMessagePaginationByRoomIdQuery> {
    private readonly _asyncRepository;
    private readonly accountAsyncRepository;
    private readonly _mapper;
    constructor(_asyncRepository: ImessageRepository, accountAsyncRepository: IaccountAsyncRepository, _mapper: AutoMapper);
    execute(command: GetMessagePaginationByRoomIdQuery): Promise<any>;
}
