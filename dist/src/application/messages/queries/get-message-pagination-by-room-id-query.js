"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetMessagePaginationByRoomIdQuery = void 0;
class GetMessagePaginationByRoomIdQuery {
    constructor(accountId, pageSize, pageNumber) {
        this.accountId = accountId;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }
}
exports.GetMessagePaginationByRoomIdQuery = GetMessagePaginationByRoomIdQuery;
//# sourceMappingURL=get-message-pagination-by-room-id-query.js.map