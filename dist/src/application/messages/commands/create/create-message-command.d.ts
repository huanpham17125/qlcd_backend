import { ICommand } from '@nestjs/cqrs';
export declare class CreateMessageCommand implements ICommand {
    senderId: string;
    templeId: string;
    content: string;
    scheduleId: number;
    constructor(senderId: string, templeId: string, content: string, scheduleId: number);
}
