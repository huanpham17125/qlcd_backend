"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMessageHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const message_entity_gateway_1 = require("../../../../domain/entities/message.entity-gateway");
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
const iaccount_async_repository_interface_1 = require("../../../../domain/interfaces/iaccount-async-repository.interface");
const imessage_repository_interface_1 = require("../../../../domain/interfaces/imessage-repository.interface");
const iroom_chat_repository_interface_1 = require("../../../../domain/interfaces/iroom-chat-repository.interface");
const res_get_message_by_account_id_1 = require("../../viewModels/res-get-message-by-account-id");
const create_message_command_1 = require("./create-message-command");
let CreateMessageHandler = class CreateMessageHandler {
    constructor(asyncRepository, roomChatRepository, accountAsyncRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this.roomChatRepository = roomChatRepository;
        this.accountAsyncRepository = accountAsyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const itemRoom = await this.roomChatRepository.getByTempleId(command.templeId);
        const reqMessage = new message_entity_gateway_1.default();
        reqMessage.AccSenderId = command.senderId;
        reqMessage.Status = domainEnum_1.StatusMessage.Sent;
        reqMessage.ReplyId = 0;
        reqMessage.RoomId = itemRoom.Id;
        reqMessage.TypeContent = domainEnum_1.TypeContentMessage.Document;
        reqMessage.Content = command.content;
        const entity = await this.asyncRepository.add(reqMessage);
        entity.Account = await this.accountAsyncRepository.getById(command.senderId);
        return this._mapper.map(entity, res_get_message_by_account_id_1.ResGetMessageByAccountId);
    }
};
CreateMessageHandler = __decorate([
    cqrs_1.CommandHandler(create_message_command_1.CreateMessageCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.MESSAGE_PROVIDE)),
    __param(1, common_1.Inject(constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE)),
    __param(2, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_PROVIDE)),
    __param(3, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, Object, Object, automapper_nartc_1.AutoMapper])
], CreateMessageHandler);
exports.CreateMessageHandler = CreateMessageHandler;
//# sourceMappingURL=create-message-handler.js.map