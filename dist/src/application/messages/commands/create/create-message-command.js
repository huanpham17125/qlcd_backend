"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMessageCommand = void 0;
class CreateMessageCommand {
    constructor(senderId, templeId, content, scheduleId) {
        this.senderId = senderId;
        this.templeId = templeId;
        this.content = content;
        this.scheduleId = scheduleId;
    }
}
exports.CreateMessageCommand = CreateMessageCommand;
//# sourceMappingURL=create-message-command.js.map