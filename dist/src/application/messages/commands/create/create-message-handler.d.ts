import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { ResGetMessageByAccountId } from '../../viewModels/res-get-message-by-account-id';
import { CreateMessageCommand } from './create-message-command';
export declare class CreateMessageHandler implements ICommandHandler<CreateMessageCommand> {
    private readonly asyncRepository;
    private readonly roomChatRepository;
    private readonly accountAsyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ImessageRepository, roomChatRepository: IroomChatRepository, accountAsyncRepository: IaccountAsyncRepository, _mapper: AutoMapper);
    execute(command: CreateMessageCommand): Promise<ResGetMessageByAccountId>;
}
