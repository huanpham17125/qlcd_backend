import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { UpdateStatusMessageCommand } from './update-status-message-command';
export declare class UpdateStatusMessageHandler implements ICommandHandler<UpdateStatusMessageCommand> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ImessageRepository, _mapper: AutoMapper);
    execute(command: UpdateStatusMessageCommand): Promise<any>;
}
