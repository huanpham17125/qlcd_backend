"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateContentMessageByIdCommand = void 0;
class UpdateContentMessageByIdCommand {
    constructor(messageId, accountId, content) {
        this.messageId = messageId;
        this.accountId = accountId;
        this.content = content;
    }
}
exports.UpdateContentMessageByIdCommand = UpdateContentMessageByIdCommand;
//# sourceMappingURL=update-content-message-by-id-command.js.map