import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import Message from 'src/domain/entities/message.entity-gateway';
import { ImessageRepository } from 'src/domain/interfaces/imessage-repository.interface';
import { UpdateContentMessageByIdCommand } from './update-content-message-by-id-command';
export declare class UpdateContentMessageByIdHandler implements ICommandHandler<UpdateContentMessageByIdCommand> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ImessageRepository, _mapper: AutoMapper);
    execute(command: UpdateContentMessageByIdCommand): Promise<Message>;
}
