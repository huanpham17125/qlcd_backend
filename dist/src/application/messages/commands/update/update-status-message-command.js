"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateStatusMessageCommand = void 0;
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
class UpdateStatusMessageCommand {
    constructor(messageId, status) {
        this.messageId = messageId;
        this.status = status;
    }
}
exports.UpdateStatusMessageCommand = UpdateStatusMessageCommand;
//# sourceMappingURL=update-status-message-command.js.map