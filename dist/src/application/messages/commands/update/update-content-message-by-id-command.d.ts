import { ICommand } from '@nestjs/cqrs';
export declare class UpdateContentMessageByIdCommand implements ICommand {
    messageId: string;
    accountId: string;
    content: string;
    constructor(messageId: string, accountId: string, content: string);
}
