"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateContentMessageByIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const message_entity_gateway_1 = require("../../../../domain/entities/message.entity-gateway");
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
const imessage_repository_interface_1 = require("../../../../domain/interfaces/imessage-repository.interface");
const update_content_message_by_id_command_1 = require("./update-content-message-by-id-command");
let UpdateContentMessageByIdHandler = class UpdateContentMessageByIdHandler {
    constructor(asyncRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const entity = await this.asyncRepository.getById(command.messageId);
        if (!entity) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.MESSAGE_NOT_FOUND);
        }
        entity.Status = domainEnum_1.StatusMessage.Updated;
        entity.UpdatedDate = new Date();
        entity.Content = command.content;
        const isUpdate = await this.asyncRepository.updateById(entity, entity.Id);
        if (!isUpdate) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.UPDATE_FAIL);
        }
        return entity;
    }
};
UpdateContentMessageByIdHandler = __decorate([
    cqrs_1.CommandHandler(update_content_message_by_id_command_1.UpdateContentMessageByIdCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.MESSAGE_PROVIDE)),
    __param(1, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, automapper_nartc_1.AutoMapper])
], UpdateContentMessageByIdHandler);
exports.UpdateContentMessageByIdHandler = UpdateContentMessageByIdHandler;
//# sourceMappingURL=update-content-message-by-id-handler.js.map