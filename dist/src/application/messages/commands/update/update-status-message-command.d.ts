import { ICommand } from "@nestjs/cqrs";
import { StatusMessage } from "src/domain/enum/domainEnum";
export declare class UpdateStatusMessageCommand implements ICommand {
    messageId: number;
    status: StatusMessage;
    constructor(messageId: number, status: StatusMessage);
}
