import { TypeContentMessage } from 'src/domain/enum/domainEnum';
export declare class ResGetMessageByAccountId {
    id: number;
    typeMessage: TypeContentMessage;
    content: any;
    senderId: string;
    senderName: string;
    senderAvatar: string;
    createdDate: Date;
    status: number;
}
