export declare class ReqContentMessageSchedule {
    typeSchedule: string;
    dankaName: string;
    houseHolderName: string;
    departedPersonName: string;
    eventDate: Date;
    startTime: string;
    endTime: string;
    place: string;
    constructor(typeSchedule: string, dankaName: string, houseHolderName: string, departedPersonName: string, eventDate: Date, start: string, end: string, place: string);
}
