"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqContentMessageSchedule = void 0;
class ReqContentMessageSchedule {
    constructor(typeSchedule, dankaName, houseHolderName, departedPersonName, eventDate, start, end, place) {
        (this.typeSchedule = typeSchedule),
            (this.dankaName = dankaName),
            (this.houseHolderName = houseHolderName),
            (this.departedPersonName = departedPersonName),
            (this.eventDate = eventDate),
            (this.startTime = start),
            (this.endTime = end),
            (this.place = place);
        return this;
    }
}
exports.ReqContentMessageSchedule = ReqContentMessageSchedule;
//# sourceMappingURL=req-content-message-schedule.js.map