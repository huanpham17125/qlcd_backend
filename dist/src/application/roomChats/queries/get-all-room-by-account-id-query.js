"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllRoomByAccountIdQuery = void 0;
class GetAllRoomByAccountIdQuery {
    constructor(accountId, pageSize, pageNumber) {
        this.accountId = accountId;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }
}
exports.GetAllRoomByAccountIdQuery = GetAllRoomByAccountIdQuery;
//# sourceMappingURL=get-all-room-by-account-id-query.js.map