"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoomByAccountIdQuery = void 0;
class GetRoomByAccountIdQuery {
    constructor(accountId) {
        this.accountId = accountId;
    }
}
exports.GetRoomByAccountIdQuery = GetRoomByAccountIdQuery;
//# sourceMappingURL=get-room-by-account-id-query.js.map