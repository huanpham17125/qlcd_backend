"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoomByCreatorIdAndCreatedIdQuery = void 0;
class GetRoomByCreatorIdAndCreatedIdQuery {
    constructor(creatorId, createdId) {
        this.creatorId = creatorId;
        this.createdId = createdId;
    }
}
exports.GetRoomByCreatorIdAndCreatedIdQuery = GetRoomByCreatorIdAndCreatedIdQuery;
//# sourceMappingURL=get-room-by-creator-id-and-created-id-query.js.map