import { IQueryHandler } from '@nestjs/cqrs';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { GetRoomByTempleIdQuery } from './get-room-by-temple-id-query';
export declare class GetRoomByTempleIdHandler implements IQueryHandler<GetRoomByTempleIdQuery> {
    private readonly asyncRepository;
    constructor(asyncRepository: IroomChatRepository);
    execute(query: GetRoomByTempleIdQuery): Promise<any>;
}
