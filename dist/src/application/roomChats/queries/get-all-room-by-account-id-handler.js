"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllRoomByAccountIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const room_chat_repository_1 = require("../../../infrastructure/repositories/room-chat-repository");
const get_all_room_by_account_id_query_1 = require("./get-all-room-by-account-id-query");
let GetAllRoomByAccountIdHandler = class GetAllRoomByAccountIdHandler {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
    }
    async execute(command) {
        return await this.asyncRepository.getAllByAccountId(command.accountId, command.pageSize || 20, command.pageNumber || 1);
    }
};
GetAllRoomByAccountIdHandler = __decorate([
    cqrs_1.CommandHandler(get_all_room_by_account_id_query_1.GetAllRoomByAccountIdQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE)),
    __metadata("design:paramtypes", [room_chat_repository_1.RoomChatRepository])
], GetAllRoomByAccountIdHandler);
exports.GetAllRoomByAccountIdHandler = GetAllRoomByAccountIdHandler;
//# sourceMappingURL=get-all-room-by-account-id-handler.js.map