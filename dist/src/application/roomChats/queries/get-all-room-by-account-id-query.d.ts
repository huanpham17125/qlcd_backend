import { ICommand } from "@nestjs/cqrs";
export declare class GetAllRoomByAccountIdQuery implements ICommand {
    accountId: string;
    pageSize?: number;
    pageNumber?: number;
    constructor(accountId: string, pageSize?: number, pageNumber?: number);
}
