import { IQuery } from '@nestjs/cqrs';
export declare class GetRoomByAccountIdQuery implements IQuery {
    accountId: string;
    constructor(accountId: string);
}
