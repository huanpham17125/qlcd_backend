import { ICommandHandler } from '@nestjs/cqrs';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { GetRoomByCreatorIdAndCreatedIdQuery } from './get-room-by-creator-id-and-created-id-query';
export declare class GetRoomByCreatorIdAndCreatedIdHandler implements ICommandHandler<GetRoomByCreatorIdAndCreatedIdQuery> {
    private readonly asyncRepository;
    constructor(asyncRepository: RoomChatRepository);
    execute(command: GetRoomByCreatorIdAndCreatedIdQuery): Promise<RoomChat | null>;
}
