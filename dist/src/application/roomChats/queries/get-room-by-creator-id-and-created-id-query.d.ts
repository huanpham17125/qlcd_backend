import { ICommand } from "@nestjs/cqrs";
export declare class GetRoomByCreatorIdAndCreatedIdQuery implements ICommand {
    creatorId: string;
    createdId: string;
    constructor(creatorId: string, createdId: string);
}
