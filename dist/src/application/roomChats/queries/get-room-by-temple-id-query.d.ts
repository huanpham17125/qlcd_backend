import { IQuery } from "@nestjs/cqrs";
export declare class GetRoomByTempleIdQuery implements IQuery {
    templeId: string;
    constructor(templeId: string);
}
