"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoomByTempleIdQuery = void 0;
class GetRoomByTempleIdQuery {
    constructor(templeId) {
        this.templeId = templeId;
    }
}
exports.GetRoomByTempleIdQuery = GetRoomByTempleIdQuery;
//# sourceMappingURL=get-room-by-temple-id-query.js.map