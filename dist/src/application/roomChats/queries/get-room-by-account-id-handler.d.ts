import { IQueryHandler } from '@nestjs/cqrs';
import { IroomChatRepository } from 'src/domain/interfaces/iroom-chat-repository.interface';
import { GetRoomByAccountIdQuery } from './get-room-by-account-id-query';
export declare class GetRoomByAccountIdHandler implements IQueryHandler<GetRoomByAccountIdQuery> {
    private readonly asyncRepository;
    constructor(asyncRepository: IroomChatRepository);
    execute(query: GetRoomByAccountIdQuery): Promise<any>;
}
