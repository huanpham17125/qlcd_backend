"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetRoomByCreatorIdAndCreatedIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const room_chat_entity_gateway_1 = require("../../../domain/entities/room-chat.entity-gateway");
const room_chat_repository_1 = require("../../../infrastructure/repositories/room-chat-repository");
const get_room_by_creator_id_and_created_id_query_1 = require("./get-room-by-creator-id-and-created-id-query");
let GetRoomByCreatorIdAndCreatedIdHandler = class GetRoomByCreatorIdAndCreatedIdHandler {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
    }
    async execute(command) {
        const entity = await this.asyncRepository.getRoomByCreatorAndCreated(command.creatorId, command.createdId);
        return entity;
    }
};
GetRoomByCreatorIdAndCreatedIdHandler = __decorate([
    cqrs_1.CommandHandler(get_room_by_creator_id_and_created_id_query_1.GetRoomByCreatorIdAndCreatedIdQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE)),
    __metadata("design:paramtypes", [room_chat_repository_1.RoomChatRepository])
], GetRoomByCreatorIdAndCreatedIdHandler);
exports.GetRoomByCreatorIdAndCreatedIdHandler = GetRoomByCreatorIdAndCreatedIdHandler;
//# sourceMappingURL=get-room-by-creator-id-and-created-id-handler.js.map