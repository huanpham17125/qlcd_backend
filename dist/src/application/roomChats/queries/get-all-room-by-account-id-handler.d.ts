import { ICommandHandler } from '@nestjs/cqrs';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { GetAllRoomByAccountIdQuery } from './get-all-room-by-account-id-query';
export declare class GetAllRoomByAccountIdHandler implements ICommandHandler<GetAllRoomByAccountIdQuery> {
    private readonly asyncRepository;
    constructor(asyncRepository: RoomChatRepository);
    execute(command: GetAllRoomByAccountIdQuery): Promise<any>;
}
