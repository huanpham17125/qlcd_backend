import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import RoomChat from 'src/domain/entities/room-chat.entity-gateway';
import { AccountRoomRepository } from 'src/infrastructure/repositories/account-room-repository';
import { RoomChatRepository } from 'src/infrastructure/repositories/room-chat-repository';
import { CreateRoomChatCommand } from './create-room-chat-command';
export declare class CreateRoomChatHandler implements ICommandHandler<CreateRoomChatCommand> {
    private readonly asyncRepository;
    private readonly accountRoomRepository;
    private readonly _mapper;
    constructor(asyncRepository: RoomChatRepository, accountRoomRepository: AccountRoomRepository, _mapper: AutoMapper);
    execute(command: CreateRoomChatCommand): Promise<RoomChat>;
}
