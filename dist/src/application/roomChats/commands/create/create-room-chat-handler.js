"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRoomChatHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const account_room_entity_gateway_1 = require("../../../../domain/entities/account-room.entity-gateway");
const room_chat_entity_gateway_1 = require("../../../../domain/entities/room-chat.entity-gateway");
const account_room_repository_1 = require("../../../../infrastructure/repositories/account-room-repository");
const room_chat_repository_1 = require("../../../../infrastructure/repositories/room-chat-repository");
const create_room_chat_command_1 = require("./create-room-chat-command");
let CreateRoomChatHandler = class CreateRoomChatHandler {
    constructor(asyncRepository, accountRoomRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this.accountRoomRepository = accountRoomRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const req = this._mapper.map(command, room_chat_entity_gateway_1.default);
        const itemRoom = await this.asyncRepository.add(req);
        const reqCreateAccountRoom = new account_room_entity_gateway_1.default();
        reqCreateAccountRoom.AccountId = command.accountId;
        reqCreateAccountRoom.RoomId = itemRoom.Id;
        await this.accountRoomRepository.add(reqCreateAccountRoom);
        return itemRoom;
    }
};
CreateRoomChatHandler = __decorate([
    cqrs_1.CommandHandler(create_room_chat_command_1.CreateRoomChatCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE)),
    __param(1, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_ROOM_PROVIDE)),
    __param(2, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [room_chat_repository_1.RoomChatRepository,
        account_room_repository_1.AccountRoomRepository,
        automapper_nartc_1.AutoMapper])
], CreateRoomChatHandler);
exports.CreateRoomChatHandler = CreateRoomChatHandler;
//# sourceMappingURL=create-room-chat-handler.js.map