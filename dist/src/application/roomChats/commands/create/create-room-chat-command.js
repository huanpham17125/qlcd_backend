"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRoomChatCommand = void 0;
class CreateRoomChatCommand {
    constructor(accountId, name, templeId) {
        this.accountId = accountId;
        this.name = name;
        this.templeId = templeId;
    }
}
exports.CreateRoomChatCommand = CreateRoomChatCommand;
//# sourceMappingURL=create-room-chat-command.js.map