import { ICommand } from '@nestjs/cqrs';
export declare class CreateRoomChatCommand implements ICommand {
    accountId: string;
    name: string;
    templeId: string;
    constructor(accountId: string, name: string, templeId: string);
}
