import { ICommand } from '@nestjs/cqrs';
import { StatusSendNotify } from 'src/domain/enum/domainEnum';
export declare class UpdateNotifyMessageByIdCommand implements ICommand {
    id: number;
    accountId: string;
    status: StatusSendNotify;
    constructor(id: number, accountId: string, status: StatusSendNotify);
}
