"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateNotifyMessageByIdCommand = void 0;
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
class UpdateNotifyMessageByIdCommand {
    constructor(id, accountId, status) {
        this.id = id;
        this.accountId = accountId;
        this.status = status;
    }
}
exports.UpdateNotifyMessageByIdCommand = UpdateNotifyMessageByIdCommand;
//# sourceMappingURL=update-notify-message-by-id-command.js.map