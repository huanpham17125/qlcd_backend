import { ICommandHandler } from '@nestjs/cqrs';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { UpdateNotifyMessageByIdCommand } from './update-notify-message-by-id-command';
export declare class UpdateNotifyMessageByIdHandler implements ICommandHandler<UpdateNotifyMessageByIdCommand> {
    private readonly asyncRepository;
    constructor(asyncRepository: ISendNotifyRepository);
    execute(command: UpdateNotifyMessageByIdCommand): Promise<any>;
}
