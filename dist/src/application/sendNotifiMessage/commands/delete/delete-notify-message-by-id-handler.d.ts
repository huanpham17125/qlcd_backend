import { ICommandHandler } from '@nestjs/cqrs';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { DeleteNotifyMessageByIdCommand } from './delete-notify-message-by-id-command';
export declare class DeleteNotifyMessageByIdHandler implements ICommandHandler<DeleteNotifyMessageByIdCommand> {
    private readonly asyncRepository;
    constructor(asyncRepository: ISendNotifyRepository);
    execute(command: DeleteNotifyMessageByIdCommand): Promise<boolean>;
}
