"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteNotifyMessageByIdCommand = void 0;
class DeleteNotifyMessageByIdCommand {
    constructor(id, accountId) {
        this.id = id;
        this.accountId = accountId;
    }
}
exports.DeleteNotifyMessageByIdCommand = DeleteNotifyMessageByIdCommand;
//# sourceMappingURL=delete-notify-message-by-id-command.js.map