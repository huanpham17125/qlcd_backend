"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteNotifyMessageByIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const isend_notify_repository_interface_1 = require("../../../../domain/interfaces/isend-notify-repository.interface");
const delete_notify_message_by_id_command_1 = require("./delete-notify-message-by-id-command");
let DeleteNotifyMessageByIdHandler = class DeleteNotifyMessageByIdHandler {
    constructor(asyncRepository) {
        this.asyncRepository = asyncRepository;
    }
    async execute(command) {
        const item = await this.asyncRepository.getById(command.id);
        if (!item) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.NOTIFY_MESSAGE_NOT_FOUND);
        }
        item.UpdatedDate = new Date();
        item.UpdatedById = command.accountId;
        item.IsActive = false;
        await this.asyncRepository.updateById(item, item.Id);
        return true;
    }
};
DeleteNotifyMessageByIdHandler = __decorate([
    cqrs_1.CommandHandler(delete_notify_message_by_id_command_1.DeleteNotifyMessageByIdCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], DeleteNotifyMessageByIdHandler);
exports.DeleteNotifyMessageByIdHandler = DeleteNotifyMessageByIdHandler;
//# sourceMappingURL=delete-notify-message-by-id-handler.js.map