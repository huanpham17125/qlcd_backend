import { ICommand } from "@nestjs/cqrs";
export declare class DeleteNotifyMessageByIdCommand implements ICommand {
    id: number;
    accountId: string;
    constructor(id: number, accountId: string);
}
