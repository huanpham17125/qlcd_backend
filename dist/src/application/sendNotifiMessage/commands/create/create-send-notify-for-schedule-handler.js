"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSendNotifyForScheduleHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const send_notify_log_entity_1 = require("../../../../domain/entities/send-notify-log.entity");
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
const isend_notify_repository_interface_1 = require("../../../../domain/interfaces/isend-notify-repository.interface");
const create_send_notify_for_schedule_command_1 = require("./create-send-notify-for-schedule-command");
let CreateSendNotifyForScheduleHandler = class CreateSendNotifyForScheduleHandler {
    constructor(asyncRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const reqInsert = new send_notify_log_entity_1.default();
        reqInsert.AccountId = command.accountId;
        reqInsert.CreatedById = command.accountId;
        reqInsert.UpdatedById = command.accountId;
        reqInsert.Body = command.body;
        reqInsert.Title = command.title;
        reqInsert.Msg = command.msg;
        reqInsert.TypeNotify = command.typeNotify;
        reqInsert.Status = domainEnum_1.StatusSendNotify.Sent;
        return await this.asyncRepository.add(reqInsert);
    }
};
CreateSendNotifyForScheduleHandler = __decorate([
    cqrs_1.CommandHandler(create_send_notify_for_schedule_command_1.CreateSendNotifyForScheduleCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)),
    __param(1, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, automapper_nartc_1.AutoMapper])
], CreateSendNotifyForScheduleHandler);
exports.CreateSendNotifyForScheduleHandler = CreateSendNotifyForScheduleHandler;
//# sourceMappingURL=create-send-notify-for-schedule-handler.js.map