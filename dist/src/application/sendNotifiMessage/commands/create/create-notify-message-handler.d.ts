import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { CreateNotifyMessageCommand } from './create-notify-message-command';
export declare class CreateNotifyMessageHandler implements ICommandHandler<CreateNotifyMessageCommand> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ISendNotifyRepository, _mapper: AutoMapper);
    execute(command: CreateNotifyMessageCommand): Promise<any>;
}
