import { ICommand } from '@nestjs/cqrs';
import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';
export declare class CreateNotifyMessageCommand implements ICommand {
    fromAccountId: string;
    toAccountId: string;
    req: ReqCreateSendnotifyMessage;
    constructor(fromAccountId: string, toAccountId: string, req: ReqCreateSendnotifyMessage);
}
