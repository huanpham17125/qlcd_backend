import { ICommand } from '@nestjs/cqrs';
export declare class CreateSendNotifyForScheduleCommand implements ICommand {
    accountId: string;
    title: string;
    body: string;
    msg: string;
    typeNotify: number;
    status: any;
    detailScheduleId: number;
    constructor(accountId: string, title: string, body: string, msg: string, typeNotify: number, status: any, detailScheduleId: number);
}
