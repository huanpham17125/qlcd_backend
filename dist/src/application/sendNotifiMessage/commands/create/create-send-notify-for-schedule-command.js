"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSendNotifyForScheduleCommand = void 0;
class CreateSendNotifyForScheduleCommand {
    constructor(accountId, title, body, msg, typeNotify, status, detailScheduleId) {
        this.accountId = accountId;
        this.title = title;
        this.body = body;
        this.msg = msg;
        this.typeNotify = typeNotify;
        this.status = status;
        this.detailScheduleId = detailScheduleId;
    }
}
exports.CreateSendNotifyForScheduleCommand = CreateSendNotifyForScheduleCommand;
//# sourceMappingURL=create-send-notify-for-schedule-command.js.map