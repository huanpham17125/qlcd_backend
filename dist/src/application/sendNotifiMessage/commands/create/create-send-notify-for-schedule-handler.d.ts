import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import SendNotifyLog from 'src/domain/entities/send-notify-log.entity';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { CreateSendNotifyForScheduleCommand } from './create-send-notify-for-schedule-command';
export declare class CreateSendNotifyForScheduleHandler implements ICommandHandler<CreateSendNotifyForScheduleCommand> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ISendNotifyRepository, _mapper: AutoMapper);
    execute(command: CreateSendNotifyForScheduleCommand): Promise<SendNotifyLog>;
}
