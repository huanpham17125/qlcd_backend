"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateNotifyMessageCommand = void 0;
const req_create_sendnotify_message_1 = require("../../../../api/viewModels/req-create-sendnotify-message");
class CreateNotifyMessageCommand {
    constructor(fromAccountId, toAccountId, req) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.req = req;
    }
}
exports.CreateNotifyMessageCommand = CreateNotifyMessageCommand;
//# sourceMappingURL=create-notify-message-command.js.map