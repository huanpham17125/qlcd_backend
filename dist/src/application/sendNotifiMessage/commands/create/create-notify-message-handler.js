"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateNotifyMessageHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const send_notify_log_entity_1 = require("../../../../domain/entities/send-notify-log.entity");
const isend_notify_repository_interface_1 = require("../../../../domain/interfaces/isend-notify-repository.interface");
const create_notify_message_command_1 = require("./create-notify-message-command");
let CreateNotifyMessageHandler = class CreateNotifyMessageHandler {
    constructor(asyncRepository, _mapper) {
        this.asyncRepository = asyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const req = this._mapper.map(command, send_notify_log_entity_1.default);
        const entity = await this.asyncRepository.add(req);
        return entity;
    }
};
CreateNotifyMessageHandler = __decorate([
    cqrs_1.CommandHandler(create_notify_message_command_1.CreateNotifyMessageCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE)),
    __param(1, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, automapper_nartc_1.AutoMapper])
], CreateNotifyMessageHandler);
exports.CreateNotifyMessageHandler = CreateNotifyMessageHandler;
//# sourceMappingURL=create-notify-message-handler.js.map