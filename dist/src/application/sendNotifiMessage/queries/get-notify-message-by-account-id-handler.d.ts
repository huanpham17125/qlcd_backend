import { IQueryHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { ISendNotifyRepository } from 'src/domain/interfaces/isend-notify-repository.interface';
import { GetNotifyMessageByAccountIdQuery } from './get-notify-message-by-account-id-query';
export declare class GetNotifyMessageByAccountIdHandler implements IQueryHandler<GetNotifyMessageByAccountIdQuery> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ISendNotifyRepository, _mapper: AutoMapper);
    execute(query: GetNotifyMessageByAccountIdQuery): Promise<any>;
}
