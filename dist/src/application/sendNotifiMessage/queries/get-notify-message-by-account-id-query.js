"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetNotifyMessageByAccountIdQuery = void 0;
class GetNotifyMessageByAccountIdQuery {
    constructor(accountId) {
        this.accountId = accountId;
    }
}
exports.GetNotifyMessageByAccountIdQuery = GetNotifyMessageByAccountIdQuery;
//# sourceMappingURL=get-notify-message-by-account-id-query.js.map