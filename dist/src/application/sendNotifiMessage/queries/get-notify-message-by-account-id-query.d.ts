import { IQuery } from '@nestjs/cqrs';
export declare class GetNotifyMessageByAccountIdQuery implements IQuery {
    accountId: string;
    constructor(accountId: string);
}
