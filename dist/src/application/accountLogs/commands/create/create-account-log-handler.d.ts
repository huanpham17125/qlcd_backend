import { ICommandHandler } from '@nestjs/cqrs';
import AccountLog from 'src/domain/entities/account-log.entity';
import { IAsyncGenericRepository } from 'src/domain/interfaces/iasync-generic-repository.interface';
import { CreateAccountLogCommand } from './create-account-log-command';
export declare class CreateAccountLogHandler implements ICommandHandler<CreateAccountLogCommand> {
    private readonly _asyncRepository;
    constructor(_asyncRepository: IAsyncGenericRepository<AccountLog>);
    execute(command: CreateAccountLogCommand): Promise<void>;
}
