"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountLogCommand = void 0;
class CreateAccountLogCommand {
    constructor(accountId) {
        this.accountId = accountId;
    }
}
exports.CreateAccountLogCommand = CreateAccountLogCommand;
//# sourceMappingURL=create-account-log-command.js.map