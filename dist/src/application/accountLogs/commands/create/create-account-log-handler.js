"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountLogHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const account_log_entity_1 = require("../../../../domain/entities/account-log.entity");
const iasync_generic_repository_interface_1 = require("../../../../domain/interfaces/iasync-generic-repository.interface");
const create_account_log_command_1 = require("./create-account-log-command");
let CreateAccountLogHandler = class CreateAccountLogHandler {
    constructor(_asyncRepository) {
        this._asyncRepository = _asyncRepository;
    }
    async execute(command) {
        const reqInsert = new account_log_entity_1.default();
        reqInsert.AccountId = command.accountId;
        await this._asyncRepository.add(reqInsert);
    }
};
CreateAccountLogHandler = __decorate([
    cqrs_1.CommandHandler(create_account_log_command_1.CreateAccountLogCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_LOG_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], CreateAccountLogHandler);
exports.CreateAccountLogHandler = CreateAccountLogHandler;
//# sourceMappingURL=create-account-log-handler.js.map