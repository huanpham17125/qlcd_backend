import { ICommand } from '@nestjs/cqrs';
export declare class CreateAccountLogCommand implements ICommand {
    accountId: string;
    constructor(accountId: string);
}
