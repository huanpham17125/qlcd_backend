import { ICommand } from '@nestjs/cqrs';
export declare class CreateSendMailLogCommand implements ICommand {
    accountId: string;
    mailSender: string;
    mailTo: string;
    mailCc: string;
    mailBcc: string;
    mailSubject: string;
    mailBody: string;
    mailType: number;
    mailAttac: string;
    constructor(accountId: string, mailSender: string, mailTo: string, mailCc: string, mailBcc: string, mailSubject: string, mailBody: string, mailType: number, mailAttac: string);
}
