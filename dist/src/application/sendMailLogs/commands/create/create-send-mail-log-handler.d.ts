import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import SendMailLog from 'src/domain/entities/send-mail-log.entity';
import { ISendMailLog } from 'src/domain/interfaces/isend-mail-log.interface';
import { CreateSendMailLogCommand } from './create-send-mail-log-command';
export declare class CreateSendMailLogHandler implements ICommandHandler<CreateSendMailLogCommand> {
    private readonly asyncRepository;
    private readonly _mapper;
    constructor(asyncRepository: ISendMailLog, _mapper: AutoMapper);
    execute(command: CreateSendMailLogCommand): Promise<SendMailLog>;
}
