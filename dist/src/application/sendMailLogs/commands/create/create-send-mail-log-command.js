"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSendMailLogCommand = void 0;
class CreateSendMailLogCommand {
    constructor(accountId, mailSender, mailTo, mailCc, mailBcc, mailSubject, mailBody, mailType, mailAttac) {
        this.accountId = accountId;
        this.mailSender = mailSender;
        this.mailTo = mailTo;
        this.mailCc = mailCc;
        this.mailBcc = mailBcc;
        this.mailSubject = mailSubject;
        this.mailBody = mailBody;
        this.mailType = mailType;
        this.mailAttac = mailAttac;
    }
}
exports.CreateSendMailLogCommand = CreateSendMailLogCommand;
//# sourceMappingURL=create-send-mail-log-command.js.map