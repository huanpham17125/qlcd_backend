"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountByEmailQuery = void 0;
class GetAccountByEmailQuery {
    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
}
exports.GetAccountByEmailQuery = GetAccountByEmailQuery;
//# sourceMappingURL=get-account-by-email-query.js.map