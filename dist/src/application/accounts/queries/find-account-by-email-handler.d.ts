import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { FindAccountByEmailQuery } from './find-account-by-email-query';
export declare class FindAccountByEmailHandler implements ICommandHandler<FindAccountByEmailQuery> {
    private readonly _accountAsyncRepository;
    private readonly _mapper;
    constructor(_accountAsyncRepository: IaccountAsyncRepository, _mapper: AutoMapper);
    execute(command: FindAccountByEmailQuery): Promise<any>;
}
