import { ICommand } from '@nestjs/cqrs';
export declare class GetAccountByEmailQuery implements ICommand {
    readonly email: string;
    readonly password: string;
    constructor(email: string, password: string);
}
