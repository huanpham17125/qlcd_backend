import { IQuery } from "@nestjs/cqrs";
export declare class DetailAccountByEmailQuery implements IQuery {
    email: string;
    constructor(email: string);
}
