"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailAccountByEmailQuery = void 0;
class DetailAccountByEmailQuery {
    constructor(email) {
        this.email = email;
    }
}
exports.DetailAccountByEmailQuery = DetailAccountByEmailQuery;
//# sourceMappingURL=detail-account-by-email-query.js.map