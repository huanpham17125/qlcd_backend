import { ICommand } from '@nestjs/cqrs';
export declare class GetAccountProfileQuery implements ICommand {
    readonly id: string;
    constructor(id: string);
}
