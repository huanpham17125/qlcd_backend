"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountByEmailHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const bad_request_exception_1 = require("../../common/exceptions/bad-request-exception");
const not_found_exception_1 = require("../../common/exceptions/not-found-exception");
const account_entity_1 = require("../../../domain/entities/account.entity");
const iaccount_async_repository_interface_1 = require("../../../domain/interfaces/iaccount-async-repository.interface");
const password_service_1 = require("../../../infrastructure/services/password.service");
const get_account_by_email_query_1 = require("./get-account-by-email-query");
let GetAccountByEmailHandler = class GetAccountByEmailHandler {
    constructor(_accountAsyncRepository, passwordService) {
        this._accountAsyncRepository = _accountAsyncRepository;
        this.passwordService = passwordService;
    }
    async execute(command) {
        const accountByEmail = await this._accountAsyncRepository.findByEmail(command.email);
        if (!accountByEmail) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_NOT_FOUND);
        }
        const isComparePassword = await this.passwordService.comparePassword(command.password, accountByEmail);
        if (!isComparePassword) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.PASSWORD_INCORECT);
        }
        else if (!accountByEmail.IsActive) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_WAS_LOCKED);
        }
        return accountByEmail;
    }
};
GetAccountByEmailHandler = __decorate([
    cqrs_1.CommandHandler(get_account_by_email_query_1.GetAccountByEmailQuery),
    __param(0, common_1.Inject('ACCOUNTS')),
    __metadata("design:paramtypes", [Object, password_service_1.PasswordService])
], GetAccountByEmailHandler);
exports.GetAccountByEmailHandler = GetAccountByEmailHandler;
//# sourceMappingURL=get-account-by-email-handler.js.map