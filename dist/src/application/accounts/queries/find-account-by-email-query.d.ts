import { ICommand } from '@nestjs/cqrs';
export declare class FindAccountByEmailQuery implements ICommand {
    readonly email: string;
    constructor(email: string);
}
