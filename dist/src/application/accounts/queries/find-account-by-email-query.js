"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FindAccountByEmailQuery = void 0;
class FindAccountByEmailQuery {
    constructor(email) {
        this.email = email;
    }
}
exports.FindAccountByEmailQuery = FindAccountByEmailQuery;
//# sourceMappingURL=find-account-by-email-query.js.map