"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountDataByEmailQuery = void 0;
class GetAccountDataByEmailQuery {
    constructor(email) {
        this.email = email;
    }
}
exports.GetAccountDataByEmailQuery = GetAccountDataByEmailQuery;
//# sourceMappingURL=get-account-data-by-email-query.js.map