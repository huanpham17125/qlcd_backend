"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailAccountByEmailHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const bad_request_exception_1 = require("../../common/exceptions/bad-request-exception");
const iaccount_async_repository_interface_1 = require("../../../domain/interfaces/iaccount-async-repository.interface");
const detail_account_by_email_query_1 = require("./detail-account-by-email-query");
let DetailAccountByEmailHandler = class DetailAccountByEmailHandler {
    constructor(_accountAsyncRepository) {
        this._accountAsyncRepository = _accountAsyncRepository;
    }
    async execute(query) {
        const item = await this._accountAsyncRepository.getByEmail(query.email);
        if (!item) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_NOT_FOUND);
        }
        return item;
    }
};
DetailAccountByEmailHandler = __decorate([
    cqrs_1.QueryHandler(detail_account_by_email_query_1.DetailAccountByEmailQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.APP_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], DetailAccountByEmailHandler);
exports.DetailAccountByEmailHandler = DetailAccountByEmailHandler;
//# sourceMappingURL=detail-account-by-email-handler.js.map