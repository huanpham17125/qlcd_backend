import { ICommandHandler } from '@nestjs/cqrs';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountDataByEmailQuery } from './get-account-data-by-email-query';
export declare class GetAccountDataByEmailHandler implements ICommandHandler<GetAccountDataByEmailQuery> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: GetAccountDataByEmailQuery): Promise<Account | null>;
}
