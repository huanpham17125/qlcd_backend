import { IQueryHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { DetailAccountByEmailQuery } from './detail-account-by-email-query';
export declare class DetailAccountByEmailHandler implements IQueryHandler<DetailAccountByEmailQuery> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(query: DetailAccountByEmailQuery): Promise<any>;
}
