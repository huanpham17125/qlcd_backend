import { ICommandHandler } from '@nestjs/cqrs';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { GetAccountByEmailQuery } from './get-account-by-email-query';
export declare class GetAccountByEmailHandler implements ICommandHandler<GetAccountByEmailQuery> {
    private readonly _accountAsyncRepository;
    private readonly passwordService;
    constructor(_accountAsyncRepository: IaccountAsyncRepository, passwordService: PasswordService);
    execute(command: GetAccountByEmailQuery): Promise<Account>;
}
