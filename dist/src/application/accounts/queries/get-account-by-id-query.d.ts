import { ICommand } from '@nestjs/cqrs';
export declare class GetAccountByIdQuery implements ICommand {
    readonly id: string;
    constructor(id: string);
}
