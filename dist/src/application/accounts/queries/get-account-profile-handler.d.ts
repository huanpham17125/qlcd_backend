import { ICommandHandler } from '@nestjs/cqrs';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountProfileQuery } from './get-account-profile-query';
export declare class GetAccountProfileHandler implements ICommandHandler<GetAccountProfileQuery> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: GetAccountProfileQuery): Promise<Account>;
}
