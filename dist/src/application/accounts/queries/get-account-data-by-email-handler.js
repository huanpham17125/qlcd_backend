"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountDataByEmailHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../common/constants");
const account_entity_1 = require("../../../domain/entities/account.entity");
const iaccount_async_repository_interface_1 = require("../../../domain/interfaces/iaccount-async-repository.interface");
const get_account_data_by_email_query_1 = require("./get-account-data-by-email-query");
let GetAccountDataByEmailHandler = class GetAccountDataByEmailHandler {
    constructor(_accountAsyncRepository) {
        this._accountAsyncRepository = _accountAsyncRepository;
    }
    async execute(command) {
        const entity = await this._accountAsyncRepository.findByEmail(command.email);
        return entity;
    }
};
GetAccountDataByEmailHandler = __decorate([
    cqrs_1.CommandHandler(get_account_data_by_email_query_1.GetAccountDataByEmailQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.APP_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], GetAccountDataByEmailHandler);
exports.GetAccountDataByEmailHandler = GetAccountDataByEmailHandler;
//# sourceMappingURL=get-account-data-by-email-handler.js.map