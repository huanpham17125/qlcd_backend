"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountByIdQuery = void 0;
class GetAccountByIdQuery {
    constructor(id) {
        this.id = id;
    }
}
exports.GetAccountByIdQuery = GetAccountByIdQuery;
//# sourceMappingURL=get-account-by-id-query.js.map