"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAccountProfileQuery = void 0;
class GetAccountProfileQuery {
    constructor(id) {
        this.id = id;
    }
}
exports.GetAccountProfileQuery = GetAccountProfileQuery;
//# sourceMappingURL=get-account-profile-query.js.map