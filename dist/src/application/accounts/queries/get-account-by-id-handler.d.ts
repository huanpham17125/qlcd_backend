import { ICommandHandler } from '@nestjs/cqrs';
import Account from 'src/domain/entities/account.entity';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { GetAccountByIdQuery } from './get-account-by-id-query';
export declare class GetAccountByIdHandler implements ICommandHandler<GetAccountByIdQuery> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: GetAccountByIdQuery): Promise<Account>;
}
