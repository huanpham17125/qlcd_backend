import { ICommand } from '@nestjs/cqrs';
export declare class GetAccountDataByEmailQuery implements ICommand {
    readonly email: string;
    constructor(email: string);
}
