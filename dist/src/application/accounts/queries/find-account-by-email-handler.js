"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FindAccountByEmailHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../common/constants");
const iaccount_async_repository_interface_1 = require("../../../domain/interfaces/iaccount-async-repository.interface");
const res_get_account_1 = require("../viewModels/res-get-account");
const find_account_by_email_query_1 = require("./find-account-by-email-query");
let FindAccountByEmailHandler = class FindAccountByEmailHandler {
    constructor(_accountAsyncRepository, _mapper) {
        this._accountAsyncRepository = _accountAsyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const entity = await this._accountAsyncRepository.findByEmail(command.email);
        return entity ? this._mapper.map(entity, res_get_account_1.default) : '';
    }
};
FindAccountByEmailHandler = __decorate([
    cqrs_1.CommandHandler(find_account_by_email_query_1.FindAccountByEmailQuery),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.APP_PROVIDE)),
    __param(1, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, automapper_nartc_1.AutoMapper])
], FindAccountByEmailHandler);
exports.FindAccountByEmailHandler = FindAccountByEmailHandler;
//# sourceMappingURL=find-account-by-email-handler.js.map