export default class ResGetAccount {
    fullName: string;
    phone: string;
    address: string;
    email: string;
    birthday: string;
    isActive: boolean;
    createdDate: string;
    constructor();
}
