export declare class ResUpdateAccount {
    name: string;
    email: string;
    colorName: string;
    colorNum: number;
    constructor(name: string, email: string, colorName: string, colorNum: number);
}
