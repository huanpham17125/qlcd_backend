"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResSelectResult = void 0;
class ResSelectResult {
    constructor(id, value) {
        (this.id = id), (this.value = value);
        return this;
    }
}
exports.ResSelectResult = ResSelectResult;
//# sourceMappingURL=select-result.js.map