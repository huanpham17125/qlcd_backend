"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResUpdateAccount = void 0;
class ResUpdateAccount {
    constructor(name, email, colorName, colorNum) {
        this.name = name;
        this.email = email;
        this.colorName = colorName;
        this.colorNum = colorNum;
    }
}
exports.ResUpdateAccount = ResUpdateAccount;
//# sourceMappingURL=res-update-account.js.map