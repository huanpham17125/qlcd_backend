import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdateStatusAccountCommand } from './update-status-account-command';
export declare class UpdateStatusAccountHandler implements ICommandHandler<UpdateStatusAccountCommand> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: UpdateStatusAccountCommand): Promise<boolean>;
}
