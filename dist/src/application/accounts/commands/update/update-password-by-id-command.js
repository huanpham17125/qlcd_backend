"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePasswordByIdCommand = void 0;
const req_change_password_1 = require("../../../../api/viewModels/req-change-password");
class UpdatePasswordByIdCommand {
    constructor(passwordHash, id) {
        this.passwordHash = passwordHash;
        this.id = id;
    }
}
exports.UpdatePasswordByIdCommand = UpdatePasswordByIdCommand;
//# sourceMappingURL=update-password-by-id-command.js.map