import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdatePasswordByIdCommand } from './update-password-by-id-command';
export declare class UpdatePasswordByIdHandler implements ICommandHandler<UpdatePasswordByIdCommand> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: UpdatePasswordByIdCommand): Promise<any>;
}
