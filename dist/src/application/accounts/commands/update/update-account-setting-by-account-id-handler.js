"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountSettingByAccountIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const account_setting_entity_1 = require("../../../../domain/entities/account-setting.entity");
const iaccount_setting_repository_interface_1 = require("../../../../domain/interfaces/iaccount-setting-repository.interface");
const update_account_setting_by_account_id_command_1 = require("./update-account-setting-by-account-id-command");
let UpdateAccountSettingByAccountIdHandler = class UpdateAccountSettingByAccountIdHandler {
    constructor(_accountAsyncRepository) {
        this._accountAsyncRepository = _accountAsyncRepository;
    }
    async execute(command) {
        const item = await this._accountAsyncRepository.getByAccountId(command.accountId);
        if (!item) {
            const reqInsert = new account_setting_entity_1.default();
            reqInsert.AccountId = command.accountId;
            await this._accountAsyncRepository.add(reqInsert);
        }
        else {
            const reqUpdate = new account_setting_entity_1.default();
            reqUpdate.UpdatedDate = new Date();
            await this._accountAsyncRepository.updateById(reqUpdate, item.Id);
        }
        return true;
    }
};
UpdateAccountSettingByAccountIdHandler = __decorate([
    cqrs_1.CommandHandler(update_account_setting_by_account_id_command_1.UpdateAccountSettingByAccountIdCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_SETTING_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], UpdateAccountSettingByAccountIdHandler);
exports.UpdateAccountSettingByAccountIdHandler = UpdateAccountSettingByAccountIdHandler;
//# sourceMappingURL=update-account-setting-by-account-id-handler.js.map