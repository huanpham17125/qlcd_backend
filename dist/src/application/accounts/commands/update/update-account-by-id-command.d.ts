import { ICommand } from '@nestjs/cqrs';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
export declare class UpdateAccountByIdCommand implements ICommand {
    Id: string;
    ColorCodeId: number;
    FullName: string;
    Email: string;
    constructor(req: ReqUpdateAccount, id: string);
}
