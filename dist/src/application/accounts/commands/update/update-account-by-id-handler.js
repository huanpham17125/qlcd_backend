"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountByIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const automapper_nartc_1 = require("automapper-nartc");
const nest_automapper_1 = require("nest-automapper");
const constants_1 = require("../../../common/constants");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const account_setting_entity_1 = require("../../../../domain/entities/account-setting.entity");
const iaccount_async_repository_interface_1 = require("../../../../domain/interfaces/iaccount-async-repository.interface");
const iaccount_setting_repository_interface_1 = require("../../../../domain/interfaces/iaccount-setting-repository.interface");
const res_update_account_1 = require("../../viewModels/res-update-account");
const update_account_by_id_command_1 = require("./update-account-by-id-command");
let UpdateAccountByIdHandler = class UpdateAccountByIdHandler {
    constructor(_accountAsyncRepository, _accountSettingAsyncRepository, _mapper) {
        this._accountAsyncRepository = _accountAsyncRepository;
        this._accountSettingAsyncRepository = _accountSettingAsyncRepository;
        this._mapper = _mapper;
    }
    async execute(command) {
        const account = await this._accountAsyncRepository.getById(command.Id);
        if (!account) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_NOT_FOUND);
        }
        account.FullName = command.FullName;
        account.Email = command.Email;
        account.UpdatedDate = new Date();
        const entity = await this._accountAsyncRepository.updateById(account, command.Id);
        if (!entity) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.UPDATE_FAIL);
        }
        const newAccountSetting = await this._accountSettingAsyncRepository.getByAccountId(command.Id);
        return this._mapper.map(newAccountSetting, res_update_account_1.ResUpdateAccount);
    }
};
UpdateAccountByIdHandler = __decorate([
    cqrs_1.CommandHandler(update_account_by_id_command_1.UpdateAccountByIdCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_PROVIDE)),
    __param(1, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_SETTING_PROVIDE)),
    __param(2, nest_automapper_1.InjectMapper()),
    __metadata("design:paramtypes", [Object, Object, automapper_nartc_1.AutoMapper])
], UpdateAccountByIdHandler);
exports.UpdateAccountByIdHandler = UpdateAccountByIdHandler;
//# sourceMappingURL=update-account-by-id-handler.js.map