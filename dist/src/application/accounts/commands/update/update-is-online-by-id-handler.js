"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateIsOnlineByIdHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const iaccount_async_repository_interface_1 = require("../../../../domain/interfaces/iaccount-async-repository.interface");
const update_is_online_by_id_command_1 = require("./update-is-online-by-id-command");
let UpdateIsOnlineByIdHandler = class UpdateIsOnlineByIdHandler {
    constructor(_accountAsyncRepository) {
        this._accountAsyncRepository = _accountAsyncRepository;
    }
    async execute(command) {
        const entity = await this._accountAsyncRepository.updateIsOnlineById(command.accountId, command.isOnline);
        return entity;
    }
};
UpdateIsOnlineByIdHandler = __decorate([
    cqrs_1.CommandHandler(update_is_online_by_id_command_1.UpdateIsOnlineByIdCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], UpdateIsOnlineByIdHandler);
exports.UpdateIsOnlineByIdHandler = UpdateIsOnlineByIdHandler;
//# sourceMappingURL=update-is-online-by-id-handler.js.map