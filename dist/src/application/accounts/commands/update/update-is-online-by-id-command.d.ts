import { ICommand } from "@nestjs/cqrs";
export declare class UpdateIsOnlineByIdCommand implements ICommand {
    accountId: string;
    isOnline: boolean;
    constructor(accountId: string, isOnline: boolean);
}
