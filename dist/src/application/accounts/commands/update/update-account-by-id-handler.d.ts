import { ICommandHandler } from '@nestjs/cqrs';
import { AutoMapper } from 'automapper-nartc';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { UpdateAccountByIdCommand } from './update-account-by-id-command';
export declare class UpdateAccountByIdHandler implements ICommandHandler<UpdateAccountByIdCommand> {
    private readonly _accountAsyncRepository;
    private readonly _accountSettingAsyncRepository;
    private readonly _mapper;
    constructor(_accountAsyncRepository: IaccountAsyncRepository, _accountSettingAsyncRepository: IaccountSettingRepository, _mapper: AutoMapper);
    execute(command: UpdateAccountByIdCommand): Promise<any>;
}
