"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateIsOnlineByIdCommand = void 0;
class UpdateIsOnlineByIdCommand {
    constructor(accountId, isOnline) {
        this.accountId = accountId;
        this.isOnline = isOnline;
    }
}
exports.UpdateIsOnlineByIdCommand = UpdateIsOnlineByIdCommand;
//# sourceMappingURL=update-is-online-by-id-command.js.map