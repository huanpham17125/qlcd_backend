import { ICommand } from '@nestjs/cqrs';
export declare class UpdateAccountSettingByAccountIdCommand implements ICommand {
    accountId: string;
    timePushNotifyId: number;
    colorCodeId: number;
    constructor(accountId: string, timePushNotifyId: number, colorCodeId: number);
}
