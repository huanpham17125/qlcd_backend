"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountSettingByAccountIdCommand = void 0;
class UpdateAccountSettingByAccountIdCommand {
    constructor(accountId, timePushNotifyId, colorCodeId) {
        this.accountId = accountId;
        this.timePushNotifyId = timePushNotifyId;
        this.colorCodeId = colorCodeId;
    }
}
exports.UpdateAccountSettingByAccountIdCommand = UpdateAccountSettingByAccountIdCommand;
//# sourceMappingURL=update-account-setting-by-account-id-command.js.map