"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccountByIdCommand = void 0;
const req_update_account_1 = require("../../../../api/viewModels/req-update-account");
const domainEnum_1 = require("../../../../domain/enum/domainEnum");
class UpdateAccountByIdCommand {
    constructor(req, id) {
        (this.Id = id), (this.FullName = req.fullName), (this.Email = req.email);
    }
}
exports.UpdateAccountByIdCommand = UpdateAccountByIdCommand;
//# sourceMappingURL=update-account-by-id-command.js.map