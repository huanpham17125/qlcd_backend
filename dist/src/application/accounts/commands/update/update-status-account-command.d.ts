import { ICommand } from '@nestjs/cqrs';
export declare class UpdateStatusAccountCommand implements ICommand {
    accountId: string;
    adminId: string;
    status: number;
    constructor(accountId: string, adminId: string, status: number);
}
