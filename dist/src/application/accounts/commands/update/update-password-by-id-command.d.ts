import { ICommand } from '@nestjs/cqrs';
export declare class UpdatePasswordByIdCommand implements ICommand {
    passwordHash: string;
    id: string;
    constructor(passwordHash: string, id: string);
}
