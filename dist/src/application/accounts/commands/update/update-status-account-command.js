"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateStatusAccountCommand = void 0;
class UpdateStatusAccountCommand {
    constructor(accountId, adminId, status) {
        this.accountId = accountId;
        this.adminId = adminId;
        this.status = status;
    }
}
exports.UpdateStatusAccountCommand = UpdateStatusAccountCommand;
//# sourceMappingURL=update-status-account-command.js.map