import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountSettingRepository } from 'src/domain/interfaces/iaccount-setting-repository.interface';
import { UpdateAccountSettingByAccountIdCommand } from './update-account-setting-by-account-id-command';
export declare class UpdateAccountSettingByAccountIdHandler implements ICommandHandler<UpdateAccountSettingByAccountIdCommand> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountSettingRepository);
    execute(command: UpdateAccountSettingByAccountIdCommand): Promise<boolean>;
}
