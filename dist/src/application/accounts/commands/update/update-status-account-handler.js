"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateStatusAccountHandler = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const constants_1 = require("../../../common/constants");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const iaccount_async_repository_interface_1 = require("../../../../domain/interfaces/iaccount-async-repository.interface");
const update_status_account_command_1 = require("./update-status-account-command");
let UpdateStatusAccountHandler = class UpdateStatusAccountHandler {
    constructor(_accountAsyncRepository) {
        this._accountAsyncRepository = _accountAsyncRepository;
    }
    async execute(command) {
        const itemAccount = await this._accountAsyncRepository.getById(command.accountId);
        const isActiveUpdate = command.status == 0 ? false : true;
        if (!itemAccount) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_NOT_FOUND);
        }
        if (isActiveUpdate == itemAccount.IsActive) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.ACCOUNT_STATUS_NOT_CHANGE);
        }
        itemAccount.IsActive = isActiveUpdate;
        itemAccount.UpdatedById = command.adminId;
        itemAccount.UpdatedDate = new Date();
        return await this._accountAsyncRepository.updateById(itemAccount, command.accountId);
    }
};
UpdateStatusAccountHandler = __decorate([
    cqrs_1.CommandHandler(update_status_account_command_1.UpdateStatusAccountCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_PROVIDE)),
    __metadata("design:paramtypes", [Object])
], UpdateStatusAccountHandler);
exports.UpdateStatusAccountHandler = UpdateStatusAccountHandler;
//# sourceMappingURL=update-status-account-handler.js.map