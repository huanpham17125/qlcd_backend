import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { UpdateIsOnlineByIdCommand } from './update-is-online-by-id-command';
export declare class UpdateIsOnlineByIdHandler implements ICommandHandler<UpdateIsOnlineByIdCommand> {
    private readonly _accountAsyncRepository;
    constructor(_accountAsyncRepository: IaccountAsyncRepository);
    execute(command: UpdateIsOnlineByIdCommand): Promise<boolean>;
}
