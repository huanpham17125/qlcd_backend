"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountCommand = void 0;
class CreateAccountCommand {
    constructor(roleId, templeId, creatorId, fullName, email, password, phone, sex) {
        this.roleId = roleId;
        this.templeId = templeId;
        this.creatorId = creatorId;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.sex = sex;
    }
}
exports.CreateAccountCommand = CreateAccountCommand;
//# sourceMappingURL=create-account-command.js.map