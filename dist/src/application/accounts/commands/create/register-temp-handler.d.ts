import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from 'src/domain/interfaces/iaccount-async-repository.interface';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { RegisterTempCommand } from './register-temp-command';
export declare class RegisterTempHandler implements ICommandHandler<RegisterTempCommand> {
    private readonly _accountAsyncRepository;
    private readonly passwordService;
    constructor(_accountAsyncRepository: IaccountAsyncRepository, passwordService: PasswordService);
    execute(command: RegisterTempCommand): Promise<any>;
}
