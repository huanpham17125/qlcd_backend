import { ICommand } from '@nestjs/cqrs';
export declare class CreateAccountCommand implements ICommand {
    roleId: string;
    templeId: string;
    creatorId: string;
    readonly fullName: string;
    readonly email: string;
    readonly password: string;
    readonly phone?: string;
    readonly sex?: string;
    constructor(roleId: string, templeId: string, creatorId: string, fullName: string, email: string, password: string, phone?: string, sex?: string);
}
