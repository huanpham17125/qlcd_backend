import { ICommandHandler } from '@nestjs/cqrs';
import { IaccountAsyncRepository } from '../../../../domain/interfaces/iaccount-async-repository.interface';
import { CreateAccountCommand } from './create-account-command';
import Account from '../../../../domain/entities/account.entity';
import { PasswordService } from 'src/infrastructure/services/password.service';
import { AccountRoleRepository } from 'src/infrastructure/repositories/account-role-repository';
export declare class CreateAccountHandler implements ICommandHandler<CreateAccountCommand> {
    private readonly _accountAsyncRepository;
    private readonly accountRoleRepository;
    private readonly passwordService;
    constructor(_accountAsyncRepository: IaccountAsyncRepository, accountRoleRepository: AccountRoleRepository, passwordService: PasswordService);
    execute(command: CreateAccountCommand): Promise<Account>;
}
