"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccountHandler = void 0;
const constants_1 = require("./../../../common/constants");
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const bad_request_exception_1 = require("../../../common/exceptions/bad-request-exception");
const create_account_command_1 = require("./create-account-command");
const account_entity_1 = require("../../../../domain/entities/account.entity");
const password_service_1 = require("../../../../infrastructure/services/password.service");
const account_role_repository_1 = require("../../../../infrastructure/repositories/account-role-repository");
const account_role_entity_1 = require("../../../../domain/entities/account-role.entity");
let CreateAccountHandler = class CreateAccountHandler {
    constructor(_accountAsyncRepository, accountRoleRepository, passwordService) {
        this._accountAsyncRepository = _accountAsyncRepository;
        this.accountRoleRepository = accountRoleRepository;
        this.passwordService = passwordService;
    }
    async execute(command) {
        const accountByEmail = await this._accountAsyncRepository.findByEmail(command.email);
        if (accountByEmail) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.EMAIL_ALREADY_EXISTED);
        }
        const newAccount = new account_entity_1.default();
        newAccount.Birthday = new Date(constants_1.Constants.DEFAULT_DATE);
        newAccount.Email = command.email;
        newAccount.FullName = command.fullName;
        newAccount.PasswordHash = command.password;
        newAccount.Gender = command.sex || '';
        newAccount.Phone = command.phone || '';
        newAccount.CreatedById = command.creatorId;
        newAccount.UpdatedById = command.creatorId;
        const reqAccount = await this.passwordService.getPasswordHash(command.password, newAccount);
        const account = await this._accountAsyncRepository.create(reqAccount);
        const reqAccRole = new account_role_entity_1.default();
        reqAccRole.AccountId = account.Id;
        reqAccRole.RoleId = command.roleId;
        await this.accountRoleRepository.add(reqAccRole);
        return account;
    }
};
CreateAccountHandler = __decorate([
    cqrs_1.CommandHandler(create_account_command_1.CreateAccountCommand),
    __param(0, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_PROVIDE)),
    __param(1, common_1.Inject(constants_1.Constants.AppProvide.ACCOUNT_ROLE)),
    __metadata("design:paramtypes", [Object, account_role_repository_1.AccountRoleRepository,
        password_service_1.PasswordService])
], CreateAccountHandler);
exports.CreateAccountHandler = CreateAccountHandler;
//# sourceMappingURL=create-account-handler.js.map