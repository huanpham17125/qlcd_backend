import { ICommand } from "@nestjs/cqrs";
export declare class RegisterTempCommand implements ICommand {
    email: string;
    password: string;
    constructor(email: string, password: string);
}
