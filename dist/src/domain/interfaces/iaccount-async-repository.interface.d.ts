import Account from '../entities/account.entity';
export interface IaccountAsyncRepository {
    getById(id: string): Promise<Account> | null;
    getByEmail(email: string): Promise<Account> | null;
    create(account: Account): Promise<Account>;
    findByEmail(email: string): Promise<Account> | null;
    getProfile(id: string): Promise<Account> | null;
    updateById(account: Account, id: string): Promise<boolean>;
    updatePasswordById(password: string, id: string): Promise<boolean>;
    updateIsOnlineById(id: string, isOnline: boolean): Promise<boolean>;
}
