import SendNotifyLog from '../entities/send-notify-log.entity';
import { IAsyncGenericRepository } from './iasync-generic-repository.interface';
export interface ISendNotifyRepository extends IAsyncGenericRepository<SendNotifyLog> {
    getByAccountId(accountId: string): Promise<Array<SendNotifyLog>>;
}
