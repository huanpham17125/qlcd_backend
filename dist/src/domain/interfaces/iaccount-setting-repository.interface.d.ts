import AccountSetting from '../entities/account-setting.entity';
import { IAsyncGenericRepository } from './iasync-generic-repository.interface';
export interface IaccountSettingRepository extends IAsyncGenericRepository<AccountSetting> {
    getByAccountId(accountId: string): Promise<AccountSetting> | null;
}
