import AccountSecurityToken from "../entities/account-security-token.entity";
import { IAsyncGenericRepository } from "./iasync-generic-repository.interface";
export interface IAccountSecutiryTokenRepository extends IAsyncGenericRepository<AccountSecurityToken> {
    getByAccountId(accountId: string): Promise<AccountSecurityToken> | null;
}
