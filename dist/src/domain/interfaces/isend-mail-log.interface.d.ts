import SendMailLog from '../entities/send-mail-log.entity';
import { IAsyncGenericRepository } from './iasync-generic-repository.interface';
export interface ISendMailLog extends IAsyncGenericRepository<SendMailLog> {
    getByAccountId(accountId: string): Promise<Array<SendMailLog>>;
}
