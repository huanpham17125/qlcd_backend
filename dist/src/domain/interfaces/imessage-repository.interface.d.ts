import { Pagination } from 'nestjs-typeorm-paginate';
import Message from '../entities/message.entity-gateway';
import { StatusMessage } from '../enum/domainEnum';
import { IAsyncGenericRepository } from './iasync-generic-repository.interface';
export interface ImessageRepository extends IAsyncGenericRepository<Message> {
    getMessagePaginationByAccountId(accountId: string, pageSize: number, pageNumber: any): Promise<Pagination<Message>>;
    updateStatusById(messageId: number, status: StatusMessage): Promise<boolean>;
}
