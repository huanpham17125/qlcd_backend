import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
import Role from './role.entity';
export default class AccountRole extends BaseEntity {
    AccountId: string;
    Account: Account;
    RoleId: string;
    Role: Role;
}
