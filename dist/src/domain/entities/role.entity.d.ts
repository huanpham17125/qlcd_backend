import AccountRole from './account-role.entity';
import AbstractBaseEntity from './bases/abstractBaseEntity';
export default class Role extends AbstractBaseEntity {
    Name: string;
    NormalizedName: string;
    Des: string;
    AccountRoles: AccountRole[];
}
