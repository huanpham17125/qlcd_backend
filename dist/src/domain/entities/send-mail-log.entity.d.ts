import { StatusSendEmail } from '../enum/domainEnum';
import Account from './account.entity';
import BaseEntityInt from './bases/baseEntityInt';
export default class SendMailLog extends BaseEntityInt {
    MailSender: string;
    MailTo: string;
    MailCc: string;
    MailBcc: string;
    MailSubject: string;
    MailBody: string;
    MailType: number;
    MailAttach: string;
    SendStatus: StatusSendEmail;
    AccountId: string;
    Account: Account;
}
