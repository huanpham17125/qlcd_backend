import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
import RoomChat from './room-chat.entity-gateway';
export default class AccountRoom extends BaseEntity {
    RoomId: string;
    RoomChat: RoomChat;
    AccountId: string;
    Account?: Account;
    IsPushNotify: boolean;
}
