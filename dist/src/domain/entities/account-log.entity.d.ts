import Account from './account.entity';
import { BaseEntityBigInt } from './bases/baseEntityInt';
export default class AccountLog extends BaseEntityBigInt {
    AccountId: string;
    Account: Account;
}
