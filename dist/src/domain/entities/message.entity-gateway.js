"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const req_content_message_schedule_1 = require("../../application/messages/viewModels/req-content-message-schedule");
const typeorm_1 = require("typeorm");
const domainEnum_1 = require("../enum/domainEnum");
const account_entity_1 = require("./account.entity");
const baseEntityInt_1 = require("./bases/baseEntityInt");
const room_chat_entity_gateway_1 = require("./room-chat.entity-gateway");
let Message = class Message extends baseEntityInt_1.BaseEntityBigInt {
};
__decorate([
    typeorm_1.Column({ name: 'RoomId' }),
    __metadata("design:type", String)
], Message.prototype, "RoomId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => room_chat_entity_gateway_1.default, (room) => room.Message),
    typeorm_1.JoinColumn({ name: 'RoomId' }),
    __metadata("design:type", room_chat_entity_gateway_1.default)
], Message.prototype, "RoomChat", void 0);
__decorate([
    typeorm_1.Column({ name: 'AccSenderId', type: 'varchar', length: 36 }),
    __metadata("design:type", String)
], Message.prototype, "AccSenderId", void 0);
__decorate([
    typeorm_1.JoinColumn({ name: 'AccSenderId' }),
    __metadata("design:type", account_entity_1.default)
], Message.prototype, "Account", void 0);
__decorate([
    typeorm_1.Column({ name: 'ReplyId', type: 'int' }),
    __metadata("design:type", Number)
], Message.prototype, "ReplyId", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', default: 1 }),
    __metadata("design:type", Number)
], Message.prototype, "TypeContent", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 3000, charset: 'utf8' }),
    __metadata("design:type", String)
], Message.prototype, "Content", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', default: 1 }),
    __metadata("design:type", Number)
], Message.prototype, "Status", void 0);
Message = __decorate([
    typeorm_1.Entity({ name: 'Messages', database: 'gateways' })
], Message);
exports.default = Message;
//# sourceMappingURL=message.entity-gateway.js.map