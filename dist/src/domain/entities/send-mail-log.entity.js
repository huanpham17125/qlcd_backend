"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const domainEnum_1 = require("../enum/domainEnum");
const account_entity_1 = require("./account.entity");
const baseEntityInt_1 = require("./bases/baseEntityInt");
let SendMailLog = class SendMailLog extends baseEntityInt_1.default {
};
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        charset: 'utf8',
        length: 128,
        default: 'no-reply@test.jp',
    }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailSender", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 128 }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailTo", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 256, default: '' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailCc", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 256, default: '' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailBcc", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 512, default: '' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailSubject", void 0);
__decorate([
    typeorm_1.Column({ type: 'text' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailBody", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', }),
    __metadata("design:type", Number)
], SendMailLog.prototype, "MailType", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 3000, default: '' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "MailAttach", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', default: domainEnum_1.StatusSendEmail.Sent }),
    __metadata("design:type", Number)
], SendMailLog.prototype, "SendStatus", void 0);
__decorate([
    typeorm_1.Column({ name: 'AccountId' }),
    __metadata("design:type", String)
], SendMailLog.prototype, "AccountId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => account_entity_1.default, (account) => account.SendMailLog),
    typeorm_1.JoinColumn({ name: 'AccountId' }),
    __metadata("design:type", account_entity_1.default)
], SendMailLog.prototype, "Account", void 0);
SendMailLog = __decorate([
    typeorm_1.Entity({ name: 'SendMailLogs' })
], SendMailLog);
exports.default = SendMailLog;
//# sourceMappingURL=send-mail-log.entity.js.map