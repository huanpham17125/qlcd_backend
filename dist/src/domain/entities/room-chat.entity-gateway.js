"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const account_room_entity_gateway_1 = require("./account-room.entity-gateway");
const abstractBaseEntity_1 = require("./bases/abstractBaseEntity");
const message_entity_gateway_1 = require("./message.entity-gateway");
let RoomChat = class RoomChat extends abstractBaseEntity_1.default {
};
__decorate([
    typeorm_1.OneToMany(() => message_entity_gateway_1.default, (message) => message.RoomChat),
    __metadata("design:type", Array)
], RoomChat.prototype, "Message", void 0);
__decorate([
    typeorm_1.OneToMany(() => account_room_entity_gateway_1.default, (accountRoom) => accountRoom.RoomChat),
    __metadata("design:type", Array)
], RoomChat.prototype, "AccountRoom", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 250 }),
    __metadata("design:type", String)
], RoomChat.prototype, "Name", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 500 }),
    __metadata("design:type", String)
], RoomChat.prototype, "Tittle", void 0);
RoomChat = __decorate([
    typeorm_1.Entity({ name: 'RoomChats', database: 'gateways' })
], RoomChat);
exports.default = RoomChat;
//# sourceMappingURL=room-chat.entity-gateway.js.map