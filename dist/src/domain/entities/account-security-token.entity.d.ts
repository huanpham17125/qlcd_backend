import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
export default class AccountSecurityToken extends BaseEntity {
    AccountId: string;
    Account: Account;
    TokenType: number;
    Value: string;
    ExpiryDate: Date;
    RemoteIpAddress: string;
}
