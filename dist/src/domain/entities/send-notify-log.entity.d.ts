import { StatusSendNotify, TypeSendNotify } from '../enum/domainEnum';
import Account from './account.entity';
import { AbstractBaseEntityBigInt } from './bases/abstractBaseEntityInt';
export default class SendNotifyLog extends AbstractBaseEntityBigInt {
    AccountId: string;
    Account: Account;
    Title: string;
    Body: string;
    Msg: string;
    TypeNotify: TypeSendNotify;
    Status: StatusSendNotify;
}
