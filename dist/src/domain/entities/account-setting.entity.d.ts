import Account from './account.entity';
import BaseEntity from './bases/baseEntity';
export default class AccountSetting extends BaseEntity {
    AccountId: string;
    Account: Account;
}
