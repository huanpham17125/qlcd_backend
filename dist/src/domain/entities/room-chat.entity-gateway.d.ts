import AccountRoom from './account-room.entity-gateway';
import AbstractBaseEntity from './bases/abstractBaseEntity';
import Message from './message.entity-gateway';
export default class RoomChat extends AbstractBaseEntity {
    Message: Message[];
    AccountRoom: AccountRoom[];
    Name: string;
    Tittle: string;
}
