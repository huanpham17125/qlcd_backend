export default class BaseEntityInt {
    Id: number;
    IsActive: boolean;
    Version: number;
    CreatedDate: Date;
    UpdatedDate: Date;
}
export declare class BaseEntityBigInt {
    Id: number;
    IsActive: boolean;
    Version: number;
    CreatedDate: Date;
    UpdatedDate: Date;
}
