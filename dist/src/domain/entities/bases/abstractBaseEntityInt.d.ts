import BaseEntityInt, { BaseEntityBigInt } from './baseEntityInt';
export default class AbstractBaseEntityInt extends BaseEntityInt {
    CreatedById: string;
    UpdatedById: string;
}
export declare class AbstractBaseEntityBigInt extends BaseEntityBigInt {
    CreatedById: string;
    UpdatedById: string;
}
