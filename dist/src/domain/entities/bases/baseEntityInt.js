"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseEntityBigInt = void 0;
const typeorm_1 = require("typeorm");
class BaseEntityInt {
}
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ type: 'int' }),
    __metadata("design:type", Number)
], BaseEntityInt.prototype, "Id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bool', default: true }),
    __metadata("design:type", Boolean)
], BaseEntityInt.prototype, "IsActive", void 0);
__decorate([
    typeorm_1.VersionColumn(),
    __metadata("design:type", Number)
], BaseEntityInt.prototype, "Version", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' }),
    __metadata("design:type", Date)
], BaseEntityInt.prototype, "CreatedDate", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' }),
    __metadata("design:type", Date)
], BaseEntityInt.prototype, "UpdatedDate", void 0);
exports.default = BaseEntityInt;
class BaseEntityBigInt {
}
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ type: 'bigint' }),
    __metadata("design:type", Number)
], BaseEntityBigInt.prototype, "Id", void 0);
__decorate([
    typeorm_1.Column({ type: 'bool', default: true }),
    __metadata("design:type", Boolean)
], BaseEntityBigInt.prototype, "IsActive", void 0);
__decorate([
    typeorm_1.VersionColumn(),
    __metadata("design:type", Number)
], BaseEntityBigInt.prototype, "Version", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' }),
    __metadata("design:type", Date)
], BaseEntityBigInt.prototype, "CreatedDate", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' }),
    __metadata("design:type", Date)
], BaseEntityBigInt.prototype, "UpdatedDate", void 0);
exports.BaseEntityBigInt = BaseEntityBigInt;
//# sourceMappingURL=baseEntityInt.js.map