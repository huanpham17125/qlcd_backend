"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractBaseEntityBigInt = void 0;
const typeorm_1 = require("typeorm");
const baseEntityInt_1 = require("./baseEntityInt");
class AbstractBaseEntityInt extends baseEntityInt_1.default {
}
__decorate([
    typeorm_1.Column({ type: 'char', length: 36 }),
    __metadata("design:type", String)
], AbstractBaseEntityInt.prototype, "CreatedById", void 0);
__decorate([
    typeorm_1.Column({ type: 'char', length: 36 }),
    __metadata("design:type", String)
], AbstractBaseEntityInt.prototype, "UpdatedById", void 0);
exports.default = AbstractBaseEntityInt;
class AbstractBaseEntityBigInt extends baseEntityInt_1.BaseEntityBigInt {
}
__decorate([
    typeorm_1.Column({ type: 'char', length: 36 }),
    __metadata("design:type", String)
], AbstractBaseEntityBigInt.prototype, "CreatedById", void 0);
__decorate([
    typeorm_1.Column({ type: 'char', length: 36 }),
    __metadata("design:type", String)
], AbstractBaseEntityBigInt.prototype, "UpdatedById", void 0);
exports.AbstractBaseEntityBigInt = AbstractBaseEntityBigInt;
//# sourceMappingURL=abstractBaseEntityInt.js.map