export default class BaseEntity {
    Id: string;
    IsActive: boolean;
    Version: number;
    CreatedDate: Date;
    UpdatedDate: Date;
}
