import BaseEntity from './baseEntity';
export default class AbstractBaseEntity extends BaseEntity {
    CreatedById: string;
    UpdatedById: string;
}
