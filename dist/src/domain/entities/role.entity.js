"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const account_role_entity_1 = require("./account-role.entity");
const abstractBaseEntity_1 = require("./bases/abstractBaseEntity");
let Role = class Role extends abstractBaseEntity_1.default {
};
__decorate([
    typeorm_1.Column({ length: 255 }),
    __metadata("design:type", String)
], Role.prototype, "Name", void 0);
__decorate([
    typeorm_1.Column({ length: 255 }),
    __metadata("design:type", String)
], Role.prototype, "NormalizedName", void 0);
__decorate([
    typeorm_1.Column({ length: 512 }),
    __metadata("design:type", String)
], Role.prototype, "Des", void 0);
__decorate([
    typeorm_1.OneToMany(() => account_role_entity_1.default, (AccountRole) => AccountRole.Role),
    __metadata("design:type", Array)
], Role.prototype, "AccountRoles", void 0);
Role = __decorate([
    typeorm_1.Entity({ name: 'Roles' })
], Role);
exports.default = Role;
//# sourceMappingURL=role.entity.js.map