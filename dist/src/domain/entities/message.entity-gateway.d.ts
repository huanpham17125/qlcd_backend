import { StatusMessage, TypeContentMessage } from '../enum/domainEnum';
import Account from './account.entity';
import { BaseEntityBigInt } from './bases/baseEntityInt';
import RoomChat from './room-chat.entity-gateway';
export default class Message extends BaseEntityBigInt {
    RoomId: string;
    RoomChat: RoomChat;
    AccSenderId: string;
    Account?: Account;
    ReplyId: number;
    TypeContent: TypeContentMessage;
    Content: string;
    Status: StatusMessage;
}
