"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const account_log_entity_1 = require("./account-log.entity");
const account_role_entity_1 = require("./account-role.entity");
const account_security_token_entity_1 = require("./account-security-token.entity");
const account_setting_entity_1 = require("./account-setting.entity");
const abstractBaseEntity_1 = require("./bases/abstractBaseEntity");
const send_mail_log_entity_1 = require("./send-mail-log.entity");
const send_notify_log_entity_1 = require("./send-notify-log.entity");
let Account = class Account extends abstractBaseEntity_1.default {
};
__decorate([
    typeorm_1.Column({ length: 255 }),
    __metadata("design:type", String)
], Account.prototype, "Email", void 0);
__decorate([
    typeorm_1.Column({ length: 512 }),
    __metadata("design:type", String)
], Account.prototype, "PasswordHash", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 20, default: '' }),
    __metadata("design:type", String)
], Account.prototype, "Phone", void 0);
__decorate([
    typeorm_1.Column({ length: 255, charset: 'utf8', default: '' }),
    __metadata("design:type", String)
], Account.prototype, "FullName", void 0);
__decorate([
    typeorm_1.Column({ type: 'datetime', default: '1900-01-01' }),
    __metadata("design:type", Date)
], Account.prototype, "Birthday", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 10 }),
    __metadata("design:type", String)
], Account.prototype, "Gender", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 50, default: '' }),
    __metadata("design:type", String)
], Account.prototype, "ZipCode", void 0);
__decorate([
    typeorm_1.Column({ length: 255, charset: 'utf8', default: '' }),
    __metadata("design:type", String)
], Account.prototype, "Province", void 0);
__decorate([
    typeorm_1.Column({ length: 255, charset: 'utf8', default: '' }),
    __metadata("design:type", String)
], Account.prototype, "City", void 0);
__decorate([
    typeorm_1.Column({ length: 255, charset: 'utf8', default: '' }),
    __metadata("design:type", String)
], Account.prototype, "Street", void 0);
__decorate([
    typeorm_1.Column({ type: 'boolean', default: true }),
    __metadata("design:type", Boolean)
], Account.prototype, "IsOnline", void 0);
__decorate([
    typeorm_1.OneToOne(() => account_setting_entity_1.default, (accountSetting) => accountSetting.Account),
    __metadata("design:type", account_setting_entity_1.default)
], Account.prototype, "AccountSetting", void 0);
__decorate([
    typeorm_1.OneToMany(() => account_role_entity_1.default, (AccountRole) => AccountRole.Account),
    __metadata("design:type", Array)
], Account.prototype, "AccountRoles", void 0);
__decorate([
    typeorm_1.OneToMany(() => send_mail_log_entity_1.default, (sendmail) => sendmail.Account),
    __metadata("design:type", Array)
], Account.prototype, "SendMailLog", void 0);
__decorate([
    typeorm_1.OneToMany(() => account_security_token_entity_1.default, (accountSecurityToken) => accountSecurityToken.Account),
    __metadata("design:type", Array)
], Account.prototype, "AccountSecurityTokens", void 0);
__decorate([
    typeorm_1.OneToMany(() => account_log_entity_1.default, (accountLog) => accountLog.Account),
    __metadata("design:type", Array)
], Account.prototype, "AccountLog", void 0);
__decorate([
    typeorm_1.OneToMany(() => send_notify_log_entity_1.default, (SendNotifyLog) => SendNotifyLog.Account),
    __metadata("design:type", Array)
], Account.prototype, "SendNotifyLog", void 0);
Account = __decorate([
    typeorm_1.Entity({ name: 'Accounts' })
], Account);
exports.default = Account;
//# sourceMappingURL=account.entity.js.map