"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const account_entity_1 = require("./account.entity");
const baseEntity_1 = require("./bases/baseEntity");
const room_chat_entity_gateway_1 = require("./room-chat.entity-gateway");
let AccountRoom = class AccountRoom extends baseEntity_1.default {
};
__decorate([
    typeorm_1.Column({ name: 'RoomId', default: 0 }),
    __metadata("design:type", String)
], AccountRoom.prototype, "RoomId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => room_chat_entity_gateway_1.default, (roomChat) => roomChat.AccountRoom),
    typeorm_1.JoinColumn({ name: 'RoomId' }),
    __metadata("design:type", room_chat_entity_gateway_1.default)
], AccountRoom.prototype, "RoomChat", void 0);
__decorate([
    typeorm_1.Column({ name: 'AccountId', type: 'varchar', length: 36 }),
    __metadata("design:type", String)
], AccountRoom.prototype, "AccountId", void 0);
__decorate([
    typeorm_1.JoinColumn({ name: 'AccountId' }),
    __metadata("design:type", account_entity_1.default)
], AccountRoom.prototype, "Account", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], AccountRoom.prototype, "IsPushNotify", void 0);
AccountRoom = __decorate([
    typeorm_1.Entity({ name: 'AccountRooms', database: 'gateways' })
], AccountRoom);
exports.default = AccountRoom;
//# sourceMappingURL=account-room.entity-gateway.js.map