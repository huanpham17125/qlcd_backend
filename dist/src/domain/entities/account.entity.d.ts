import AccountLog from './account-log.entity';
import AccountRole from './account-role.entity';
import AccountSecurityToken from './account-security-token.entity';
import AccountSetting from './account-setting.entity';
import AbstractBaseEntity from './bases/abstractBaseEntity';
import SendMailLog from './send-mail-log.entity';
import SendNotifyLog from './send-notify-log.entity';
export default class Account extends AbstractBaseEntity {
    Email: string;
    PasswordHash: string;
    Phone: string;
    FullName: string;
    Birthday: Date;
    Gender: string;
    ZipCode: string;
    Province: string;
    City: string;
    Street: string;
    IsOnline: boolean;
    AccountSetting: AccountSetting;
    AccountRoles: AccountRole[];
    SendMailLog: SendMailLog[];
    AccountSecurityTokens: AccountSecurityToken[];
    AccountLog: AccountLog[];
    SendNotifyLog: SendNotifyLog[];
}
