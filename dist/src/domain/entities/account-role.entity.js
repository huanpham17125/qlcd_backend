"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const account_entity_1 = require("./account.entity");
const baseEntity_1 = require("./bases/baseEntity");
const role_entity_1 = require("./role.entity");
let AccountRole = class AccountRole extends baseEntity_1.default {
};
__decorate([
    typeorm_1.Column({ name: 'AccountId' }),
    __metadata("design:type", String)
], AccountRole.prototype, "AccountId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => account_entity_1.default, (account) => account.AccountRoles),
    typeorm_1.JoinColumn({ name: 'AccountId' }),
    __metadata("design:type", account_entity_1.default)
], AccountRole.prototype, "Account", void 0);
__decorate([
    typeorm_1.Column({ name: 'RoleId' }),
    __metadata("design:type", String)
], AccountRole.prototype, "RoleId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => role_entity_1.default, (role) => role.AccountRoles),
    typeorm_1.JoinColumn([{ name: 'RoleId', referencedColumnName: 'Id' }]),
    __metadata("design:type", role_entity_1.default)
], AccountRole.prototype, "Role", void 0);
AccountRole = __decorate([
    typeorm_1.Entity({ name: 'AccountRoles' })
], AccountRole);
exports.default = AccountRole;
//# sourceMappingURL=account-role.entity.js.map