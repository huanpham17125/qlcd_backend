"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const domainEnum_1 = require("../enum/domainEnum");
const account_entity_1 = require("./account.entity");
const abstractBaseEntityInt_1 = require("./bases/abstractBaseEntityInt");
let SendNotifyLog = class SendNotifyLog extends abstractBaseEntityInt_1.AbstractBaseEntityBigInt {
};
__decorate([
    typeorm_1.Column({ name: 'AccountId' }),
    __metadata("design:type", String)
], SendNotifyLog.prototype, "AccountId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => account_entity_1.default, (account) => account.SendNotifyLog),
    typeorm_1.JoinColumn({ name: 'AccountId' }),
    __metadata("design:type", account_entity_1.default)
], SendNotifyLog.prototype, "Account", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 150 }),
    __metadata("design:type", String)
], SendNotifyLog.prototype, "Title", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 512 }),
    __metadata("design:type", String)
], SendNotifyLog.prototype, "Body", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', charset: 'utf8', length: 1050 }),
    __metadata("design:type", String)
], SendNotifyLog.prototype, "Msg", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', default: domainEnum_1.TypeSendNotify.Schedule }),
    __metadata("design:type", Number)
], SendNotifyLog.prototype, "TypeNotify", void 0);
__decorate([
    typeorm_1.Column({ type: 'int', default: domainEnum_1.StatusSendNotify.Sent }),
    __metadata("design:type", Number)
], SendNotifyLog.prototype, "Status", void 0);
SendNotifyLog = __decorate([
    typeorm_1.Entity({ name: 'SendNotifyLogs' })
], SendNotifyLog);
exports.default = SendNotifyLog;
//# sourceMappingURL=send-notify-log.entity.js.map