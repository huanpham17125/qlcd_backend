"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceName = exports.StatusInviteEmail = exports.StatusSendEmail = exports.TypeSendMail = exports.TypeSendNotify = exports.StatusSendNotify = exports.TypeContentMessage = exports.StatusMessage = exports.TypeToken = exports.TypeGender = void 0;
var TypeGender;
(function (TypeGender) {
    TypeGender["MALE"] = "male";
    TypeGender["FEMALE"] = "female";
    TypeGender["NOPE"] = "";
})(TypeGender = exports.TypeGender || (exports.TypeGender = {}));
var TypeToken;
(function (TypeToken) {
    TypeToken[TypeToken["Access_Token"] = 1] = "Access_Token";
    TypeToken[TypeToken["Refresh_Token"] = 2] = "Refresh_Token";
})(TypeToken = exports.TypeToken || (exports.TypeToken = {}));
var StatusMessage;
(function (StatusMessage) {
    StatusMessage[StatusMessage["Deleted"] = -1] = "Deleted";
    StatusMessage[StatusMessage["Sent"] = 0] = "Sent";
    StatusMessage[StatusMessage["Received"] = 1] = "Received";
    StatusMessage[StatusMessage["Updated"] = 2] = "Updated";
    StatusMessage[StatusMessage["Watched"] = 3] = "Watched";
})(StatusMessage = exports.StatusMessage || (exports.StatusMessage = {}));
var TypeContentMessage;
(function (TypeContentMessage) {
    TypeContentMessage[TypeContentMessage["Document"] = 1] = "Document";
    TypeContentMessage[TypeContentMessage["Schedule"] = 2] = "Schedule";
})(TypeContentMessage = exports.TypeContentMessage || (exports.TypeContentMessage = {}));
var StatusSendNotify;
(function (StatusSendNotify) {
    StatusSendNotify[StatusSendNotify["Sent"] = 0] = "Sent";
    StatusSendNotify[StatusSendNotify["Delete"] = -1] = "Delete";
    StatusSendNotify[StatusSendNotify["Watched"] = 1] = "Watched";
})(StatusSendNotify = exports.StatusSendNotify || (exports.StatusSendNotify = {}));
var TypeSendNotify;
(function (TypeSendNotify) {
    TypeSendNotify[TypeSendNotify["Schedule"] = 1] = "Schedule";
    TypeSendNotify[TypeSendNotify["AdditionalSchedule"] = 0] = "AdditionalSchedule";
    TypeSendNotify[TypeSendNotify["Document"] = 2] = "Document";
    TypeSendNotify[TypeSendNotify["Message"] = 3] = "Message";
})(TypeSendNotify = exports.TypeSendNotify || (exports.TypeSendNotify = {}));
var TypeSendMail;
(function (TypeSendMail) {
    TypeSendMail[TypeSendMail["Register_Office"] = 1] = "Register_Office";
    TypeSendMail[TypeSendMail["Schedule"] = 0] = "Schedule";
    TypeSendMail[TypeSendMail["Invite"] = 2] = "Invite";
    TypeSendMail[TypeSendMail["Message"] = 3] = "Message";
    TypeSendMail[TypeSendMail["ForgotPassword"] = 4] = "ForgotPassword";
})(TypeSendMail = exports.TypeSendMail || (exports.TypeSendMail = {}));
var StatusSendEmail;
(function (StatusSendEmail) {
    StatusSendEmail[StatusSendEmail["Watched"] = 1] = "Watched";
    StatusSendEmail[StatusSendEmail["Sent"] = 0] = "Sent";
    StatusSendEmail[StatusSendEmail["Deleted"] = -1] = "Deleted";
})(StatusSendEmail = exports.StatusSendEmail || (exports.StatusSendEmail = {}));
var StatusInviteEmail;
(function (StatusInviteEmail) {
    StatusInviteEmail[StatusInviteEmail["Sent"] = 0] = "Sent";
    StatusInviteEmail[StatusInviteEmail["Refuse"] = -1] = "Refuse";
    StatusInviteEmail[StatusInviteEmail["Accepted"] = 1] = "Accepted";
})(StatusInviteEmail = exports.StatusInviteEmail || (exports.StatusInviteEmail = {}));
var DeviceName;
(function (DeviceName) {
    DeviceName["WEB"] = "WEB";
    DeviceName["IOS"] = "IOS";
    DeviceName["ANDROID"] = "ANDROID";
})(DeviceName = exports.DeviceName || (exports.DeviceName = {}));
//# sourceMappingURL=domainEnum.js.map