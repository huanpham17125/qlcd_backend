export declare enum TypeGender {
    MALE = "male",
    FEMALE = "female",
    NOPE = ""
}
export declare enum TypeToken {
    Access_Token = 1,
    Refresh_Token = 2
}
export declare enum StatusMessage {
    Deleted = -1,
    Sent = 0,
    Received = 1,
    Updated = 2,
    Watched = 3
}
export declare enum TypeContentMessage {
    Document = 1,
    Schedule = 2
}
export declare enum StatusSendNotify {
    Sent = 0,
    Delete = -1,
    Watched = 1
}
export declare enum TypeSendNotify {
    Schedule = 1,
    AdditionalSchedule = 0,
    Document = 2,
    Message = 3
}
export declare enum TypeSendMail {
    Register_Office = 1,
    Schedule = 0,
    Invite = 2,
    Message = 3,
    ForgotPassword = 4
}
export declare enum StatusSendEmail {
    Watched = 1,
    Sent = 0,
    Deleted = -1
}
export declare enum StatusInviteEmail {
    Sent = 0,
    Refuse = -1,
    Accepted = 1
}
export declare enum DeviceName {
    WEB = "WEB",
    IOS = "IOS",
    ANDROID = "ANDROID"
}
