import { NestSchedule } from 'nest-schedule';
export declare class CronNotifyScheduleService extends NestSchedule {
    cronJob(): Promise<void>;
    onceJob(): void;
    intervalJob(): boolean;
}
