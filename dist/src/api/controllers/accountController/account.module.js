"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const cqrs_1 = require("@nestjs/cqrs");
const jwt_1 = require("@nestjs/jwt");
const platform_express_1 = require("@nestjs/platform-express");
const typeorm_1 = require("@nestjs/typeorm");
const authorize_guard_1 = require("../../guards/authorize.guard");
const create_account_role_handler_1 = require("../../../application/accountRoles/commands/create/create-account-role-handler");
const update_account_by_id_handler_1 = require("../../../application/accounts/commands/update/update-account-by-id-handler");
const update_account_setting_by_account_id_handler_1 = require("../../../application/accounts/commands/update/update-account-setting-by-account-id-handler");
const get_account_data_by_email_handler_1 = require("../../../application/accounts/queries/get-account-data-by-email-handler");
const get_account_profile_handler_1 = require("../../../application/accounts/queries/get-account-profile-handler");
const constants_1 = require("../../../application/common/constants");
const get_role_by_name_handler_1 = require("../../../application/roles/queries/get-role-by-name-handler");
const create_send_mail_log_handler_1 = require("../../../application/sendMailLogs/commands/create/create-send-mail-log-handler");
const account_role_entity_1 = require("../../../domain/entities/account-role.entity");
const account_setting_entity_1 = require("../../../domain/entities/account-setting.entity");
const send_mail_log_entity_1 = require("../../../domain/entities/send-mail-log.entity");
const send_notify_log_entity_1 = require("../../../domain/entities/send-notify-log.entity");
const role_entity_1 = require("../../../domain/entities/role.entity");
const account_role_repository_1 = require("../../../infrastructure/repositories/account-role-repository");
const account_setting_repository_1 = require("../../../infrastructure/repositories/account-setting-repository");
const accountAsyncRepository_1 = require("../../../infrastructure/repositories/accountAsyncRepository");
const send_email_log_repository_1 = require("../../../infrastructure/repositories/send-email-log-repository");
const role_repository_1 = require("../../../infrastructure/repositories/role-repository");
const account_service_1 = require("../../../infrastructure/services/account.service");
const send_mail_service_1 = require("../../../infrastructure/services/send-mail.service");
const token_service_1 = require("../../../infrastructure/services/token.service");
const account_entity_1 = require("../../../domain/entities/account.entity");
const account_controller_1 = require("./account.controller");
let AccountModule = class AccountModule {
};
AccountModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                account_entity_1.default,
                account_setting_entity_1.default,
                send_mail_log_entity_1.default,
                role_entity_1.default,
                account_role_entity_1.default,
                send_notify_log_entity_1.default,
            ]),
            cqrs_1.CqrsModule,
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    signOptions: {
                        expiresIn: config.get('JWT_EXPIRATION_ACCESS'),
                    },
                    secret: config.get('JWT_SECRETKEY'),
                }),
                inject: [config_1.ConfigService],
            }),
            platform_express_1.MulterModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    dest: config.get('RES_AVATAR'),
                }),
                inject: [config_1.ConfigService],
            })
        ],
        controllers: [account_controller_1.AccountController],
        providers: [
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_PROVIDE,
                useClass: accountAsyncRepository_1.AccountAsyncRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_SETTING_PROVIDE,
                useClass: account_setting_repository_1.AccountSettingRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_ROLE,
                useClass: account_role_repository_1.AccountRoleRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ROLE_PROVIDE,
                useClass: role_repository_1.RoleRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.SEND_MAIL_LOG_PROVIDE,
                useClass: send_email_log_repository_1.SendEmailLogRepository,
            },
            authorize_guard_1.AuthorizeGuard,
            get_account_profile_handler_1.GetAccountProfileHandler,
            update_account_by_id_handler_1.UpdateAccountByIdHandler,
            create_account_role_handler_1.CreateAccountRoleHandler,
            get_role_by_name_handler_1.GetRoleByNameHandler,
            update_account_setting_by_account_id_handler_1.UpdateAccountSettingByAccountIdHandler,
            get_account_data_by_email_handler_1.GetAccountDataByEmailHandler,
            create_send_mail_log_handler_1.CreateSendMailLogHandler,
            account_service_1.AccountService,
            token_service_1.TokenService,
            send_mail_service_1.SendMailService,
        ],
    })
], AccountModule);
exports.AccountModule = AccountModule;
//# sourceMappingURL=account.module.js.map