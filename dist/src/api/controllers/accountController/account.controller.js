"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountController = void 0;
const common_1 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const platform_express_1 = require("@nestjs/platform-express");
const swagger_1 = require("@nestjs/swagger");
const authorize_guard_1 = require("../../guards/authorize.guard");
const policy_decorator_1 = require("../../guards/policy.decorator");
const req_update_account_1 = require("../../viewModels/req-update-account");
const res_get_profile_1 = require("../../viewModels/res-get-profile");
const bad_request_exception_1 = require("../../../application/common/exceptions/bad-request-exception");
const account_service_1 = require("../../../infrastructure/services/account.service");
const constants_1 = require("../../../application/common/constants");
const res_api_1 = require("../../viewModels/res-api");
const base_controller_1 = require("../base.controller");
const api_upload_file_decorator_1 = require("../decorators/api-upload-file.decorator");
const multer_1 = require("multer");
const req_account_lock_or_unlock_1 = require("../../viewModels/req-account-lock-or-unlock");
const send_mail_service_1 = require("../../../infrastructure/services/send-mail.service");
let AccountController = class AccountController extends base_controller_1.BaseController {
    constructor(accountService, queryBus, sendMailService) {
        super();
        this.accountService = accountService;
        this.queryBus = queryBus;
        this.sendMailService = sendMailService;
    }
    async Get(req) {
        const itemAccount = await this.accountService.getDetailProfile(req.claims.id);
        const accountSetting = null;
        const roleNormalizedName = itemAccount.AccountRoles[0].Role.NormalizedName;
        const resAccount = new res_get_profile_1.ResGetProfile(itemAccount.Id, itemAccount.Email, itemAccount.FullName, roleNormalizedName, itemAccount.IsActive, roleNormalizedName == constants_1.Constants.AppPolicy.ADMIN ? false : true, true);
        return this.Res(new res_api_1.ResApi().Success(200, resAccount, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async GetAccountById(req, param) {
        const itemAccount = await this.accountService.getDetailProfile(param);
        const accountSetting = null;
        const roleNormalizedName = itemAccount.AccountRoles[0].Role.NormalizedName;
        const resAccount = new res_get_profile_1.ResGetProfile(itemAccount.Id, itemAccount.Email, itemAccount.FullName, roleNormalizedName, itemAccount.IsActive, roleNormalizedName == constants_1.Constants.AppPolicy.USER ? false : true, true);
        return this.Res(new res_api_1.ResApi().Success(200, resAccount, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async UpdateAccount(req, body) {
        const resAccount = await this.accountService.updatelProfile(body, req.claims.id);
        return this.Res(new res_api_1.ResApi().Success(200, resAccount, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async LockOrUnlockAccount(req, body) {
        const resUpdate = await this.accountService.updateStatusAccount(body.type, body.accountId, req.claims.id);
        return this.Res(new res_api_1.ResApi().Success(200, resUpdate, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async UploadAvatar(dataFile) {
        return this.Res(new res_api_1.ResApi().Success(200, dataFile, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiOperation({ summary: 'getAccount' }),
    common_1.Get('Profile'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "Get", null);
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiOperation({ summary: 'getAccount' }),
    common_1.Get('GetById/:id'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "GetAccountById", null);
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiResponse({ status: 422, description: 'Request validation required' }),
    swagger_1.ApiOperation({ summary: 'updateAccount' }),
    common_1.Put('UpdateProfile'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_update_account_1.ReqUpdateAccount]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "UpdateAccount", null);
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiResponse({ status: 422, description: 'Request validation required' }),
    swagger_1.ApiOperation({ summary: 'update status Account' }),
    common_1.Put('UpdateStatus'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_account_lock_or_unlock_1.ReqAccountLockOrUnlock]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "LockOrUnlockAccount", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiResponse({ status: 422, description: 'Request validation required' }),
    swagger_1.ApiOperation({ summary: 'uploadAvatar' }),
    swagger_1.ApiConsumes('multipart/form-data'),
    api_upload_file_decorator_1.ApiUploadFile('file'),
    common_1.Post('UploadAvatar'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', {
        fileFilter: (req, file, cb) => {
            if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
                file.originalname = req.claims.id;
                file.filename = `${file.originalname}`;
                cb(null, true);
            }
            else {
                cb(new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.FILE_NOT_TYPE_IMAGE), false);
            }
        },
        storage: multer_1.diskStorage({
            destination: process.env.RES_AVATAR,
            filename: (req, file, cb) => {
                return cb(null, `${file.originalname + '.jpg'}`);
            },
        }),
    })),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AccountController.prototype, "UploadAvatar", null);
AccountController = __decorate([
    swagger_1.ApiTags('Account'),
    common_1.Controller('Account'),
    swagger_1.ApiBearerAuth('access-token'),
    __metadata("design:paramtypes", [account_service_1.AccountService,
        cqrs_1.QueryBus,
        send_mail_service_1.SendMailService])
], AccountController);
exports.AccountController = AccountController;
//# sourceMappingURL=account.controller.js.map