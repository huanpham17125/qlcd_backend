import { QueryBus } from '@nestjs/cqrs';
import { ReqUpdateAccount } from 'src/api/viewModels/req-update-account';
import { AccountService } from 'src/infrastructure/services/account.service';
import { ResApi } from '../../viewModels/res-api';
import { BaseController } from '../base.controller';
import { ReqAccountLockOrUnlock } from 'src/api/viewModels/req-account-lock-or-unlock';
import { SendMailService } from 'src/infrastructure/services/send-mail.service';
export declare class AccountController extends BaseController {
    private readonly accountService;
    private readonly queryBus;
    private readonly sendMailService;
    constructor(accountService: AccountService, queryBus: QueryBus, sendMailService: SendMailService);
    Get(req: any): Promise<ResApi>;
    GetAccountById(req: any, param: string): Promise<ResApi>;
    UpdateAccount(req: any, body: ReqUpdateAccount): Promise<ResApi>;
    LockOrUnlockAccount(req: any, body: ReqAccountLockOrUnlock): Promise<ResApi>;
    UploadAvatar(dataFile: any): Promise<ResApi>;
}
