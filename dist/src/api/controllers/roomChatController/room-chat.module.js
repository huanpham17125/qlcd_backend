"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomChatModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const cqrs_1 = require("@nestjs/cqrs");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const authorize_guard_1 = require("../../guards/authorize.guard");
const get_account_data_by_email_handler_1 = require("../../../application/accounts/queries/get-account-data-by-email-handler");
const constants_1 = require("../../../application/common/constants");
const create_room_chat_handler_1 = require("../../../application/roomChats/commands/create/create-room-chat-handler");
const get_all_room_by_account_id_handler_1 = require("../../../application/roomChats/queries/get-all-room-by-account-id-handler");
const get_room_by_creator_id_and_created_id_handler_1 = require("../../../application/roomChats/queries/get-room-by-creator-id-and-created-id-handler");
const account_room_entity_gateway_1 = require("../../../domain/entities/account-room.entity-gateway");
const account_setting_entity_1 = require("../../../domain/entities/account-setting.entity");
const account_entity_1 = require("../../../domain/entities/account.entity");
const message_entity_gateway_1 = require("../../../domain/entities/message.entity-gateway");
const room_chat_entity_gateway_1 = require("../../../domain/entities/room-chat.entity-gateway");
const account_room_repository_1 = require("../../../infrastructure/repositories/account-room-repository");
const accountAsyncRepository_1 = require("../../../infrastructure/repositories/accountAsyncRepository");
const message_repository_1 = require("../../../infrastructure/repositories/message-repository");
const room_chat_repository_1 = require("../../../infrastructure/repositories/room-chat-repository");
const account_service_1 = require("../../../infrastructure/services/account.service");
const room_chat_service_1 = require("../../../infrastructure/services/room-chat.service");
const token_service_1 = require("../../../infrastructure/services/token.service");
const room_chat_controller_1 = require("./room-chat.controller");
let RoomChatModule = class RoomChatModule {
};
RoomChatModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                account_entity_1.default,
                account_setting_entity_1.default,
                room_chat_entity_gateway_1.default,
                account_room_entity_gateway_1.default,
                message_entity_gateway_1.default,
            ]),
            cqrs_1.CqrsModule,
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    signOptions: {
                        expiresIn: config.get('JWT_EXPIRATION_ACCESS'),
                    },
                    secret: config.get('JWT_SECRETKEY'),
                }),
                inject: [config_1.ConfigService],
            }),
        ],
        controllers: [room_chat_controller_1.RoomChatController],
        providers: [
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_PROVIDE,
                useClass: accountAsyncRepository_1.AccountAsyncRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE,
                useClass: room_chat_repository_1.RoomChatRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.MESSAGE_PROVIDE,
                useClass: message_repository_1.MessageRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
                useClass: account_room_repository_1.AccountRoomRepository,
            },
            authorize_guard_1.AuthorizeGuard,
            account_service_1.AccountService,
            token_service_1.TokenService,
            room_chat_service_1.RoomChatService,
            get_all_room_by_account_id_handler_1.GetAllRoomByAccountIdHandler,
            get_account_data_by_email_handler_1.GetAccountDataByEmailHandler,
            create_room_chat_handler_1.CreateRoomChatHandler,
            get_room_by_creator_id_and_created_id_handler_1.GetRoomByCreatorIdAndCreatedIdHandler,
        ],
    })
], RoomChatModule);
exports.RoomChatModule = RoomChatModule;
//# sourceMappingURL=room-chat.module.js.map