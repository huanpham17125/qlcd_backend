"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomChatController = void 0;
const common_1 = require("@nestjs/common");
const common_2 = require("@nestjs/common");
const cqrs_1 = require("@nestjs/cqrs");
const swagger_1 = require("@nestjs/swagger");
const authorize_guard_1 = require("../../guards/authorize.guard");
const policy_decorator_1 = require("../../guards/policy.decorator");
const req_add_account_to_room_1 = require("../../viewModels/req-add-account-to-room");
const req_create_room_chat_1 = require("../../viewModels/req-create-room-chat");
const res_api_1 = require("../../viewModels/res-api");
const constants_1 = require("../../../application/common/constants");
const room_chat_service_1 = require("../../../infrastructure/services/room-chat.service");
const base_controller_1 = require("../base.controller");
let RoomChatController = class RoomChatController extends base_controller_1.BaseController {
    constructor(commandBus, roomService) {
        super();
        this.commandBus = commandBus;
        this.roomService = roomService;
    }
    async GetRoomChat(req) {
        const lst = await this.roomService.fillAllByAccountId(req.claims.id);
        return this.Res(new res_api_1.ResApi().Success(200, lst, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async CreateRoomChat(req, body) {
        const item = await this.roomService.createRoom(req.claims.id, body.email, '', req.claims.templeId);
        return this.Res(new res_api_1.ResApi().Success(201, item, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async AddAccountToRoomChat(req, body) {
        const itemRoomChat = await this.roomService.addAccountToRoom(req.claims.id, body.roomId, body.lstEmail);
        return this.Res(new res_api_1.ResApi().Success(201, itemRoomChat, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiOperation({ summary: 'get list room chat pagination' }),
    common_1.Get(),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RoomChatController.prototype, "GetRoomChat", null);
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 400, description: 'Account not found' }),
    swagger_1.ApiOperation({ summary: 'create room chat' }),
    common_1.Post('CreateRoomChat'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_create_room_chat_1.ReqCreateRoomChat]),
    __metadata("design:returntype", Promise)
], RoomChatController.prototype, "CreateRoomChat", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiOperation({ summary: 'add more account to room chat' }),
    common_1.Post('AddAccountToRoom'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_add_account_to_room_1.ReqAddAccountToRoom]),
    __metadata("design:returntype", Promise)
], RoomChatController.prototype, "AddAccountToRoomChat", null);
RoomChatController = __decorate([
    swagger_1.ApiTags('RoomChat'),
    common_2.Controller('Room'),
    swagger_1.ApiBearerAuth('access-token'),
    __metadata("design:paramtypes", [cqrs_1.CommandBus,
        room_chat_service_1.RoomChatService])
], RoomChatController);
exports.RoomChatController = RoomChatController;
//# sourceMappingURL=room-chat.controller.js.map