import { CommandBus } from '@nestjs/cqrs';
import { ReqAddAccountToRoom } from 'src/api/viewModels/req-add-account-to-room';
import { ReqCreateRoomChat } from 'src/api/viewModels/req-create-room-chat';
import { ResApi } from 'src/api/viewModels/res-api';
import { RoomChatService } from 'src/infrastructure/services/room-chat.service';
import { BaseController } from '../base.controller';
export declare class RoomChatController extends BaseController {
    private readonly commandBus;
    private readonly roomService;
    constructor(commandBus: CommandBus, roomService: RoomChatService);
    GetRoomChat(req: any): Promise<ResApi>;
    CreateRoomChat(req: any, body: ReqCreateRoomChat): Promise<ResApi>;
    AddAccountToRoomChat(req: any, body: ReqAddAccountToRoom): Promise<ResApi>;
}
