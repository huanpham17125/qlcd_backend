"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotifyController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const authorize_guard_1 = require("../../guards/authorize.guard");
const policy_decorator_1 = require("../../guards/policy.decorator");
const req_create_sendnotify_message_1 = require("../../viewModels/req-create-sendnotify-message");
const req_update_notify_message_by_id_1 = require("../../viewModels/req-update-notify-message-by-id");
const res_api_1 = require("../../viewModels/res-api");
const constants_1 = require("../../../application/common/constants");
const send_notify_service_1 = require("../../../infrastructure/services/send-notify.service");
const base_controller_1 = require("../base.controller");
let SendNotifyController = class SendNotifyController extends base_controller_1.BaseController {
    constructor(sendNotifyService) {
        super();
        this.sendNotifyService = sendNotifyService;
    }
    async PushNotify(req, body) {
        const item = await this.sendNotifyService.sendNotifyToFirebase(req.claims.id, body);
        return this.Res(new res_api_1.ResApi().Success(201, item, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async GetNotify(req) {
        const lst = await this.sendNotifyService.getActiveNotifyMessage(req.claims.id);
        return this.Res(new res_api_1.ResApi().Success(200, lst, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async UpdateNotifyById(req, body) {
        const resUpdate = await this.sendNotifyService.updateNotifyMessageById(body.id, req.claims.id, body.status);
        return this.Res(new res_api_1.ResApi().Success(200, resUpdate, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 422, description: 'Error validation' }),
    swagger_1.ApiOperation({ summary: 'Create notify' }),
    common_1.Post('CreateNewNotify'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_create_sendnotify_message_1.ReqCreateSendnotifyMessage]),
    __metadata("design:returntype", Promise)
], SendNotifyController.prototype, "PushNotify", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 422, description: 'Error validation' }),
    swagger_1.ApiOperation({ summary: 'Get Notify By AccountId' }),
    common_1.Get('GetNotify'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SendNotifyController.prototype, "GetNotify", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiResponse({ status: 422, description: 'Error validation' }),
    swagger_1.ApiOperation({ summary: 'Update notify by id' }),
    common_1.Put('UpdateNotifyById'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_update_notify_message_by_id_1.ReqUpdateNotifyMessageById]),
    __metadata("design:returntype", Promise)
], SendNotifyController.prototype, "UpdateNotifyById", null);
SendNotifyController = __decorate([
    swagger_1.ApiTags('Notify'),
    common_1.Controller('Notify'),
    swagger_1.ApiBearerAuth('access-token'),
    __metadata("design:paramtypes", [send_notify_service_1.SendNotifyService])
], SendNotifyController);
exports.SendNotifyController = SendNotifyController;
//# sourceMappingURL=send-notify.controller.js.map