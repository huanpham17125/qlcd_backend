"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendNotifyModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const cqrs_1 = require("@nestjs/cqrs");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const authorize_guard_1 = require("../../guards/authorize.guard");
const constants_1 = require("../../../application/common/constants");
const create_notify_message_handler_1 = require("../../../application/sendNotifiMessage/commands/create/create-notify-message-handler");
const delete_notify_message_by_id_handler_1 = require("../../../application/sendNotifiMessage/commands/delete/delete-notify-message-by-id-handler");
const update_notify_message_by_id_handler_1 = require("../../../application/sendNotifiMessage/commands/update/update-notify-message-by-id-handler");
const get_notify_message_by_account_id_handler_1 = require("../../../application/sendNotifiMessage/queries/get-notify-message-by-account-id-handler");
const account_role_entity_1 = require("../../../domain/entities/account-role.entity");
const account_setting_entity_1 = require("../../../domain/entities/account-setting.entity");
const account_entity_1 = require("../../../domain/entities/account.entity");
const send_notify_log_entity_1 = require("../../../domain/entities/send-notify-log.entity");
const account_setting_repository_1 = require("../../../infrastructure/repositories/account-setting-repository");
const accountAsyncRepository_1 = require("../../../infrastructure/repositories/accountAsyncRepository");
const send_notify_message_log_repository_1 = require("../../../infrastructure/repositories/send-notify-message-log-repository");
const account_service_1 = require("../../../infrastructure/services/account.service");
const fire_store_service_1 = require("../../../infrastructure/services/fire-store.service");
const send_notify_service_1 = require("../../../infrastructure/services/send-notify.service");
const token_service_1 = require("../../../infrastructure/services/token.service");
const send_notify_controller_1 = require("./send-notify.controller");
let SendNotifyModule = class SendNotifyModule {
};
SendNotifyModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                account_entity_1.default,
                account_setting_entity_1.default,
                account_role_entity_1.default,
                send_notify_log_entity_1.default,
            ]),
            cqrs_1.CqrsModule,
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    signOptions: {
                        expiresIn: config.get('JWT_EXPIRATION_ACCESS'),
                    },
                    secret: config.get('JWT_SECRETKEY'),
                }),
                inject: [config_1.ConfigService],
            }),
        ],
        controllers: [send_notify_controller_1.SendNotifyController],
        providers: [
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_PROVIDE,
                useClass: accountAsyncRepository_1.AccountAsyncRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_SETTING_PROVIDE,
                useClass: account_setting_repository_1.AccountSettingRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.SEND_NOTIFYMESSAGE_LOG_PROVIDE,
                useClass: send_notify_message_log_repository_1.SendNotifyMessageLogRepository,
            },
            authorize_guard_1.AuthorizeGuard,
            account_service_1.AccountService,
            token_service_1.TokenService,
            fire_store_service_1.FireStoreService,
            send_notify_service_1.SendNotifyService,
            create_notify_message_handler_1.CreateNotifyMessageHandler,
            get_notify_message_by_account_id_handler_1.GetNotifyMessageByAccountIdHandler,
            delete_notify_message_by_id_handler_1.DeleteNotifyMessageByIdHandler,
            update_notify_message_by_id_handler_1.UpdateNotifyMessageByIdHandler,
        ]
    })
], SendNotifyModule);
exports.SendNotifyModule = SendNotifyModule;
//# sourceMappingURL=send-notify.module.js.map