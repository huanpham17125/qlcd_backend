import { ReqCreateSendnotifyMessage } from 'src/api/viewModels/req-create-sendnotify-message';
import { ReqUpdateNotifyMessageById } from 'src/api/viewModels/req-update-notify-message-by-id';
import { ResApi } from 'src/api/viewModels/res-api';
import { SendNotifyService } from 'src/infrastructure/services/send-notify.service';
import { BaseController } from '../base.controller';
export declare class SendNotifyController extends BaseController {
    private readonly sendNotifyService;
    constructor(sendNotifyService: SendNotifyService);
    PushNotify(req: any, body: ReqCreateSendnotifyMessage): Promise<ResApi>;
    GetNotify(req: any): Promise<ResApi>;
    UpdateNotifyById(req: any, body: ReqUpdateNotifyMessageById): Promise<ResApi>;
}
