"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiUploadFile = void 0;
const swagger_1 = require("@nestjs/swagger");
exports.ApiUploadFile = (fileName = 'file') => (target, propertyKey, descriptor) => {
    swagger_1.ApiBody({
        schema: {
            type: 'object',
            properties: {
                [fileName]: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })(target, propertyKey, descriptor);
};
//# sourceMappingURL=api-upload-file.decorator.js.map