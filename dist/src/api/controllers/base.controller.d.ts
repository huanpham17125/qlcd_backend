import { ResApi } from '../viewModels/res-api';
export declare class BaseController {
    protected Res(res: ResApi): ResApi;
}
