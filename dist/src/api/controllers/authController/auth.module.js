"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const create_account_handler_1 = require("./../../../application/accounts/commands/create/create-account-handler");
const common_1 = require("@nestjs/common");
const auth_controller_1 = require("./auth.controller");
const accountAsyncRepository_1 = require("../../../infrastructure/repositories/accountAsyncRepository");
const cqrs_1 = require("@nestjs/cqrs");
const account_service_1 = require("../../../infrastructure/services/account.service");
const token_service_1 = require("../../../infrastructure/services/token.service");
const jwt_1 = require("@nestjs/jwt");
const get_account_by_email_handler_1 = require("../../../application/accounts/queries/get-account-by-email-handler");
const password_service_1 = require("../../../infrastructure/services/password.service");
const typeorm_1 = require("@nestjs/typeorm");
const account_entity_1 = require("../../../domain/entities/account.entity");
const role_entity_1 = require("../../../domain/entities/role.entity");
const account_role_entity_1 = require("../../../domain/entities/account-role.entity");
const account_security_token_entity_1 = require("../../../domain/entities/account-security-token.entity");
const create_account_security_token_handler_1 = require("../../../application/accountSecurityTokens/commands/create/create-account-security-token-handler");
const account_security_token_reposity_1 = require("../../../infrastructure/repositories/account-security-token-reposity");
const account_security_token_service_1 = require("../../../infrastructure/services/account-security-token.service");
const get_by_account_id_handler_1 = require("../../../application/accountSecurityTokens/queries/get-by-account-id-handler");
const update_account_security_token_handler_1 = require("../../../application/accountSecurityTokens/commands/update/update-account-security-token-handler");
const account_setting_entity_1 = require("../../../domain/entities/account-setting.entity");
const update_password_by_id_handler_1 = require("../../../application/accounts/commands/update/update-password-by-id-handler");
const get_account_by_id_handler_1 = require("../../../application/accounts/queries/get-account-by-id-handler");
const config_1 = require("@nestjs/config");
const constants_1 = require("../../../application/common/constants");
const account_role_repository_1 = require("../../../infrastructure/repositories/account-role-repository");
const role_repository_1 = require("../../../infrastructure/repositories/role-repository");
const create_account_role_handler_1 = require("../../../application/accountRoles/commands/create/create-account-role-handler");
const get_role_by_name_handler_1 = require("../../../application/roles/queries/get-role-by-name-handler");
const account_log_repository_1 = require("../../../infrastructure/repositories/account-log-repository");
const create_account_log_handler_1 = require("../../../application/accountLogs/commands/create/create-account-log-handler");
const update_status_account_handler_1 = require("../../../application/accounts/commands/update/update-status-account-handler");
const account_log_entity_1 = require("../../../domain/entities/account-log.entity");
const find_account_by_email_handler_1 = require("../../../application/accounts/queries/find-account-by-email-handler");
const authorize_guard_1 = require("../../guards/authorize.guard");
const send_mail_service_1 = require("../../../infrastructure/services/send-mail.service");
const detail_account_by_email_handler_1 = require("../../../application/accounts/queries/detail-account-by-email-handler");
const register_temp_handler_1 = require("../../../application/accounts/commands/create/register-temp-handler");
const send_email_log_repository_1 = require("../../../infrastructure/repositories/send-email-log-repository");
const create_send_mail_log_handler_1 = require("../../../application/sendMailLogs/commands/create/create-send-mail-log-handler");
const send_mail_log_entity_1 = require("../../../domain/entities/send-mail-log.entity");
const message_service_1 = require("../../../infrastructure/services/message.service");
const get_room_by_account_id_handler_1 = require("../../../application/roomChats/queries/get-room-by-account-id-handler");
const create_room_chat_handler_1 = require("../../../application/roomChats/commands/create/create-room-chat-handler");
const create_account_room_handler_1 = require("../../../application/accountRooms/commands/create/create-account-room-handler");
const get_room_by_temple_id_handler_1 = require("../../../application/roomChats/queries/get-room-by-temple-id-handler");
const room_chat_entity_gateway_1 = require("../../../domain/entities/room-chat.entity-gateway");
const account_room_entity_gateway_1 = require("../../../domain/entities/account-room.entity-gateway");
const account_room_repository_1 = require("../../../infrastructure/repositories/account-room-repository");
const room_chat_repository_1 = require("../../../infrastructure/repositories/room-chat-repository");
const room_chat_service_1 = require("../../../infrastructure/services/room-chat.service");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            cqrs_1.CqrsModule,
            typeorm_1.TypeOrmModule.forFeature([
                account_entity_1.default,
                account_log_entity_1.default,
                role_entity_1.default,
                account_role_entity_1.default,
                account_security_token_entity_1.default,
                account_setting_entity_1.default,
                send_mail_log_entity_1.default,
                room_chat_entity_gateway_1.default,
                account_room_entity_gateway_1.default,
            ]),
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                useFactory: async (config) => ({
                    signOptions: {
                        expiresIn: config.get('JWT_EXPIRATION_ACCESS'),
                    },
                    secret: config.get('JWT_SECRETKEY'),
                }),
                inject: [config_1.ConfigService],
            }),
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [
            {
                provide: 'ACCOUNTS',
                useClass: accountAsyncRepository_1.AccountAsyncRepository,
            },
            {
                provide: 'ACCOUNSECURITYTOKENS',
                useClass: account_security_token_reposity_1.AccountSecurityTokenReposity,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_ROLE,
                useClass: account_role_repository_1.AccountRoleRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ROLE_PROVIDE,
                useClass: role_repository_1.RoleRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_LOG_PROVIDE,
                useClass: account_log_repository_1.AccountLogRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.SEND_MAIL_LOG_PROVIDE,
                useClass: send_email_log_repository_1.SendEmailLogRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ACCOUNT_ROOM_PROVIDE,
                useClass: account_room_repository_1.AccountRoomRepository,
            },
            {
                provide: constants_1.Constants.AppProvide.ROOM_CHAT_PROVIDE,
                useClass: room_chat_repository_1.RoomChatRepository,
            },
            authorize_guard_1.AuthorizeGuard,
            account_service_1.AccountService,
            password_service_1.PasswordService,
            account_security_token_service_1.AccountSecurityTokenService,
            token_service_1.TokenService,
            send_mail_service_1.SendMailService,
            message_service_1.MessageService,
            room_chat_service_1.RoomChatService,
            create_account_handler_1.CreateAccountHandler,
            get_account_by_email_handler_1.GetAccountByEmailHandler,
            create_account_security_token_handler_1.CreateAccountSecurityTokenHandler,
            get_by_account_id_handler_1.GetByAccountIdHandler,
            update_account_security_token_handler_1.UpdateAccountSecurityTokenHandler,
            update_password_by_id_handler_1.UpdatePasswordByIdHandler,
            get_account_by_id_handler_1.GetAccountByIdHandler,
            create_account_role_handler_1.CreateAccountRoleHandler,
            get_role_by_name_handler_1.GetRoleByNameHandler,
            create_account_log_handler_1.CreateAccountLogHandler,
            update_status_account_handler_1.UpdateStatusAccountHandler,
            find_account_by_email_handler_1.FindAccountByEmailHandler,
            get_role_by_name_handler_1.GetRoleByNameHandler,
            detail_account_by_email_handler_1.DetailAccountByEmailHandler,
            register_temp_handler_1.RegisterTempHandler,
            create_send_mail_log_handler_1.CreateSendMailLogHandler,
            get_room_by_account_id_handler_1.GetRoomByAccountIdHandler,
            create_room_chat_handler_1.CreateRoomChatHandler,
            create_account_room_handler_1.CreateAccountRoomHandler,
            get_room_by_temple_id_handler_1.GetRoomByTempleIdHandler,
        ],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map