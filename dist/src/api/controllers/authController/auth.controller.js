"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const base_controller_1 = require("../base.controller");
const swagger_1 = require("@nestjs/swagger");
const res_api_1 = require("../../viewModels/res-api");
const constants_1 = require("../../../application/common/constants");
const req_register_1 = require("../../viewModels/req-register");
const account_service_1 = require("../../../infrastructure/services/account.service");
const token_service_1 = require("../../../infrastructure/services/token.service");
const res_login_1 = require("../../viewModels/res-login");
const req_login_1 = require("../../viewModels/req-login");
const domainEnum_1 = require("../../../domain/enum/domainEnum");
const account_security_token_service_1 = require("../../../infrastructure/services/account-security-token.service");
const req_change_password_1 = require("../../viewModels/req-change-password");
const authorize_guard_1 = require("../../guards/authorize.guard");
const policy_decorator_1 = require("../../guards/policy.decorator");
const password_service_1 = require("../../../infrastructure/services/password.service");
const bad_request_exception_1 = require("../../../application/common/exceptions/bad-request-exception");
const cqrs_1 = require("@nestjs/cqrs");
const create_account_log_command_1 = require("../../../application/accountLogs/commands/create/create-account-log-command");
const req_register_user_1 = require("../../viewModels/req-register-user");
const send_mail_service_1 = require("../../../infrastructure/services/send-mail.service");
const req_forgot_password_1 = require("../../viewModels/req-forgot-password");
const req_reset_password_1 = require("../../viewModels/req-reset-password");
const message_service_1 = require("../../../infrastructure/services/message.service");
const room_chat_service_1 = require("../../../infrastructure/services/room-chat.service");
let AuthController = class AuthController extends base_controller_1.BaseController {
    constructor(accountService, tokenService, commandBus, passwordService, accountSecurityTokenService, sendMailService, messageService, roomChatService) {
        super();
        this.accountService = accountService;
        this.tokenService = tokenService;
        this.commandBus = commandBus;
        this.passwordService = passwordService;
        this.accountSecurityTokenService = accountSecurityTokenService;
        this.sendMailService = sendMailService;
        this.messageService = messageService;
        this.roomChatService = roomChatService;
    }
    async Register(req, reqRegister) {
        const newAccount = await this.accountService.createAccount(reqRegister, req.claims.id);
        const accessToken = await this.tokenService.generateAccessToken(newAccount);
        const resLogin = new res_login_1.ResLogin(accessToken.accessToken, accessToken.expiresIn, accessToken.expiresDate, '');
        return this.Res(new res_api_1.ResApi().Success(constants_1.Constants.AppStatus.StatusCode201, resLogin, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async Login(reqlogin) {
        const account = await this.accountService.getAccountByEmailAndPassword(reqlogin.email, reqlogin.password);
        await this.commandBus.execute(new create_account_log_command_1.CreateAccountLogCommand(account.Id));
        const accessToken = await this.tokenService.generateAccessToken(account);
        const accountToken = await this.accountSecurityTokenService.getByAccountId(account.Id);
        if (!accountToken) {
            await this.accountSecurityTokenService.create(account.Id, domainEnum_1.TypeToken.Access_Token, accessToken.accessToken, accessToken.expiresDate);
        }
        else {
            await this.accountSecurityTokenService.update(accessToken.accessToken, accessToken.expiresDate, accountToken.Id);
        }
        const resLogin = new res_login_1.ResLogin(accessToken.accessToken, accessToken.expiresIn, accessToken.expiresDate, '');
        return this.Res(new res_api_1.ResApi().Success(constants_1.Constants.AppStatus.StatusCode200, resLogin, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async RegisterUser(reqBody) {
        const resAccount = await this.accountService.adminCreateAccount(reqBody);
        const account = await this.accountService.getAccountByEmail(resAccount.Email);
        const accessToken = await this.tokenService.generateAccessToken(account);
        const resToken = new res_login_1.ResLogin(accessToken.accessToken, accessToken.expiresIn, accessToken.expiresDate, '');
        return this.Res(new res_api_1.ResApi().Success(constants_1.Constants.AppStatus.StatusCode201, resToken, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async ChangePassword(req, body) {
        if (body.passwordConfirm != body.passwordNew) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.PASSWORD_DISPARITY);
        }
        const account = await this.accountService.getAccountById(req.claims.id);
        const verifyPassword = await this.passwordService.comparePassword(body.password, account);
        if (!verifyPassword) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.PASSWORD_INCORECT);
        }
        const newPasswordHash = await this.passwordService.getPasswordHash(body.passwordNew, account);
        await this.accountService.changePassword(newPasswordHash.PasswordHash, account.Id);
        return this.Res(new res_api_1.ResApi().Success(200, null, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
    async ForgotPassword(body) {
        const account = await this.accountService.getAccountByEmail(body.email);
        this.sendMailService.sendMailForgotPassword(account.Id, account.Email, constants_1.Constants.AppSubjectSendEmail.RESET_PASSWORD);
        return this.Res(new res_api_1.ResApi().Success(200, constants_1.Constants.AppResource.LOG_IN_EMAIL_TO_RESET_PASSWORD, constants_1.Constants.AppResource.LOG_IN_EMAIL_TO_RESET_PASSWORD));
    }
    async ResetPassword(body) {
        const account = await this.accountService.getAccountByEmail(body.email);
        if (body.passwordConfirm != body.passwordNew) {
            throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.PASSWORD_DISPARITY);
        }
        const newPasswordHash = await this.passwordService.getPasswordHash(body.passwordNew, account);
        await this.accountService.changePassword(newPasswordHash.PasswordHash, account.Id);
        return this.Res(new res_api_1.ResApi().Success(200, constants_1.Constants.AppResource.RESET_PASSWORD_SUCCESS, constants_1.Constants.AppResource.RESET_PASSWORD_SUCCESS));
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'Data already exist' }),
    swagger_1.ApiOperation({ summary: 'Register' }),
    common_1.Post('Register'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_register_1.ReqRegister]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "Register", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'data not found' }),
    swagger_1.ApiOperation({ summary: 'Login' }),
    common_1.Post('Login'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_login_1.ReqLogin]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "Login", null);
__decorate([
    swagger_1.ApiResponse({ status: 201, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'Data already exist' }),
    swagger_1.ApiOperation({ summary: 'register user' }),
    common_1.Post('Register/User'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_register_user_1.ReqRegisterUser]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "RegisterUser", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'User not found' }),
    swagger_1.ApiOperation({ summary: 'ChangePassword' }),
    swagger_1.ApiBearerAuth('access-token'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    common_1.Put('ChangePassword'),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_change_password_1.ReqChangePassword]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "ChangePassword", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'User not found' }),
    swagger_1.ApiOperation({ summary: 'Forgot password' }),
    common_1.Post('ForgotPassword'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_forgot_password_1.ReqForgotPassword]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "ForgotPassword", null);
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 422, description: 'Validation' }),
    swagger_1.ApiResponse({ status: 400, description: 'User not found' }),
    swagger_1.ApiOperation({ summary: 'Reset password' }),
    common_1.Put('ResetPassword'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [req_reset_password_1.ReqResetPassword]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "ResetPassword", null);
AuthController = __decorate([
    swagger_1.ApiTags('Auth'),
    common_1.Controller('Auth'),
    __metadata("design:paramtypes", [account_service_1.AccountService,
        token_service_1.TokenService,
        cqrs_1.CommandBus,
        password_service_1.PasswordService,
        account_security_token_service_1.AccountSecurityTokenService,
        send_mail_service_1.SendMailService,
        message_service_1.MessageService,
        room_chat_service_1.RoomChatService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map