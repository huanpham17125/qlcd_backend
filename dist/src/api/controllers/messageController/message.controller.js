"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const authorize_guard_1 = require("../../guards/authorize.guard");
const policy_decorator_1 = require("../../guards/policy.decorator");
const req_get_message_by_room_id_1 = require("../../viewModels/req-get-message-by-room-id");
const res_api_1 = require("../../viewModels/res-api");
const constants_1 = require("../../../application/common/constants");
const message_service_1 = require("../../../infrastructure/services/message.service");
const base_controller_1 = require("../base.controller");
let MessageController = class MessageController extends base_controller_1.BaseController {
    constructor(messageService) {
        super();
        this.messageService = messageService;
    }
    async GetMessage(req, parram) {
        const lst = await this.messageService.getMessageByRoomId(req.claims.id, parram.pageSize ? parram.pageSize : 1, parram.pageNumber ? parram.pageNumber : 10);
        return this.Res(new res_api_1.ResApi().Success(200, lst, constants_1.Constants.AppResource.ACCTION_SUCCESS));
    }
};
__decorate([
    swagger_1.ApiResponse({ status: 200, description: 'Success.' }),
    swagger_1.ApiResponse({ status: 400, description: 'Error request.' }),
    swagger_1.ApiResponse({ status: 401, description: 'UnAuthorize' }),
    swagger_1.ApiOperation({ summary: 'get list message by roomId pagination' }),
    common_1.Get('/:pageSize/:pageNumber'),
    common_1.UseGuards(authorize_guard_1.AuthorizeGuard),
    policy_decorator_1.Policies(),
    __param(0, common_1.Req()), __param(1, common_1.Param()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, req_get_message_by_room_id_1.ReqGetMessageByRoomId]),
    __metadata("design:returntype", Promise)
], MessageController.prototype, "GetMessage", null);
MessageController = __decorate([
    swagger_1.ApiTags('Message'),
    common_1.Controller('Message'),
    swagger_1.ApiBearerAuth('access-token'),
    __metadata("design:paramtypes", [message_service_1.MessageService])
], MessageController);
exports.MessageController = MessageController;
//# sourceMappingURL=message.controller.js.map