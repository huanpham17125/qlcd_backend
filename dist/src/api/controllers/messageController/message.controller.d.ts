import { ReqGetMessageByRoomId } from 'src/api/viewModels/req-get-message-by-room-id';
import { ResApi } from 'src/api/viewModels/res-api';
import { MessageService } from 'src/infrastructure/services/message.service';
import { BaseController } from '../base.controller';
export declare class MessageController extends BaseController {
    private readonly messageService;
    constructor(messageService: MessageService);
    GetMessage(req: any, parram: ReqGetMessageByRoomId): Promise<ResApi>;
}
