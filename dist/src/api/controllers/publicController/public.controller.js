"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicController = void 0;
const common_1 = require("@nestjs/common");
const common_2 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const constants_1 = require("../../../application/common/constants");
const public_service_1 = require("../../../infrastructure/services/public.service");
const fs = require("fs");
const bad_request_exception_1 = require("../../../application/common/exceptions/bad-request-exception");
const cqrs_1 = require("@nestjs/cqrs");
let PublicController = class PublicController {
    constructor(publicService, queryBus) {
        this.publicService = publicService;
        this.queryBus = queryBus;
    }
    async uploadAvatar(param, res) {
        let urlImg = `${process.env.RES_AVATAR + param + '.jpg'}`;
        if (!fs.existsSync(urlImg)) {
            urlImg = `${process.env.RES_AVATAR + process.env.DEFAULT_AVATAR}`;
        }
        fs.readFile(urlImg, (err, data) => {
            if (err) {
                throw new bad_request_exception_1.BadRequestException(constants_1.Constants.AppResource.EXCEPTION_UNKNOWN);
            }
            res.end(data);
        });
    }
    async getLogo(res) {
        const urlImg = `${process.env.RES_LOGO + 'logo.png'}`;
        fs.readFile(urlImg, (err, data) => {
            res.end(data);
        });
    }
    async getConfig(res) {
        return res.send({
            resource: process.env.APP_DOMAIN,
            webUrl: '',
            androidUrl: '',
            iosUrl: '',
            appVersion: {
                android: '1.1.10',
                ios: '1.1.10',
                news: ['Please download the latest version of APP!'],
            },
        });
    }
};
__decorate([
    swagger_1.ApiOperation({ summary: 'get avatar' }),
    common_1.HttpCode(constants_1.Constants.AppStatus.StatusCode200),
    common_1.Header('Content-Type', 'image/jpg'),
    common_1.Get('Avt/:id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "uploadAvatar", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'get logo' }),
    common_1.HttpCode(constants_1.Constants.AppStatus.StatusCode200),
    common_1.Header('Content-Type', 'image/jpg'),
    common_1.Get('logo'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getLogo", null);
__decorate([
    swagger_1.ApiOperation({ summary: 'get config' }),
    common_1.HttpCode(constants_1.Constants.AppStatus.StatusCode200),
    common_1.Get('GetConfig'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PublicController.prototype, "getConfig", null);
PublicController = __decorate([
    common_2.Controller('Public'),
    swagger_1.ApiTags('Public'),
    __metadata("design:paramtypes", [public_service_1.PublicService,
        cqrs_1.QueryBus])
], PublicController);
exports.PublicController = PublicController;
//# sourceMappingURL=public.controller.js.map