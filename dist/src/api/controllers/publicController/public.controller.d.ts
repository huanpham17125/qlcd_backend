import { PublicService } from 'src/infrastructure/services/public.service';
import { QueryBus } from '@nestjs/cqrs';
export declare class PublicController {
    private readonly publicService;
    private readonly queryBus;
    constructor(publicService: PublicService, queryBus: QueryBus);
    uploadAvatar(param: string, res: any): Promise<void>;
    getLogo(res: any): Promise<void>;
    getConfig(res: any): Promise<any>;
}
