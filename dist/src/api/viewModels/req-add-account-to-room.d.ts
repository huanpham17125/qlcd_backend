import { ReqGetAccountByEmail } from './req-get-account-by-email';
export declare class ReqAddAccountToRoom {
    lstEmail: ReqGetAccountByEmail[];
    roomId: string;
}
