"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResGetNotify = void 0;
const domainEnum_1 = require("../../domain/enum/domainEnum");
class ResGetNotify {
    constructor(itemNotify, enventName, dankaName, houseHolderName, passedPersonName) {
        this.id = itemNotify.Id;
        this.createdDate = itemNotify.CreatedDate;
        this.updatedDate = itemNotify.UpdatedDate;
        this.title = itemNotify.Title;
        this.body = itemNotify.Body;
        this.message = itemNotify.Msg;
        this.type =
            itemNotify.Type == domainEnum_1.TypeSendNotify.AdditionalSchedule
                ? 'AdditionalSchedule'
                : 'Other';
        this.status =
            itemNotify.Status == domainEnum_1.StatusSendNotify.Sent
                ? 'Sent'
                : itemNotify.Status == domainEnum_1.StatusSendNotify.Watched
                    ? 'Watched'
                    : 'Deleted';
        dankaName ? (this.dankaName = dankaName) : null;
        houseHolderName ? (this.houseHolderName = houseHolderName) : null;
        passedPersonName ? (this.passedPersonName = passedPersonName) : null;
        enventName ? (this.eventName = enventName) : null;
        return this;
    }
}
exports.ResGetNotify = ResGetNotify;
//# sourceMappingURL=res-get-notify.js.map