"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqAddAccountToRoom = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const req_get_account_by_email_1 = require("./req-get-account-by-email");
class ReqAddAccountToRoom {
}
__decorate([
    class_validator_1.IsArray(),
    class_transformer_1.Type(() => req_get_account_by_email_1.ReqGetAccountByEmail),
    class_validator_1.ValidateNested({ each: true }),
    class_validator_1.IsDefined(),
    swagger_1.ApiProperty({ type: req_get_account_by_email_1.ReqGetAccountByEmail, isArray: true }),
    __metadata("design:type", Array)
], ReqAddAccountToRoom.prototype, "lstEmail", void 0);
__decorate([
    class_validator_1.IsString({ message: 'Please enter the correct id room format.' }),
    class_validator_1.IsNotEmpty({ message: 'Please enter this room Id' }),
    swagger_1.ApiProperty({ example: 'c69ab67f-f515-473b-bcdb-025b6f087ecf' }),
    __metadata("design:type", String)
], ReqAddAccountToRoom.prototype, "roomId", void 0);
exports.ReqAddAccountToRoom = ReqAddAccountToRoom;
//# sourceMappingURL=req-add-account-to-room.js.map