export declare class ReqChangePassword {
    password: string;
    passwordNew: string;
    passwordConfirm: string;
}
