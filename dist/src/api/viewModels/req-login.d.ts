import { DeviceName } from 'src/domain/enum/domainEnum';
export declare class ReqLogin {
    email: string;
    password: string;
    deviceName: DeviceName;
    deviceId: string;
}
