export declare class ResApi {
    code: number;
    data: any;
    message: string;
    constructor();
    Success(code: number, data: any, message: string): ResApi;
    Error(code: number, data: any, message: string): this;
    ToJson(data: ResApi): string;
}
