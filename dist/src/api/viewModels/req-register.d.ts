export declare class ReqRegister {
    fullName: string;
    email: string;
    password: string;
    phone: string;
    gender: string;
}
