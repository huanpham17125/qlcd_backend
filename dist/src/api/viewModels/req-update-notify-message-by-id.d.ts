import { StatusSendNotify } from 'src/domain/enum/domainEnum';
export declare class ReqUpdateNotifyMessageById {
    id: number;
    status: StatusSendNotify;
}
