import { TypeSendNotify } from 'src/domain/enum/domainEnum';
export declare class ReqCreateSendnotifyMessage {
    accountId: string;
    title: string;
    body: string;
    message: string;
    type: TypeSendNotify;
}
