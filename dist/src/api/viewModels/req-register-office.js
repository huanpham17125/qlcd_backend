"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqRegisterOffice = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ReqRegisterOffice {
}
__decorate([
    class_validator_1.IsEmail({}, { message: 'Please enter the correct email format.' }),
    swagger_1.ApiProperty({ example: 'vmoadmintemple@gmail.com' }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "email", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter a name.' }),
    swagger_1.ApiProperty({ example: 'AdminUser' }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "name", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter a temple name.' }),
    swagger_1.ApiProperty({ example: 'Hiroshima Temple' }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "templeName", void 0);
__decorate([
    class_validator_1.IsString(),
    swagger_1.ApiProperty({ example: 'Buddhism' }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "domination", void 0);
__decorate([
    class_validator_1.IsString(),
    swagger_1.ApiProperty({ example: 'sub Buddhism' }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "subDomination", void 0);
__decorate([
    class_validator_1.IsString(),
    swagger_1.ApiProperty({ example: '8455555555' }),
    class_validator_1.MinLength(9, {
        message: 'Phone is not format(min 9 characters). Please check again.',
    }),
    class_validator_1.MaxLength(15, {
        message: 'Phone is not format(max 15 characters). Please check again.',
    }),
    __metadata("design:type", String)
], ReqRegisterOffice.prototype, "phone", void 0);
exports.ReqRegisterOffice = ReqRegisterOffice;
//# sourceMappingURL=req-register-office.js.map