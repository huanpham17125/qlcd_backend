export declare class ResGetProfile {
    id: string;
    email: string;
    name: string;
    roleName: string;
    isActive: boolean;
    isAdmin: boolean;
    canEdit: boolean;
    constructor(id: string, email: string, fullName: string, normalizedRole: string, isActive: boolean, isAdmin: boolean, canEdit: boolean);
}
