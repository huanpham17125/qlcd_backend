export declare class ReqResetPassword {
    email: string;
    passwordNew: string;
    passwordConfirm: string;
}
