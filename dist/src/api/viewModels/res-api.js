"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResApi = void 0;
const constants_1 = require("../../application/common/constants");
class ResApi {
    constructor() {
        this.code = constants_1.Constants.AppStatus.StatusCode200;
        this.data = null;
        this.message = '';
    }
    Success(code, data, message) {
        this.code = code;
        this.data = data;
        this.message = message ? message : constants_1.Constants.AppResource.ACCTION_SUCCESS;
        return this;
    }
    Error(code, data, message) {
        this.code = code;
        this.data = data;
        this.message = message ? message : constants_1.Constants.AppResource.EXCEPTION_UNKNOWN;
        return this;
    }
    ToJson(data) {
        return JSON.stringify(data);
    }
}
exports.ResApi = ResApi;
//# sourceMappingURL=res-api.js.map