"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClaimsIdentityToken = void 0;
class ClaimsIdentityToken {
    constructor(id, email, roleId, roleName, jti) {
        this.Id = id;
        this.Email = email;
        this.RoleId = roleId;
        this.RoleName = roleName;
        this.Jti = jti;
        return this;
    }
}
exports.ClaimsIdentityToken = ClaimsIdentityToken;
//# sourceMappingURL=claims-identity-token.js.map