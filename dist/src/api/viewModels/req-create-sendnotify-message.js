"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqCreateSendnotifyMessage = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const domainEnum_1 = require("../../domain/enum/domainEnum");
class ReqCreateSendnotifyMessage {
}
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter accountId' }),
    swagger_1.ApiProperty({ example: '196483EE-73F7-EA11-9A2D-887873E284BF' }),
    __metadata("design:type", String)
], ReqCreateSendnotifyMessage.prototype, "accountId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter title message' }),
    swagger_1.ApiProperty({ example: 'notification' }),
    __metadata("design:type", String)
], ReqCreateSendnotifyMessage.prototype, "title", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter body message' }),
    swagger_1.ApiProperty({ example: 'Please complete your profile' }),
    __metadata("design:type", String)
], ReqCreateSendnotifyMessage.prototype, "body", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter message' }),
    swagger_1.ApiProperty({ example: 'Please complete your profile' }),
    __metadata("design:type", String)
], ReqCreateSendnotifyMessage.prototype, "message", void 0);
__decorate([
    class_validator_1.IsNumber({}, { message: 'TypeMessage is number format' }),
    class_validator_1.IsNotEmpty({ message: 'Please enter type notify' }),
    swagger_1.ApiProperty({ example: 3 }),
    __metadata("design:type", Number)
], ReqCreateSendnotifyMessage.prototype, "type", void 0);
exports.ReqCreateSendnotifyMessage = ReqCreateSendnotifyMessage;
//# sourceMappingURL=req-create-sendnotify-message.js.map