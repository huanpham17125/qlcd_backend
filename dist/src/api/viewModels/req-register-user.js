"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqRegisterUser = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ReqRegisterUser {
}
__decorate([
    class_validator_1.IsEmail({}, { message: 'Please enter the correct email format.' }),
    swagger_1.ApiProperty({ example: 'vmoadmintemple@gmail.com' }),
    __metadata("design:type", String)
], ReqRegisterUser.prototype, "email", void 0);
__decorate([
    class_validator_1.IsString({ message: 'Please enter the name.' }),
    class_validator_1.IsNotEmpty({ message: 'Please enter the name.' }),
    swagger_1.ApiProperty({ example: '123456@gmail.com' }),
    __metadata("design:type", String)
], ReqRegisterUser.prototype, "name", void 0);
__decorate([
    swagger_1.ApiProperty({ example: 'Aa12345678' }),
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please enter a password than 8 characters.' }),
    class_validator_1.MinLength(8, { message: 'Please enter a password than 8 characters.' }),
    class_validator_1.MaxLength(50, { message: 'Please enter a password less 50 characters.' }),
    class_validator_1.Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
        message: 'Password too weak! Please fix to continue.',
    }),
    __metadata("design:type", String)
], ReqRegisterUser.prototype, "password", void 0);
exports.ReqRegisterUser = ReqRegisterUser;
//# sourceMappingURL=req-register-user.js.map