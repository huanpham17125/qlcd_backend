export declare class ResGetNotify {
    id: number;
    createdDate: Date;
    updatedDate: Date;
    title: string;
    body: string;
    message: string;
    type: string;
    status: string;
    dankaName?: string;
    houseHolderName?: string;
    passedPersonName?: string;
    eventName: string;
    constructor(itemNotify: any, enventName?: string, dankaName?: string, houseHolderName?: string, passedPersonName?: string);
}
