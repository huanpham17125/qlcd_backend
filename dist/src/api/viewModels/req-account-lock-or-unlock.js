"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReqAccountLockOrUnlock = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class ReqAccountLockOrUnlock {
}
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsNotEmpty({ message: 'Please choose your type lock.' }),
    class_validator_1.Min(0, { message: 'Type lock is not correct format.' }),
    class_validator_1.Max(1, { message: 'Type lock is not correct format.' }),
    swagger_1.ApiProperty({ example: 1, description: '0: lock, 1: unlock' }),
    __metadata("design:type", Number)
], ReqAccountLockOrUnlock.prototype, "type", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty({ message: 'Please choose account.' }),
    swagger_1.ApiProperty({ example: '0A6F6B55-2909-4469-B086-71E2600EEAC6' }),
    __metadata("design:type", String)
], ReqAccountLockOrUnlock.prototype, "accountId", void 0);
exports.ReqAccountLockOrUnlock = ReqAccountLockOrUnlock;
//# sourceMappingURL=req-account-lock-or-unlock.js.map