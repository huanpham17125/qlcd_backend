export declare class ReqRegisterOffice {
    email: string;
    name: string;
    templeName: string;
    domination: string;
    subDomination: string;
    phone: string;
}
