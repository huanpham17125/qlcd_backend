export declare class ClaimsIdentityToken {
    Id: string;
    FullName: string;
    Email: string;
    RoleId: string;
    RoleName: string;
    Jti: string;
    constructor(id: string, email: string, roleId: string, roleName: string, jti: string);
}
