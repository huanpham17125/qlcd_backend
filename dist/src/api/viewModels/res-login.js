"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResLogin = void 0;
class ResLogin {
    constructor(accessToken, expirse, expirseDate, refreshToken) {
        (this.accessToken = accessToken),
            (this.expiresDate = expirseDate),
            (this.expiresInSeconds = expirse),
            (this.refreshToken = refreshToken);
        return this;
    }
}
exports.ResLogin = ResLogin;
//# sourceMappingURL=res-login.js.map