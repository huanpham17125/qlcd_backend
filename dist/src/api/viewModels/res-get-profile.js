"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResGetProfile = void 0;
class ResGetProfile {
    constructor(id, email, fullName, normalizedRole, isActive, isAdmin, canEdit) {
        (this.id = id),
            (this.email = email),
            (this.name = fullName),
            (this.roleName = normalizedRole),
            (this.isActive = isActive),
            (this.isAdmin = isAdmin),
            (this.canEdit = canEdit);
        return this;
    }
}
exports.ResGetProfile = ResGetProfile;
//# sourceMappingURL=res-get-profile.js.map