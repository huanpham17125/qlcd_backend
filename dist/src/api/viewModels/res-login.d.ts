export declare class ResLogin {
    accessToken: string;
    expiresInSeconds: number;
    expiresDate: Date;
    refreshToken: string;
    constructor(accessToken: string, expirse: number, expirseDate: Date, refreshToken: string);
}
