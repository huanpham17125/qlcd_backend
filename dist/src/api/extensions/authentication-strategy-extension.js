"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationStrategyExtension = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const passport_jwt_1 = require("passport-jwt");
const un_authorize_exception_1 = require("../../application/common/exceptions/un-authorize-exception");
const account_service_1 = require("../../infrastructure/services/account.service");
let AuthenticationStrategyExtension = class AuthenticationStrategyExtension extends passport_1.PassportStrategy(passport_jwt_1.Strategy) {
    constructor(accountService) {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: 'PDv7DrqznYL6nv7DrqzjnQYO9JxIsWdcjnQYL6nu0f',
            passReqToCallback: true,
        });
        this.accountService = accountService;
    }
    async validate(claims) {
        const account = await this.accountService.getAccountById(claims.id);
        if (!account) {
            throw new un_authorize_exception_1.UnAuthorizeException('Token invalid or expired');
        }
        return claims;
    }
};
AuthenticationStrategyExtension = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [account_service_1.AccountService])
], AuthenticationStrategyExtension);
exports.AuthenticationStrategyExtension = AuthenticationStrategyExtension;
//# sourceMappingURL=authentication-strategy-extension.js.map