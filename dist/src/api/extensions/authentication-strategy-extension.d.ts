import { Strategy } from 'passport-jwt';
import { AccountService } from 'src/infrastructure/services/account.service';
declare type ClaimsToken = {
    id: string;
    email: string;
    fullName: string;
    roleId: string;
    groupId: string;
};
declare const AuthenticationStrategyExtension_base: new (...args: any[]) => Strategy;
export declare class AuthenticationStrategyExtension extends AuthenticationStrategyExtension_base {
    private readonly accountService;
    constructor(accountService: AccountService);
    validate(claims: ClaimsToken): Promise<ClaimsToken | null>;
}
export {};
