import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class GlobalExceptionFilterAttribute implements ExceptionFilter {
    catch(exception: unknown, host: ArgumentsHost): void;
    private handlerValidationException;
    private handerUnKnowException;
}
