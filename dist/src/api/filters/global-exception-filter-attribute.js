"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalExceptionFilterAttribute = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("../../application/common/constants");
const bad_request_exception_1 = require("../../application/common/exceptions/bad-request-exception");
const un_authorize_exception_1 = require("../../application/common/exceptions/un-authorize-exception");
const validation_exception_1 = require("../../application/common/exceptions/validation-exception");
let GlobalExceptionFilterAttribute = class GlobalExceptionFilterAttribute {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        if (exception instanceof bad_request_exception_1.BadRequestException) {
            this.handlerValidationException(exception, response);
        }
        else if (exception instanceof validation_exception_1.ValidationException) {
            this.handlerValidationException(exception, response);
        }
        else if (exception instanceof un_authorize_exception_1.UnAuthorizeException) {
            this.handlerValidationException(exception, response);
        }
        else if (exception instanceof common_1.NotFoundException) {
            this.handlerValidationException(exception, response);
        }
        else {
            this.handerUnKnowException(exception, response);
        }
    }
    handlerValidationException(exception, res) {
        return res.status(exception.getStatus()).json({
            code: exception.getStatus(),
            data: null,
            message: exception.getResponse(),
        });
    }
    handerUnKnowException(exception, res) {
        return res.status(constants_1.Constants.AppStatus.StatusCode500).json({
            code: constants_1.Constants.AppStatus.StatusCode500,
            data: null,
            message: exception.message,
        });
    }
};
GlobalExceptionFilterAttribute = __decorate([
    common_1.Catch()
], GlobalExceptionFilterAttribute);
exports.GlobalExceptionFilterAttribute = GlobalExceptionFilterAttribute;
//# sourceMappingURL=global-exception-filter-attribute.js.map