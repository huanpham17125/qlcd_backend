"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Policies = void 0;
const common_1 = require("@nestjs/common");
exports.Policies = (...roleNames) => common_1.SetMetadata('policies', roleNames);
//# sourceMappingURL=policy.decorator.js.map