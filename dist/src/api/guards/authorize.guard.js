"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthorizeGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const constants_1 = require("../../application/common/constants");
const un_authorize_exception_1 = require("../../application/common/exceptions/un-authorize-exception");
const token_service_1 = require("../../infrastructure/services/token.service");
let AuthorizeGuard = class AuthorizeGuard {
    constructor(reflector, tokenService) {
        this.reflector = reflector;
        this.tokenService = tokenService;
    }
    async canActivate(context) {
        const req = context.switchToHttp().getRequest();
        const tokenHeader = req.headers.authorization;
        if (!tokenHeader || tokenHeader == '') {
            throw new un_authorize_exception_1.UnAuthorizeException(constants_1.Constants.AppResource.NO_AUTHORIZE);
        }
        const bearerToken = tokenHeader.replace('Bearer ', '');
        const claimToken = await this.tokenService.verifyToken(bearerToken);
        if (!claimToken) {
            throw new un_authorize_exception_1.UnAuthorizeException(constants_1.Constants.AppResource.TOKEN_EXPIRED);
        }
        const roleNames = this.reflector.get('policies', context.getHandler());
        if ((!roleNames || roleNames.length > 0) &&
            !roleNames.includes(claimToken.roleName)) {
            throw new un_authorize_exception_1.UnAuthorizeException(constants_1.Constants.AppResource.UNAUTHORIZE);
        }
        req.claims = claimToken;
        return true;
    }
};
AuthorizeGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector,
        token_service_1.TokenService])
], AuthorizeGuard);
exports.AuthorizeGuard = AuthorizeGuard;
//# sourceMappingURL=authorize.guard.js.map