import { CanActivate } from '@nestjs/common';
import { TokenService } from 'src/infrastructure/services/token.service';
export declare class GatewayAuthorizeGuard implements CanActivate {
    private tokenService;
    constructor(tokenService: TokenService);
    canActivate(context: any): Promise<boolean>;
}
