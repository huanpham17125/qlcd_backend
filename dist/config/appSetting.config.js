"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppSettingConfig = {
    nodeEnv: process.env.NODE_ENV || 'localhost',
    port: Number(process.env.PORT) || 4000,
    dbConnection: {
        type: 'mysql',
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        database: process.env.DB_NAME,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
    },
    jwt: {
        secret: process.env.JWT_SECRETKEY,
        expiration: Number(process.env.JwT_EXPIRATION_ACCESS),
        expirationRefresh: Number(process.env.JWT_EXPIRATION_REFRESH),
        Issuer: process.env.JWT_ISSUER,
        Audience: process.env.JWT_AUDIENCE,
        RequiredHttpsMetadata: true,
        TokenValidationParameter: {
            ValidateIssuer: true,
            ValidateAudience: true,
            ValidateLifetime: true,
            ValidateIssuerSigningKey: true,
        },
    },
};
exports.default = AppSettingConfig;
//# sourceMappingURL=appSetting.config.js.map