declare type AppSettingConfigType = {
    nodeEnv: string;
    port: number;
    dbConnection: {
        type: string;
        host: string;
        port: number;
        database: string;
        username: string;
        password: string;
    };
    jwt: {
        secret: string;
        expiration: number;
        expirationRefresh: number;
        Issuer: string;
        Audience: string;
        RequiredHttpsMetadata: boolean;
        TokenValidationParameter: {
            ValidateIssuer: boolean;
            ValidateAudience: boolean;
            ValidateLifetime: boolean;
            ValidateIssuerSigningKey: boolean;
        };
    };
};
declare const AppSettingConfig: AppSettingConfigType;
export default AppSettingConfig;
