"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventstoreConfig = void 0;
const config_1 = require("@nestjs/config");
exports.eventstoreConfig = config_1.registerAs('Db_test', () => ({
    url: process.env.DATABASE_URL || 'mysql://localhost:3306/test',
}));
//# sourceMappingURL=eventstore.config.js.map