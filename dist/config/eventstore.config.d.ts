export declare const eventstoreConfig: (() => {
    url: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost;
