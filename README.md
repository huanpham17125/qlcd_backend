
## Description

 Qlcd_backend

## Installation

```bash
$ npm install
```

## Migration

```bash
#generate
$ npm run migrate:generate {name}

#run
$ npm run migrate:run
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the app on Docker(staging)
```bash
# start
$ docker-compose -f docker/staging/staging.docker-compose.yml up -d
# down 
$ docker-compose -f docker/staging/staging.docker-compose.yml down --rmi local
# logs
$ docker-compose -f docker/staging/staging.docker-compose.yml logs
```

## Support

 ///


